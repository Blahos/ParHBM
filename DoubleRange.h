
struct DoubleRange;

#pragma once

struct DoubleRange
{
public:
    double Min;
    double Max;
    
    DoubleRange(double aMin, double aMax) : Min(aMin), Max(aMax) { if (Min > Max) throw "Invalid double range!"; }
    bool IsIn(double aValue) const { return aValue >= Min && aValue <= Max; }
};
