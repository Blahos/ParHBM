
struct LinearSystemMatrices;
struct LinearSystem;

#pragma once
#include "Teuchos_RCP.hpp"
#include "Epetra_CrsMatrix.h"
#include "Epetra_MultiVector.h"

#include "PhysicalParameters.h"

struct LinearSystemMatrices
{
    Teuchos::RCP<Epetra_CrsMatrix> M;
    Teuchos::RCP<Epetra_CrsMatrix> K;
    Teuchos::RCP<Epetra_CrsMatrix> D;
	
    PhysicalParameters PhysicalParams;
};

struct LinearSystem
{
public:
	
	LinearSystemMatrices Matrices;
    // force values (ampitudes) for each dof (in physical coordinates) for each harmonic coefficient
    std::vector<Teuchos::RCP<Epetra_MultiVector>> F;
	
	PhysicalParameters PhysicalParams() const { return Matrices.PhysicalParams; }
    
};
