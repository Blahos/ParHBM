
#include <fstream>

#include "LinearSolverMUMPS.h"
#include "../Misc.h"
#include "../Time.h"

#include "mpi.h"

#include "Epetra_Import.h"
#include "Epetra_Comm.h"

#include "../Functions.h"
#include "../Functions.hpp"

#include "../Exception.h"


void LinearSolverMUMPS::InitInner_v1(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS)
{
	int lCount = aMatrix.NumMyNonzeros();
	mMatrixValues.Init(lCount);
	mMatrixRowIndices.Init(lCount);
	mMatrixColIndices.Init(lCount);
	
	mMumpsStruct.comm_fortran = MUMPS_USE_COMM_WORLD;
	mMumpsStruct.sym = 0; // nonsymmetric
	mMumpsStruct.par = 1; // host process is involved in computations as any other
	
	// Initialise MUMPS
	mMumpsStruct.job = -1; // initialise
	dmumps_c(&mMumpsStruct);
	
	int lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];
	if (lErrorCode < 0) throw "Mumps error after init phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after init phase: " << std::to_string(lErrorCode) << std::endl;
	
	// Prepare matrix
	mMumpsStruct.icntl[MUMPS_INDEX(5)] = 0; // assembled matrix
	mMumpsStruct.icntl[MUMPS_INDEX(18)] = 3; // distributed matrix
	
	// console output settings
	mMumpsStruct.icntl[MUMPS_INDEX(2)] = -1; // output level
	mMumpsStruct.icntl[MUMPS_INDEX(3)] = -1; // output level
	mMumpsStruct.icntl[MUMPS_INDEX(4)] = -1; // output level

	mMumpsStruct.n = aMatrix.NumGlobalRows(); // total matrix size
	mMumpsStruct.nz_loc = aMatrix.NumMyNonzeros(); // number of local nonzeros
	
	int lValIndex = 0;
	for (int iRow = 0; iRow < aMatrix.NumMyRows(); iRow++)
	{
		int lRowValuesCount = aMatrix.NumMyEntries(iRow);
		int lGlobalRowIndex = aMatrix.RowMap().MyGlobalElements()[iRow];		
		
		int lCheckCount;
		// column indices of values on this row
		mRowColIndices.Init(lRowValuesCount);
		mRowValues.Init(lRowValuesCount);
// 		int* lRowColInd = new int[lRowValuesCount];
// 		double* lRowValues = new double[lRowValuesCount];
		
		int lError = aMatrix.ExtractGlobalRowCopy(lGlobalRowIndex, lRowValuesCount, lCheckCount, mRowValues.Array(), mRowColIndices.Array());
		
		if (lError != 0) throw "MUMPS solver init: Error (" + std::to_string(lError) + ") occured when extracting matrix row data!";
		if (lCheckCount != lRowValuesCount) throw "Check array size does not match! (global row index = " + std::to_string(lGlobalRowIndex) + ")";
		
		for (int iVal = 0; iVal < lRowValuesCount; iVal++)
		{
// 			if (!mWasRun)
			{
				mMatrixRowIndices[lValIndex] = lGlobalRowIndex + 1;
				mMatrixColIndices[lValIndex] = mRowColIndices[iVal] + 1;
			}
			mMatrixValues[lValIndex] = mRowValues[iVal];
			lValIndex++;
		}
		
// 		delete[] lRowColInd;
// 		delete[] lRowValues;
	}
	if (lValIndex != aMatrix.NumMyNonzeros()) throw "Total number of matrix non-zero elements on this process does not match the check!";
	
	mMumpsStruct.irn_loc = mMatrixRowIndices.Array();
	mMumpsStruct.jcn_loc = mMatrixColIndices.Array();
// 	mMumpsStruct.a_loc = mMatrixValues.Array();
	
	
	std::ofstream lFileRow("row_ind_rank" + std::to_string(aMatrix.Comm().MyPID()));
	ExportArray(lFileRow, mMatrixRowIndices.Array(), mMatrixRowIndices.Size());
	lFileRow.flush();
	lFileRow.close();
	
	std::ofstream lFileCol("col_ind_rank" + std::to_string(aMatrix.Comm().MyPID()));
	ExportArray(lFileCol, mMatrixColIndices.Array(), mMatrixColIndices.Size());
	lFileCol.flush();
	lFileCol.close();
	
	// Analysis phase
	mMumpsStruct.job = 1;
	Time lTimeAn;
	lTimeAn.Start();
	dmumps_c(&mMumpsStruct);
	mAnalysisTotalTime += lTimeAn.Stop();
	
	lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];
	if (lErrorCode < 0) throw "Mumps error after analysis phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after analysis phase: " << std::to_string(lErrorCode) << std::endl;
}
void LinearSolverMUMPS::InitInner_v2(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS)
{
	int lCount = aMatrix.NumMyNonzeros();
// 	mMatrixValues.Init(lCount);
	mMatrixRowIndices.Init(lCount);
// 	mMatrixColIndices.Init(lCount);
	
	mMumpsStruct.comm_fortran = MUMPS_USE_COMM_WORLD;
	mMumpsStruct.sym = 0; // nonsymmetric
	mMumpsStruct.par = 1; // host process is involved in computations as any other
	
	// Initialise MUMPS
	mMumpsStruct.job = -1; // initialise
	dmumps_c(&mMumpsStruct);
	
	int lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];
	if (lErrorCode < 0) throw "Mumps error after init phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after init phase: " << std::to_string(lErrorCode) << std::endl;
	
	// Prepare matrix
	mMumpsStruct.icntl[MUMPS_INDEX(5)] = 0; // assembled matrix
	mMumpsStruct.icntl[MUMPS_INDEX(18)] = 3; // distributed matrix
	
	// console output settings
	mMumpsStruct.icntl[MUMPS_INDEX(2)] = -1; // output level
	mMumpsStruct.icntl[MUMPS_INDEX(3)] = -1; // output level
	mMumpsStruct.icntl[MUMPS_INDEX(4)] = -1; // output level

	mMumpsStruct.n = aMatrix.NumGlobalRows(); // total matrix size
	mMumpsStruct.nz_loc = aMatrix.NumMyNonzeros(); // number of local nonzeros
	
	int* lIndexOffset;
	int* lColIndices;
	double *lValues;
	
	const int lExtractSuccess = aMatrix.ExtractCrsDataPointers(lIndexOffset, lColIndices, lValues);
	if (lExtractSuccess != 0) throw ParHBM::Exception("ExtractCrsDataPointers failed! (error code = " + std::to_string(lExtractSuccess) + ")", 0, __func__, typeid(*this).name());
	
	int lValIndex = 0;
	for (int iRow = 0; iRow < aMatrix.NumMyRows(); iRow++)
	{
		int lRowValuesCount = aMatrix.NumMyEntries(iRow);
		int lGlobalRowIndex = aMatrix.RowMap().MyGlobalElements()[iRow];		
		
		int lCheckCount;
		// column indices of values on this row
		mRowColIndices.Init(lRowValuesCount);
		mRowValues.Init(lRowValuesCount);
// 		int* lRowColInd = new int[lRowValuesCount];
// 		double* lRowValues = new double[lRowValuesCount];
		
		int lError = aMatrix.ExtractGlobalRowCopy(lGlobalRowIndex, lRowValuesCount, lCheckCount, mRowValues.Array(), mRowColIndices.Array());
		
		if (lError != 0) throw "MUMPS solver init: Error (" + std::to_string(lError) + ") occured when extracting matrix row data!";
		if (lCheckCount != lRowValuesCount) throw "Check array size does not match! (global row index = " + std::to_string(lGlobalRowIndex) + ")";
		
		for (int iVal = 0; iVal < lRowValuesCount; iVal++)
		{
// 			if (!mWasRun)
			{
				mMatrixRowIndices[lValIndex] = lGlobalRowIndex + 1;
// 				mMatrixColIndices[lValIndex] = mRowColIndices[iVal] + 1;
			}
// 			mMatrixValues[lValIndex] = mRowValues[iVal];
			lValIndex++;
		}
		
		if (lIndexOffset[iRow + 1] != lValIndex) throw "Extracted row offset not matching!";
		
// 		delete[] lRowColInd;
// 		delete[] lRowValues;
	}
	if (lValIndex != aMatrix.NumMyNonzeros()) throw "Total number of matrix non-zero elements on this process does not match the check!";
	
	//TODO figure out if this barrier is necessary
	MPI_Barrier(MPI_COMM_WORLD);
	
	// modify the indices to get the 1 based indexing
// 	for (int i = 0; i < aMatrix.NumMyRows() + 1; i++)
// 	{
// 		lIndexOffset[i]++;
// 	}
	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
	{
		lColIndices[i]++;
	}
	
	mMumpsStruct.irn_loc = mMatrixRowIndices.Array();
	mMumpsStruct.jcn_loc = lColIndices;
// 	mMumpsStruct.a_loc = mMatrixValues.Array();
	
// 	std::ofstream lFileRow("row_ind_rank" + std::to_string(aMatrix.Comm().MyPID()));
// 	ExportArray(lFileRow, mMatrixRowIndices.Array(), mMatrixRowIndices.Size());
// 	lFileRow.flush();
// 	lFileRow.close();
// 	
// 	std::ofstream lFileCol("col_ind_rank" + std::to_string(aMatrix.Comm().MyPID()));
// 	ExportArray(lFileCol, lColIndices, mMatrixRowIndices.Size());
// 	lFileCol.flush();
// 	lFileCol.close();
	
	// Analysis phase
	mMumpsStruct.job = 1;
	Time lTimeAn;
	lTimeAn.Start();
	dmumps_c(&mMumpsStruct);
	mAnalysisTotalTime += lTimeAn.Stop();
	
	lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];
	if (lErrorCode < 0) throw "Mumps error after analysis phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after analysis phase: " << std::to_string(lErrorCode) << std::endl;
	
	//TODO figure out if this barrier is necessary
	MPI_Barrier(MPI_COMM_WORLD);
	// reverse the changes to the matrix
	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
	{
		lColIndices[i]--;
	}
}

void LinearSolverMUMPS::SolveInner_v1(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
{
	int lRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &lRank);
	
// 	aSolution.PutScalar(0.0);
	
// 	CheckMatrixForZeroRows(aMatrix);
	
	int lValIndex = 0;
	for (int iRow = 0; iRow < aMatrix.NumMyRows(); iRow++)
	{
		int lRowValuesCount = aMatrix.NumMyEntries(iRow);
		int lGlobalRowIndex = aMatrix.RowMap().GID(iRow);		
		
		int lCheckCount;
		// column indices of values on this row
		mRowColIndices.Init(lRowValuesCount);
		mRowValues.Init(lRowValuesCount);
		
		int lError = aMatrix.ExtractGlobalRowCopy(lGlobalRowIndex, lRowValuesCount, lCheckCount, mRowValues.Array(), mRowColIndices.Array());
		if (lError != 0) throw "MUMPS solver solve: Error (" + std::to_string(lError) + ") occured when extracting matrix row data!"; 
		if (lCheckCount != lRowValuesCount) throw "Check array size does not match! (global row index = " + std::to_string(lGlobalRowIndex) + ")";
		
		for (int iVal = 0; iVal < lRowValuesCount; iVal++)
		{
			if (mMatrixRowIndices[lValIndex] != lGlobalRowIndex + 1) throw "Row index not matching!";
			if (mMatrixColIndices[lValIndex] != mRowColIndices[iVal] + 1) throw "Column index not matching!";
			mMatrixValues[lValIndex] = mRowValues[iVal];
			lValIndex++;
		}
	}
	if (lValIndex != aMatrix.NumMyNonzeros()) throw "Total number of matrix non-zero elements on this process does not match the check!";
	
// 	mMumpsStruct.irn_loc = mMatrixRowIndices.Array();
// 	mMumpsStruct.jcn_loc = mMatrixColIndices.Array();
	mMumpsStruct.a_loc = mMatrixValues.Array();
	
	std::ofstream lFileVal("val_rank" + std::to_string(aMatrix.Comm().MyPID()));
	ExportArray(lFileVal, mMatrixValues.Array(), mMatrixValues.Size());
	lFileVal.flush();
	lFileVal.close();
		
	// prepare import for the rhs (everthing has to be sent to process 0)
	int lMumpsRhsElemCount = 0;
	if (lRank == 0)
	{
		lMumpsRhsElemCount = aMatrix.NumGlobalRows();
	}
	mProc0TargetIndices.Init(lMumpsRhsElemCount);
// 	int* lProc0TargetIndices = new int[lMumpsRhsElemCount];
	for (int i = 0; i < lMumpsRhsElemCount; i++)
	{
		mProc0TargetIndices[i] = i;
	}
	
	Epetra_Map lProc0Map(aMatrix.NumGlobalRows(), lMumpsRhsElemCount, mProc0TargetIndices.Array(), 0, aMatrix.Comm());
	Epetra_Import lProc0Import(lProc0Map, aMatrix.RowMap());
	
	// import the whole RHS to rank 0 (mumps needs it all there)
	Epetra_Vector lProc0Rhs(lProc0Map);
	lProc0Rhs.Import(aRHS, lProc0Import, Epetra_CombineMode::Add, 0);
	
	mMumpsRHS.Init(lMumpsRhsElemCount);
	for (int i = 0; i < lMumpsRhsElemCount; i++)
	{
		mMumpsRHS[i] = lProc0Rhs[i];
	}
	
	mMumpsStruct.icntl[MUMPS_INDEX(20)] = 0; // dense right hand side
	mMumpsStruct.rhs = mMumpsRHS.Array();
	mMumpsStruct.nrhs = 1;
	
	// factorisation phase
	mMumpsStruct.job = 2;
	Time lTimeFac;
	lTimeFac.Start();
	dmumps_c(&mMumpsStruct);
	mFactorisationTotalTime += lTimeFac.Stop();
	
	int lErrorCodeG = mMumpsStruct.infog[MUMPS_INDEX(1)];
	if (lErrorCodeG == -9) // try to solve the errorcode -9 (not enough memory allocated by mumps)
	{
		int lTryCount = 0;
		while (lErrorCodeG != 0 && lTryCount < 8)
		{
			lTryCount++;
			mMumpsStruct.icntl[MUMPS_INDEX(14)] += 10;
			std::cout << "Error -9 occured, running factorisation again with icntl(14) = " << mMumpsStruct.icntl[MUMPS_INDEX(14)] << std::endl;
			dmumps_c(&mMumpsStruct);
			lErrorCodeG = mMumpsStruct.infog[MUMPS_INDEX(1)];
		}
	}
	
	int lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];	
	if (lErrorCode < 0) throw "Mumps error after factorisation phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after factorisation phase: " << std::to_string(lErrorCode) << std::endl;
	
	mMumpsStruct.icntl[MUMPS_INDEX(21)] = 1; // distributed solution
	
	mSol.Init(mMumpsStruct.info[MUMPS_INDEX(23)]);
	mSolIndices.Init(mMumpsStruct.info[MUMPS_INDEX(23)]);
	
	mMumpsStruct.sol_loc = mSol.Array();
	mMumpsStruct.isol_loc = mSolIndices.Array();
	mMumpsStruct.lsol_loc = mMumpsStruct.info[MUMPS_INDEX(23)];
	
	// solve phase
	mMumpsStruct.job = 3;
	Time lTimeSolve;
	lTimeSolve.Start();
	dmumps_c(&mMumpsStruct);
	mSolveTotalTime += lTimeSolve.Stop();
	
	lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];
	if (lErrorCode < 0) throw "Mumps error after solve phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after solve phase: " << std::to_string(lErrorCode) << std::endl;
	
	// shift indices to make them c style
	for (int i = 0; i < mMumpsStruct.info[MUMPS_INDEX(23)]; i++)
	{
		mSolIndices[i]--;
	}
	Epetra_Map lMumpsMap(aMatrix.NumGlobalRows(), mMumpsStruct.info[MUMPS_INDEX(23)], mSolIndices.Array(), 0, aMatrix.Comm());
	
	Epetra_Vector lMumpsSol(lMumpsMap);
	
	lMumpsSol.SumIntoGlobalValues(mMumpsStruct.info[MUMPS_INDEX(23)], mSol.Array(), mSolIndices.Array());
	
	Epetra_Import lImport(aSolution.Map(), lMumpsMap);
	
	aSolution.PutScalar(0.0);
    aSolution.Import(lMumpsSol, lImport, Epetra_CombineMode::Add, 0);
	
	std::cout << "MUMPS total analysis time: " << mAnalysisTotalTime << std::endl;
	std::cout << "MUMPS total factorisation time: " << mFactorisationTotalTime << std::endl;
	std::cout << "MUMPS total solve time: " << mSolveTotalTime << std::endl;
}
void LinearSolverMUMPS::SolveInner_v2(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
{
	int lRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &lRank);
	
// 	aSolution.PutScalar(0.0);
	
// 	CheckMatrixForZeroRows(aMatrix);
	
	int lValIndex = 0;
	for (int iRow = 0; iRow < aMatrix.NumMyRows(); iRow++)
	{
		int lRowValuesCount = aMatrix.NumMyEntries(iRow);
		int lGlobalRowIndex = aMatrix.RowMap().GID(iRow);		
		
		int lCheckCount;
		// column indices of values on this row
		mRowColIndices.Init(lRowValuesCount);
		mRowValues.Init(lRowValuesCount);
		
		int lError = aMatrix.ExtractGlobalRowCopy(lGlobalRowIndex, lRowValuesCount, lCheckCount, mRowValues.Array(), mRowColIndices.Array());
		if (lError != 0) throw "MUMPS solver solve: Error (" + std::to_string(lError) + ") occured when extracting matrix row data!"; 
		if (lCheckCount != lRowValuesCount) throw "Check array size does not match! (global row index = " + std::to_string(lGlobalRowIndex) + ")";
		
		for (int iVal = 0; iVal < lRowValuesCount; iVal++)
		{
			if (mMatrixRowIndices[lValIndex] != lGlobalRowIndex + 1) throw "Row index not matching!";
// 			if (mMatrixColIndices[lValIndex] != mRowColIndices[iVal] + 1) throw "Column index not matching!";
// 			mMatrixValues[lValIndex] = mRowValues[iVal];
			lValIndex++;
		}
	}
	if (lValIndex != aMatrix.NumMyNonzeros()) throw "Total number of matrix non-zero elements on this process does not match the check!";
	
	int* lIndexOffset;
	int* lColIndices;
	double *lValues;
	
	const int lExtractSuccess = aMatrix.ExtractCrsDataPointers(lIndexOffset, lColIndices, lValues);
	
	//TODO figure out if this barrier is necessary
	MPI_Barrier(MPI_COMM_WORLD);
	
	if (lExtractSuccess != 0) throw ParHBM::Exception("ExtractCrsDataPointers failed! (error code = " + std::to_string(lExtractSuccess) + ")", 0, __func__, typeid(*this).name());
	
	// modify the indices to get the 1 based indexing
// 	for (int i = 0; i < aMatrix.NumMyRows() + 1; i++)
// 	{
// 		lIndexOffset[i]++;
// 	}
	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
	{
		lColIndices[i]++;
	}
	
// 	mMumpsStruct.irn_loc = mMatrixRowIndices.Array();
// 	mMumpsStruct.jcn_loc = mMatrixColIndices.Array();
	mMumpsStruct.a_loc = lValues;
	
	
	std::ofstream lFileVal("val_rank" + std::to_string(aMatrix.Comm().MyPID()));
	ExportArray(lFileVal, lValues, mMatrixRowIndices.Size());
	lFileVal.flush();
	lFileVal.close();
		
	// prepare import for the rhs (everthing has to be sent to process 0)
	int lMumpsRhsElemCount = 0;
	if (lRank == 0)
	{
		lMumpsRhsElemCount = aMatrix.NumGlobalRows();
	}
	mProc0TargetIndices.Init(lMumpsRhsElemCount);
// 	int* lProc0TargetIndices = new int[lMumpsRhsElemCount];
	for (int i = 0; i < lMumpsRhsElemCount; i++)
	{
		mProc0TargetIndices[i] = i;
	}
	
	Epetra_Map lProc0Map(aMatrix.NumGlobalRows(), lMumpsRhsElemCount, mProc0TargetIndices.Array(), 0, aMatrix.Comm());
	Epetra_Import lProc0Import(lProc0Map, aMatrix.RowMap());
	
	// import the whole RHS to rank 0 (mumps needs it all there)
	Epetra_Vector lProc0Rhs(lProc0Map);
	lProc0Rhs.Import(aRHS, lProc0Import, Epetra_CombineMode::Add, 0);
	
	mMumpsRHS.Init(lMumpsRhsElemCount);
	for (int i = 0; i < lMumpsRhsElemCount; i++)
	{
		mMumpsRHS[i] = lProc0Rhs[i];
	}
	
	mMumpsStruct.icntl[MUMPS_INDEX(20)] = 0; // dense right hand side
	mMumpsStruct.rhs = mMumpsRHS.Array();
	mMumpsStruct.nrhs = 1;
	
	// factorisation phase
	mMumpsStruct.job = 2;
	Time lTimeFac;
	lTimeFac.Start();
	dmumps_c(&mMumpsStruct);
	mFactorisationTotalTime += lTimeFac.Stop();
	
	int lErrorCodeG = mMumpsStruct.infog[MUMPS_INDEX(1)];
	if (lErrorCodeG == -9) // try to solve the errorcode -9 (not enough memory allocated by mumps)
	{
		int lTryCount = 0;
		while (lErrorCodeG != 0 && lTryCount < 8)
		{
			lTryCount++;
			mMumpsStruct.icntl[MUMPS_INDEX(14)] += 10;
			std::cout << "Error -9 occured, running factorisation again with icntl(14) = " << mMumpsStruct.icntl[MUMPS_INDEX(14)] << std::endl;
			dmumps_c(&mMumpsStruct);
			lErrorCodeG = mMumpsStruct.infog[MUMPS_INDEX(1)];
		}
	}
	
	int lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];	
	if (lErrorCode < 0) throw "Mumps error after factorisation phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after factorisation phase: " << std::to_string(lErrorCode) << std::endl;
	
	mMumpsStruct.icntl[MUMPS_INDEX(21)] = 1; // distributed solution
	
	mSol.Init(mMumpsStruct.info[MUMPS_INDEX(23)]);
	mSolIndices.Init(mMumpsStruct.info[MUMPS_INDEX(23)]);
	
	mMumpsStruct.sol_loc = mSol.Array();
	mMumpsStruct.isol_loc = mSolIndices.Array();
	mMumpsStruct.lsol_loc = mMumpsStruct.info[MUMPS_INDEX(23)];
	
	// solve phase
	mMumpsStruct.job = 3;
	Time lTimeSolve;
	lTimeSolve.Start();
	dmumps_c(&mMumpsStruct);
	mSolveTotalTime += lTimeSolve.Stop();
	
	lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];
	if (lErrorCode < 0) throw "Mumps error after solve phase: " + std::to_string(lErrorCode);
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after solve phase: " << std::to_string(lErrorCode) << std::endl;
	
	// shift indices to make them c style
	for (int i = 0; i < mMumpsStruct.info[MUMPS_INDEX(23)]; i++)
	{
		mSolIndices[i]--;
	}
	Epetra_Map lMumpsMap(aMatrix.NumGlobalRows(), mMumpsStruct.info[MUMPS_INDEX(23)], mSolIndices.Array(), 0, aMatrix.Comm());
	
	Epetra_Vector lMumpsSol(lMumpsMap);
	
	lMumpsSol.SumIntoGlobalValues(mMumpsStruct.info[MUMPS_INDEX(23)], mSol.Array(), mSolIndices.Array());
	
	Epetra_Import lImport(aSolution.Map(), lMumpsMap);
	
	aSolution.PutScalar(0.0);
    aSolution.Import(lMumpsSol, lImport, Epetra_CombineMode::Add, 0);
		
	//TODO figure out if this barrier is necessary
	MPI_Barrier(MPI_COMM_WORLD);
	// reverse the changes to the matrix
	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
	{
		lColIndices[i]--;
	}
	
	std::cout << "MUMPS total analysis time: " << mAnalysisTotalTime << std::endl;
	std::cout << "MUMPS total factorisation time: " << mFactorisationTotalTime << std::endl;
	std::cout << "MUMPS total solve time: " << mSolveTotalTime << std::endl;
}

void LinearSolverMUMPS::InitInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS)
{
// 	InitInner_v1(aMatrix, aRHS);
	InitInner_v2(aMatrix, aRHS);
}

void LinearSolverMUMPS::SolveInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
{
// 	SolveInner_v1(aMatrix, aRHS, aSolution);
	SolveInner_v2(aMatrix, aRHS, aSolution);
}

LinearSolverMUMPS::~LinearSolverMUMPS()
{
	// free mumps arrays
	mMumpsStruct.job = -2;
	dmumps_c(&mMumpsStruct);
	
	int lErrorCode = mMumpsStruct.info[MUMPS_INDEX(1)];
	if (lErrorCode < 0) 
	{
		std::cout << "Mumps error after finalise phase: " << std::to_string(lErrorCode) << std::endl;
	}
	if (mMumpsStruct.info[MUMPS_INDEX(1)] > 0) std::cout << "Mumps warning after finalise phase: " << std::to_string(lErrorCode) << std::endl;
}
