
class LinearSolverMUMPS;

#pragma once
#include "LinearSolver.h"

#include "Teuchos_RCP.hpp"
#include "Epetra_Import.h"

#include "../ExpandableArray.h"

#include "dmumps_c.h"
#define MUMPS_INDEX(I) ((I)-1) /* macro s.t. indices match documentation */
#define MUMPS_USE_COMM_WORLD -987654

class LinearSolverMUMPS : public LinearSolver
{
private:
	ExpandableArray<int> mMatrixRowIndices;
	ExpandableArray<int> mMatrixColIndices;
	ExpandableArray<double> mMatrixValues;
	
	ExpandableArray<int> mRowColIndices;
	ExpandableArray<double> mRowValues;
	
	ExpandableArray<int> mSolIndices;
	ExpandableArray<double> mSol;
	ExpandableArray<double> mMumpsRHS;
	ExpandableArray<int> mProc0TargetIndices;
	
	double mAnalysisTotalTime = 0.0;
	double mFactorisationTotalTime = 0.0;
	double mSolveTotalTime = 0.0;
	
	DMUMPS_STRUC_C mMumpsStruct;
	
	void InitInner_v1(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS);
	void InitInner_v2(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS);
	
	void SolveInner_v1(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution);
	void SolveInner_v2(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution);
	
protected:
	virtual void InitInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS) override;
	virtual void SolveInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution) override;
	
public:
	virtual ~LinearSolverMUMPS();
};
