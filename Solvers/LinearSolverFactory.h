
class LinearSolverFactory;

#pragma once
#include "LinearSolver.h"
#include "SolverInfo.h"

#ifdef WITH_MUMPS
#include "LinearSolverMUMPS.h"
#endif

#ifdef WITH_MKL
#include "LinearSolverMKLPDSS.h"
#endif

class LinearSolverFactory
{
public:
	// creates a new instance of a linear solver, so the pointer must be deleted at some point
	static LinearSolver* CreateSolver(const ParHBM::Solver& aSolverInfo)
	{
		switch (aSolverInfo)
		{
			case ParHBM::Solver::MUMPS:
#ifdef WITH_MUMPS
				return new LinearSolverMUMPS();
#else
				throw "MUMPS can not be used as linear solver because ParHBM was not compiled with MUMPS! Use -DWith_MUMPS=TRUE in CMake.";
#endif
				break;
			case ParHBM::Solver::MKLPDSS:
#ifdef WITH_MKL
				return new LinearSolverMKLPDSS();
#else
				throw "MKL can not be used as linear solver because ParHBM was not compiled with MKL! Use -DWith_MKL=TRUE in CMake.";
#endif
				break;
			default:
				throw "Unknown solver info provided!";
				break;
		}
	}
};
