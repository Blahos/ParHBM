
#include "NewtonSolver.h"

NewtonSolver::NewtonSolver(const Config& aConfig)
	: cConfig(aConfig)
{
	
}

NewtonResult NewtonSolver::Solve(LinearSolver* const aSolver, const Teuchos::RCP<HBMProblemInterface>& aProblemInterface, 
						 Epetra_CrsMatrix& aJacobiMatrix, Epetra_Vector& aSolution)
{
	NewtonResult lReturnResult;
	
	int lSystemSize = aSolution.GlobalLength();
	
	Epetra_Vector lFVector(aSolution.Map());
	Epetra_Vector lSolutionStep(aSolution.Map());
	// dummy fill type var
	NOX::Epetra::Interface::Required::FillType lDummyFillType = NOX::Epetra::Interface::Required::FillType::User;
	
	int lNewtonItCount = 0;
	// norm of F
	double lNormF;
	aProblemInterface->computeF(aSolution, lFVector, lDummyFillType);
	
	lFVector.Norm2(&lNormF);
	lNormF /= lSystemSize;
	
	while (lNormF > cConfig.NewtonNormF && lNewtonItCount < cConfig.MaxStepsNewton)
	{
		lNewtonItCount++;
		
		aProblemInterface->computeJacobian(aSolution, aJacobiMatrix);
		
		lFVector.Scale(-1.0);
		
		////////////////////
		// solve J*step = -F
		////////////////////
		aSolver->Solve(aJacobiMatrix, lFVector, lSolutionStep);
		
		// compute new solution guess (shift the solution by the computed step)
		for (int iVal = 0; iVal < aSolution.Map().NumMyElements(); iVal++)
		{
			aSolution[iVal] += lSolutionStep[iVal];
		}
		
		aProblemInterface->computeF(aSolution, lFVector, lDummyFillType);
		
		double lNormFOld = lNormF;
		
		lFVector.Norm2(&lNormF);
		lNormF /= lSystemSize;
		
		std::cout << "Newton step F norm change: " << lNormFOld << " -> " << lNormF << std::endl;
	}
	
	lReturnResult.IterationCount = lNewtonItCount;
	lReturnResult.FinalNormF = lNormF;
	if (lNormF <= cConfig.NewtonNormF)
	{
		lReturnResult.Converged = true;
	}
	else
	{
		lReturnResult.Converged = false;
	}
	
	return lReturnResult;
}
