#pragma once

// the namespace was introduced here because the name GMRES was clashing with some Trilinos GMRES constant/typedef/whatever
namespace ParHBM
{
    enum class Solver
    {
        MKLPDSS, MUMPS
    };
    
    enum class SolverType
    {
        Direct, Iterative
    };
}


