
class LinearEqMKLPDSS;

#pragma once
#include "LinearSolver.h"

class LinearSolverMKLPDSS : public LinearSolver
{
private:
	
	// copies the matrix data
	void SolveInner_v1(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution);
	// modifies the matrix data, passes pointers directly to the solver, then modifies them back 
	// no copy here
	void SolveInner_v2(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution);
	void SolveInner_v3(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution);
	
public:
	virtual void InitInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS) override;
	virtual void SolveInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution) override;
};
