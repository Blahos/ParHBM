#include <typeinfo>
#include "LinearSolverMKLPDSS.h"

#include "mpi.h"

#include "mkl_cluster_sparse_solver.h"

#include "../Misc.h"

#include "../Exception.h"

#include "../MKLPDSSWrapperLib/FunctionsMKL.h"

// for tests
// #include "../MKLPDSS_Simple/MKLPDSSData.h"

void LinearSolverMKLPDSS::InitInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS)
{
	
}

void LinearSolverMKLPDSS::SolveInner_v1(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
{
	long long lPt[64];
	int lIParm[64];
	
	for (int i = 0; i < 64; i++) lPt[i] = 0;
	for (int i = 0; i < 64; i++) lIParm[i] = 0;
	
	lIParm[0] = 1; // don't use default values
	lIParm[1] = 10; // distributed dissection of the matrix
// 	lIParm[9] = 13; 
	lIParm[17] = 0; // report number of nonzeros
// 	lIParm[26] = 1; // matrix indices checking
	lIParm[34] = 0; // one based indexing
// 	lIParm[36] = 0; // matrix storage format
	lIParm[39] = 2; // distributed matrix, rhs and solution (all with same row distribution)
	lIParm[40] = aMatrix.RowMap().MyGlobalElements()[0] + 1; // first row for this process
	lIParm[41] = aMatrix.RowMap().MyGlobalElements()[aMatrix.RowMap().NumMyElements() - 1] + 1; // last row for this process
		
// 	std::cout << "Start row: " << lIParm[40] << std::endl;
// 	std::cout << "End row: " << lIParm[41] << std::endl;
	
	int lMatrixType = 1;
	int lPhase = 13;
	
// 	STOP
	
	int lEqNum = aMatrix.NumGlobalRows();
// 	std::cout << "Eq num: " << lEqNum << std::endl;
	
	int* lAllRowIndices = new int[aMatrix.NumMyRows() + 1];
	int* lAllColIndices = new int[aMatrix.NumMyNonzeros()];
	double* lAllValues = new double[aMatrix.NumMyNonzeros()];
	
	int lValIndex = 0;
	for (int iRow = 0; iRow < aMatrix.NumMyRows(); iRow++)
	{
		int lRowValuesCount = aMatrix.NumMyEntries(iRow);
		int lGlobalRowIndex = aMatrix.RowMap().GID(iRow);
		
		lAllRowIndices[iRow] = lValIndex + 1;
		
		int lCheckCount;
		// column indices of values on this row
		int* lRowColInd = new int[lRowValuesCount];
		double* lRowValues = new double[lRowValuesCount];
		int lError = aMatrix.ExtractGlobalRowCopy(lGlobalRowIndex, lRowValuesCount, lCheckCount, lRowValues, lRowColInd);
		if (lError != 0) throw "Error occured when extracting matrix view!"; 
		if (lCheckCount != lRowValuesCount) throw "Check array size does not match! (global row index = " + std::to_string(lGlobalRowIndex) + ")";
		
		for (int iVal = 0; iVal < lRowValuesCount; iVal++)
		{
			lAllColIndices[lValIndex] = lRowColInd[iVal] + 1;
			lAllValues[lValIndex] = lRowValues[iVal];
			lValIndex++;
		}
		
		delete[] lRowColInd;
		delete[] lRowValues;
	}
	lAllRowIndices[aMatrix.NumMyRows()] = aMatrix.NumMyNonzeros() + 1;
	
	if (lValIndex != aMatrix.NumMyNonzeros()) throw "Total number of matrix non-zero elements on this process does not match the check!";
	
	int lMessageLevel = 0;
	int lNRHS = 1;
	int lFortranMPIComm = MPI_Comm_c2f(MPI_COMM_WORLD);
	
	int lError = 0;
	
	double* lRHSCopy = new double[aRHS.MyLength()];
	for (int i = 0; i < aRHS.MyLength(); i++) lRHSCopy[i] = aRHS[i];
	
	std::cout << "MKLPDSS solver wrapper: Data copied" << std::endl;
	
	double* lSolutionBuffer = new double[aSolution.MyLength()];
	
	int lDummy1 = 1;
	int lDummy0 = 0;
	
//	MKLPDSSData lTestData;
//	lTestData.NumEq = lEqNum;
//	lTestData.FirstRow = lIParm[40];
//	lTestData.LastRow = lIParm[41];
//	lTestData.IndexOffset = lAllRowIndices;
//	lTestData.IndexOffsetSize = aMatrix.NumMyRows() + 1;
//	lTestData.ColumnIndices = lAllColIndices;
//	lTestData.ColumnIndicesSize = aMatrix.NumMyNonzeros();
//	lTestData.Values = lAllValues;
//	lTestData.ValuesSize = aMatrix.NumMyNonzeros();
//	lTestData.RHS = lRHSCopy;
//	lTestData.RHSSize = aRHS.MyLength();
//	lTestData.SolutionSize = aSolution.MyLength();
	
//	lTestData.ExportToFile("MKLPDSS_DATA_TEST_FILE", MPI_COMM_WORLD);

	std::cout << "cluster_sparse_solver() called" << std::endl;
		
	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lAllValues, lAllRowIndices, lAllColIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
	
	std::cout << "cluster_sparse_solver() finished" << std::endl;


	if (lError != 0) throw "MKLPDSS (solve phase) returned error " + std::to_string(lError) + "!";
	
	for (int i = 0; i < aSolution.MyLength(); i++) aSolution[i] = lSolutionBuffer[i];
	
// 	std::cout << "Solution copied" << std::endl;
	
	// release
	lPhase = -1;
	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lAllValues, lAllRowIndices, lAllColIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
	
	if (lError != 0) throw "MKLPDSS (free phase) returned error " + std::to_string(lError) + "!";
	
	delete[] lAllValues;
	delete[] lAllRowIndices;
	delete[] lAllColIndices;
	delete[] lRHSCopy;
	delete[] lSolutionBuffer;
}
void LinearSolverMKLPDSS::SolveInner_v2(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
{
// 	long long lPt[64];
// 	int lIParm[64];
// 	
// 	for (int i = 0; i < 64; i++) lPt[i] = 0;
// 	for (int i = 0; i < 64; i++) lIParm[i] = 0;
// 	
// 	lIParm[0] = 1; // don't use default values
// 	lIParm[1] = 10; // distributed dissection of the matrix
// // 	lIParm[9] = 13; 
// 	lIParm[17] = 0; // report number of nonzeros
// // 	lIParm[26] = 1; // matrix indices checking
// 	lIParm[34] = 0; // one based indexing
// // 	lIParm[36] = 0; // matrix storage format
// 	lIParm[39] = 2; // distributed matrix, rhs and solution (all with same row distribution)
// 	lIParm[40] = aMatrix.RowMap().MyGlobalElements()[0] + 1; // first row for this process
// 	lIParm[41] = aMatrix.RowMap().MyGlobalElements()[aMatrix.RowMap().NumMyElements() - 1] + 1; // last row for this process
// 		
// 	int lMatrixType = 1;
// 	int lPhase = 13;
// 	
// 	int lEqNum = aMatrix.NumGlobalRows();
// 	int lMessageLevel = 0;
// 	int lNRHS = 1;
// 	int lFortranMPIComm = MPI_Comm_c2f(MPI_COMM_WORLD);
// 	
// 	int lError = 0;
// 	
// 	double* lRHSCopy = new double[aRHS.MyLength()];
// 	for (int i = 0; i < aRHS.MyLength(); i++) lRHSCopy[i] = aRHS[i];
// 	
// 	double* lSolutionBuffer = new double[aSolution.MyLength()];
// 	
// 	int lDummy1 = 1;
// 	int lDummy0 = 0;
// 	
// 	int* lIndexOffset;
// 	int* lColIndices;
// 	double* lValues;
// 	
// 	const int lExtractSuccess = aMatrix.ExtractCrsDataPointers(lIndexOffset, lColIndices, lValues);
// 	
// 	//TODO figure out if this barrier is necessary
// 	MPI_Barrier(MPI_COMM_WORLD);
// 	
// 	if (lExtractSuccess != 0) throw ParHBM::Exception("ExtractCrsDataPointers failed! (error code = " + std::to_string(lExtractSuccess) + ")", 0, __func__, typeid(*this).name());
// 	
// 	// modify the indices to get the 1 based indexing
// 	for (int i = 0; i < aMatrix.NumMyRows() + 1; i++)
// 	{
// 		lIndexOffset[i]++;
// 	}
// 	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
// 	{
// 		lColIndices[i]++;
// 	}
// 	
// 	
// 	MKLPDSSData lTestData;
// 	lTestData.NumEq = lEqNum;
// 	lTestData.FirstRow = lIParm[40];
// 	lTestData.LastRow = lIParm[41];
// 	lTestData.IndexOffset = lIndexOffset;
// 	lTestData.IndexOffsetSize = aMatrix.NumMyRows() + 1;
// 	lTestData.ColumnIndices = lColIndices;
// 	lTestData.ColumnIndicesSize = aMatrix.NumMyNonzeros();
// 	lTestData.Values = lValues;
// 	lTestData.ValuesSize = aMatrix.NumMyNonzeros();
// 	lTestData.RHS = lRHSCopy;
// 	lTestData.RHSSize = aRHS.MyLength();
// 	lTestData.SolutionSize = aSolution.MyLength();
// 	
// 	lTestData.ExportToFile("MKLPDSS_DATA_TEST_FILE", MPI_COMM_WORLD);
// 	
// 	std::cout << "Calling the cluster_sparse_solver() ..." << std::endl;
// 	
// 	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lValues, lIndexOffset, lColIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
// 		
// 	std::cout << "Calling the cluster_sparse_solver() ... finished" << std::endl;
// 	
// 	if (lError != 0) throw "MKLPDSS (solve phase) returned error " + std::to_string(lError) + "!";
// 	
// 	for (int i = 0; i < aSolution.MyLength(); i++) aSolution[i] = lSolutionBuffer[i];
// 	
// // 	std::cout << "Solution copied" << std::endl;
// 	
// 	// release
// 	lPhase = -1;
// 	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lValues, lIndexOffset, lColIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
// 	
// 	if (lError != 0) throw "MKLPDSS (free phase) returned error " + std::to_string(lError) + "!";
// 	
// 	// modify the indices back to the original state )since we were touching directly the matrix data, not a copy)
// 	for (int i = 0; i < aMatrix.NumMyRows() + 1; i++)
// 	{
// 		lIndexOffset[i]--;
// 	}
// 	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
// 	{
// 		lColIndices[i]--;
// 	}
// 	
// 	//TODO figure out if this barrier is necessary
// 	MPI_Barrier(MPI_COMM_WORLD);
// 	
// 	delete[] lRHSCopy;
// 	delete[] lSolutionBuffer;
}
void LinearSolverMKLPDSS::SolveInner_v3(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
{
// // 		long long lPt[64];
// // 	int lIParm[64];
// // 	
// // 	for (int i = 0; i < 64; i++) lPt[i] = 0;
// // 	for (int i = 0; i < 64; i++) lIParm[i] = 0;
// // 	
// // 	lIParm[0] = 1; // don't use default values
// // 	lIParm[1] = 10; // distributed dissection of the matrix
// // // 	lIParm[9] = 13; 
// // 	lIParm[17] = 0; // report number of nonzeros
// // // 	lIParm[26] = 1; // matrix indices checking
// // 	lIParm[34] = 0; // one based indexing
// // // 	lIParm[36] = 0; // matrix storage format
// // 	lIParm[39] = 2; // distributed matrix, rhs and solution (all with same row distribution)
// // 	lIParm[40] = aMatrix.RowMap().MyGlobalElements()[0] + 1; // first row for this process
// // 	lIParm[41] = aMatrix.RowMap().MyGlobalElements()[aMatrix.RowMap().NumMyElements() - 1] + 1; // last row for this process
// 		
// // 	int lMatrixType = 1;
// // 	int lPhase = 13;
// 	
// 	int lEqNum = aMatrix.NumGlobalRows();
// // 	int lMessageLevel = 0;
// 	int lNRHS = 1;
// // 	int lFortranMPIComm = MPI_Comm_c2f(MPI_COMM_WORLD);
// 	
// // 	int lError = 0;
// 	
// // 	double* lRHSCopy = new double[aRHS.MyLength()];
// // 	for (int i = 0; i < aRHS.MyLength(); i++) lRHSCopy[i] = aRHS[i];
// 	
// // 	double* lSolutionBuffer = new double[aSolution.MyLength()];
// 	
// 	int lDummy1 = 1;
// 	int lDummy0 = 0;
// 	
// 	int* lIndexOffset;
// 	int* lColIndices;
// 	double* lValues;
// 	
// 	const int lExtractSuccess = aMatrix.ExtractCrsDataPointers(lIndexOffset, lColIndices, lValues);
// 	
// 	//TODO figure out if this barrier is necessary
// 	MPI_Barrier(MPI_COMM_WORLD);
// 	
// 	if (lExtractSuccess != 0) throw ParHBM::Exception("ExtractCrsDataPointers failed! (error code = " + std::to_string(lExtractSuccess) + ")", 0, __func__, typeid(*this).name());
// 	
// 	// modify the indices to get the 1 based indexing
// 	for (int i = 0; i < aMatrix.NumMyRows() + 1; i++)
// 	{
// 		lIndexOffset[i]++;
// 	}
// 	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
// 	{
// 		lColIndices[i]++;
// 	}
// 	
// 	double* lRHSCopy = new double[aRHS.MyLength()];
// 	for (int i = 0; i < aRHS.MyLength(); i++) lRHSCopy[i] = aRHS[i];
// 	
// 	
// 	MKLPDSSData lMKLPDSSData;
// 	lMKLPDSSData.NumEq = lEqNum;
// 	lMKLPDSSData.FirstRow = aMatrix.RowMap().MyGlobalElements()[0] + 1; // first row for this process
// 	lMKLPDSSData.LastRow = aMatrix.RowMap().MyGlobalElements()[aMatrix.RowMap().NumMyElements() - 1] + 1; // last row for this process
// 	lMKLPDSSData.IndexOffset = lIndexOffset;
// 	lMKLPDSSData.IndexOffsetSize = aMatrix.NumMyRows() + 1;
// 	lMKLPDSSData.ColumnIndices = lColIndices;
// 	lMKLPDSSData.ColumnIndicesSize = aMatrix.NumMyNonzeros();
// 	lMKLPDSSData.Values = lValues;
// 	lMKLPDSSData.ValuesSize = aMatrix.NumMyNonzeros();
// 	lMKLPDSSData.RHS = lRHSCopy;
// 	lMKLPDSSData.RHSSize = aRHS.MyLength();
// 	lMKLPDSSData.SolutionSize = aSolution.MyLength();
// 	
// 	std::cout << "Calling MKLPDSS wrapper Solve() ..." << std::endl;
// 	
// 	::Solve(lMKLPDSSData, &aSolution[0]);
// 	
// 	std::cout << "Calling MKLPDSS wrapper Solve() ... done" << std::endl;
// 	
// // 	lTestData.ExportToFile("MKLPDSS_DATA_TEST_FILE", MPI_COMM_WORLD);
// 	
// 	
// // 	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lValues, lIndexOffset, lColIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
// 		
// // 	std::cout << "Calling the cluster_sparse_solver() ... finished" << std::endl;
// 	
// // 	if (lError != 0) throw "MKLPDSS (solve phase) returned error " + std::to_string(lError) + "!";
// 	
// // 	for (int i = 0; i < aSolution.MyLength(); i++) aSolution[i] = lSolutionBuffer[i];
// 	
// // 	std::cout << "Solution copied" << std::endl;
// 	
// 	// release
// // 	lPhase = -1;
// // 	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lValues, lIndexOffset, lColIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
// 	
// // 	if (lError != 0) throw "MKLPDSS (free phase) returned error " + std::to_string(lError) + "!";
// 	
// 	// modify the indices back to the original state )since we were touching directly the matrix data, not a copy)
// 	for (int i = 0; i < aMatrix.NumMyRows() + 1; i++)
// 	{
// 		lIndexOffset[i]--;
// 	}
// 	for (int i = 0; i < aMatrix.NumMyNonzeros(); i++)
// 	{
// 		lColIndices[i]--;
// 	}
// 	
// 	//TODO figure out if this barrier is necessary
// 	MPI_Barrier(MPI_COMM_WORLD);
// 	
// // 	delete[] lRHSCopy;
// // 	delete[] lSolutionBuffer;
}

void LinearSolverMKLPDSS::SolveInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
{
 SolveInner_v1(aMatrix, aRHS, aSolution);
//	SolveInner_v2(aMatrix, aRHS, aSolution);
// 	SolveInner_v3(aMatrix, aRHS, aSolution);
}
