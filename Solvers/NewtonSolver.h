
class NewtonSolver;
struct NewtonResult;

#pragma once 
#include "../Config.h"
#include "../HBMProblemInterface.h"
#include "LinearSolver.h"
#include "../Continuation/ConstraintFunction.h"

struct NewtonResult
{
public:
	bool Converged;
	int IterationCount;
	double FinalNormF;
};

class NewtonSolver
{
private:
	const Config cConfig;
public:
	NewtonSolver(const Config& aConfig);
	
	// the aSolution vector serves as the initial guess for the newton solver at the input
	// at the output it will hold the converged solution (or the solution guess from the newton iteration, if convergence was not achieved)
	NewtonResult Solve(LinearSolver* const aSolver, const Teuchos::RCP<HBMProblemInterface>& aProblemInterface, Epetra_CrsMatrix& aJacobiMatrix, Epetra_Vector& aSolution);
	NewtonResult Solve(LinearSolver* const aSolver, const Teuchos::RCP<HBMProblemInterface>& aProblemInterface, Epetra_CrsMatrix& aJacobiMatrix, Epetra_Vector& aSolution, double& aW, const Epetra_Vector& aTangentSol, double aTangentW, const ConstraintFunction& aConstraintFunction);
	
};
