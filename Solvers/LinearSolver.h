
class LinearSolver;

#pragma once
#include "Epetra_CrsMatrix.h"
#include "Epetra_Vector.h"

#include "../Time.h"

class LinearSolver
{
private:
	bool mIsInitialised = false;
	double mSolveTotalTime = 0.0;
	
protected:
	virtual void InitInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS) = 0;
	virtual void SolveInner(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution) = 0;
	
public:
	virtual ~LinearSolver() { };
	// Use to prepare the solver for the current matrix and right hand side.
	// After calling this, the Solve method can be called multiple times for any matrix and RHS with the same pattern, number of nonzeros and distribution among ranks
	void Init(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS)
	{
		std::cout << "Linear solver Init() started" << std::endl;
		InitInner(aMatrix, aRHS);
		mIsInitialised = true;
		std::cout << "Linear solver Init() finished" << std::endl;
	}
	void Solve(const Epetra_CrsMatrix& aMatrix, const Epetra_Vector& aRHS, Epetra_Vector& aSolution)
	{
		std::cout << "Linear solver Solve() started" << std::endl;
		
		if (!mIsInitialised) throw "Solver is not initialised!";
		
		Time lTime;
		lTime.Start();
		SolveInner(aMatrix, aRHS, aSolution);
		mSolveTotalTime += lTime.Stop();
		std::cout << "Linear solver Solve() finished" << std::endl;
		std::cout << "Linear solver total time: " << mSolveTotalTime << std::endl;
	}
};
