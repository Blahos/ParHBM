
class PseudoArcLength;

#pragma once
#include "ConstraintFunction.h"

// implements a scalar equation F(x, w) = 0, where x is the solution (guess) in continuation and w is frequency (parameter in continuation)
class PseudoArcLength : public ConstraintFunction
{
private:
public:
	
	virtual double ComputeF(const Epetra_Vector& aPrevX, const double aPrevW, const Epetra_Vector& aTangentX, const double aTangentW, const double aPredStepSize, const Epetra_Vector& aCurrentX, const double aCurrentW) const override;
	
	virtual Epetra_Vector ComputeDFDX(const Epetra_Vector& aPrevX, const double aPrevW, const Epetra_Vector& aTangentX, const double aTangentW, const double aPredStepSize, const Epetra_Vector& aCurrentX, const double aCurrentW) const override;
	
	virtual double ComputeDFDW(const Epetra_Vector& aPrevX, const double aPrevW, const Epetra_Vector& aTangentX, const double aTangentW, const double aPredStepSize, const Epetra_Vector& aCurrentX, const double aCurrentW) const override;
};
