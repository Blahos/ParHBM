
#include "Continuation.h"

Continuation::Continuation(const Config& aConfig)
	: cConfig(aConfig)
{
}

void Continuation::RunContinuation(const Teuchos::RCP<HBMProblemInterface> aProblemInterface, const Epetra_Vector* const aInitialGuess)
{
	// Converge for the initial guess
	
	double lW = cConfig.FrequencyStart * cConfig.ScaleT;
	aProblemInterface->SetFrequency(lW);
	int lNewtonItCount = 0;
}

