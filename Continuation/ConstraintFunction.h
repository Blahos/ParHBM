
class ConstraintFunction;

#pragma once
#include "Epetra_Vector.h"

class ConstraintFunction
{
public:
	virtual double ComputeF(const Epetra_Vector& aPrevX, const double aPrevW, const Epetra_Vector& aTangentX, const double aTangentW, const double aPredStepSize, const Epetra_Vector& aCurrentX, const double aCurrentW) const;
	
	virtual Epetra_Vector ComputeDFDX(const Epetra_Vector& aPrevX, const double aPrevW, const Epetra_Vector& aTangentX, const double aTangentW, const double aPredStepSize, const Epetra_Vector& aCurrentX, const double aCurrentW) const;
	
	virtual double ComputeDFDW(const Epetra_Vector& aPrevX, const double aPrevW, const Epetra_Vector& aTangentX, const double aTangentW, const double aPredStepSize, const Epetra_Vector& aCurrentX, const double aCurrentW) const;
};
