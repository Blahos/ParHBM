
class Continuation;

#pragma once
#include "../Config.h"
#include "../HBMProblemInterface.h"

#include "Epetra_Vector.h"

class Continuation
{
private:
	const Config cConfig;
public:
	Continuation(const Config& aConfig);
	
	void RunContinuation(const Teuchos::RCP<HBMProblemInterface> aProblemInterface, const Epetra_Vector* const aInitialGuess = nullptr);
};
