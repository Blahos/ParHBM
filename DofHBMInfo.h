
struct DofHBMInfo;

#pragma once
#include <string>

#include "DofInfo.h"

struct DofHBMInfo
{
public:
    DofInfo PhysicalDofInfo;
    int HarmonicWaveIndex;
    int HarmonicCoeffIndex;
    WaveType HarmonicType;
    int DofIdHBM;
    
public:
    static std::vector<int> GetDofIDs(const std::vector<DofHBMInfo>& aDofs)
    {
        std::vector<int> lReturnVector;
        lReturnVector.reserve(aDofs.size());
        
        for (int i = 0; i < aDofs.size(); i++)
            lReturnVector.push_back(aDofs[i].DofIdHBM);
        
        return lReturnVector;
    }
    int HarmonicTypeInt() const
	{
		return (int)HarmonicType;
	}
};
