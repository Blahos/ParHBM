
Configuring project:

1) navigate to your desired build directory
2) call cmake -G "Unix Makefiles" <path-to-ParHBM-source>

Possible CMake parameters:

-D CMAKE_BUILD_TYPE=<"Release" | "Debug">
	-- Type of build.

-D MKL_ROOT_DIR=<path>
	-- Base directory of MKL (if used).

-D With_MUMPS=<TRUE | FALSE> 
	-- Compile with MUMPS direct solver.

-D With_INTEL=<TRUE | FALSE> 
	-- Kind of a shady flag at the moment, if set, 2 extra things will happen: 
		1) Library libmkl_blacs_intelmpi_lp64.a will be linked instead of libmkl_blacs_openmpi_lp64.a (if MKL is used)
		2) Library ifcore will be linked (regardless of MKL use)
		This flag is here to make the code work on Salomon supercomputer where everything is built with Intel compiler.

-D With_MKL=<TRUE | FALSE> 
	-- Compile with Intel MKL (includes MKLPDSS direct solver).

-D TRILINOS_ROOT_DIR=<path>
	-- Base Trilinos directory. Trilinos must be provided, so if this is not set, your trilinos include and lib paths must be in the standard environment variables

-D TRILINOS_BUILD_DIR=<path>
	-- Same as before, but path to where trilinos was build

-D LIBMESH_BUILD_DIR=<path>
	-- Path to the build directory of LibMesh. Same as Trilinos, LibMesh is necessary for ParHBM, so must be either provided or in environment vars.

-D PETSC_ROOT_DIR=<path>
	-- PETSc is necessary if LibMesh was built with it. Path to the base PETSc directory. 

-D PETSC_BUILD_DIR=<path>
	-- Same as before but the build directory of PETSc. This path can also serve as path for metis and parmetis
		libraries (as they are also put into the lib directory of PETSc if PETSc is built with flag to download them)

-D ADDITIONAL_LIB_DIR=<path>
	-- Can be used for providing extra directories for target_link_libraries, such as for metis/parmetis if they were built separately for example.


-D With_UNIT_TESTS=<TRUE | FALSE>
	-- Flag to build unit tests executable (ParHBM_UnitTests)

