

// for templated functions

#pragma once
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <functional>
#include <limits>
#include <iomanip>
#include <type_traits>

template <class T>
void CheckString(const std::string& aString, const std::map<std::string, T>& aPossibilities, const std::string& aGroupName)
{
    const bool lIsIn = aPossibilities.find(aString) != aPossibilities.end();
    
    if (!lIsIn)
    {
        std::stringstream lStringBuilder;
        lStringBuilder << "Value \"" + aString + "\" is not a valid option for \"" + aGroupName + "\"! Valid options are: " << std::endl;
        
        if (aPossibilities.size() == 0) lStringBuilder << " none (you are fucked)";
        
        int lCount = 0;
        for (auto nIt = aPossibilities.begin(); nIt != aPossibilities.end(); nIt++)
        {
            lStringBuilder << nIt->first;
            if (lCount < aPossibilities.size() - 1)
                lStringBuilder << ", ";
            
            lCount++;
        }
        
        lStringBuilder << std::endl;
        
        throw lStringBuilder.str();
    }
}

template <class TElem, class TKey>
bool IsUnique(const std::vector<TElem>& aVec, const std::function<const TKey*(const TElem&)>& aSelector)
{
	for (int i = 0; i < aVec.size(); i++)
	{
		const TKey* lKey1 = aSelector(aVec[i]);
		
		for (int j = 0; j < aVec.size(); j++)
		{
			if (i == j) continue;
			
			const TKey* lKey2 = aSelector(aVec[j]);
			
			if (*lKey1 == *lKey2) return false;
		}
	}
	return true;
}

template <class TElem>
bool IsUnique(const std::vector<TElem>& aVec)
{
	const std::function<const TElem*(const TElem&)> lIdentitySelector = [](const TElem& aElem) 
	{ 
		return &aElem; 
	};
	
	return IsUnique<TElem, TElem>(aVec, lIdentitySelector);
}

template <class T>
void ExportArray(std::ostream& aStream, T* const aData, const int aSize)
{
	aStream << aSize << " ";
	
	if (std::is_same<T, double>::value)
		aStream << std::setprecision(std::numeric_limits<double>::digits10 + 1);
	
	for (int i = 0; i < aSize; i++)
	{
		aStream << aData[i] << " ";
	}
}

template <class T>
void ImportArray(std::istream& aStream, T*& aData, int& aSize)
{	
	aStream >> aSize;
	
	aData = new T[aSize];
	
	for (int i = 0; i < aSize; i++)
	{
		aStream >> aData[i];
	}
}
