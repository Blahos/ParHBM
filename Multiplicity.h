
#pragma once

enum class Multiplicity
{
    Once, Always
};
