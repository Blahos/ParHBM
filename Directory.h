
#pragma once
#include <string>
#include "mpi.h"

bool FileExists(const std::string& aPath);
bool IsAbsolutePath(const std::string& aPath);

// only the last directory in the path is allowed to not exist, the all predecessors must already be created
void CreateDirectory(const std::string& aPath);
// allows the path to include multiple directories separated by "/" that don't exist yet
void CreateDirectories(const std::string& aPath);
bool DirectoryExists(const std::string& aPath);
bool LinkExists(const std::string& aPath);

// MPI functions
void CreateDirectories(const std::string& aPath, const MPI_Comm& aComm);
void CreateSymbolicLink(const std::string& aLinkedDirectory, const std::string& aLinkName, const MPI_Comm& aComm);
// will only work on empty directories
void RemoveDirectoryOrFile(const std::string& aPath, const MPI_Comm& aComm);
