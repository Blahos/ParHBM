
#include <iostream>
#include <fstream>
#include <algorithm>

#include "mpi.h"

// for creating directories
#include <sys/types.h> // required for stat.h
#include <sys/stat.h> // no clue why required -- man pages say so
#include <sys/types.h> // required for stat.h

#include "Functions.h"
#include "Misc.h"
#include "Exception.h"

#include "Epetra_Vector.h"


// Be careful when changing this function! Some indexing operations (local <-> global indices and such) may require this function to have certain properties to be valid
// this could be defined as a compile time macro to make it more efficient
int IndexStandardToHBM(const int aStandardIndex, const int aHarmonicCoeffIndex, const int aHarmonicCoeffCount)
{
    return aStandardIndex * aHarmonicCoeffCount + aHarmonicCoeffIndex;
}
void IndexStandardToHBMInPlace(int* const aStandardIndices, int aHarmonicCoeffIndex, const int aSize, const int aHarmonicCoeffCount)
{
    for (int i = 0; i < aSize; i++)
    {
        int lHBMIndex = IndexStandardToHBM(aStandardIndices[i], aHarmonicCoeffIndex, aHarmonicCoeffCount);
        aStandardIndices[i] = lHBMIndex;
    }
}
std::vector<int> IndexStandardToHBM(const int* const aStandardIndices, int aHarmonicCoeffIndex, const int aSize, const int aHarmonicCoeffCount)
{
    std::vector<int> lReturnVector;
    lReturnVector.reserve(aSize);
    
    for (int i = 0; i < aSize; i++)
    {
        int lHBMIndex = IndexStandardToHBM(aStandardIndices[i], aHarmonicCoeffIndex, aHarmonicCoeffCount);
        lReturnVector.push_back(lHBMIndex);
    }
    
    return lReturnVector;
}

std::vector<int> IndexStandardToHBM(const std::vector<int>& aStandardIndices, int aHarmonicCoeffIndex, const int aHarmonicCoeffCount)
{
	return IndexStandardToHBM(&aStandardIndices[0], aHarmonicCoeffIndex, aStandardIndices.size(), aHarmonicCoeffCount);
}
std::vector<int> IndexStandardToHBM(const int* const aStandardIndices, const int* const aHarmonicCoeffIndices, const int aSize, const int aHarmonicCoeffCount)
{
    std::vector<int> lReturnVector;
    lReturnVector.reserve(aSize);
    
    for (int i = 0; i < aSize; i++)
    {
        int lHBMIndex = IndexStandardToHBM(aStandardIndices[i], aHarmonicCoeffIndices[i], aHarmonicCoeffCount);
        lReturnVector.push_back(lHBMIndex);
    }
    
    return lReturnVector;
}

std::vector<PointForceDef> ReadExcitation(const std::string aFilePath)
{
    std::vector<PointForceDef> lReturnVector;
    std::vector<std::string> lLines = ReadAllValidLines(aFilePath);
    
    for (int i = 0; i < lLines.size(); i++)
    {
        PointForceDef lNewDef;
        
        std::vector<std::string> lSeparateStrings = SplitString(lLines[i], " ");
        if (lSeparateStrings.size() != 7) throw "Expected 7 values on line " + std::to_string(i + 1) + "! Obtained " + std::to_string(lSeparateStrings.size()) + " (ignoring commented lines)";
        
        try
        {
            lNewDef.X = std::stod(lSeparateStrings[0]);
            lNewDef.Y = std::stod(lSeparateStrings[1]);
            lNewDef.Z = std::stod(lSeparateStrings[2]);
            
            lNewDef.Dim = std::stoi(lSeparateStrings[3]);
            if (lNewDef.Dim < 0 || lNewDef.Dim >= 3) throw "Invalid dimension value!";
            
            lNewDef.WaveIndex = std::stoi(lSeparateStrings[4]);
            if (lNewDef.WaveIndex < 0) throw "Invalid wave index value!";
            
            int lWaveTypeInt = std::stoi(lSeparateStrings[5]);
            if (lWaveTypeInt < 0 || lWaveTypeInt >= 3) throw "Invalid wve type value! (0 - DC, 1 - Cos, 2 - Sin)";
            if (lWaveTypeInt == 0) lNewDef.Type = WaveType::DC;
            if (lWaveTypeInt == 1) lNewDef.Type = WaveType::Cos;
            if (lWaveTypeInt == 2) lNewDef.Type = WaveType::Sin;
            
            if (lNewDef.Type == WaveType::DC && lNewDef.WaveIndex != 0) throw "DC wave must have index 0!";
            if (lNewDef.Type == WaveType::Cos && lNewDef.WaveIndex == 0) throw "Cos wave can't have index 0!";
            if (lNewDef.Type == WaveType::Sin && lNewDef.WaveIndex == 0) throw "Sin wave can't have index 0!";
            
            lNewDef.Amp = std::stod(lSeparateStrings[6]);
        }
        catch (std::exception& aEx)
        {
            printf("%s\n", aEx.what());
            throw "Error parsing line " + std::to_string(i + 1) + " (ignoring comments) in excitation file \"" + aFilePath + "\"!";
        }
        catch (const char* aEx)
        {
            printf("%s\n", aEx);;
            throw "Error parsing line " + std::to_string(i + 1) + " (ignoring comments) in excitation file \"" + aFilePath + "\"!";
        }
        catch (const std::string& aEx)
        {
            printf("%s\n", aEx.c_str());
            throw "Error parsing line " + std::to_string(i + 1) + " (ignoring comments) in excitation file \"" + aFilePath + "\"!";
        }
        
        lReturnVector.push_back(lNewDef);
    }
    
    return lReturnVector;
}

std::vector<WaveInfo> CreateHarmonicInfo(const std::vector<int>& aHarmonicWaves)
{
    // check
    if (aHarmonicWaves.size() <= 0) throw "There must be at least one harmonic used!";
    
    for (int i = 0; i < aHarmonicWaves.size(); i++)
    {
        if (aHarmonicWaves.at(i) < 0)
            throw "Harmonic indices must be non-negative!";
    }
    
    for (int i = 0; i < aHarmonicWaves.size() - 1; i++)
    {
        if (aHarmonicWaves.at(i) >= aHarmonicWaves.at(i + 1))
            throw "Harmonic indices must be sorted in the ascending order and no duplicates are allowed!";
    }
    
    std::vector<WaveInfo> lReturnVector;
    
	// 0th (DC) harmonic must, if present
	// this is not a necessity, but it is a chosen convention to make the code simpler
	// the input list being sorted guarantees that
	// then follow the next harmonics, the waves are always next to each other in order cos sin
    for (int i = 0; i < aHarmonicWaves.size(); i++)
    {
        if (aHarmonicWaves.at(i) == 0)
        {
            WaveInfo lNewInfo;
            lNewInfo.Type = WaveType::DC;
            lNewInfo.WaveIndex = 0;
            
            lReturnVector.push_back(lNewInfo);
        }
        else
        {
            WaveInfo lNewInfo1;
            lNewInfo1.Type = WaveType::Cos;
            lNewInfo1.WaveIndex = aHarmonicWaves.at(i);
            
            WaveInfo lNewInfo2;
            lNewInfo2.Type = WaveType::Sin;
            lNewInfo2.WaveIndex = aHarmonicWaves.at(i);
            
            lReturnVector.push_back(lNewInfo1);
            lReturnVector.push_back(lNewInfo2);
        }
    }
    
    return lReturnVector;
}

void GetMatrixRowValues(const Epetra_CrsMatrix& aMatrix, int aGlobalRowIndex, CrsMatrixRow& aResult)
{
    int lSize = aMatrix.NumGlobalEntries(aGlobalRowIndex);
	
	aResult.Init(lSize);
    aMatrix.ExtractGlobalRowCopy(aGlobalRowIndex, lSize, lSize, aResult.Data()->Values(), aResult.Data()->Indices());
	
	aResult.GlobalRowIndex = aGlobalRowIndex;
}
bool AreColumnIndicesSorted(const Epetra_CrsMatrix& aMatrix)
{
	CrsMatrixRow lRow;
	
	for (int i = 0; i < aMatrix.NumMyRows(); i++)
	{
		int lGlobalRowIndex = aMatrix.RowMap().MyGlobalElements()[i];
		
		GetMatrixRowValues(aMatrix, lGlobalRowIndex, lRow);
		
		if (!lRow.AreIndicesSorted())
		{
			printf("Matrix row %d doesn't have sorted column indices\n", lGlobalRowIndex);
			return false;
		}
	}
	return true;
}

void AddNodalForce(Epetra_MultiVector& aF, double aX, double aY, double aZ, int aDim, double aMagnitude, std::vector<DofInfo> aDofInfo, const Epetra_Comm& aComm)
{
    int lProcID = aComm.MyPID();
    
    printf("Starting AddNodalForce in process %d\n", lProcID);
    std::sort(aDofInfo.begin(), aDofInfo.end(), [aX, aY, aZ](const DofInfo& aInfo1, const DofInfo& aInfo2)
    {
        double lDist1X = (aInfo1.X - aX);
        double lDist1Y = (aInfo1.Y - aY);
        double lDist1Z = (aInfo1.Z - aZ);
        double lDist1Sqr = lDist1X * lDist1X + lDist1Y * lDist1Y + lDist1Z * lDist1Z;
        
        double lDist2X = (aInfo2.X - aX);
        double lDist2Y = (aInfo2.Y - aY);
        double lDist2Z = (aInfo2.Z - aZ);
        double lDist2Sqr = lDist2X * lDist2X + lDist2Y * lDist2Y + lDist2Z * lDist2Z;
        
        return lDist1Sqr < lDist2Sqr;
    });
    
    DofInfo lClosestDof;
    for (int i = 0; i < aDofInfo.size(); i++)
    {
        const DofInfo& lInfo = aDofInfo[i];
        if (lInfo.Dim == aDim)
        {
            lClosestDof = lInfo;
            break;
        }
    }
    
    double lDofDistSqr = 
        (aX - lClosestDof.X) * (aX - lClosestDof.X) + 
        (aY - lClosestDof.Y) * (aY - lClosestDof.Y) + 
        (aZ - lClosestDof.Z) * (aZ - lClosestDof.Z);
        
    double lDofDistSqrGlobalMin = -1.0;
    
    aComm.MinAll(&lDofDistSqr, &lDofDistSqrGlobalMin, 1);
    
    if (lDofDistSqr != lDofDistSqrGlobalMin) aMagnitude = 0;
    
    aF.SumIntoGlobalValue(lClosestDof.DofId, 0, aMagnitude);
    
    if (lDofDistSqr == lDofDistSqrGlobalMin)
    {
        printf("Added force %f to dof %d node %d dimension %d [%f; %f; %f] at process %d, distance: %f\n", 
               aMagnitude, lClosestDof.DofId, lClosestDof.NodeId, lClosestDof.Dim, lClosestDof.X, lClosestDof.Y, lClosestDof.Z, lProcID, std::sqrt(lDofDistSqr));
    }
    printf("Finishing AddNodalForce in process %d\n", lProcID);
}
void ExportMatrix(const Epetra_CrsMatrix& aMatrix, const std::string& aFileName, int aMaxRows)
{
    const std::string lHeader = "Row, Col, Value";
	
	int lRank = aMatrix.Comm().MyPID();
    
    std::ofstream lFile(aFileName + "_rank" + std::to_string(lRank));
    lFile << std::setprecision(std::numeric_limits<long double>::digits10 + 1);
    
	CrsMatrixRow lRowValues;
	
	int lRowCount = aMatrix.RowMap().NumMyElements();
	if (aMaxRows > 0 && lRowCount > aMaxRows) lRowCount = aMaxRows;
		
    for (int iRow = 0; iRow < lRowCount; iRow++)
    {
        const int lGlobalRowIndex = aMatrix.RowMap().MyGlobalElements()[iRow];
        GetMatrixRowValues(aMatrix, lGlobalRowIndex, lRowValues);
        
		const int* const lIndices = lRowValues.Data()->Indices();
		const double* const lValues = lRowValues.Data()->Values();
		
		const double* lValuesDirect = aMatrix[iRow];
		
        for (int iVal = 0; iVal < lRowValues.Size(); iVal++)
        {
            lFile << lGlobalRowIndex << ", " << lIndices[iVal] << ", " << lValues[iVal] << ", " << lValuesDirect[iVal] << std::endl;
        }
    }
    lFile.close();
}
void ExportVector(const Epetra_Vector& aVector, const std::string& aFileName)
{
    const std::string lHeader = "Row, Value";
	
	int lRank = aVector.Comm().MyPID();
    
    std::ofstream lFile(aFileName + "_rank" + std::to_string(lRank));
    lFile << std::setprecision(std::numeric_limits<long double>::digits10 + 1);
    
	CrsMatrixRow lRowValues;
    for (int iRow = 0; iRow < aVector.Map().NumMyElements(); iRow++)
    {
        const int lGlobalRowIndex = aVector.Map().MyGlobalElements()[iRow];
//         GetMatrixRowValues(aMatrix, lGlobalRowIndex, lRowValues);
        
		double lValue = aVector[iRow];
		
		lFile << lGlobalRowIndex << ", " << lValue << std::endl;
    }
    lFile.close();
}

std::map<std::string, std::vector<std::string>> ParseGeneralConfigFile(const std::string& aFilePath)
{
    std::map<std::string, std::vector<std::string>> lReturnMap;
    
    std::vector<std::string> lAllValidLines = ReadAllValidLines(aFilePath);
    
    std::string lLastKey = "";
    
    for (int iLine = 0; iLine < lAllValidLines.size(); iLine++)
    {
        std::string lLine = lAllValidLines[iLine];
        
        if (lLine[0] == KEY_PREFIX_CHAR)
        {
            // this line is a key
            // skip the key prefix character
            lLine = lLine.substr(1);
            
            // skip possible whitespace after the key prefix character
            lLine = SkipWhiteSpaces(lLine);
            
            if (lReturnMap.find(lLine) != lReturnMap.end()) throw "Key \"" + lLine + "\" occurs in the file \"" + aFilePath + "\" multiple times!";
                        
            lReturnMap[lLine] = std::vector<std::string>();
            
            lLastKey = lLine;
        }
        else
        {
            if (lLastKey.size() == 0) throw "First valid line in the configuration file must be a key definition! Key definition starts with the \'" + std::to_string(KEY_PREFIX_CHAR) + "\' character.";
            
            lReturnMap[lLastKey].push_back(lLine);
        }
    }
    
    return lReturnMap;
}

std::vector<std::string> ReadAllValidLines(const std::string& aFilePath)
{
    std::ifstream lInputFile(aFilePath);
    if (!lInputFile.is_open()) throw std::string("Unable to open file \"" + aFilePath + "\"!");
    
    std::vector<std::string> lReturnVector;
    
    bool lIsValidLine = true;
    
    while (true)
    {
        std::string lLine = GetNextValidLine(lInputFile, lIsValidLine);
        if (!lIsValidLine) break;
        
        lReturnVector.push_back(lLine);
    }
    
    return lReturnVector;
}
std::string GetNextValidLine(std::ifstream& aFile, bool& aIsValid)
{
    std::string lReturnValue = "";
    
    bool lFoundValidLine = false;
    
    while (!aFile.eof())
    {
        std::getline(aFile, lReturnValue);
        
        lReturnValue = SkipWhiteSpaces(lReturnValue);
        
        if (lReturnValue.size() == 0) continue;
        if (lReturnValue[0] == '#') continue;
        
        lFoundValidLine = true;
        break;
    }
    
    aIsValid = lFoundValidLine;
    
    return lReturnValue;
}
std::vector<std::string> SplitString(const std::string& aString, const std::string& aDelimiter)
{
    std::vector<std::string> lReturnVector;
    
    std::string lStringCopy(aString);

    size_t lPos = 0;
    std::string lToken;
    while ((lPos = lStringCopy.find(aDelimiter)) != std::string::npos) 
    {
        lToken = lStringCopy.substr(0, lPos);
        
        if (lToken.length() > 0)
            lReturnVector.push_back(lToken);
        
        lStringCopy.erase(0, lPos + aDelimiter.length());
    }
    
    // the last part after the last delimiter
    if (lStringCopy.length() > 0)
        lReturnVector.push_back(lStringCopy);
    
    return lReturnVector;
}
void SaveHBMSolutions(const std::vector<double>& aFrequencies, const std::vector<Epetra_Vector>& aSolutions, const std::string aFileName, double aTimeScale, double aLengthScale, const Epetra_Comm& aComm)
{
	if (aFrequencies.size() != aSolutions.size()) throw "Sizes of frequencies and solutions vectors don't match!";
	
	if (aFrequencies.size() <= 0)
    {
        std::cout << "No solutions provided, nothing to save." << std::endl;
        return;
    }
    const std::string lDelim = ",";
    
    int lRank = aComm.MyPID();
    
    std::ofstream lOutputFile(aFileName + "_" + "proc" + std::to_string(lRank) + ".txt");
    if (!lOutputFile.is_open()) throw "Could not create an output file \"" + aFileName + "\" on process num. " + std::to_string(lRank);
    
    lOutputFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
    
    for (int i = 0; i < aFrequencies.size(); i++)
    {
        double lFreq = aFrequencies[i];
        const Epetra_Vector& lSolution = aSolutions[i];
        
        int lSolutionSize = lSolution.Map().NumMyElements();
        const double* const lSolutionValues = lSolution.Values();
        
        lOutputFile << lFreq / aTimeScale << lDelim;
        
        for (int j = 0; j < lSolutionSize; j++)
        {
            lOutputFile << lSolutionValues[j] * aLengthScale;
            if (j < lSolutionSize - 1) lOutputFile << lDelim;
        }
        
        lOutputFile << std::endl;
    }
    
    lOutputFile.close();
}

std::ostream& operator<<(std::ostream& aStream, const DofInfo& aInfo)
{
    aStream << "Node " << aInfo.NodeId << ", dof " << aInfo.DofId << ", dim " << aInfo.Dim << ", [" << aInfo.X << ", " << aInfo.Y << ", " << aInfo.Z << "]";
    
    return aStream;
}
std::ostream& operator<<(std::ostream& aStream, const DofHBMInfo& aInfo)
{
    aStream << "HBM dof " << aInfo.DofIdHBM << ", harm coeff " << aInfo.HarmonicCoeffIndex << ", harm wave " << aInfo.HarmonicWaveIndex << ", physical dof (" << aInfo.PhysicalDofInfo << ")";
    
    return aStream;
}

std::string SkipWhiteSpaces(const std::string& aString)
{
    int lFirstNonWhitespace = 0;
    while (lFirstNonWhitespace < aString.size())
    {
        char lChar = aString[lFirstNonWhitespace];
        if (lChar != ' ' && lChar != '\t') break;
        lFirstNonWhitespace++;
    }
    
    if (lFirstNonWhitespace >= aString.size()) return "";
    
    std::string lReturnValue = aString.substr(lFirstNonWhitespace);
    
    return lReturnValue;
}
void CheckString(const std::string& aString, const std::vector<std::string>& aPossibilities, const std::string& aGroupName)
{
    bool lIsIn = std::find(aPossibilities.begin(), aPossibilities.end(), aString) != aPossibilities.end();
    
    if (!lIsIn)
    {
        std::stringstream lStringBuilder;
        lStringBuilder << "Value \"" + aString + "\" is not a valid option for \"" + aGroupName + "\"! Valid options are: " << std::endl;
        
        if (aPossibilities.size() == 0) lStringBuilder << " none (you are fucked)";
        
        for (int i = 0; i < aPossibilities.size(); i++)
        {
            lStringBuilder << aPossibilities[i];
            if (i < aPossibilities.size() - 1)
                lStringBuilder << ", ";
        }
        
        lStringBuilder << std::endl;
        
        throw lStringBuilder.str();
    }
}

std::string WaveTypeString(const WaveType& aType)
{
    if (aType == WaveType::DC)  return "DC";
    if (aType == WaveType::Cos) return "Cos";
    if (aType == WaveType::Sin) return "Sin";
    throw "Unrecognised wave type enum value!";
}
std::vector<double> ParseMatlabStyleVector(const std::string& aString)
{
	std::vector<double> lReturnVector;
	
	std::vector<std::string> lSplit = SplitString(aString, ":");
	if (lSplit.size() < 2 || lSplit.size() > 3) throw "Invalid matlab style vector, must have 2 or 3 parts separated by the \":\" character!";
	
	double lMin;
	double lStep;
	double lMax;
	try
	{
		lMin = std::stod(lSplit[0]);
		if (lSplit.size() == 2)
		{
			lStep = 1.0;
			lMax = std::stod(lSplit[1]);
		}
		else
		{
			lStep = std::stod(lSplit[1]);
			lMax = std::stod(lSplit[2]);
		}
	}
	catch (std::exception& aEx)
	{
		throw "Error parsing some part of \"" + aString + "\" as double! Inner exception message: " + std::string(aEx.what());
	}
	catch (const char* aEx)
	{
		throw "Error parsing some part of \"" + aString + "\" as double! Inner exception message: " + std::string(aEx);
	}
	
	for (double lVal = lMin; lVal <= lMax; lVal += lStep)
		lReturnVector.push_back(lVal);
	
	return lReturnVector;
}

ParHBM::SolverType GetSolverType(ParHBM::Solver aSolver)
{
	switch (aSolver)
	{
		case ParHBM::Solver::MUMPS:
			return ParHBM::SolverType::Direct;
			break;
		case ParHBM::Solver::MKLPDSS:
			return ParHBM::SolverType::Direct;
			break;
		default:
			throw "Unrecognised solver!";
			break;
	}
}

void CheckMatrixForZeroRows(const Epetra_CrsMatrix& aMatrix)
{
	CrsMatrixRow lRow;
	
	for (int iRow = 0; iRow < aMatrix.NumMyRows(); iRow++)
	{
		const int lGlobalRowIndex = aMatrix.RowMap().MyGlobalElements()[iRow];
		
		GetMatrixRowValues(aMatrix, lGlobalRowIndex, lRow);
		
		bool lAllZero = true;
		for (int iVal = 0; iVal < lRow.Size(); iVal++)
		{
			if (lRow.Data()->Values()[iVal] != 0.0)
			{
				lAllZero = false;
				break;
			}
		}
		if (lAllZero) throw "All elements in the matrix are zero for row: " + std::to_string(lGlobalRowIndex) + " (number of elements in this row = " + std::to_string(lRow.Size()) + ")";
	}
}
void CheckMatrixForEmptyRows(const Epetra_CrsMatrix& aMatrix)
{
	for (int iRow = 0; iRow < aMatrix.NumMyRows(); iRow++)
	{
		const int lGlobalRowIndex = aMatrix.RowMap().MyGlobalElements()[iRow];
		if (aMatrix.NumMyEntries(iRow) == 0)
			throw "Matrix has no entries in row " + std::to_string(lGlobalRowIndex) + "!";
	}
}
bool RunInTryCatch(const std::function<void()>& aFunc, int aRank, bool aPrintException, bool aThrowOut)
{
	std::string lRankString = std::to_string(aRank);
	
	const std::string lUnknownLiteral = "unknown";
	if (aRank < 0) lRankString = lUnknownLiteral;
	
	try
	{
		aFunc();
		return true;
	}
    catch (ParHBM::Exception& aEx)
    {
		if (aPrintException)
			printf("ParHBM exception (rank %s):\n", lRankString.c_str());
			printf("	Message: %s\n", aEx.Message.size() != 0 ? aEx.Message.c_str() : lUnknownLiteral.c_str());
			printf("	Code: %d\n", aEx.Code);
			printf("	Function: %s\n", aEx.FunctionName.size() != 0 ? aEx.FunctionName.c_str() : lUnknownLiteral.c_str());
			printf("	Class: %s\n", aEx.ClassName.size() != 0 ? aEx.ClassName.c_str() : lUnknownLiteral.c_str());
		if (aThrowOut)
			throw;
		return false;
    }
    catch (std::exception& aEx)
    {
		if (aPrintException)
			printf("Exception (rank %s): %s\n", lRankString.c_str(), aEx.what());
		if (aThrowOut)
			throw;
		return false;
    }
    catch (const char* aEx)
    {
		if (aPrintException)
			printf("Exception (rank %s): %s\n", lRankString.c_str(), aEx);
		if (aThrowOut)
			throw;
		return false;
    }
    catch (const std::string& aEx)
    {
		if (aPrintException)
			printf("Exception (rank %s): %s\n", lRankString.c_str(), aEx.c_str());
		if (aThrowOut)
			throw;
		return false;
    }
    catch (...)
	{
		if (aPrintException)
			printf("Unknown exception occured (rank %s), terminating.\n", lRankString.c_str());
		if (aThrowOut)
			throw;
		return false;
	}
}

void MPISerialize(const std::function<void()>& aFunc, const MPI_Comm& aComm)
{
	int lRank;
	int lRankCount;
	
	MPI_Comm_rank(aComm, &lRank);
	MPI_Comm_size(aComm, &lRankCount);
	
	MPI_Barrier(aComm);
	
	for (int iRank = 0; iRank < lRankCount; iRank++)
	{		
		if (lRank == iRank)
		{
			aFunc();
		}
		MPI_Barrier(aComm);
	}
}
bool IsInArray(const std::vector<int>& aArray, const int aValue, const bool aIsSorted, const bool aIsContiguous)
{
	if (aArray.size() == 0) return false;
	
	if (aIsSorted)
	{
		if (aIsContiguous)
		{
			return aValue >= aArray[0] && aValue <= aArray[aArray.size() - 1];
		}
		else
		{
			return std::binary_search(aArray.begin(), aArray.end(), aValue);
		}
	}
	else
	{
		return std::find(aArray.begin(), aArray.end(), aValue) != aArray.end();
	}
}

void Merge(const std::vector<int>& aArray1, const std::vector<int>& aArray2, std::vector<int>& aArrayResult)
{
	if (&aArray1 == &aArrayResult) throw PARHBM_EXCEPTION_GLOBAL("First input array and result array can not be the same!");
	if (&aArray2 == &aArrayResult) throw PARHBM_EXCEPTION_GLOBAL("Second input array and result array can not be the same!");
	
	aArrayResult.clear();
	aArrayResult.reserve(aArray1.size() + aArray2.size());
	
	int lPos1 = 0;
	int lPos2 = 0;
	
	while (lPos1 < aArray1.size() || lPos2 < aArray2.size())
	{
		int lCurrentValue = std::numeric_limits<int>::max();
		if (lPos1 < aArray1.size() && aArray1[lPos1] < lCurrentValue) lCurrentValue = aArray1[lPos1];
		if (lPos2 < aArray2.size() && aArray2[lPos2] < lCurrentValue) lCurrentValue = aArray2[lPos2];
		
		if (aArrayResult.size() == 0 || aArrayResult[aArrayResult.size() - 1] != lCurrentValue)
		{
			aArrayResult.push_back(lCurrentValue);
		}
		
		if (lPos1 < aArray1.size() && aArray1[lPos1] == lCurrentValue)
		{
			lPos1++;
			continue;
		}
		
		if (lPos2 < aArray2.size() && aArray2[lPos2] == lCurrentValue)
		{
			lPos2++;
			continue;
		}
	}
}
