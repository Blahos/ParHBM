
#include "Aft.h"

#include <cmath>

#include "Functions.h"


Aft::Aft(int aTimePointCount, const std::vector<DofInfo>& aPhysicalDofs, const std::vector<DofHBMInfo>& aHBMDofs)
: Aft(aTimePointCount, DofInfoToDofId(aPhysicalDofs), aHBMDofs)
{
}
Aft::Aft(int aTimePointCount, const std::vector<int>& aPhysicalDofs, const std::vector<DofHBMInfo>& aHBMDofs)
: cPhysicalDofs(aPhysicalDofs), cHBMDofs(aHBMDofs),
cTimePointCount(aTimePointCount), cPhysicalDofCount(aPhysicalDofs.size())
// cFftwTotalSize(aTimePointCount * cPhysicalDofCount),
// cFreqDomainFftwData(new fftw_complex[cFftwTotalSize]),
// cTimeDomainFftwData(new fftw_complex[cFftwTotalSize]),
// cFreqToTimePlan(fftw_plan_many_dft(
//                 1, &cTimePointCount, cPhysicalDofCount,
//                 &cFreqDomainFftwData[0], nullptr, 
//                 1, cTimePointCount,
//                 &cTimeDomainFftwData[0], nullptr, 
//                 1, cTimePointCount, 
//                 FFTW_BACKWARD, 0)),
//                 
// cTimeToFreqPlan(fftw_plan_many_dft(
//                 1, &cTimePointCount, cPhysicalDofCount,
//                 &cTimeDomainFftwData[0], nullptr, 
//                 1, cTimePointCount,
//                 &cFreqDomainFftwData[0], nullptr, 
//                 1, cTimePointCount, 
//                 FFTW_FORWARD, 0))
{
    if (cTimePointCount <= 0) throw "Number of time points must be positive!";
    if (cPhysicalDofCount <= 0) throw "Number of physical degrees of freedom must be positive!";
    
    mCosValues.reserve(cTimePointCount);
    mSinValues.reserve(cTimePointCount);
    
    for (int i = 0; i < cTimePointCount; i++)
    {
        double lTimeValue = (double)i / cTimePointCount;
        
        double lCosValue = std::cos(lTimeValue * 2 * PI);
        double lSinValue = std::sin(lTimeValue * 2 * PI);
        
        mCosValues.push_back(lCosValue);
        mSinValues.push_back(lSinValue);
    }
}
std::vector<double> Aft::FreqToTime(const Epetra_MultiVector& aX, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex) const
{
    const int lHarmCoeffCount = aHarmonics.size();
    const int lFreqElements = aX.Map().NumMyElements();
    if (lFreqElements % lHarmCoeffCount != 0) throw "Size of the X vector must be a multiple of the number of harmonic coefficients!";
    if (aTimePointIndex < 0 || aTimePointIndex >= cTimePointCount) throw "Invalid time point index!";
    
    const int lTimeElements = lFreqElements / lHarmCoeffCount;
    
    std::vector<double> lReturnVector;
    lReturnVector.reserve(lTimeElements);
    
    for (int iVarTime = 0; iVarTime < lTimeElements; iVarTime++)
    {
        double lTimeValue = 0;
        
        const int lPhysicalDofId = cPhysicalDofs[iVarTime];
        
        for (int iHarm = 0; iHarm < lHarmCoeffCount; iHarm++)
        {
            const double lWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iHarm]);
            
            const int lVarIndexHBM = IndexStandardToHBM(iVarTime, iHarm, lHarmCoeffCount);
            
            const int lPhysicalDofId2 = cHBMDofs[lVarIndexHBM].PhysicalDofInfo.DofId;
            
            // just a check if we are not messing up the indexing
            // if this ever throws, we need to fix the index manipulations somewhere/somehow
            // it is commented out now because it does not work properly when this class is initialised with dof info for overlapping mesh
            // because we don't put here the overlapping info for HBM dofs, only for physical dofs, so they are mismatching and this check 
            // fails (the HBM dof info is put here only for this check so the mismatch does not matter)
//             if (lPhysicalDofId != lPhysicalDofId2) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
            
            lTimeValue += aX[0][lVarIndexHBM] * lWaveValue;
        }
        
        lReturnVector.push_back(lTimeValue);
    }
    
    return lReturnVector;
}
// std::vector<double> Aft::FreqToTimeFftw(const Epetra_MultiVector& aX, const std::vector<WaveInfo>& aHarmonics) const
// {
//     std::vector<double> lReturnVector;
//     lReturnVector.reserve(cFftwTotalSize);
//     
//     for (int i = 0; i < cFftwTotalSize; i++)
//     {
//         cFreqDomainFftwData[i][0] = 0;
//         cFreqDomainFftwData[i][1] = 0;
//         cTimeDomainFftwData[i][0] = 0;
//         cTimeDomainFftwData[i][1] = 0;
//     }
//     
//     for (int iDof = 0; iDof < cPhysicalDofCount; iDof++)
//     {
//         for (int iHarm = 0; iHarm < aHarmonics.size(); iHarm++)
//         {
//             const WaveInfo& lHarmInfo = aHarmonics[iHarm];
//             int lFreqIndex = IndexStandardToHBM(iDof, iHarm, aHarmonics.size());
//             double lWaveValue = aX[0][lFreqIndex];
//             int lArrayIndex = IndexStandardToHBM(iDof, lHarmInfo.WaveIndex, cTimePointCount);
//             
//             if (lHarmInfo.Type == WaveType::DC || lHarmInfo.Type == WaveType::Cos)
//             {
//                 cFreqDomainFftwData[lArrayIndex][0] = lWaveValue;
//             }
//             else if (lHarmInfo.Type == WaveType::Sin)
//             {
//                 // FFTW uses minus for sin (imaginary) values
//                 cFreqDomainFftwData[lArrayIndex][1] = -lWaveValue;
//             }
//             else throw "Unknown wave type!";
//         }
//     }
//     
//     fftw_execute(cFreqToTimePlan);
//     
//     for (int i = 0; i < cFftwTotalSize; i++)
//     {
//         lReturnVector.push_back(cTimeDomainFftwData[i][0]);
//     }
//     
//     return lReturnVector;
// }
std::vector<double> Aft::TimeToFreq(const Epetra_MultiVector& aF, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex) const
{
    if (aTimePointIndex < 0 || aTimePointIndex >= cTimePointCount) throw "Invalid time point index!";
    
    int lTimeElements = aF.Map().NumMyElements();
    const int lHarmCoeffCount = aHarmonics.size();
    const int lFreqElements = lTimeElements * lHarmCoeffCount;
    
    std::vector<double> lReturnVector(lFreqElements, 0.0);
    
    for (int iTime = 0; iTime < lTimeElements; iTime++)
    {
        const double lTimeValue = aF[0][iTime];
        const int lPhysicalDofId = cPhysicalDofs[iTime];
        
        for (int iHarm = 0; iHarm < lHarmCoeffCount; iHarm++)
        {            
            const double lWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iHarm]);
            
            const int lVarIndexHBM = IndexStandardToHBM(iTime, iHarm, lHarmCoeffCount);
            
            const int lPhysicalDofId2 = cHBMDofs[lVarIndexHBM].PhysicalDofInfo.DofId;
            
            // just a check if we are not messing up the indexing
            // if this ever throws, we need to fix the index manipulations somewhere/somehow
            if (lPhysicalDofId != lPhysicalDofId2) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
            
            lReturnVector[lVarIndexHBM] = lWaveValue * lTimeValue / cTimePointCount;
        }
    }
    
    return lReturnVector;
}
// std::vector<double> Aft::TimeToFreqFftw(const std::vector<double>& aF, const std::vector<WaveInfo>& aHarmonics) const
// {
//     if (aF.size() != cFftwTotalSize) throw "Invalid size of the F vector!"; 
//     std::vector<double> lReturnVector(cPhysicalDofCount * aHarmonics.size(), 0.0);
//     
//     for (int i = 0; i < cFftwTotalSize; i++)
//     {
//         cFreqDomainFftwData[i][0] = 0;
//         cFreqDomainFftwData[i][1] = 0;
//         cTimeDomainFftwData[i][0] = 0;
//         cTimeDomainFftwData[i][1] = 0;
//     }
//     
//     for (int i = 0; i < cFftwTotalSize; i++)
//     {
//         cTimeDomainFftwData[i][0] = aF[i];
//     }
//     
//     fftw_execute(cTimeToFreqPlan);
//     
//     for (int iDof = 0; iDof < cPhysicalDofCount; iDof++)
//     {
//         for (int iHarm = 0; iHarm < aHarmonics.size(); iHarm++)
//         {
//             const WaveInfo& lHarmInfo = aHarmonics[iHarm];
//             int lFreqIndex = IndexStandardToHBM(iDof, iHarm, aHarmonics.size());
//             int lArrayIndex = IndexStandardToHBM(iDof, lHarmInfo.WaveIndex, cTimePointCount);
//             
//             double lWaveValue1 = cFreqDomainFftwData[lArrayIndex][0];
//             double lWaveValue2 = cFreqDomainFftwData[lArrayIndex][1];
//             
//             if (lHarmInfo.Type == WaveType::DC)
//             {
//                 lReturnVector[lFreqIndex] = lWaveValue1 / cTimePointCount;
//             }
//             else if (lHarmInfo.Type == WaveType::Cos)
//             {
//                 lReturnVector[lFreqIndex] = lWaveValue1 / cTimePointCount;
//             }
//             else if (lHarmInfo.Type == WaveType::Sin)
//             {
//                 // FFTW uses minus for sin (imaginary) values
//                 lReturnVector[lFreqIndex] = -lWaveValue2 / cTimePointCount;
//             }
//             else throw "Unknown wave type!";
//         }
//     }
//     
//     return lReturnVector;
// }
// std::vector<double> Aft::TimeToFreqFftwBig(const std::vector<double>& aJacRowAllTimes, const std::vector<WaveInfo>& aHarmonics, int aScaleHarmonic, int aMaxSize)
// {
//     if (aScaleHarmonic < 0 || aScaleHarmonic >= aHarmonics.size()) throw "Invalid scale harmonic index!";
//     if (aMaxSize % cTimePointCount != 0) throw "Invalid max size!";
//     if (aJacRowAllTimes.size() % cTimePointCount != 0) throw "Invalid jac row array size!";
//     if (aJacRowAllTimes.size() > aMaxSize) throw "Jac row array is too big!";
//     if (aJacRowAllTimes.size() > mFftwBigTotalSize) throw "Jac row array is too big";
//     int lPhysicalDofCount = aJacRowAllTimes.size() / cTimePointCount;
//     
//     if (mFftwBigTotalSize < 0) AllocateBigPlan(aMaxSize);
//     if (aMaxSize > mFftwBigTotalSize) throw "Max size presented is bigger than the one used for the allocation. Reallocation is not possible!";
// //     if (aJacRowAllTimes.size() != cFftwBigTotalSize) throw "Invalid size of the Jac row vector!";
//     std::vector<double> lReturnVector(lPhysicalDofCount * aHarmonics.size(), 0.0);
//     
//     for (int i = 0; i < aJacRowAllTimes.size(); i++)
//     {
//         mFreqDomainFftwDataBig[i][0] = 0;
//         mFreqDomainFftwDataBig[i][1] = 0;
//         mTimeDomainFftwDataBig[i][0] = 0;
//         mTimeDomainFftwDataBig[i][1] = 0;
//     }
//     
//     for (int iTime = 0; iTime < cTimePointCount; iTime++)
//     {
//         const WaveInfo& lWaveInfo = aHarmonics[aScaleHarmonic];
//         double lWaveValue = GetHarmonicValue(iTime, lWaveInfo);
//         
//         for (int iDof = 0; iDof < lPhysicalDofCount; iDof++)
//         {
//             const int lIndex = IndexStandardToHBM(iDof, iTime, cTimePointCount);
//             mTimeDomainFftwDataBig[lIndex][0] = aJacRowAllTimes[lIndex] * lWaveValue;
//         }
//     }
//     
//     fftw_execute(mTimeToFreqPlanBig);
//     
//     for (int iDof = 0; iDof < lPhysicalDofCount; iDof++)
//     {
//         for (int iHarm = 0; iHarm < aHarmonics.size(); iHarm++)
//         {
//             const WaveInfo& lHarmInfo = aHarmonics[iHarm];
//             int lFreqIndex = IndexStandardToHBM(iDof, iHarm, aHarmonics.size());
//             int lArrayIndex = IndexStandardToHBM(iDof, lHarmInfo.WaveIndex, cTimePointCount);
//             
//             double lWaveValue1 = mFreqDomainFftwDataBig[lArrayIndex][0];
//             double lWaveValue2 = mFreqDomainFftwDataBig[lArrayIndex][1];
//             
//             if (lHarmInfo.Type == WaveType::DC)
//             {
//                 lReturnVector[lFreqIndex] = lWaveValue1 / cTimePointCount;
//             }
//             else if (lHarmInfo.Type == WaveType::Cos)
//             {
//                 lReturnVector[lFreqIndex] = lWaveValue1 / cTimePointCount;
//             }
//             else if (lHarmInfo.Type == WaveType::Sin)
//             {
//                 // FFTW uses minus for sin (imaginary) values
//                 lReturnVector[lFreqIndex] = -lWaveValue2 / cTimePointCount;
//             }
//             else throw "Unknown wave type!";
//         }
//     }
// 
//     return lReturnVector;
// }

std::vector<CrsMatrixRow> Aft::TimeToFreq(const Epetra_CrsMatrix& aJac, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex) const
{
    if (aTimePointIndex < 0 || aTimePointIndex >= cTimePointCount) throw "Invalid time point index!";
    
    int lTimeRows = aJac.RowMap().NumMyElements();
    const int lHarmCoeffCount = aHarmonics.size();
    const int lFreqRows = lTimeRows * lHarmCoeffCount;
    
    std::vector<CrsMatrixRow> lReturnVector(lFreqRows, CrsMatrixRow());
    
	CrsMatrixRow lTimeRowValues;
	
    for (int iRowTime = 0; iRowTime < lTimeRows; iRowTime++)
    {
        const int lGlobalRowIndexTime = aJac.RowMap().MyGlobalElements()[iRowTime];
        const int lRowPhysicalDofId = cPhysicalDofs[iRowTime];
        
        GetMatrixRowValues(aJac, lGlobalRowIndexTime, lTimeRowValues);
		
		const int* const lTimeRowValuesIndices = lTimeRowValues.Data()->Indices();
		const double* const lTimeRowValuesValues = lTimeRowValues.Data()->Values();
        
        for (int iRowHarm = 0; iRowHarm < lHarmCoeffCount; iRowHarm++)
        {
            const int lRowIndexHBM = IndexStandardToHBM(iRowTime, iRowHarm, lHarmCoeffCount);
            const int lRowPhysicalDofId2 = cHBMDofs[lRowIndexHBM].PhysicalDofInfo.DofId;
            
            if (lRowPhysicalDofId != lRowPhysicalDofId2) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
            
            const double lRowWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iRowHarm]);
            
            CrsMatrixRow& lResultRow = lReturnVector[lRowIndexHBM];
            
            lResultRow.Init(lTimeRowValues.Size() * lHarmCoeffCount);
            lResultRow.GlobalRowIndex = IndexStandardToHBM(lGlobalRowIndexTime, iRowHarm, lHarmCoeffCount);
            
            if (lResultRow.GlobalRowIndex != cHBMDofs[lRowIndexHBM].DofIdHBM) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
            
            for (int iColHarm = 0; iColHarm < lHarmCoeffCount; iColHarm++)
            {
                const double lColWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iColHarm]);
                
                for (int iColTime = 0; iColTime < lTimeRowValues.Size(); iColTime++)
                {
                    const int lTimeColIndexGlobal = lTimeRowValuesIndices[iColTime];
                    double lTimeValue = lTimeRowValuesValues[iColTime];
                    
                    int lHarmColIndexGlobal = IndexStandardToHBM(lTimeColIndexGlobal, iColHarm, lHarmCoeffCount);
                    int lHarmColIndexLocal = IndexStandardToHBM(iColTime, iColHarm, lHarmCoeffCount);
                    
					// TODO optimise the use of Indices() and Values() functions
                    lResultRow.Data()->Indices()[lHarmColIndexLocal] = lHarmColIndexGlobal;
                    lResultRow.Data()->Values()[lHarmColIndexLocal] = lTimeValue * lRowWaveValue * lColWaveValue / cTimePointCount;
                }
            }
        }
    }
    
    return lReturnVector;
}

std::vector<CrsMatrixRow> Aft::TimeToFreq(const Epetra_CrsMatrix& aJac, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex, int aLocalMatrixRowIndex) const
{
    if (aTimePointIndex < 0 || aTimePointIndex >= cTimePointCount) throw "Invalid time point index!";
    
//     int lTimeRows = aJac.RowMap().NumMyElements();
    int lTimeRows = 1;
    const int lHarmCoeffCount = aHarmonics.size();
    const int lFreqRows = lTimeRows * lHarmCoeffCount;
    
    std::vector<CrsMatrixRow> lReturnVector(lFreqRows, CrsMatrixRow());
    
    const int lGlobalRowIndexTime = aJac.RowMap().MyGlobalElements()[aLocalMatrixRowIndex];
    const int lRowPhysicalDofId = cPhysicalDofs[aLocalMatrixRowIndex];
    
	CrsMatrixRow lTimeRowValues;
    GetMatrixRowValues(aJac, lGlobalRowIndexTime, lTimeRowValues);
	
	const int* const lTimeRowValuesIndices = lTimeRowValues.Data()->Indices();
	const double* const lTimeRowValuesValues = lTimeRowValues.Data()->Values();
    
    for (int iRowHarm = 0; iRowHarm < lHarmCoeffCount; iRowHarm++)
    {
        const int lRowIndexHBM = IndexStandardToHBM(aLocalMatrixRowIndex, iRowHarm, lHarmCoeffCount);
        const int lRowPhysicalDofId2 = cHBMDofs[lRowIndexHBM].PhysicalDofInfo.DofId;
        
        if (lRowPhysicalDofId != lRowPhysicalDofId2) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
        
        const double lRowWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iRowHarm]);
        
// 		std::cout << "Accessing element " << iRowHarm << " out of " << lReturnVector.size() << std::endl;
//         CrsMatrixRow& lResultRow = lReturnVector[lRowIndexHBM];
        CrsMatrixRow& lResultRow = lReturnVector[iRowHarm];
        
        lResultRow.Init(lTimeRowValues.Size() * lHarmCoeffCount);
        lResultRow.GlobalRowIndex = IndexStandardToHBM(lGlobalRowIndexTime, iRowHarm, lHarmCoeffCount);
        
        if (lResultRow.GlobalRowIndex != cHBMDofs[lRowIndexHBM].DofIdHBM) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
        
        for (int iColHarm = 0; iColHarm < lHarmCoeffCount; iColHarm++)
        {
            const double lColWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iColHarm]);
            
            for (int iColTime = 0; iColTime < lTimeRowValues.Size(); iColTime++)
            {
                const int lTimeColIndexGlobal = lTimeRowValuesIndices[iColTime];
                double lTimeValue = lTimeRowValuesValues[iColTime];
                
                int lHarmColIndexGlobal = IndexStandardToHBM(lTimeColIndexGlobal, iColHarm, lHarmCoeffCount);
                int lHarmColIndexLocal = IndexStandardToHBM(iColTime, iColHarm, lHarmCoeffCount);
                
                lResultRow.Data()->Indices()[lHarmColIndexLocal] = lHarmColIndexGlobal;
                lResultRow.Data()->Values()[lHarmColIndexLocal] = lTimeValue * lRowWaveValue * lColWaveValue / cTimePointCount;
            }
        }
    }
    
//     std::cout << "AFT of one row done" << std::endl;
// 	STOP
    
    return lReturnVector;
}
void Aft::TimeToFreq2(const Epetra_CrsMatrix& aJac, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex, int aLocalMatrixRowIndex, std::vector<CrsMatrixRow>& aResult) const
{
	if (aTimePointIndex < 0 || aTimePointIndex >= cTimePointCount) throw "Invalid time point index!";
    
//     int lTimeRows = aJac.RowMap().NumMyElements();
    int lTimeRows = 1;
    const int lHarmCoeffCount = aHarmonics.size();
//     const int lFreqRows = lTimeRows * lHarmCoeffCount;
	
	if (aResult.size() != lHarmCoeffCount) throw "Wrong size of the result vector!";
    
//     std::vector<CrsMatrixRow> lReturnVector(lFreqRows, CrsMatrixRow());
    
    const int lGlobalRowIndexTime = aJac.RowMap().MyGlobalElements()[aLocalMatrixRowIndex];
    const int lRowPhysicalDofId = cPhysicalDofs[aLocalMatrixRowIndex];
    
	CrsMatrixRow lTimeRowValues;
    GetMatrixRowValues(aJac, lGlobalRowIndexTime, lTimeRowValues);
	
	const int* const lTimeRowValuesIndices = lTimeRowValues.Data()->Indices();
	const double* const lTimeRowValuesValues = lTimeRowValues.Data()->Values();
		
    for (int iRowHarm = 0; iRowHarm < lHarmCoeffCount; iRowHarm++)
    {
        const int lRowIndexHBM = IndexStandardToHBM(aLocalMatrixRowIndex, iRowHarm, lHarmCoeffCount);
        const int lRowPhysicalDofId2 = cHBMDofs[lRowIndexHBM].PhysicalDofInfo.DofId;
        
        if (lRowPhysicalDofId != lRowPhysicalDofId2) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
        
        const double lRowWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iRowHarm]);
        
// 		std::cout << "Accessing element " << iRowHarm << " out of " << lReturnVector.size() << std::endl;
//         CrsMatrixRow& lResultRow = lReturnVector[lRowIndexHBM];
        CrsMatrixRow& lResultRow = aResult[iRowHarm];
		        
        lResultRow.Init(lTimeRowValues.Size() * lHarmCoeffCount);
        lResultRow.GlobalRowIndex = IndexStandardToHBM(lGlobalRowIndexTime, iRowHarm, lHarmCoeffCount);
        
        if (lResultRow.GlobalRowIndex != cHBMDofs[lRowIndexHBM].DofIdHBM) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
        
        for (int iColHarm = 0; iColHarm < lHarmCoeffCount; iColHarm++)
        {
            const double lColWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iColHarm]);
            
            for (int iColTime = 0; iColTime < lTimeRowValues.Size(); iColTime++)
            {
                const int lTimeColIndexGlobal = lTimeRowValuesIndices[iColTime];
                double lTimeValue = lTimeRowValuesValues[iColTime];
                
                int lHarmColIndexGlobal = IndexStandardToHBM(lTimeColIndexGlobal, iColHarm, lHarmCoeffCount);
                int lHarmColIndexLocal = IndexStandardToHBM(iColTime, iColHarm, lHarmCoeffCount);
                
                lResultRow.Data()->Indices()[lHarmColIndexLocal] = lHarmColIndexGlobal;
                lResultRow.Data()->Values()[lHarmColIndexLocal] = lTimeValue * lRowWaveValue * lColWaveValue / cTimePointCount;
            }
        }
    }
}

void Aft::TimeToFreqDirect(const Epetra_CrsMatrix& aJacTime, Epetra_CrsMatrix& aJacFreq, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex, int aLocalMatrixRowIndex)
{
	if (aTimePointIndex < 0 || aTimePointIndex >= cTimePointCount) throw "Invalid time point index!";
    
//     int lTimeRows = aJac.RowMap().NumMyElements();
//     int lTimeRows = 1;
    const int lHarmCoeffCount = aHarmonics.size();
//     const int lFreqRows = lTimeRows * lHarmCoeffCount;
	
// 	if (aResult.size() != lHarmCoeffCount) throw "Wrong size of the result vector!";
    
//     std::vector<CrsMatrixRow> lReturnVector(lFreqRows, CrsMatrixRow());
    
//     const int lGlobalRowIndexTime = aJacTime.RowMap().MyGlobalElements()[aLocalMatrixRowIndex];
    const int lRowPhysicalDofId = cPhysicalDofs[aLocalMatrixRowIndex];
    
// 	CrsMatrixRow lTimeRowValues;
//     GetMatrixRowValues(aJac, lGlobalRowIndexTime, lTimeRowValues);
	
// 	const int* const lTimeRowValuesIndices = lTimeRowValues.Data()->Indices();
// 	const double* const lTimeRowValuesValues = lTimeRowValues.Data()->Values();
    
    for (int iRowHarm = 0; iRowHarm < lHarmCoeffCount; iRowHarm++)
    {
        const int lRowIndexHBM = IndexStandardToHBM(aLocalMatrixRowIndex, iRowHarm, lHarmCoeffCount);
        const int lRowPhysicalDofId2 = cHBMDofs[lRowIndexHBM].PhysicalDofInfo.DofId;
        
        if (lRowPhysicalDofId != lRowPhysicalDofId2) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
        
        const double lRowWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iRowHarm]);
        
// 		std::cout << "Accessing element " << iRowHarm << " out of " << lReturnVector.size() << std::endl;
//         CrsMatrixRow& lResultRow = lReturnVector[lRowIndexHBM];
//         CrsMatrixRow& lResultRow = aResult[iRowHarm];
		        
//         lResultRow.Init(lTimeRowValues.Size() * lHarmCoeffCount);
//         lResultRow.GlobalRowIndex = IndexStandardToHBM(lGlobalRowIndexTime, iRowHarm, lHarmCoeffCount);

		
//         if (lResultRow.GlobalRowIndex != cHBMDofs[lRowIndexHBM].DofIdHBM) throw "Problem with dof indexing! Fix needed. Line: " + std::to_string(__LINE__);
        
		for (int iColTime = 0; iColTime < aJacTime.NumMyEntries(aLocalMatrixRowIndex); iColTime++)
		{
			const double lTimeValue = aJacTime[aLocalMatrixRowIndex][iColTime];
			
			for (int iColHarm = 0; iColHarm < lHarmCoeffCount; iColHarm++)
			{
				const double lColWaveValue = GetHarmonicValue(aTimePointIndex, aHarmonics[iColHarm]);
            
//                 const int lTimeColIndexGlobal = lTimeRowValuesIndices[iColTime];
                
//                 int lHarmColIndexGlobal = IndexStandardToHBM(lTimeColIndexGlobal, iColHarm, lHarmCoeffCount);
                int lHarmColIndexLocal = IndexStandardToHBM(iColTime, iColHarm, lHarmCoeffCount);
                
//                 lResultRow.Data()->Indices()[lHarmColIndexLocal] = lHarmColIndexGlobal;
                const double lFinalValue = lTimeValue * lRowWaveValue * lColWaveValue / cTimePointCount;
				aJacFreq[lRowIndexHBM][lHarmColIndexLocal] += lFinalValue;
            }
        }
    }
}

double Aft::GetHarmonicValue(int aTimePointIndex, const WaveInfo& aWaveInfo) const
{
    const int lWaveIndex = aWaveInfo.WaveIndex;
            
    double lWaveValue = 1.0;
    
    switch (aWaveInfo.Type)
    {
        case WaveType::Cos:
            lWaveValue = mCosValues.at((lWaveIndex * aTimePointIndex) % cTimePointCount);
            break;
        case WaveType::Sin:
            lWaveValue = mSinValues.at((lWaveIndex * aTimePointIndex) % cTimePointCount);
            break;
        case WaveType::DC:
            // no need to do anything, we have already set 1.0
            break;
        default:
            throw "Unknwon wave type!";
            break;
    }
    
    return lWaveValue;
}
// void Aft::AllocateBigPlan(int aMaxSize)
// {
//     std::string lErrorMessage = "Allocation already happened!";
//     if (aMaxSize <= 0) throw "Invalid max size! Must be a positive integer!";
//     if (mFftwBigTotalSize >= 0) throw lErrorMessage;
//     if (mTimeDomainFftwDataBig != nullptr) throw lErrorMessage;
//     if (mFreqDomainFftwDataBig != nullptr) throw lErrorMessage;
//     if (mIsBigPlanCreated) throw lErrorMessage;
//     if (aMaxSize % cTimePointCount != 0) throw "Invalid max size!";
//     
//     mFftwBigTotalSize = aMaxSize;
//     mTimeDomainFftwDataBig = new fftw_complex[aMaxSize];
//     mFreqDomainFftwDataBig = new fftw_complex[aMaxSize];
//     
//     for (int i = 0; i < aMaxSize; i++)
//     {
//         mTimeDomainFftwDataBig[i][0] = 0;
//         mTimeDomainFftwDataBig[i][1] = 0;
//         mFreqDomainFftwDataBig[i][0] = 0;
//         mFreqDomainFftwDataBig[i][1] = 0;
//     }
//     
//     int lMaxPhysicalDofs = aMaxSize / cTimePointCount;
//     
//     mTimeToFreqPlanBig = fftw_plan_many_dft(
//                 1, &cTimePointCount, lMaxPhysicalDofs,
//                 &mTimeDomainFftwDataBig[0], nullptr, 
//                 1, cTimePointCount,
//                 &mFreqDomainFftwDataBig[0], nullptr, 
//                 1, cTimePointCount, 
//                 FFTW_FORWARD, 0);
//     mIsBigPlanCreated = true;    
// }
std::vector<int> Aft::DofInfoToDofId(const std::vector<DofInfo>& aDofInfo) const
{
    std::vector<int> lReturnVector;
    lReturnVector.reserve(aDofInfo.size());
    
    for (int i = 0; i < aDofInfo.size(); i++)
    {
        lReturnVector.push_back(aDofInfo[i].DofId);
    }
    
    return lReturnVector;
}
