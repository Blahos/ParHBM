
#include "NonlinearStaticProblem.h"
#include "Functions.h"

NonlinearStaticProblem::NonlinearStaticProblem(FE* const aFE, const PhysicalParameters& aParams, const std::function<Epetra_SerialDenseVector(double, double, double)>& aBodyForce, const Epetra_Comm& aComm, double aBodyForceScale)
: cFE(aFE), cParams(aParams), cBodyForce(aBodyForce), cTrilinosComm(aComm), cBodyForceScale(aBodyForceScale)
{
    
}

const Epetra_Map& NonlinearStaticProblem::GetRowMap() const
{
    return cFE->GetRowMap();
}


bool NonlinearStaticProblem::computeF(const Epetra_Vector& aX, Epetra_Vector& aF, NOX::Epetra::Interface::Required::FillType aFillType)
{
    if (mK.is_null()) mK = cFE->CreateStiffnessMatrix(cParams);
    if (mF.is_null()) 
    {
        mF = cFE->CreateRHS(cParams, cBodyForce);
        // HACK
        std::vector<DofInfo> lDofInfo = cFE->GetDofInfo();
//         AddNodalForce(*(mF.get()), 0., 0., 0.5, 0, cBodyForceScale * (-1e5), lDofInfo, cTrilinosComm);
        AddNodalForce(*(mF.get()), 0., 0., 0.5, 0, cBodyForceScale * (-1e5), lDofInfo, cTrilinosComm);
//         getchar();
//         cTrilinosComm.Barrier();
    }
    
    
    Teuchos::RCP<Epetra_MultiVector> lNonlinRHS = cFE->CreateNonlinearStiffnessRHS(aX, cParams);
    
//     std::cout << "Original F: " << std::endl;
//     std::cout << aF << std::endl;
    
//     std::cout << "ComputeF multiplication" << std::endl;
    mK->Multiply(false, aX, aF);
    
//     std::cout << "F after multiplication: " << std::endl;
//     std::cout << aF << std::endl;
    
//     std::cout << "ComputeF adding the nonlinear part ..." << std::endl;
    for (int i = 0; i < aF.Map().NumMyElements(); i++)
    {
        double lValue1 = (*lNonlinRHS)[0][i];
//         double lValue1 = 0;
        double lValue2 = (*mF)[0][i];
//         std::cout << "Added nonlinear value: " << lValue1 << " - " << lValue2 << " at position " << i << std::endl;
        aF[i] += lValue1 - lValue2;
    }
//     std::cout << "ComputeF adding the nonlinear part ... finished" << std::endl;
    
//     std::cout << "F at the end: " << std::endl;
//     std::cout << aF << std::endl;
    
    return true;
}
bool NonlinearStaticProblem::computeJacobian(const Epetra_Vector& aX, Epetra_Operator& aJac)
{
    std::cout << "Computing jacobian" << std::endl;
    if (mK.is_null()) mK = cFE->CreateStiffnessMatrix(cParams);
    
    Teuchos::RCP<Epetra_FECrsMatrix> lNonlinK = cFE->CreateNonlinearStiffnessMatrix(aX, cParams);
    
    // these two following maps should be the same, but just for sake of correctness
    const Epetra_Map& lKRowMap = mK->RowMap();
    const Epetra_Map& lNonlinKRowMap = lNonlinK->RowMap();
    
    Epetra_CrsMatrix lJac(Epetra_DataAccess::Copy, lKRowMap, 0);
            
    // get maximum number of values in a row
    
    int lMaxValsPerRow = 0;
    for (int iRow = 0; iRow < mK->NumMyRows(); iRow++)
    {
        int lValuesCount = mK->NumMyEntries(iRow);
        
        if (lValuesCount > lMaxValsPerRow)
            lMaxValsPerRow = lValuesCount;
    }
    for (int iRow = 0; iRow < lNonlinK->NumMyRows(); iRow++)
    {
        int lValuesCount = lNonlinK->NumMyEntries(iRow);
        
        if (lValuesCount > lMaxValsPerRow)
            lMaxValsPerRow = lValuesCount;
    }

    // help arrays/values
    int* lIndices = new int[lMaxValsPerRow];
    double* lValues = new double[lMaxValsPerRow];
    int lExtractedCount;
    
    for (int iRow = 0; iRow < mK->NumMyRows(); iRow++)
    {
        int lRowGlobalIndex = lKRowMap.GID(iRow);
                
        // this could be potentially replaced by "extract view" for better performance
        mK->ExtractMyRowCopy(iRow, lMaxValsPerRow, lExtractedCount, lValues, lIndices);
        
        lJac.InsertGlobalValues(lRowGlobalIndex, lExtractedCount, lValues, lIndices);
    }
    
    for (int iRow = 0; iRow < lNonlinK->NumMyRows(); iRow++)
    {
        int lRowGlobalIndex = lNonlinKRowMap.GID(iRow);
                
        // this could be potentially replaced by "extract view" for better performance
        lNonlinK->ExtractMyRowCopy(iRow, lMaxValsPerRow, lExtractedCount, lValues, lIndices);
        
        lJac.InsertGlobalValues(lRowGlobalIndex, lExtractedCount, lValues, lIndices);
    }
    
    delete[] lIndices;
    delete[] lValues;
    
    int lResult = lJac.FillComplete();
        
    aJac = lJac;
    
    return true;
}
