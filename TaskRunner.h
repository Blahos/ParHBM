
class TaskRunner;
class HBMProblemInterface;

#pragma once
#include "Teuchos_ParameterList.hpp"

#include "Config.h"
#include "FE.h"
#include "PointForceDef.h"
#include "LinearSystem.h"
#include "HBMSettings.h"
#include "PhysicalParameters.h"

class TaskRunner
{
private:
    const std::string cInterruptionFileName = "delete_to_interrupt";
    // should be destroyed before any finalisation calls (like PETSc finalise and such)
    // const in terms of the pointer, not the field itself
    FE* const cFE;
    const Config cConfig;
    const Epetra_Comm* const cTrilinosComm;
    const std::string cOutputPath;
    
public:
    TaskRunner(const Config& aConfig, const libMesh::Parallel::Communicator& aLibmeshComm, const Epetra_Comm& aTrilinosComm, const std::string& aOutputPath);
    ~TaskRunner();
    
    void RunContinuation();
	void RunContinuation2();
	void RunFrequencySet();
    void RunEigenfrequencies();
	void RunMatrixOutput();
    
private:
	LinearSystemMatrices CreateLinearSystemMatrices();
    LinearSystem CreateLinearSystem(const std::vector<PointForceDef>& aForces, const HBMSettings& aHBMSettings);
	// Creates the physical parameters structure based on the values provided in the configuration, scales it by the unit scaling factors (also provided in the configuration)
	PhysicalParameters CreateScaledParams();
    HBMSettings CreateHBMSettings();
    Teuchos::RCP<Teuchos::ParameterList> CreateContinuationParams();
    void AddNodalForces(LinearSystem& aTargetLinearSystem, const std::vector<PointForceDef>& aForces, const HBMSettings& aHBMSettings, const PhysicalParameters& aParams);
    std::vector<PointForceDef> LoadHBMExcitation();
    void SetUpInterruptionFile();
    void RemoveInterruptionFile();
	
};
