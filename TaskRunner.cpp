
#include <fstream>
#include <unistd.h>

// #include "Ifpack_Hypre.h"
#include "NOX_Epetra_LinearSystem_AztecOO.H"
#include "NOX_Epetra_LinearSystem_Amesos.H"
#include "LOCA_Epetra_Group.H"
#include "Amesos.h"
#include "AnasaziEigenproblem.hpp"
#include "AnasaziBasicEigenproblem.hpp"
#include "AnasaziBlockKrylovSchur.hpp"
#include "AnasaziBlockKrylovSchurSolMgr.hpp"
#include "AnasaziBlockDavidson.hpp"
#include "AnasaziBlockDavidsonSolMgr.hpp"
#include "AnasaziLOBPCG.hpp"
#include "AnasaziLOBPCGSolMgr.hpp"
#include "AnasaziEpetraAdapter.hpp"

#include "TaskRunner.h"
#include "CrsMatrixRow.h"
#include "Functions.h"
#include "LinearSystem.h"
#include "HBMSettings.h"
#include "HBMProblemInterface.h"
#include "Time.h"
#include "AmesosGenOp.h"
#include "Interruption.h"

#include "Solvers/LinearSolver.h"
#include "Directory.h"
#include "Continuation/Continuation.h"
#include "Solvers/NewtonSolver.h"
#include "Solvers/LinearSolverFactory.h"

TaskRunner::TaskRunner(const Config& aConfig, const libMesh::Parallel::Communicator& aLibmeshComm, const Epetra_Comm& aTrilinosComm, const std::string& aOutputPath)
: cFE(new FE(aConfig.ScaleL, aConfig.QuadratureOrder)), cConfig(aConfig), cTrilinosComm(&aTrilinosComm), cOutputPath(aOutputPath)
{
    if (IsAbsolutePath(aConfig.MeshFile)) cFE->LoadMesh(aConfig.MeshFile, aLibmeshComm, aTrilinosComm);
    else cFE->LoadMesh(aConfig.ConfigFilePath + "/" + aConfig.MeshFile, aLibmeshComm, aTrilinosComm);
	
    cFE->SetTestValue(cConfig.TestValue);
}

TaskRunner::~TaskRunner()
{
    delete cFE;
}

void TaskRunner::RunContinuation()
{
    std::cout << "Solving continuation problem ..." << std::endl;
	
	std::string lTaskOutputPath = cOutputPath + "/Continuation/";
	CreateDirectories(lTaskOutputPath, MPI_COMM_WORLD);
	
    std::string lOutputFileName = "solutions";
	std::string lDofInfoFileName = "dof_info";
    
    // Load the excitation file
    std::vector<PointForceDef> lExcitation = LoadHBMExcitation();
    
    //////
    // HBM
    //////
    
    HBMSettings lHBMSettings = CreateHBMSettings();
    
    //////////////////
    // FINITE ELEMENTS
    //////////////////
    
    LinearSystem lLinearSystem = CreateLinearSystem(lExcitation, lHBMSettings);
    
    ////////////////////
    // PROBLEM INTERFACE
    ////////////////////
    
    std::cout << "Creating the problem interface ..." << std::endl;
    
    Teuchos::RCP<HBMProblemInterface> lProblemInterface = Teuchos::rcp(new HBMProblemInterface(lLinearSystem, *cFE, cConfig.UseGreen, lHBMSettings, *cTrilinosComm));
    
    std::cout << "Problem interface created" << std::endl;
    
    ///////////////
    // CONTINUATION
    ///////////////
    
    Teuchos::RCP<Teuchos::ParameterList> lParamList = CreateContinuationParams();
    Teuchos::ParameterList& lNonlinearList = lParamList->sublist("NOX");
    Teuchos::ParameterList& lNonlinearPrintList = lNonlinearList.sublist("Printing");
    Teuchos::ParameterList& lLsParams = lNonlinearList.sublist("Direction").sublist("Newton").sublist("Linear Solver");
    
    ///////////////////
    // PROBLEM ASSEMBLY
    ///////////////////
    
    std::cout << "Problem parameters set up, preparing the jacobian matrix ..." << std::endl;
    
    // Add export ranges
//     lProblemInterface->AddExportInfo(1075 / cFreqScale, 1080 / cFreqScale, Multiplicity::Once);

    Teuchos::RCP<Epetra_CrsMatrix> lJac = lProblemInterface->PrepareJacobiMatrix();

    std::cout << "The jacobian matrix ready" << std::endl;
//     getchar();

    Epetra_Vector lInitGuess(lJac->RowMap());
//     lInitGuess.PutScalar(1.0);

    NOX::Epetra::Vector lInitGuessNOX(lInitGuess, NOX::DeepCopy);

    // Create global data object
    Teuchos::RCP<LOCA::GlobalData> lGlobalData =
        LOCA::createGlobalData(lParamList);

    Teuchos::RCP<LOCA::Epetra::Interface::Required> lIRequired = lProblemInterface;
    Teuchos::RCP<NOX::Epetra::Interface::Jacobian>  lIJacobian = lProblemInterface;

    Teuchos::RCP<NOX::Epetra::LinearSystem> lLinSys;
    if (GetSolverType(cConfig.Solver) == ParHBM::SolverType::Iterative)
    {
        lLinSys = Teuchos::rcp(
            new NOX::Epetra::LinearSystemAztecOO(
                lNonlinearList, lLsParams, lIRequired, lIJacobian, lJac, lInitGuessNOX));
    }
    else if (GetSolverType(cConfig.Solver) == ParHBM::SolverType::Direct)
    {
        lLinSys = Teuchos::rcp(
            new NOX::Epetra::LinearSystemAmesos(
                lNonlinearList, lLsParams, lIRequired, lIJacobian, lJac, lInitGuessNOX));
    }
    else throw "Unknown solver type!";
    
    // Parameter list for continuation parameter
    LOCA::ParameterVector lParamVectorCont;
    lParamVectorCont.addParameter(HBMProblemInterface::cContParamName, 0.0);

    Teuchos::RCP<LOCA::Epetra::Group> lGroup =
        Teuchos::rcp(new LOCA::Epetra::Group(lGlobalData, lNonlinearPrintList, lIRequired, lInitGuessNOX, lLinSys, lParamVectorCont));
    
    // Set up the status tests
    Teuchos::RCP<NOX::StatusTest::NormF> normF =
        Teuchos::rcp(new NOX::StatusTest::NormF(cConfig.NewtonNormF));
    Teuchos::RCP<NOX::StatusTest::MaxIters> maxIters =
        Teuchos::rcp(new NOX::StatusTest::MaxIters(cConfig.MaxStepsNewton));
    Teuchos::RCP<NOX::StatusTest::Generic> lComboOR =
        Teuchos::rcp(new NOX::StatusTest::Combo(NOX::StatusTest::Combo::OR, normF, maxIters));

    std::cout << "Writing the system info into files ... " << std::endl;
    lProblemInterface->SaveDofInfo(lTaskOutputPath + "/" + lDofInfoFileName, cConfig.ScaleL);
    std::cout << "Writing the system info into files ... done" << std::endl;
    
    LOCA::Stepper lStepper(lGlobalData, lGroup, lComboOR, lParamList);
    
//     std::cout << "Press Enter to run the stepper" << std::endl;
//     getchar();
//     aTrilinosComm.Barrier();
    
    cTrilinosComm->Barrier();
    std::cout << "Continuation set up finished, running the stepper ... " << std::endl;
    
    SetUpInterruptionFile();
    lProblemInterface->SetInterruptionFile(cInterruptionFileName);
    
    ///////////////////////////
    // CONTINUATION STEPPER RUN
    ///////////////////////////
    
    Time lStepperTime;
    lStepperTime.Start();
    
    try
    {
        printf("Stepper started in process %d\n", cTrilinosComm->MyPID());
        LOCA::Abstract::Iterator::IteratorStatus lStatus = lStepper.run();
    }
    catch (Interruption& aInt)
    {
        std::cout << "The stepper was interrupted by the standard interruption procedure." << std::endl;
    }
    
    RemoveInterruptionFile();
    
    double lElapsedStepper = lStepperTime.Stop();
    
    std::cout << "Stepper finished, elapsed time: " << lElapsedStepper << " ms" << std::endl;
    
    std::cout << "Stepper finished" << std::endl;
    
    std::cout << "Saving solutions into a file ..." << std::endl;
    
    lProblemInterface->SaveSolutions(lTaskOutputPath + "/" + lOutputFileName, cConfig.ScaleL, cConfig.ScaleT);
    
    std::cout << "Saving solutions into a file ... done" << std::endl;
    
//     getchar();
    
    LOCA::destroyGlobalData(lGlobalData);
    
    std::cout << "Solving continuation problem ... done" << std::endl;
}
void TaskRunner::RunContinuation2()
{
    std::cout << "Solving continuation problem ..." << std::endl;
	
	std::string lTaskOutputPath = cOutputPath + "/Continuation/";
	CreateDirectories(lTaskOutputPath, MPI_COMM_WORLD);
	
    std::string lOutputFileName = "solutions";
	std::string lDofInfoFileName = "dof_info";
    
    // Load the excitation file
    std::vector<PointForceDef> lExcitation = LoadHBMExcitation();
    
    //////
    // HBM
    //////
    
    HBMSettings lHBMSettings = CreateHBMSettings();
    
    //////////////////
    // FINITE ELEMENTS
    //////////////////
    
    LinearSystem lLinearSystem = CreateLinearSystem(lExcitation, lHBMSettings);
    
    ////////////////////
    // PROBLEM INTERFACE
    ////////////////////
    
    std::cout << "Creating the problem interface ..." << std::endl;
    
    Teuchos::RCP<HBMProblemInterface> lProblemInterface = Teuchos::rcp(new HBMProblemInterface(lLinearSystem, *cFE, cConfig.UseGreen, lHBMSettings, *cTrilinosComm));
    
    std::cout << "Problem interface created" << std::endl;
    
    ///////////////
    // CONTINUATION
    ///////////////

	Continuation lContinuation(cConfig);
    
    std::cout << "Solving continuation problem ... done" << std::endl;
    
}

void TaskRunner::RunFrequencySet()
{
    std::cout << "Solving problem for set of frequencies ..." << std::endl;
	
	std::string lTaskOutputPath = cOutputPath + "/FrequencySet/";
	CreateDirectories(lTaskOutputPath, MPI_COMM_WORLD);
    std::string lOutputFileName = "solutions";
	std::string lDofInfoFileName = "dof_info";
	
    // Load the excitation file
    std::vector<PointForceDef> lExcitation = LoadHBMExcitation();
	
    //////
    // HBM
    //////
    
    HBMSettings lHBMSettings = CreateHBMSettings();
    
    //////////////////
    // FINITE ELEMENTS
    //////////////////
    
    LinearSystem lLinearSystem = CreateLinearSystem(lExcitation, lHBMSettings);
	
    ////////////////////
    // PROBLEM INTERFACE
    ////////////////////
	
    std::cout << "Creating the problem interface ..." << std::endl;
    
    Teuchos::RCP<HBMProblemInterface> lProblemInterface = Teuchos::rcp(new HBMProblemInterface(lLinearSystem, *cFE, cConfig.UseGreen, lHBMSettings, *cTrilinosComm));
    
    std::cout << "Problem interface created" << std::endl;
	
	//////////////////
	// WRITE DOF INFOS
	//////////////////
	
    std::cout << "Writing the system info into files ... " << std::endl;
    lProblemInterface->SaveDofInfo(lTaskOutputPath + "/" + lDofInfoFileName, cConfig.ScaleL);
    std::cout << "Writing the system info into files ... done" << std::endl;
	
	///////////////////
    // PROBLEM ASSEMBLY
    ///////////////////
    
    std::cout << "Problem parameters set up, preparing the jacobian matrix ..." << std::endl;
    
    // Add export ranges
//     lProblemInterface->AddExportInfo(1075 / cFreqScale, 1080 / cFreqScale, Multiplicity::Once);

    Teuchos::RCP<Epetra_CrsMatrix> lJac = lProblemInterface->PrepareJacobiMatrix();
	
	int lSystemSize = lJac->RowMap().NumGlobalElements();
	int lLocalSize = lJac->RowMap().NumMyElements();

    std::cout << "The jacobian matrix ready" << std::endl;
	
	/////////////////
	// FREQUENCY LOOP
	/////////////////
	
	std::vector<double> lFrequencies;
	std::vector<Epetra_Vector> lSolutions;
	
	Epetra_Vector lSolutionGuess(lJac->RowMap());
	bool lPreviousConverged = false;
	
	//////////////////////
	// Solvers preparation
	//////////////////////
	// create a specific solver here
	LinearSolver* lSystemSolver = LinearSolverFactory::CreateSolver(cConfig.Solver);
// 	lSystemSolver = new LinearEqMKLPDSS();
	// init the solver with an arbitrary matrix and vector, just to give it the row distribution and matrix pattern
	// let's just hope that some solvers don't ignore zeros in matrices for instance when deducing their patterns
	lSystemSolver->Init(*(lJac.get()), lSolutionGuess);
	NewtonSolver lNewtonSolver(cConfig);
	
	for (int iFreq = 0; iFreq < cConfig.FrequencyList.size(); iFreq++)
	{
		if (!cConfig.UsePrevious || !lPreviousConverged)
			lSolutionGuess.PutScalar(0.0);
		
		lProblemInterface->SetFrequency(cConfig.FrequencyList[iFreq] * cConfig.ScaleT);
		
		if (lSystemSolver == nullptr) throw "No linear system solver created!";
		NewtonResult lNewtonResult = lNewtonSolver.Solve(lSystemSolver, lProblemInterface, *(lJac.get()), lSolutionGuess);
		
		if (lNewtonResult.Converged)
		{
			std::cout << "Newton converged for frequency " << cConfig.FrequencyList[iFreq] << " in " << lNewtonResult.IterationCount << " iterations." << std::endl;
			std::cout << "NormF = " << lNewtonResult.FinalNormF << " <= " << cConfig.NewtonNormF << std::endl;
			lPreviousConverged = true;
		}
		else
		{
			std::cout << "Newton failed to converge for frequency " << cConfig.FrequencyList[iFreq] << " in " << cConfig.MaxStepsNewton << " iterations." << std::endl;
			std::cout << "NormF = " << lNewtonResult.FinalNormF << " > " << cConfig.NewtonNormF << std::endl;
			lPreviousConverged = false;
		}
		
		// save the solution regardless of convergence
		// we save the frequency in the scaled format because that how it's supposed to be handled in the code
		// everywhere in the code we manipulate with the scaled values, only for output they get scaled back to the real units
		lFrequencies.push_back(cConfig.FrequencyList[iFreq] * cConfig.ScaleT);
// 		std::cout << "New freqeuncy saved" << std::endl;
		lSolutions.push_back(lSolutionGuess);
// 		std::cout << "New solution saved" << std::endl;
	}
	
	if (lSystemSolver != nullptr) delete lSystemSolver;
	
	SaveHBMSolutions(lFrequencies, lSolutions, lTaskOutputPath + "/" + lOutputFileName, cConfig.ScaleT, cConfig.ScaleL, *cTrilinosComm);
	
    std::cout << "Solving problem for set of frequencies ... done" << std::endl;
}

void TaskRunner::RunEigenfrequencies()
{
    if (cConfig.EigenfreqNum <= 0) 
    {
        std::cout << "No eigenfrequencies required, returning immediately!" << std::endl;
        return;
    }
    
    LinearSystemMatrices lMatrices = CreateLinearSystemMatrices();
    
    /////////////////////
    // Eigenvalue problem
    /////////////////////
    std::cout << "Passed the barrier, entering the eigenvalue part ... " << std::endl;
    
    // Clarification: We are looking for the first N natural frequencies, i.e. for the first N smallest 
    // eigenvalues of the matrix inv(M)*K. In the code, we compute the N largest eigenvalues of the inv(K)*M
    // and then take their inverse values.
                    
    Teuchos::RCP<Epetra_Vector> lInitGuessPtr = Teuchos::rcp(new Epetra_Vector(lMatrices.K->RowMap()));
    
    lInitGuessPtr->Random();
    
//         std::cout << "Stiffness copy: " << std::endl;
//         std::cout << *lKPtr << std::endl;
//         std::cout << "Mass copy: " << std::endl;
//         std::cout << *lMPtr << std::endl;
    
    // Create linear solver for linear systems with K
    //
    // Anasazi uses shift and invert, with a "shift" of zero, to find
    // the eigenvalues of least magnitude.  In this example, we
    // implement the "invert" part of shift and invert by using an
    // Amesos direct solver.
    //
    // Create Epetra linear problem class for solving linear systems
    // with K.  This implements the inverse operator for shift and
    // invert.
    Epetra_LinearProblem lAmesosProblem;
    // Tell the linear problem about the matrix K.  Epetra_LinearProblem
    // doesn't know about RCP, so we have to give it a raw pointer.
    lAmesosProblem.SetOperator(lMatrices.K.get());
    
    // Create Amesos factory and solver for solving linear systems with
    // K.  The solver uses the KLU library to do a sparse LU
    // factorization.
    //
    // Note that the AmesosProblem object "absorbs" K.  Anasazi doesn't
    // see K, just the operator that implements K^{-1} M.
    Amesos lAmesosFactory;
    Teuchos::RCP<Amesos_BaseSolver> lAmesosSolver;
    
    std::string lSolverName = "Klu";
    lAmesosSolver = Teuchos::rcp(lAmesosFactory.Create(lSolverName, lAmesosProblem));
    
    if (lAmesosSolver.is_null()) 
    {
        throw "Amesos appears not to have the " + lSolverName + " solver enabled!";
    }
    else
    {
        std::cout << "Amesos solver set up" << std::endl;
    }

    
    // The AmesosGenOp class assumes that the symbolic and numeric
    // factorizations have already been performed on the linear problem.
    lAmesosSolver->SymbolicFactorization();
    lAmesosSolver->NumericFactorization();
    
    double lTol = 1.0e-8; // convergence tolerance
    int lEigCount = cConfig.EigenfreqNum; // number of eigenvalues for which to solve
    int lBlockSize = 1; // block size (number of eigenvectors processed at once)
    int lNumBlocks = 8; // restart length
    int lMaxRestarts = 100; // maximum number of restart cycles
    std::string lWhich = "LM"; // largest magnitude
//         std::string lWhich = "SM"; // smallest magnitude
//         Errors = 0,                 /*!< Errors [ always printed ] */
//         Warnings = 0x1,             /*!< Internal warnings */
//         IterationDetails = 0x2,     /*!< Approximate eigenvalues, errors */
//         OrthoDetails = 0x4,         /*!< Orthogonalization/orthonormalization details */
//         FinalSummary = 0x8,         /*!< Final computational summary */
//         TimingDetails = 0x10,       /*!< Timing details, uses MPI_COMM_WORLD by default. */
//         StatusTestDetails = 0x20,   /*!< Status test details */
//         Debug = 0x40                /*!< Debugging information */

    std::cout << "Number of required eigenvalues: " << lEigCount << std::endl;
    
//     int lVerbosity = Anasazi::Errors + Anasazi::Warnings + Anasazi::IterationDetails + Anasazi::OrthoDetails + Anasazi::StatusTestDetails + Anasazi::FinalSummary;
    int lVerbosity = Anasazi::FinalSummary;
    
    Teuchos::ParameterList lEigParams;
    lEigParams.set ("Verbosity", lVerbosity);
    lEigParams.set ("Which", lWhich);
//     lEigParams.set ("Block Size", lBlockSize);
//     lEigParams.set ("Num Blocks", lNumBlocks);
//     lEigParams.set ("Maximum Restarts", lMaxRestarts);
    lEigParams.set ("Convergence Tolerance", lTol);
    
    std::cout << "Eigenvalue problem parameters set up" << std::endl;
    
    // Create the Epetra_Operator for the spectral transformation using
    // the Amesos direct solver.
    //
    // The AmesosGenOp object is the operator we give to Anasazi.  Thus,
    // Anasazi just sees an operator that computes y = K^{-1} M x.  The
    // matrix K got absorbed into AmesosProblem (the
    // Epetra_LinearProblem object).  Later, when we set up the Anasazi
    // eigensolver, we will need to tell it about M, so that it can
    // orthogonalize basis vectors with respect to the inner product
    // defined by M (since it is symmetric positive definite).
    Teuchos::RCP<AmesosGenOp> lAOp = Teuchos::rcp(new AmesosGenOp(lAmesosSolver, lMatrices.M));
    
    Teuchos::RCP<Anasazi::BasicEigenproblem<double, Epetra_MultiVector, Epetra_Operator>> lEigProblem =
        Teuchos::rcp(new Anasazi::BasicEigenproblem<double, Epetra_MultiVector, Epetra_Operator>(lAOp, lMatrices.M, lInitGuessPtr));
        
    lEigProblem->setHermitian(true);
    lEigProblem->setNEV(lEigCount);
    lEigProblem->setProblem();
    
    std::cout << "Eigenvalue problem set up" << std::endl;
    
    // Create the Block Krylov-Schur eigensolver.
    Anasazi::BlockKrylovSchurSolMgr<double, Epetra_MultiVector, Epetra_Operator> lEigSolver(lEigProblem, lEigParams);
    
    // Create the Block Davidson eigensolver.
//         Anasazi::BlockDavidsonSolMgr<double, Epetra_MultiVector, Epetra_Operator> lEigSolver(lEigProblem, lEigParams);
    
    // Create the LOBPCG eigensolver.
//         Anasazi::LOBPCGSolMgr<double, Epetra_MultiVector, Epetra_Operator> lEigSolver(lEigProblem, lEigParams);
    
    std::cout << "Eigenvalue solver set up, solving ... " << std::endl;
    
    Anasazi::ReturnType lEigReturnCode = lEigSolver.solve();
    if (lEigReturnCode != Anasazi::Converged) 
    {
        std::cout << "Anasazi eigensolver did not converge." << std::endl;
    }
    else
    {
        std::cout << "Anasazi eigensolver did converge." << std::endl;
    }
    
    Anasazi::Eigensolution<double, Epetra_MultiVector> lEigSol = lEigProblem->getSolution();
    
    std::vector<Anasazi::Value<double>> lEigValues = lEigSol.Evals;
    
    std::cout << std::endl;
    std::cout << BORDER << std::endl;
    std::cout << "Computed eigenfrequencies: " << std::endl;
    std::cout << BORDER << std::endl;
    for (int i = 0; i < lEigValues.size(); i++)
    {
//         std::cout << "Real part:         " << lEigValues[i].realpart << std::endl;
//         std::cout << "Real part inverse: " << 1.0 / lEigValues[i].realpart << std::endl;
//         std::cout << "Imaginary part:    " << lEigValues[i].imagpart << std::endl;
        
//         std::cout << (i + 1) << ". natural frequency (scaled): " << 1.0 / sqrt(lEigValues[i].realpart) << std::endl;
        std::cout << std::setw(5);
        std::cout << (i + 1) << ": " << 1.0 / sqrt(lEigValues[i].realpart) / cConfig.ScaleT << std::endl;
    }
    std::cout << BORDER << std::endl;
}

void TaskRunner::RunMatrixOutput()
{
    std::cout << "Outputting linear system matrices ..." << std::endl;
	
	std::string lTaskOutputPath = cOutputPath + "/Matrices/";
	CreateDirectories(lTaskOutputPath, MPI_COMM_WORLD);
	
	PhysicalParameters lParams = CreateScaledParams();
	
	std::vector<Epetra_SerialDenseMatrix>* lKLocalMatrices = nullptr;
	std::vector<Epetra_SerialDenseMatrix>* lMLocalMatrices = nullptr;
	
	if (cConfig.OutputLocalMatrices)
	{
		lKLocalMatrices = new std::vector<Epetra_SerialDenseMatrix>();
		lMLocalMatrices = new std::vector<Epetra_SerialDenseMatrix>();
	}
    
	Time lKTime;
	lKTime.Start();
    Teuchos::RCP<Epetra_CrsMatrix> lK = cFE->CreateStiffnessMatrix(lParams, lKLocalMatrices);
	double lKElapsed = lKTime.Stop();
    std::cout << "Stiffness matrix created, elapsed time: " << lKElapsed << "ms" << std::endl;
    
	Time lMTime;
	lMTime.Start();
    Teuchos::RCP<Epetra_CrsMatrix> lM = cFE->CreateMassMatrix(lParams, lMLocalMatrices);
	double lMElapsed = lMTime.Stop();
    std::cout << "Mass matrix created, elapsed time: " << lMElapsed << "ms" << std::endl;
	
	if (cConfig.OutputGlobalMatrices)
	{
		std::ofstream lKGlobalFile(lTaskOutputPath + "/GlobalK_proc" + std::to_string(cTrilinosComm->MyPID()) + ".txt");
		lKGlobalFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
		lKGlobalFile << *(lK.get());
		lKGlobalFile.close();
		
		std::cout << "Global stiffness matrix written into file." << std::endl;
		
		std::ofstream lMGlobalFile(lTaskOutputPath + "/GlobalM_proc" + std::to_string(cTrilinosComm->MyPID()) + ".txt");
		lMGlobalFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
		lMGlobalFile << *(lM.get());
		lMGlobalFile.close();
		
		std::cout << "Global mass matrix written into file." << std::endl;
	}
	
	if (cConfig.OutputLocalMatrices)
	{
		std::ofstream lKLocalFile(lTaskOutputPath + "/LocalK_proc" + std::to_string(cTrilinosComm->MyPID()) + ".txt");
		lKLocalFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
		
		for (int i = 0; i < lKLocalMatrices->size(); i++)
		{
			lKLocalFile << "Element " << i << std::endl;
			lKLocalFile << lKLocalMatrices->at(i) << std::endl;
		}
		
		lKLocalFile.close();
		
		std::cout << "Local stiffness matrices written into file." << std::endl;
		
		std::ofstream lMLocalFile(lTaskOutputPath + "/LocalM_proc" + std::to_string(cTrilinosComm->MyPID()) + ".txt");
		lMLocalFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
		
		for (int i = 0; i < lMLocalMatrices->size(); i++)
		{
			lMLocalFile << "Element " << i << std::endl;
			lMLocalFile << lMLocalMatrices->at(i) << std::endl;
		}
		
		lMLocalFile.close();
		
		std::cout << "Local mass matrices written into file." << std::endl;
	}
	
	if (lKLocalMatrices != nullptr) delete lKLocalMatrices;
	if (lMLocalMatrices != nullptr) delete lMLocalMatrices;
	
    std::cout << "Outputting linear system matrices ... done" << std::endl;
}

LinearSystemMatrices TaskRunner::CreateLinearSystemMatrices()
{
	const std::vector<DofInfo>& lDofInfo = cFE->GetDofInfo();
	
	PhysicalParameters lParams = CreateScaledParams();
    
    Teuchos::RCP<Epetra_CrsMatrix> lK = cFE->CreateStiffnessMatrix(lParams);
    
    std::cout << "Stiffness matrix created" << std::endl;
    
    Teuchos::RCP<Epetra_CrsMatrix> lM = cFE->CreateMassMatrix(lParams);
    
    std::cout << "Mass matrix created" << std::endl;
    
    Teuchos::RCP<Epetra_CrsMatrix> lD = Teuchos::rcp(new Epetra_CrsMatrix(Epetra_DataAccess::Copy, lK->RowMap(), 0));
    
	const bool lHasMDependency = cConfig.RayleighM != 0.0;
	const bool lHasKDependency = cConfig.RayleighK != 0.0;
	
    // fill the damping matrix
    for (int iDof = 0; iDof < lDofInfo.size(); iDof++)
    {
		// we don't want to add anything to the damping matrix for the dirichlet dofs
		// if we don̈́'t shortcut here, there could be 1 added from the stiffness matrix
		// column zeroing out should already be properly handled in assembly of K and M
		if (lDofInfo[iDof].IsDirichlet) continue;
		
        const int lGlobalRowIndex = lDofInfo[iDof].DofId;
		
		if (lHasMDependency) 
		{
			CrsMatrixRow lMRowData;
			GetMatrixRowValues(*lM, lGlobalRowIndex, lMRowData);
			lMRowData.Data()->ScaleValues(cConfig.RayleighM * cConfig.ScaleT);
        	lD->InsertGlobalValues(lGlobalRowIndex, lMRowData.Size(), lMRowData.Data()->Values(), lMRowData.Data()->Indices());
		}
		if (lHasKDependency)
		{
			CrsMatrixRow lKRowData;
			GetMatrixRowValues(*(lK.get()), lGlobalRowIndex, lKRowData);
			lKRowData.Data()->ScaleValues(cConfig.RayleighK * cConfig.ScaleT * cConfig.ScaleL);
			lD->InsertGlobalValues(lGlobalRowIndex, lKRowData.Size(), lKRowData.Data()->Values(), lKRowData.Data()->Indices());
		}
    }
    
    lD->FillComplete();
    
    std::cout << "Damping matrix created" << std::endl;
    
    LinearSystemMatrices lMatrices;
    
    lMatrices.M = lM;
    lMatrices.K = lK;
    lMatrices.D = lD;
    
    lMatrices.PhysicalParams = lParams;
	
	return lMatrices;
}
LinearSystem TaskRunner::CreateLinearSystem(const std::vector<PointForceDef>& aForces, const HBMSettings& aHBMSettings)
{
    LinearSystem lLinearSystem;
	
	lLinearSystem.Matrices = CreateLinearSystemMatrices();
    
    // Excitation
    AddNodalForces(lLinearSystem, aForces, aHBMSettings, lLinearSystem.PhysicalParams());
    
    return lLinearSystem;
}
PhysicalParameters TaskRunner::CreateScaledParams()
{
	PhysicalParameters lParams;
    
    lParams.Rho = cConfig.Rho * (cConfig.ScaleL * cConfig.ScaleL * cConfig.ScaleL / cConfig.ScaleW);
    lParams.nu = cConfig.Nu;
    lParams.E = cConfig.E * (cConfig.ScaleT * cConfig.ScaleT * cConfig.ScaleL / cConfig.ScaleW);
	
	return lParams;
}
HBMSettings TaskRunner::CreateHBMSettings()
{
    HBMSettings lHBMSettings;
    
    lHBMSettings.IntegrationPointCount = cConfig.IntPointCount;
    
    std::vector<int> lHarmonics;
    for (int i = 0; i < cConfig.HarmonicWaveList.size(); i++)
    {
        lHarmonics.push_back(cConfig.HarmonicWaveList[i]);
    }
    
    lHBMSettings.HarmonicInfo = CreateHarmonicInfo(lHarmonics);
    lHBMSettings.HarmonicWaves = lHarmonics;
    
    
    lHBMSettings.StartFrequency = cConfig.FrequencyStart * cConfig.ScaleT;
    lHBMSettings.EndFrequency = cConfig.FrequencyEnd * cConfig.ScaleT;
    
    std::cout << "Frequency range (scaled): <" << lHBMSettings.StartFrequency << "; " << lHBMSettings.EndFrequency << ">" << std::endl;
    std::cout << "Frequency range (not scaled): <" << lHBMSettings.StartFrequency / cConfig.ScaleT << "; " << lHBMSettings.EndFrequency / cConfig.ScaleT << ">" << std::endl;
    
    return lHBMSettings;
}
Teuchos::RCP<Teuchos::ParameterList> TaskRunner::CreateContinuationParams()
{
    Teuchos::RCP<Teuchos::ParameterList> lParamList = Teuchos::rcp(new Teuchos::ParameterList());
    
    // Create LOCA sublist
    Teuchos::ParameterList& lLocaParamList = lParamList->sublist("LOCA");

    // Create the stepper sublist and set the stepper parameters
    Teuchos::ParameterList& lStepperList = lLocaParamList.sublist("Stepper");
    lStepperList.set("Continuation Method", "Arc Length");// Default
//         lStepperList.set("Continuation Method", "Natural");
    lStepperList.set("Continuation Parameter", HBMProblemInterface::cContParamName);  // Must set
    lStepperList.set("Initial Value", 0.0); // Must set
    lStepperList.set("Min Value", 0.0);
    lStepperList.set("Max Value", 1.0);             // Must set
    lStepperList.set("Max Steps", cConfig.MaxStepsContinuation);                    // Should set
    
    lStepperList.set("Enable Arc Length Scaling", cConfig.EnableArcLengthScaling);        // Default
//     lStepperList.set("Initial Scale Factor", 1.e-4);        
//     lStepperList.set("Min Scale Factor", 1.e-4);        
//     lStepperList.set("Goal Arc Length Parameter Contribution", 1e-5);
//     lStepperList.set("Max Arc Length Parameter Contribution", 1e-4);
    
    lStepperList.set("Max Nonlinear Iterations", cConfig.MaxStepsNewton); // Should set
    lStepperList.set("Compute Eigenvalues", false);        // Default
    lStepperList.set("Enable Tangent Factor Step Size Scaling", false);        // Default
    lStepperList.set("Skip Parameter Derivative", false);        
//     lStepperList.set("Min Tangent Factor", 0.1);        // Default

    // Create predictor sublist
    Teuchos::ParameterList& lPredictorList = lLocaParamList.sublist("Predictor");
    lPredictorList.set("Method", cConfig.PredictorType);

    // Create step size sublist
    Teuchos::ParameterList& lStepSizeList = lLocaParamList.sublist("Step Size");
    lStepSizeList.set("Method", "Adaptive");
    lStepSizeList.set("Initial Step Size", cConfig.StepSizeInitial);   // Should set
    lStepSizeList.set("Min Step Size", cConfig.StepSizeMin);    // Should set
    lStepSizeList.set("Max Step Size", cConfig.StepSizeMax);

    // Create the "Solver" parameters sublist to be used with NOX Solvers
    Teuchos::ParameterList& lNonlinearList = lParamList->sublist("NOX");
    Teuchos::ParameterList& lNonlinearPrintList = lNonlinearList.sublist("Printing");
    lNonlinearPrintList.set("Output Information",
//             NOX::Utils::Debug +
            NOX::Utils::Details +
            NOX::Utils::OuterIteration +
            NOX::Utils::InnerIteration +
            NOX::Utils::Warning +
            NOX::Utils::StepperIteration +
            NOX::Utils::StepperDetails +
            NOX::Utils::StepperParameters +
            NOX::Utils::LinearSolverDetails +
                     0);  // Should set
    
    Teuchos::ParameterList& lSearchParams = lNonlinearList.sublist("Line Search");
    lSearchParams.set("Method", "Polynomial");
    
    // Parameters for picking the search direction.
    Teuchos::ParameterList& lDirParams = lNonlinearList.sublist("Direction");
    // Use Newton's method to pick the search direction.
    lDirParams.set("Method", "Newton");
    
    // Parameters for Newton's method.
    Teuchos::ParameterList& lNewtonParams = lDirParams.sublist("Newton");
    lNewtonParams.set("Forcing Term Method", "Constant");
    
    //
    // Newton's method invokes a linear solver repeatedly.
    // Set the parameters for the linear solver.
    //
    Teuchos::ParameterList& lLsParams = lNewtonParams.sublist("Linear Solver");
    
//     int lDofCount = lProblemInterface->DofCount();
    
	lLsParams.set("Amesos Solver", "Amesos_Klu");
//     if (cConfig.Solver == ParHBM::Solver::KLU)              lLsParams.set("Amesos Solver", "Amesos_Klu");
//     else if (cConfig.Solver == ParHBM::Solver::SuperLUDist) lLsParams.set("Amesos Solver", "Amesos_Superludist");
//     else if (cConfig.Solver == ParHBM::Solver::Pardiso)     lLsParams.set("Amesos Solver", "Amesos_Pardiso");
//     else if (cConfig.Solver == ParHBM::Solver::MUMPS)       lLsParams.set("Amesos Solver", "Amesos_Mumps");
//     else if (cConfig.Solver == ParHBM::Solver::GMRES)       lLsParams.set("Aztec Solver", "GMRES");
//     else if (cConfig.Solver == ParHBM::Solver::BiCGStab)    lLsParams.set("Aztec Solver", "BiCGStab");
//     else throw "Unknown solver!";
    
//     lLsParams.set("Amesos Solver", "Amesos_Klu");
//     lLsParams.set("Amesos Solver", "Amesos_Superludist");
//     lLsParams.set("Amesos Solver", "Amesos_Pardiso");
//     lLsParams.set("Preconditioner", "None");
    
//     lLsParams.set("amesos: solver type", "Paraklete");
//     lLsParams.set("Overlap", 1);
    lLsParams.set("Compute Scaling Manually", false);
    
//     lLsParams.set("Convergence Test", "r0");
    lLsParams.set("Convergence Test", "no scaling");
    lLsParams.set("Tolerance", 1e-12);
//     lLsParams.set("Aztec Solver", "GMRES");
    int lMaxIt = cFE->DofCount() * 2 * cConfig.HarmonicWaveCount;
    std::cout << "Linear solver max iterations: " << lMaxIt << std::endl;
    lLsParams.set("Max Iterations", lMaxIt);
    lLsParams.set("Output Frequency", 20);
//     lLsParams.set("Aztec Preconditioner", "ilu");
    
    if (GetSolverType(cConfig.Solver) == ParHBM::SolverType::Iterative)
    {
        lLsParams.set("Preconditioner", "New Ifpack");
        lLsParams.set("Preconditioner Operator", "Use Jacobian");
        lLsParams.set("Ifpack Preconditioner", cConfig.Preconditioner);
        
        Teuchos::ParameterList& lPrecondList = lLsParams.sublist("Ifpack");
        
//         lPrecondList.set("schwarz: reordering type", "rcm");
        
        if (cConfig.Preconditioner == "Amesos stand-alone")
        {
            lPrecondList.set("amesos: solver type", "Amesos_Klu");
//             lPrecondList.set("amesos: solver type", "Amesos_Paraklete");
//             lPrecondList.set("amesos: solver type", "Amesos_Pardiso");
            lPrecondList.set("Redistribute", false);
            
    //         Teuchos::ParameterList& lSuperlulist = lPrecondList.sublist("Superludist");
    //         lSuperlulist.set("Equil", true);
    //         lSuperlulist.set("ReuseSymbolic", true);
    //         lSuperlulist.set("IterRefine", "DOUBLE");
    //         lSuperlulist.set("ColPerm", "MMD_AT_PLUS_A");
    //         lSuperlulist.set("RowPerm", "NATURAL");
    //         lSuperlulist.set("IterRefine", "EXTRA");
        }
    }
    
    return lParamList;
}
void TaskRunner::AddNodalForces(LinearSystem& aTargetLinearSystem, const std::vector<PointForceDef>& aForces, const HBMSettings& aHBMSettings, const PhysicalParameters& aParams)
{
    std::vector<DofInfo> lDofInfo = cFE->GetDofInfo();
    
    double lT2ByLByW = cConfig.ScaleT * cConfig.ScaleT / cConfig.ScaleL / cConfig.ScaleW;
    std::cout << "Force scale multiplier: " << lT2ByLByW << std::endl;
        
    // zero body force
    std::function<Epetra_SerialDenseVector(double, double, double)> lZeroForceFunc = [](double aX, double aY, double aZ) 
    { 
        Epetra_SerialDenseVector lVec(3);
        return lVec;
    };
    
    int lMaxArrayIndexWithNonzeroForce = -1;
    std::vector<std::vector<Teuchos::RCP<Epetra_MultiVector>>> lForceVectors;
    for (int i = 0; i < aHBMSettings.HarmonicWaves.size(); i++)
    {
        std::vector<Teuchos::RCP<Epetra_MultiVector>> lForceVectorsForHarmonic;
        
        lForceVectorsForHarmonic.push_back(cFE->CreateRHS(aParams, lZeroForceFunc));
        
        if (aHBMSettings.HarmonicWaves[i] != 0) // add once more (the first one will be cos, this one sin)
            lForceVectorsForHarmonic.push_back(cFE->CreateRHS(aParams, lZeroForceFunc));
        
        lForceVectors.push_back(lForceVectorsForHarmonic);
    }
    
    for (int iExc = 0; iExc < aForces.size(); iExc++)
    {
        PointForceDef lForceDef = aForces[iExc];
        int lWaveIndex = lForceDef.WaveIndex;
        
        int lForceVectorArrayIndex = -1;
        for (int iWave = 0; iWave < aHBMSettings.HarmonicWaves.size(); iWave++)
        {
            if (lWaveIndex == aHBMSettings.HarmonicWaves[iWave])
            {
                lForceVectorArrayIndex = iWave;
                break;
            }
        }
        if (lForceVectorArrayIndex < 0) throw "Wave with index " + std::to_string(lWaveIndex) + " is not present in the simulation!";
        
        Teuchos::RCP<Epetra_MultiVector> lTargetForceVec;
        
        if (lForceDef.Type == WaveType::DC || lForceDef.Type == WaveType::Cos) 
            lTargetForceVec = lForceVectors[lForceVectorArrayIndex][0];
        else if (lForceVectors[lForceVectorArrayIndex].size() == 2)
            lTargetForceVec = lForceVectors[lForceVectorArrayIndex][1];
        else throw "Excitation force number " + std::to_string(iExc) + " can not be put into the system!";
        
        std::cout << "Adding point force on harmonic wave " << lForceDef.WaveIndex << ", component " << WaveTypeString(lForceDef.Type) << std::endl;
        
        // The desing of the code has changed, the mesh will not be scaled by the ScaleL factor, it will always be the same mesh
        // in its original size. Its "scaling" will be emulated inside the code (when assembling the system matrices)
        // Therefore the dof info will be in real coordinates and the force coordinates too, hence no need for any scaling when searching
        // for node by its position
        AddNodalForce(*(lTargetForceVec.get()), 
                      lForceDef.X, lForceDef.Y, lForceDef.Z,
                      lForceDef.Dim,
                      lForceDef.Amp * lT2ByLByW, 
                      lDofInfo, *cTrilinosComm);
        
//         AddNodalForce(*(lTargetForceVec.get()), 
//                       lForceDef.X / cConfig.ScaleL, lForceDef.Y / cConfig.ScaleL, lForceDef.Z / cConfig.ScaleL,
//                       lForceDef.Dim,
//                       lForceDef.Amp * lT2ByLByW, 
//                       lDofInfo, *cTrilinosComm);
        
        if (lMaxArrayIndexWithNonzeroForce < lForceVectorArrayIndex)
            lMaxArrayIndexWithNonzeroForce = lForceVectorArrayIndex;
    }
    
    for (int i = 0; i <= lMaxArrayIndexWithNonzeroForce; i++)
    {
        for (int j = 0; j < lForceVectors[i].size(); j++)
            aTargetLinearSystem.F.push_back(lForceVectors[i][j]);
    }
}
std::vector<PointForceDef> TaskRunner::LoadHBMExcitation()
{
	std::vector<PointForceDef> lExcitation;
	if (cConfig.ExcitationForceFile.length() > 0)
	{
		lExcitation = ReadExcitation(cConfig.ConfigFilePath + "/" + cConfig.ExcitationForceFile);
		if (lExcitation.size() == 0) std::cout << "Warning: no excitation specified in file \"" << cConfig.ExcitationForceFile << "\"!" << std::endl;
		else std::cout << lExcitation.size() << " point force(s) loaded as excitation. " << std::endl;
	}
	else std::cout << "No excitation definition file provided, excitation forces will be zero!";
    
    return lExcitation;
}
void TaskRunner::SetUpInterruptionFile()
{
//     cTrilinosComm->Barrier();
    printf("Process %d set up start\n", cTrilinosComm->MyPID());
    if (cTrilinosComm->MyPID() == 0)
    {
        std::ofstream lFile(cInterruptionFileName);
        
        if (!lFile.good()) std::cout << "Warning! Could not create the interruption file!" << std::endl;
        else lFile.close();
    }
    printf("Process %d set up finished\n", cTrilinosComm->MyPID());
    // This barrier does not need to be here but it's better, and it doesn't hurt anything.
    // Check for the interruption file existence is also done on rank 0, that's why this doesn't have to be here
    // But if it was ever done on another rank, this could matter 
    // (another rank would throw exception for this file missing before it is created 
    // at this rank)
    cTrilinosComm->Barrier();
}
void TaskRunner::RemoveInterruptionFile()
{
    if (cTrilinosComm->MyPID() == 0)
    {
        if (FileExists(cInterruptionFileName))
        {
            std::remove(cInterruptionFileName.c_str());
        }
    }
}

