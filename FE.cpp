
#include <algorithm>

#include "Epetra_Comm.h"

#include "libmesh/mesh_tools.h"

#include "FE.h"
#include "Time.h"
#include "Misc.h"

FE::FE(double aLengthScale, int aQuadratureOrder) : 
    cLengthScale(aLengthScale), mQRule(3, libMesh::OrderWrapper(CheckQOrderValidity(aQuadratureOrder)))
{
	
}

FE::~FE()
{    
    if (mStandardDofMap != nullptr)
        delete mStandardDofMap;
    
    if (mStandardDofMapOverlapping != nullptr)
        delete mStandardDofMapOverlapping;
    
    if (mEquationSystems != nullptr)
        delete mEquationSystems;
    
//     std::cout << "The FE destructor finished" << std::endl;
}

void FE::LoadMesh(const std::string& aFileName, const libMesh::Parallel::Communicator& aLibmeshComm, const Epetra_Comm& aEpetraComm)
{
    ///////////////////
    // Prepare the mesh
    ///////////////////
    if (mMeshLoaded) throw "Mesh already loaded!";
    
    std::cout << "Loading mesh from the file: " << aFileName << std::endl;
    
    mMesh = libMesh::UniquePtr<libMesh::DistributedMesh>(new libMesh::DistributedMesh(aLibmeshComm));
    
    mMesh->read(aFileName);
    
    mMesh->prepare_for_use();
    
    mMesh->print_info();
	
	if (mMesh->local_elements_begin() == mMesh->local_elements_end())
		throw "No mesh elements present on this rank! Try using fewer MPI processes.";
	
	libMesh::Order lMeshOrder = libMesh::Order::INVALID_ORDER;
	
	for (auto nElem = mMesh->local_elements_begin(); nElem != mMesh->local_elements_end(); nElem++)
	{
		libMesh::Order lElemOrder = (*nElem)->default_order();
		
		if (lMeshOrder == libMesh::Order::INVALID_ORDER) lMeshOrder = lElemOrder;
		if (lMeshOrder != lElemOrder) throw "Element orders are not consistent! Found element with order " + std::to_string(lElemOrder) + " while previous elements were all of order " + std::to_string(lMeshOrder) + ".";
	}
	
	int* lOrderGatherArray = new int[aEpetraComm.NumProc()];
	
	aEpetraComm.GatherAll((int*)&lMeshOrder, lOrderGatherArray, 1);
	
	for (int i = 1; i < aEpetraComm.NumProc(); i++)
	{
		if (lOrderGatherArray[i] != lOrderGatherArray[0])
			throw "Inconsistent element orders! Rank 0 has elements of order " + std::to_string(lOrderGatherArray[0]) + " while rank " + std::to_string(i) + " has elements of order " + std::to_string(lOrderGatherArray[i]) + ".";
	}
	
	CheckQOrderSuitability(mQRule, lOrderGatherArray[0]);
	
	delete[] lOrderGatherArray;
	
	std::cout << "Mesh order checked, all elements have order: " << lMeshOrder << std::endl;
    
    mMeshLoaded = true;
    
    //////////////////////////////
    // Prepare the equation system
    //////////////////////////////
    
    mEquationSystems = new libMesh::EquationSystems(*mMesh);
    
    mEquationSystems->add_system<libMesh::LinearImplicitSystem>(cSystemName);
    
    std::vector<std::string> lVariableNames;
    lVariableNames.push_back(cVarXName);
    lVariableNames.push_back(cVarYName);
    lVariableNames.push_back(cVarZName);
    
    mEquationSystems->get_system(cSystemName).add_variables(lVariableNames, lMeshOrder);
    
    std::vector<unsigned int> lVarIDs;
    lVarIDs.push_back(0);
    lVarIDs.push_back(1);
    lVarIDs.push_back(2);
    
    libMesh::ZeroFunction<libMesh::Number> lZero;
    
    std::set<libMesh::boundary_id_type> lBoundaries = mMesh->get_boundary_info().get_boundary_ids();
	
	// put all the boundary ids from the map to a vector
	std::vector<libMesh::boundary_id_type> lTempBcIdVec;
	for (auto nBid = lBoundaries.begin(); nBid != lBoundaries.end(); nBid++) lTempBcIdVec.push_back(*nBid);
	
	// gather values for bids from all processes
	aLibmeshComm.allgather(lTempBcIdVec);
	
	// add boundary ids into the map that are not there yet (to satisfy some libmesh assertion that the boundary ids must be the same on all ranks when it checks the dirichlet conditions
	
	for (auto nBid = lTempBcIdVec.begin(); nBid != lTempBcIdVec.end(); nBid++)
	{
		if (lBoundaries.find(*nBid) == lBoundaries.end()) lBoundaries.insert(*nBid);
	}
	
    libMesh::DirichletBoundary lDirichlet(lBoundaries, lVarIDs, lZero);
    
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
	
    lDofMap.add_dirichlet_boundary(lDirichlet);
  
    mEquationSystems->init();
    
    std::cout << "Equation system before constraint gather: " << std::endl;
    mEquationSystems->print_info();
    
    ///////////////////////
    // Prepare the FE class
    ///////////////////////
    
    libMesh::FEType lType = mEquationSystems->get_system(cSystemName).variable_type(0);
    mFE = libMesh::FEBase::build(3, lType);
    mFE->attach_quadrature_rule(&mQRule);
    
    // you have to call everything that you will need at the beginning so libmesh knows it has to recalculate those values for each element reinit
    // we could theoretically create a separate fe object for each matrix and rhs, because each of these need a different 
    // set of fields, so we wouldn't recalculate all the fields with each reinit
    mPhi  = &mFE->get_phi();
    mDPhi = &mFE->get_dphi();
    mJxW  = &mFE->get_JxW();
    mXYZ  = &mFE->get_xyz();
    
    mFE->print_info(std::cout);
    
    ////////////////////
    // Local dof indices
    ////////////////////
    
    std::vector<DofInfo> lLocalDofs;
    // this also includes dofs that are on other processes, but they belong to elements that are on this process
    // this array will be used to create a map that will be used to communicate displacement values for the domain boundary elements
    std::vector<int> lLocalDofsOverlapping;
	
	// even more "extended" than overlapping (superset of overlapping which is a superset of standard local dof indices)
	std::vector<int> lLocalDofsSemilocal;
	// temp for first storing the indices even with duplicates, then unique list will be transfered into the non temp vector
	std::vector<int> lLocalDofsSemilocalTemp;
    
    lLocalDofs.reserve(lDofMap.n_local_dofs());
    std::vector<int> lConstrainedDofs;
	lConstrainedDofs.reserve(lDofMap.n_local_constrained_dofs());
	
    // Indices strictly for this process
    for (auto nNode : mMesh->local_node_ptr_range())
    {
        std::vector<DofInfo> lDofsForNode = DofInfoForNode(*nNode);
        
        for (int iDof = 0; iDof < lDofsForNode.size(); iDof++)
        {
			// we can not use the IsDirichlet property (coming from the DofInfoForNode() function
			// because that is based on the array we haven't yet constructed at this point
			if (lDofMap.is_constrained_dof(lDofsForNode[iDof].DofId))
			{
				lDofsForNode[iDof].IsDirichlet = true;
			}
			lLocalDofs.push_back(lDofsForNode[iDof]);
			if (lDofsForNode[iDof].IsDirichlet)
			{
				lConstrainedDofs.push_back(lDofsForNode[iDof].DofId);
			}
        }
    }
    
    std::sort(lLocalDofs.begin(), lLocalDofs.end(), [](const DofInfo& aInfo1, const DofInfo& aInfo2) { return aInfo1.DofId < aInfo2.DofId; });
	
    // check dof array continuity and monotonicity
    mAreDofsContiguous = true;
	for (int i = 1; i < lLocalDofs.size(); i++)
	{
		if (lLocalDofs[i - 1].DofId != lLocalDofs[i].DofId - 1)
		{
			mAreDofsContiguous = false;
			break;
		}
	}
    
    std::vector<libMesh::dof_id_type> lElemDofs;
	
    // Indices on this process plus "neighbour" indices (on other processes, but on elements belonging to this process)
    for (auto nElem : mMesh->active_local_element_ptr_range())
    {
        lDofMap.dof_indices(nElem, lElemDofs);
        
        if (lElemDofs.size() != nElem->n_nodes() * 3) throw "Unexpected number of dofs on element! (got " + std::to_string(lElemDofs.size()) + ", expected " + std::to_string(nElem->n_nodes() * 3) + ")";
        
        for (int iElemDof = 0; iElemDof < lElemDofs.size(); iElemDof++)
        {
            bool lIsIn = false;
            for (int i = 0; i < lLocalDofsOverlapping.size(); i++)
            {
                if (lLocalDofsOverlapping[i] == lElemDofs[iElemDof])
                {
                    lIsIn = true;
                    break;
                }
            }
            if (!lIsIn) lLocalDofsOverlapping.push_back(lElemDofs[iElemDof]);
        }
    }
    
    std::sort(lLocalDofsOverlapping.begin(), lLocalDofsOverlapping.end());
    
    // Indices on this process plus indices of dofs that are on elements that coincide with nodes on this process
    // superset of the overlapping indices, so we start from them
	lLocalDofsSemilocalTemp.reserve(lLocalDofsOverlapping.size());
	
	for (int i = 0; i < lLocalDofsOverlapping.size(); i++)
		lLocalDofsSemilocalTemp.push_back(lLocalDofsOverlapping[i]);
	
	for (auto nElem = mMesh->active_semilocal_elements_begin(); nElem != mMesh->active_semilocal_elements_end(); nElem++)
	{
		lElemDofs.clear();
        lDofMap.dof_indices(*nElem, lElemDofs);
		
		for (int iDof = 0; iDof < lElemDofs.size(); iDof++)
		{
			lLocalDofsSemilocalTemp.push_back((int)lElemDofs[iDof]);
		}
	}
	
    std::sort(lLocalDofsSemilocalTemp.begin(), lLocalDofsSemilocalTemp.end());
	
	for (int iDof = 0; iDof < lLocalDofsSemilocalTemp.size(); iDof++)
	{
		if (lLocalDofsSemilocal.size() == 0) 
		{
			lLocalDofsSemilocal.push_back(lLocalDofsSemilocalTemp[iDof]);
			continue;
		}
		if (lLocalDofsSemilocalTemp[iDof] != lLocalDofsSemilocal[lLocalDofsSemilocal.size() - 1])
		{
			lLocalDofsSemilocal.push_back(lLocalDofsSemilocalTemp[iDof]);
		}
	}
	// here it should be unique and sorted
	
	// we need all constrained dofs (also from other ranks) to be able to zero out also columns and not only rows in the system matrices
	aLibmeshComm.allgather(lConstrainedDofs);
	
	// sort and eliminate duplicates (are they possible?)
	sort(lConstrainedDofs.begin(), lConstrainedDofs.end());
	
	mConstrainedDofsUnique.reserve(lDofMap.n_constrained_dofs());
	
	for (int i = 0; i < lConstrainedDofs.size(); i++)
	{
		bool lIsIn = false;
		if (mConstrainedDofsUnique.size() > 0 && mConstrainedDofsUnique[mConstrainedDofsUnique.size() - 1] == lConstrainedDofs[i])
			lIsIn = true;
		if (!lIsIn) mConstrainedDofsUnique.push_back(lConstrainedDofs[i]);
	}
	
    /////////////////////////////////////////////////
    // Create the Epetra Map for the problem matrices
    /////////////////////////////////////////////////
    
    std::vector<int> lLocalDofIds = DofInfo::GetDofIDs(lLocalDofs);
    
    mStandardDofMap = new Epetra_Map(lDofMap.n_dofs(), lLocalDofIds.size(), &lLocalDofIds[0], 0, aEpetraComm);
    
    // let the system compute the total size itself here
    mStandardDofMapOverlapping = new Epetra_Map(-1, lLocalDofsOverlapping.size(), &lLocalDofsOverlapping[0], 0, aEpetraComm);
    
    // misc set up
    mDofCount = lDofMap.n_dofs();
    mDofCountLocal = lDofMap.n_local_dofs();
    mDofInfo = lLocalDofs;
	mDofIndices = lLocalDofIds;
    mDofOverlappingIndices = lLocalDofsOverlapping;
	mDofSemilocalIndices = lLocalDofsSemilocal;
    if (mDofInfo.size() != mDofCountLocal) throw "Inconsistent dof info!";
    
    std::cout << "Mesh and all the FE structures are loaded and prepared" << std::endl;
}

Teuchos::RCP<Epetra_FECrsMatrix> FE::CreateStiffnessMatrix(const PhysicalParameters& aParams, std::vector<Epetra_SerialDenseMatrix>* aLocalMatrices) const
{
    if (!mMeshLoaded) throw "No mesh was loaded!";
    
    Epetra_SerialDenseMatrix lCMatrix = CreateCMatrix(aParams);
    
    Teuchos::RCP<Epetra_FECrsMatrix> lReturnMatrix = Teuchos::rcp(new Epetra_FECrsMatrix(Epetra_DataAccess::Copy, *mStandardDofMap, 0));
    
    std::vector<DofInfo> lElementDofInfo;
	
	// for dirichlet BC
	const double lDummy1 = 1.0;
	    
    for (auto nElem : mMesh->active_local_element_ptr_range())
    {
        lElementDofInfo.clear();
        Epetra_SerialDenseMatrix lLocalMatrix = CreateLocalStiffnessMatrix(*nElem, lCMatrix, lElementDofInfo);
		
// 		std::vector<int> lElementDofsGlobalIndices = DofInfo::GetDofIDs(lElementDofInfo);
		
		if (aLocalMatrices != nullptr) aLocalMatrices->push_back(lLocalMatrix);
                
        // filter out the constrained dofs, also do the renumbering
//         std::vector<int> lNonBCDofsGlobal;
//         std::vector<int> lNonBCDofsLocal;
        
//         FilterOutBCDofs(lDofIndices, lNonBCDofsGlobal, lNonBCDofsLocal);
        
        // Fill the global matrix
        for (int iDof = 0; iDof < lElementDofInfo.size(); iDof++)
        {
			if (!lElementDofInfo[iDof].IsDirichlet)
			{
				std::vector<int> lIndices;
				std::vector<double> lValues;
				lValues.reserve(lElementDofInfo.size());
	//             int lDofIdexGlobal = lNonBCDofsGlobal[iDof];
	//             int lDofIndexLocal = lNonBCDofsLocal[iDof];
				int lDofIdexGlobal = lElementDofInfo[iDof].DofId;
				int lDofIndexLocal = iDof;
				
				for (int iDof2 = 0; iDof2 < lElementDofInfo.size(); iDof2++)
				{
					// only add the value if this is not a dirichlet node (zeroing out the columns of the matrix)
					if (!lElementDofInfo[iDof2].IsDirichlet)
					{
						lIndices.push_back(lElementDofInfo[iDof2].DofId);
						double lValue = lLocalMatrix(lDofIndexLocal, iDof2);
						lValues.push_back(lValue);
					}
				}
				
				lReturnMatrix->InsertGlobalValues(lDofIdexGlobal, lValues.size(), &lValues[0], &lIndices[0]);
			}
			else
			{
				// avoid adding the "1" row multiple times from various ranks in case when the dirichlet node is on a boundary between domains
				if (IsDofLocal(lElementDofInfo[iDof].DofId))
				{
					lReturnMatrix->InsertGlobalValues(lElementDofInfo[iDof].DofId, 1, &lDummy1, &lElementDofInfo[iDof].DofId);
				}
			}
        }
    }
    
    lReturnMatrix->GlobalAssemble();
    lReturnMatrix->OptimizeStorage();
    
    // scaling only for jacobian, the eps (shape function derivatives) vectors are scaled during creation of local matrices
    int lResult = lReturnMatrix->Scale(1.0 / (cLengthScale * cLengthScale * cLengthScale));
    if (lResult != 0) throw "Scale did not work!";
    
    return lReturnMatrix;
}

Teuchos::RCP<Epetra_FECrsMatrix> FE::CreateNonlinearStiffnessMatrix(const Epetra_Vector& aU, const PhysicalParameters& aParams, bool aUseImport)
{    
    std::function<void(Teuchos::RCP<Epetra_FECrsMatrix>, int, int, double*, int*)> lFillFunction;
    
    if (!mIsNonlinStiffInnerAllocated)
    {
        Teuchos::RCP<Epetra_FECrsMatrix> lNewMatrix = Teuchos::rcp(new Epetra_FECrsMatrix(Epetra_DataAccess::Copy, *mStandardDofMap, 0));
        lFillFunction = [](Teuchos::RCP<Epetra_FECrsMatrix> aMatrix, int aRowIndex, int aElementCount, double* aValues, int* aIndices)
        {
            aMatrix->InsertGlobalValues(aRowIndex, aElementCount, aValues, aIndices);
        };
        FillNonlinearStiffnessMatrix(lNewMatrix, aU, aParams, lFillFunction, aUseImport);
        Time lAssemblyTime;
        lAssemblyTime.Start();
        lNewMatrix->GlobalAssemble();
        lNewMatrix->OptimizeStorage();
        mNonlinStiffnessInner = lNewMatrix;
        mIsNonlinStiffInnerAllocated = true;
    }
    else
    {
        mNonlinStiffnessInner->PutScalar(0.0);
        lFillFunction = [](Teuchos::RCP<Epetra_FECrsMatrix> aMatrix, int aRowIndex, int aElementCount, double* aValues, int* aIndices)
        {
            aMatrix->SumIntoGlobalValues(aRowIndex, aElementCount, aValues, aIndices);
        };
        FillNonlinearStiffnessMatrix(mNonlinStiffnessInner, aU, aParams, lFillFunction, aUseImport);
		// GlobalAssemble communicates the ghosted data to their target destination
		// but we don't want to call FillComplete anymore (hence the false flag) because the matrix has already been assembled before
		// Now it was just refilled with new values
        mNonlinStiffnessInner->GlobalAssemble(false);
    }
    
    // scaling only for jacobian, the eps (shape function derivatives) vectors are scaled during creation of local matrices
    int lResult = mNonlinStiffnessInner->Scale(1.0 / (cLengthScale * cLengthScale * cLengthScale));
    if (lResult != 0) throw "Scale did not work!";
    
    return mNonlinStiffnessInner;
}

Teuchos::RCP<Epetra_FECrsMatrix> FE::CreateMassMatrix(const PhysicalParameters& aParams, std::vector<Epetra_SerialDenseMatrix>* aLocalMatrices) const
{
    if (!mMeshLoaded) throw "No mesh was loaded!";
    
    Epetra_FECrsMatrix* lReturnMatrix = new Epetra_FECrsMatrix(Epetra_DataAccess::Copy, *mStandardDofMap, 0);
    
    std::vector<DofInfo> lElementDofInfo;
	    
	// for dirichlet BC
// 	const double lDummy1 = 1.0;
    for (auto nElem : mMesh->active_local_element_ptr_range())
    {
        Epetra_SerialDenseMatrix lLocalMatrix = CreateLocalMassMatrix(*nElem, aParams, lElementDofInfo);
		
// 		std::vector<int> lElementDofsGlobalIndices = DofInfo::GetDofIDs(lElementDofInfo);
		
		if (aLocalMatrices != nullptr) aLocalMatrices->push_back(lLocalMatrix);
                
//         // filter out the constrained dofs, also do the renumbering
//         std::vector<int> lNonBCDofsGlobal;
//         std::vector<int> lNonBCDofsLocal;
//         
//         FilterOutBCDofs(lDofIndices, lNonBCDofsGlobal, lNonBCDofsLocal);

        // Fill the global matrix
		int lDirCount = 0;
        for (int iDof = 0; iDof < lElementDofInfo.size(); iDof++)
        {
			if (!lElementDofInfo[iDof].IsDirichlet)
			{
				int lDofIndexGlobal = lElementDofInfo[iDof].DofId;
				int lDofIndexLocal = iDof;
				
				// TODO remove this vector creation for each element, do it once ahead, or use the expandable array or something
				// avoid allocation in the loop
				std::vector<int> lIndices;
				std::vector<double> lValues;
				lIndices.reserve(lElementDofInfo.size());
				lValues.reserve(lElementDofInfo.size());
				
				for (int iDof2 = 0; iDof2 < lElementDofInfo.size(); iDof2++)
				{
					// only add the value if this is not a dirichlet node (zeroing out the columns of the matrix)
					if (!lElementDofInfo[iDof2].IsDirichlet)
					{
						lIndices.push_back(lElementDofInfo[iDof2].DofId);
						double lValue = lLocalMatrix(lDofIndexLocal, iDof2);
						lValues.push_back(lValue);
					}
// 					else lValues.push_back(0.0); 
				}
				
				lReturnMatrix->InsertGlobalValues(lDofIndexGlobal, lValues.size(), &lValues[0], &lIndices[0]);
			}
			else
			{
				lDirCount++;
				// mass matrix will have zero on the diagonal for dirichlet dofs (is that right?)
				// avoid adding the value multiple times from various ranks in case when the dirichlet node is on a boundary between domains
// 				if (IsDofLocal(lElementDofInfo[iDof].DofId))
// 				{
// 					lReturnMatrix->InsertGlobalValues(lElementDofInfo[iDof].DofId, 1, &lDummy1, &lElementDofInfo[iDof].DofId);
// 				}
			}
        }
    }
    
    lReturnMatrix->GlobalAssemble();
    lReturnMatrix->OptimizeStorage();
    
    int lResult = lReturnMatrix->Scale(1.0 / (cLengthScale * cLengthScale * cLengthScale));
    if (lResult != 0) throw "Scale did not work!";
    
    return Teuchos::rcp(lReturnMatrix);
}

Teuchos::RCP<Epetra_MultiVector> FE::CreateRHS(const PhysicalParameters& aParams, const std::function<Epetra_SerialDenseVector(double, double, double)>& aBodyForce) const
{
    if (!mMeshLoaded) throw "No mesh was loaded!";
    
    Epetra_FEVector* lReturnVector = new Epetra_FEVector(*mStandardDofMap);
    
    std::vector<DofInfo> lElementDofInfo;
	
	const double lDummy0 = 0.0;
    
    for (auto nElem : mMesh->active_local_element_ptr_range())
    {        
        Epetra_SerialDenseVector lLocalVector = CreateLocalRHS(*nElem, aParams, aBodyForce, lElementDofInfo);
        
//         std::vector<int> lNonBCDofsGlobal;
//         std::vector<int> lNonBCDofsLocal;
//         
//         FilterOutBCDofs(lDofIndices, lNonBCDofsGlobal, lNonBCDofsLocal);
        
        for (int iDof = 0; iDof < lElementDofInfo.size(); iDof++)
        {
			if (!lElementDofInfo[iDof].IsDirichlet)
			{
				int lDofIndexGlobal = lElementDofInfo[iDof].DofId;
				int lDofIndexLocal = iDof;
				
				double lValue = lLocalVector[lDofIndexLocal];
				
				lReturnVector->SumIntoGlobalValues(1, &lDofIndexGlobal, &lValue);
				
				// if the dirichlet condition is not 0 then you'd also have to add it's value 
				// here to the F vector (or substract, I don't know now, somehow handle it)
			}
			else
			{
				// avoid adding the dirichlet value multiple times from various ranks 
				// in case when the dirichlet node is on a boundary between domains
				// (since we iterate over elements)
				if (IsDofLocal(lElementDofInfo[iDof].DofId))
				{
					// in the future you can put a dirichlet value here, now we just use 0
					lReturnVector->SumIntoGlobalValues(1, &lElementDofInfo[iDof].DofId, &lDummy0);
				}
			}
        }
    }
    
    lReturnVector->GlobalAssemble();
    
    int lResult = lReturnVector->Scale(1.0 / (cLengthScale * cLengthScale * cLengthScale));
    if (lResult != 0) throw "Scale did not work!";
    
    return Teuchos::rcp(lReturnVector);
}

// Teuchos::RCP<Epetra_MultiVector> FE::CreateNonlinearStiffnessRHS(const Epetra_Vector& aU, const PhysicalParameters& aParams) const
// {
//     if (!mMeshLoaded) throw "No mesh was loaded!";
//     
//     Epetra_SerialDenseMatrix lCMatrix = CreateCMatrix(aParams);
//     
//     // Import neighbour nodes values (values from dofs on other processes which will be needed for this process)
//     
//     Epetra_Vector lUOverlapping(*mStandardDofMapOverlapping);
//     
//     Epetra_Import lImport(*mStandardDofMapOverlapping, *mStandardDofMap);
//     
//     lUOverlapping.Import(aU, lImport, Epetra_CombineMode::Add, 0);
//     
//     Epetra_FEVector* lReturnVector = new Epetra_FEVector(*mStandardDofMap);
//     
//     std::vector<DofInfo> lElementDofInfo;
//     
//     for (auto nElem : mMesh->active_local_element_ptr_range())
//     {        
//         Epetra_SerialDenseVector lLocalVector = CreateLocalNonlinearStiffnessRHS(lUOverlapping, *nElem, lCMatrix, lDofIndices);
//         
// //         std::vector<int> lNonBCDofsGlobal;
// //         std::vector<int> lNonBCDofsLocal;
// //         
// //         FilterOutBCDofs(lDofIndices, lNonBCDofsGlobal, lNonBCDofsLocal);
//         
//         for (int iDof = 0; iDof < lNonBCDofsGlobal.size(); iDof++)
//         {
//             int lDofIndexGlobal = lNonBCDofsGlobal[iDof];
//             int lDofIndexLocal = lNonBCDofsLocal[iDof];
//             
//             double lValue = lLocalVector[lDofIndexLocal];
//             
//             lReturnVector->SumIntoGlobalValues(1, &lDofIndexGlobal, &lValue);
//         }
//     }
//     
//     lReturnVector->GlobalAssemble();
//     
//     // scaling only for jacobian, the eps (shape function derivatives) vectors are scaled during creation of local vectors
//     int lResult = lReturnVector->Scale(1.0 / (cLengthScale * cLengthScale * cLengthScale));
//     if (lResult != 0) throw "Scale did not work!";
//     
//     return Teuchos::rcp(lReturnVector);
// }

Teuchos::RCP<Epetra_MultiVector> FE::CreateNonlinearStiffnessRHS(const Epetra_Vector& aU, const PhysicalParameters& aParams,  bool aUseImport) const
{
    if (!mMeshLoaded) throw "No mesh was loaded!";
    
    Epetra_SerialDenseMatrix lCMatrix = CreateCMatrix(aParams);
    
    // Import neighbour nodes values (values from dofs on other processes which will be needed for this process)
    
    Epetra_FEVector* lReturnVector = new Epetra_FEVector(*mStandardDofMap);
	
    const Epetra_Vector* lU = &aU;
    
    Teuchos::RCP<Epetra_Vector> lUOverlapping;
    
    if (aUseImport)
    {
        lUOverlapping = Teuchos::rcp(new Epetra_Vector(*mStandardDofMapOverlapping));
        
        Epetra_Import lImport(*mStandardDofMapOverlapping, *mStandardDofMap);
        
        lUOverlapping->Import(aU, lImport, Epetra_CombineMode::Add, 0);
        
        lU = lUOverlapping.get();
    }
    
    std::vector<DofInfo> lElementDofInfo;
    
	const double lDummy0 = 0.0;
	
    for (auto nElem : mMesh->active_local_element_ptr_range())
    {
        Epetra_SerialDenseVector lLocalVector = CreateLocalNonlinearStiffnessRHS(*lU, *nElem, lCMatrix, lElementDofInfo);
        
//         std::vector<int> lNonBCDofsGlobal;
//         std::vector<int> lNonBCDofsLocal;
//         
//         FilterOutBCDofs(lDofIndices, lNonBCDofsGlobal, lNonBCDofsLocal);
        
        for (int iDof = 0; iDof < lElementDofInfo.size(); iDof++)
        {
			if (!lElementDofInfo[iDof].IsDirichlet)
			{
				int lDofIndexGlobal = lElementDofInfo[iDof].DofId;
				int lDofIndexLocal = iDof;
				
				double lValue = lLocalVector[lDofIndexLocal];
				
				lReturnVector->SumIntoGlobalValues(1, &lDofIndexGlobal, &lValue);
			}
			else
			{
				// nothing here, we are just adding zero
			}
        }
    }
    
    lReturnVector->GlobalAssemble();
    
    // scaling only for jacobian, the eps (shape function derivatives) vectors are scaled during creation of local vectors
    int lResult = lReturnVector->Scale(1.0 / (cLengthScale * cLengthScale * cLengthScale));
    if (lResult != 0) throw "Scale did not work!";
    
    return Teuchos::rcp(lReturnVector);
}

const Epetra_Map& FE::GetRowMap() const
{
    return *mStandardDofMap;
}
const Epetra_Map& FE::GetRowMapOverlapping() const
{
    return *mStandardDofMapOverlapping;
}

int FE::DofCount() const
{
    if (mDofCount < 0) throw "Dof count not set!";
    return mDofCount;
}
int FE::DofCountLocal() const
{
    if (mDofCountLocal < 0) throw "Local dof count not set!";
    return mDofCountLocal;
}
const std::vector<DofInfo>& FE::GetDofInfo() const
{
    return mDofInfo;
}
const std::vector<int>& FE::GetDofOverlappingIndices() const
{
    return mDofOverlappingIndices;
}

const std::vector<int>& FE::GetDofSemilocalIndices() const
{
    return mDofSemilocalIndices;
}
void FE::ZeroOutDirichletDofs(Epetra_MultiVector& aVec) const
{
	for (int iDof = 0; iDof < mDofInfo.size(); iDof++)
	{
		if (mDofInfo[iDof].IsDirichlet)
		{
			const int lGlobalRow = mDofInfo[iDof].DofId;
			for (int iVec = 0; iVec < aVec.NumVectors(); iVec++)
			{
				const int lResult = aVec.ReplaceGlobalValue(lGlobalRow, iVec, 0.0);
				if (lResult != 0) 
				{
					throw "Required dof (" + std::to_string(lGlobalRow) + ") is not on this rank!";
				}
			}
		}
	}
}
Epetra_SerialDenseMatrix FE::CreateLocalStiffnessMatrix(const libMesh::Elem& aElem, const Epetra_SerialDenseMatrix& aCMatrix, std::vector<DofInfo>& aDofInfo) const
{	
    mFE->reinit(&aElem);
    aDofInfo.clear();
    
    // which dimension the given node represents (x, y or z displacement)
//     std::vector<int> lDofDim;
    std::vector<int> lDofNode;
    
    FillDofData(aElem, aDofInfo, lDofNode);
    
    int lDofCount = aDofInfo.size();
    
    const std::vector<std::vector<libMesh::RealGradient>>& lDPhi = *mDPhi;
    const std::vector<libMesh::Real>& lJxW = *mJxW;
    
    Epetra_SerialDenseMatrix lMatrix(lDofCount, lDofCount);
	
    Epetra_SerialDenseMatrix lEpsVectors(6, lDofCount);
        
    for (int iGauss = 0; iGauss < mFE->n_quadrature_points(); iGauss++)
    {
        for (int iDof = 0; iDof < lDofCount; iDof++)
        {
            int lDim = aDofInfo[iDof].Dim;
            int lNode = lDofNode[iDof];
            
            // off diagonal elements of the eps matrix (before vectorisation)
            // indices of nonzero elements in the eps vector depend on the dimension
            int lOffDiagInd1;
            int lOffDiagInd2;
            
            // other dimensions, the two other to the lDim one
            int lOtherDim1;
            int lOtherDim2;
            
            if (lDim == 0)
            {
                lOffDiagInd1 = 3;
                lOffDiagInd2 = 4;
                lOtherDim1 = 1;
                lOtherDim2 = 2;
            }
            else if (lDim == 1)
            {
                lOffDiagInd1 = 3;
                lOffDiagInd2 = 5;
                lOtherDim1 = 0;
                lOtherDim2 = 2;
            }
            else
            {
                lOffDiagInd1 = 4;
                lOffDiagInd2 = 5;
                lOtherDim1 = 0;
                lOtherDim2 = 1;
            }
            
            lEpsVectors(lDim, iDof) = lDPhi[lNode][iGauss](lDim) * cLengthScale;
            
            lEpsVectors(lOffDiagInd1, iDof) = lDPhi[lNode][iGauss](lOtherDim1) * cLengthScale;
            lEpsVectors(lOffDiagInd2, iDof) = lDPhi[lNode][iGauss](lOtherDim2) * cLengthScale;
        }
        
        Epetra_SerialDenseMatrix lTemp(lDofCount, 6);
        Epetra_SerialDenseMatrix lEpsTCEps(lDofCount, lDofCount);
        
        lEpsVectors.Multiply(true, aCMatrix, lTemp);
        lTemp.Multiply(false, lEpsVectors, lEpsTCEps);
        
        lEpsTCEps.Scale(lJxW[iGauss]);
        
        lMatrix += lEpsTCEps;
    }
    
    // Symmetrise the matrix (there must be a better way but I just couldn't find any)
    for (int i = 0; i < lDofCount; i++)
    {
        for (int j = i; j < lDofCount; j++)
        {
            lMatrix(i, j) = 1 / 2.0 * (lMatrix(i, j) + lMatrix(j, i));
            
            if (i != j) 
                lMatrix(j, i) = lMatrix(i, j);
        }
    }
    
    return lMatrix;
}

Epetra_SerialDenseMatrix FE::CreateLocalNonlinearStiffnessMatrix(const Epetra_Vector& aUOverlapping, const libMesh::Elem& aElem, const Epetra_SerialDenseMatrix& aCMatrix, std::vector<DofInfo>& aDofInfo) const
{
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
    
    mFE->reinit(&aElem);
    aDofInfo.clear();
    
    // which dimension the given node represents (x, y or z displacement)
//     std::vector<int> lDofDim;
    std::vector<int> lDofNode;
    
    FillDofData(aElem, aDofInfo, lDofNode);
	
    int lDofCount = aDofInfo.size();
    
    Epetra_SerialDenseMatrix lReturnMatrix(lDofCount, lDofCount);
        
    // get the local displacement vector for the corresponding dofs
    Epetra_SerialDenseVector lULocal(lDofCount);
    
	Epetra_SerialDenseMatrix lBT(9, lDofCount);
	
    for (int iDof = 0; iDof < lDofCount; iDof++)
    {
        int lGlobalDofIndex = aDofInfo[iDof].DofId;
        // Dirichlet nodes will be zero, we assume zero dirichlet boundary everywhere
//         if (lDofMap.is_constrained_dof(lGlobalDofIndex)) 
//         {
//             continue;
//         }
        
//         int lGlobalDofIndexOffset = lGlobalDofIndex - mDofOffset[lGlobalDofIndex];
        int lGlobalDofIndexOffset = lGlobalDofIndex;
        int lLocalDofIndex = mStandardDofMapOverlapping->LID(lGlobalDofIndexOffset);
        
        // we use 2 different meanings of "local", sometimes it means local as local for this process, 
        // sometimes local as local for this element
        lULocal(iDof) = aUOverlapping[lLocalDofIndex];
    }
        
    const std::vector<std::vector<libMesh::RealGradient>>& lDPhi = *mDPhi;
    const std::vector<libMesh::Real>& lJxW = *mJxW;
	
    for (int iGauss = 0; iGauss < mFE->n_quadrature_points(); iGauss++)
    {
        // derivatives of all shape functions on the element, size: lDofCount x 3
        // for the given gauss point
//         Epetra_SerialDenseMatrix lDVAll(lDofCount, 3);
		
		// derivatives of all shape functions on the element, in matrix form (each derivative is considered to be a 3x3 matrix)
		// a ROW of 3x3 matrices
		Epetra_SerialDenseMatrix lDVAllMat(3, 3 * lDofCount);
		// jacobi matrix of U for this gauss point (3x3 matrix)
		Epetra_SerialDenseMatrix lDUMat(3, 3);
        
		std::vector<Epetra_SerialDenseMatrix> lDVMat;
		lDVMat.reserve(lDofCount);
        for (int iDof = 0; iDof < lDofCount; iDof++)
        {
            int lNode = lDofNode[iDof];
            
			Epetra_SerialDenseMatrix lDVMatTemp(3, 3);
            for (int iDim = 0; iDim < 3; iDim++)
			{
				double lInsertedValue = lDPhi[lNode][iGauss](iDim) * cLengthScale;
				
				lDVAllMat(aDofInfo[iDof].Dim, 3 * iDof + iDim) = lInsertedValue;
				lDUMat(aDofInfo[iDof].Dim, iDim) += lULocal(iDof) * lInsertedValue;
				
				lDVMatTemp(aDofInfo[iDof].Dim, iDim) = lInsertedValue;
				lBT(3 * aDofInfo[iDof].Dim + iDim, iDof) = lInsertedValue;
			}
			lDVMat.push_back(lDVMatTemp);
        }
        
        //////////////////
        // L * C * dR part
        //////////////////
        
        // TODO this part is potentially dangerous because it assumes certain dof ordering (first node - all 3 dofs, next node - all 3 dofs
        // etc. now this is the case but if it ever changes this will not work anymore, so it should be rewritten to follow
        // the previously established dof ordering
        
        Epetra_SerialDenseMatrix lDUTDUHalfMat = MultiplyTN(lDUMat, lDUMat);
		lDUTDUHalfMat.Scale(0.5);
		Epetra_SerialDenseMatrix lDUSymMat = lDUMat;
		AddTranspose3x3(lDUSymMat);
		lDUSymMat.Scale(0.5);
        
        // Construct the "left" term of the intergral expression (L'*C*R)
        Epetra_SerialDenseMatrix lE(3, 3);
		lE = lDUTDUHalfMat;
		lE += lDUSymMat;
		
		Epetra_SerialDenseMatrix lEVec = Vectorise(lE);
		Epetra_SerialDenseMatrix lSVec = MultiplyTN(aCMatrix, lEVec);
		
		Epetra_SerialDenseMatrix lSBar(9, 9);
		for (int i = 0; i < 3; i++)
		{
			lSBar(3 * i + 0, 3 * i + 0) = lSVec(0, 0);
			lSBar(3 * i + 1, 3 * i + 1) = lSVec(1, 0);
			lSBar(3 * i + 2, 3 * i + 2) = lSVec(2, 0);
			
			lSBar(3 * i + 0, 3 * i + 1) = lSVec(3, 0);
			lSBar(3 * i + 1, 3 * i + 0) = lSVec(3, 0);
			
			lSBar(3 * i + 0, 3 * i + 2) = lSVec(4, 0);
			lSBar(3 * i + 2, 3 * i + 0) = lSVec(4, 0);
			
			lSBar(3 * i + 1, 3 * i + 2) = lSVec(5, 0);
			lSBar(3 * i + 2, 3 * i + 1) = lSVec(5, 0);
		}
		
		Epetra_SerialDenseMatrix lSBarBT(9, lDofCount);
		lSBarBT.Multiply('N', 'N', 1.0, lSBar, lBT, 0.0);
		Epetra_SerialDenseMatrix lBSbarBT = MultiplyTN(lBT, lSBarBT);
		
		lBSbarBT.Scale(lJxW[iGauss]);
		lReturnMatrix += lBSbarBT;
		
        //////////////////
        // dL * C * R part
        //////////////////
		
		// R part
        Epetra_SerialDenseMatrix lRight(3, 3 * lDofCount);
        Epetra_SerialDenseMatrix lRightLin(3, 3 * lDofCount);
        		
		lRightLin = lDVAllMat;
		
		lRight.Multiply('T', 'N', 1.0, lDUMat, lDVAllMat, 0.0);
		lRight += lRightLin;
		AddTranspose3x3(lRight);
		lRight.Scale(0.5);
		
		AddTranspose3x3(lRightLin);
		lRightLin.Scale(0.5);
        
        Epetra_SerialDenseMatrix lRightVec = Vectorise(lRight);
        Epetra_SerialDenseMatrix lRightLinVec = Vectorise(lRightLin);
		
		// the R and dL matrices are the same (also the lin parts), therefore we use only the R
		const Epetra_SerialDenseMatrix& lDLeftVec = lRightVec;
		const Epetra_SerialDenseMatrix& lDLeftLinVec = lRightLinVec;
        
		Epetra_SerialDenseMatrix lIntegralValues(lDofCount, lDofCount);
							
		Epetra_SerialDenseMatrix lTemp1 = MultiplyTN(lDLeftVec, aCMatrix);
		
		Epetra_SerialDenseMatrix lTemp2(lDofCount, lDofCount);
		lTemp2.Multiply('N', 'N', 1, lTemp1, lRightVec, 0);
		
		Epetra_SerialDenseMatrix lTemp3 = MultiplyTN(lDLeftLinVec, aCMatrix);
		
		Epetra_SerialDenseMatrix lTemp4(lDofCount, lDofCount);
		lTemp4.Multiply('N', 'N', 1, lTemp3, lRightLinVec, 0);
		lTemp4.Scale(-1);
		
		lIntegralValues += lTemp2;
		lIntegralValues += lTemp4;
		lIntegralValues.Scale(lJxW[iGauss]);
		
		lReturnMatrix += lIntegralValues;
    }
    
    
    // Symmetrise the matrix (there must be a better way but I just couldn't find any)
    for (int i = 0; i < lDofCount; i++)
    {
        for (int j = i + 1; j < lDofCount; j++)
        {
            lReturnMatrix(i, j) = 1 / 2.0 * (lReturnMatrix(i, j) + lReturnMatrix(j, i));
            lReturnMatrix(j, i) = lReturnMatrix(i, j);
        }
    }
    
    return lReturnMatrix;
}


Epetra_SerialDenseMatrix FE::CreateLocalMassMatrix(const libMesh::Elem& aElem, const PhysicalParameters& aParams, std::vector<DofInfo>& aDofInfo) const
{
    mFE->reinit(&aElem);
    
    aDofInfo.clear();
    
    // which dimension the given node represents (x, y or z displacement)
//     std::vector<int> lDofDim;
    std::vector<int> lDofNode;
    
    FillDofData(aElem, aDofInfo, lDofNode);
    
    int lDofCount = aDofInfo.size();
    
    const std::vector<std::vector<libMesh::Real>>& lPhi = *mPhi;
    const std::vector<libMesh::Real>& lJxW = *mJxW;
    
    Epetra_SerialDenseMatrix lMatrix(lDofCount, lDofCount);
    
    for (int iGauss = 0; iGauss < mFE->n_quadrature_points(); iGauss++)
    {
        Epetra_SerialDenseMatrix lTemp(lDofCount, lDofCount);
        
        for (int iDof = 0; iDof < lDofCount; iDof++)
        {
            int lNode = lDofNode[iDof];
            int lDim = aDofInfo[iDof].Dim;
            
            for (int jDof = 0; jDof < lDofCount; jDof++)
            {
                int lDim2 = aDofInfo[jDof].Dim;
                if (lDim != lDim2) continue;
                
                int lNode2 = lDofNode[jDof];
                                
                double lVal = lPhi[lNode][iGauss] * lPhi[lNode2][iGauss];
                
                lTemp(iDof, jDof) = lVal;
            }
        }
        
        lTemp.Scale(lJxW[iGauss]);
        
        lMatrix += lTemp;
    }
    
    lMatrix.Scale(aParams.Rho);
    
    // Symmetrise the matrix (there must be a better way but I just couldn't find any)
    for (int i = 0; i < lDofCount; i++)
    {
        for (int j = i; j < lDofCount; j++)
        {
            lMatrix(i, j) = 1 / 2.0 * (lMatrix(i, j) + lMatrix(j, i));
            
            if (i != j)
                lMatrix(j, i) = lMatrix(i, j);
        }
    }
    
    return lMatrix;
}

Epetra_SerialDenseVector FE::CreateLocalRHS(const libMesh::Elem& aElem, const PhysicalParameters& aParams, const std::function<Epetra_SerialDenseVector(double, double, double)>& aBodyForce, std::vector<DofInfo>& aDofInfo) const
{
    mFE->reinit(&aElem);
    
//     std::vector<int> lDofDim;
    std::vector<int> lDofNode;
    
    aDofInfo.clear();
    FillDofData(aElem, aDofInfo, lDofNode);
    
    int lDofCount = aDofInfo.size();
    
    const std::vector<libMesh::Point>& lXYZ = *mXYZ;
    const std::vector<std::vector<libMesh::Real>>& lPhi = *mPhi;
    const std::vector<libMesh::Real>& lJxW = *mJxW;
    
    Epetra_SerialDenseVector lReturnVector(lDofCount);
    
    for (int iGauss = 0; iGauss < mFE->n_quadrature_points(); iGauss++)
    {
        auto lForceValues = aBodyForce(lXYZ[iGauss](0), lXYZ[iGauss](1), lXYZ[iGauss](2));
        
        Epetra_SerialDenseVector lTemp(lDofCount);
                
        for (int iDof = 0; iDof < lDofCount; iDof++)
        {
            int lDim = aDofInfo[iDof].Dim;
            int lNode = lDofNode[iDof];
            double lPhiValue = lPhi[lNode][iGauss];
            
            lTemp(iDof) = lPhiValue * lForceValues[lDim];
        }
        lTemp.Scale(lJxW[iGauss]);
        
        lReturnVector += lTemp;
    }
    
    lReturnVector.Scale(aParams.Rho);
        
    return lReturnVector;
}

Epetra_SerialDenseVector FE::CreateLocalNonlinearStiffnessRHS(const Epetra_Vector& aUOverlapping, const libMesh::Elem& aElem, const Epetra_SerialDenseMatrix& aCMatrix, std::vector<DofInfo>& aDofInfo) const
{
    mFE->reinit(&aElem);
    
//     std::vector<int> lDofDim;
    std::vector<int> lDofNode;
    
    aDofInfo.clear();
    FillDofData(aElem, aDofInfo, lDofNode);
    
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
    
    int lDofCount = aDofInfo.size();
    
    // get the local displacement vector for the corresponding dofs
    Epetra_SerialDenseVector lULocal(lDofCount);
    
    for (int iDof = 0; iDof < lDofCount; iDof++)
    {        
        int lGlobalDofIndex = aDofInfo[iDof].DofId;
		
        // Dirichlet nodes will be zero, we assume zero dirichlet boundary everywhere
//         if (lDofMap.is_constrained_dof(lGlobalDofIndex)) 
//         {
//             continue;
//         }
        
//         int lGlobalDofIndexOffset = lGlobalDofIndex - mDofOffset[lGlobalDofIndex];
        int lGlobalDofIndexOffset = lGlobalDofIndex;
        int lLocalDofIndex = mStandardDofMapOverlapping->LID(lGlobalDofIndexOffset);
        
        // we use 2 different meanings of "local", sometimes it means local as local for this process, 
        // sometimes local as local for this element
        lULocal(iDof) = aUOverlapping[lLocalDofIndex];
    }
    
    // Precalculate some vectors which are used multiple times
    Epetra_SerialDenseMatrix lUBar(3, lDofCount);
    for (int iDof = 0; iDof < lDofCount; iDof++)
    {
        int lDim = aDofInfo[iDof].Dim;
        lUBar(lDim, iDof) = lULocal(iDof);
    }
    
    Epetra_SerialDenseMatrix lUBarTUBar(lDofCount, lDofCount);
    lUBarTUBar.Multiply('T', 'N', 1, lUBar, lUBar, 0);
    
    const std::vector<std::vector<libMesh::RealGradient>>& lDPhi = *mDPhi;
    const std::vector<libMesh::Real>& lJxW = *mJxW;
    
    Epetra_SerialDenseVector lReturnVector(lDofCount);
    
    for (int iGauss = 0; iGauss < mFE->n_quadrature_points(); iGauss++)
    {
        // derivatives of all shape functions on the element, size: lDofCount x 3
        // for the given gauss point
        Epetra_SerialDenseMatrix lDVAll(lDofCount, 3);
        
        for (int iDof = 0; iDof < lDofCount; iDof++)
        {
            int lNode = lDofNode[iDof];
            
            for (int iDim = 0; iDim < 3; iDim++)
                lDVAll(iDof, iDim) = lDPhi[lNode][iGauss](iDim) * cLengthScale;
        }
        
        // Construct the "left" term of the intergral expression (L'*C*R)
        Epetra_SerialDenseMatrix lLeft(3, 3);
        
        // Linear part of L
        Epetra_SerialDenseMatrix lLeftLin(3, 3);
        
        //////////////////
        // L * C * R part
        //////////////////
        
        // Block just to be able to reuse the "Temp" names
        // so we don't have to create temp1, temp2, temp3, ..., temp24 etc.
        {
            Epetra_SerialDenseMatrix lTemp1(lDofCount, 3);
            lTemp1.Multiply('N', 'N', 1, lUBarTUBar, lDVAll, 0);
            
            Epetra_SerialDenseMatrix lTemp2(3, 3);
            lTemp2.Multiply('T', 'N', 1, lDVAll, lTemp1, 0);
            
            lLeft += lTemp2;
            lLeft.Scale(0.5);
            
            lLeftLin.Multiply('T', 'T', 1, lDVAll, lUBar, 0);
            lLeftLin += GetTranspose(lLeftLin);
            lLeftLin.Scale(0.5);
            
            lLeft += lLeftLin;
        }
        
        Epetra_SerialDenseMatrix lLeftVec = Vectorise(lLeft);
        Epetra_SerialDenseMatrix lLeftLinVec = Vectorise(lLeftLin);
        
        // R part
        // A "row" of 3x3 matrices, one for each dof
        Epetra_SerialDenseMatrix lRight(3, 3 * lDofCount);
        Epetra_SerialDenseMatrix lRightLin(3, 3 * lDofCount);
        
        for (int iDof = 0; iDof < lDofCount; iDof++)
        {
            int lNode = lDofNode[iDof];
            int lDim = aDofInfo[iDof].Dim;
            
            Epetra_SerialDenseMatrix lDVj(3, 3);
            
            for (int i = 0; i < 3; i++)
            {
                lDVj(lDim, i) = lDPhi[lNode][iGauss](i) * cLengthScale;
            }
            {
                Epetra_SerialDenseMatrix lTemp1(3, 3);
                lTemp1 += lDVj;
                lTemp1 += GetTranspose(lDVj);
                lTemp1.Scale(0.5);
                
                Epetra_SerialDenseMatrix lTemp2(3, 3);
                lTemp2.Multiply('T', 'T', 1, lDVAll, lUBar, 0);
                
                Epetra_SerialDenseMatrix lTemp3(3, 3);
                lTemp3.Multiply('N', 'N', 1, lTemp2, lDVj, 0);
                
                lTemp3 += GetTranspose(lTemp3);
                lTemp3.Scale(0.5);
                
                lTemp3 += lTemp1;
                
                Copy3x3Matrix(lRightLin, lTemp1, 0, 3 * iDof);
                Copy3x3Matrix(lRight, lTemp3, 0, 3 * iDof);
            }
        }
        
        Epetra_SerialDenseMatrix lRightVec = Vectorise(lRight);
        Epetra_SerialDenseMatrix lRightLinVec = Vectorise(lRightLin);
        
        {
            Epetra_SerialDenseMatrix lTemp1(1, 6);
            lTemp1.Multiply('T', 'N', 1, lLeftVec, aCMatrix, 0);
            
            Epetra_SerialDenseMatrix lTemp2(1, lDofCount);
            lTemp2.Multiply('N', 'N', 1, lTemp1, lRightVec, 0);
            
            Epetra_SerialDenseMatrix lTemp3(1, 6);
            lTemp3.Multiply('T', 'N', 1, lLeftLinVec, aCMatrix, 0);
            
            Epetra_SerialDenseMatrix lTemp4(1, lDofCount);
            lTemp4.Multiply('N', 'N', 1, lTemp3, lRightLinVec, 0);
            lTemp4.Scale(-1);
            
            Epetra_SerialDenseMatrix lIntegralValues(1, lDofCount);
            
            lIntegralValues += lTemp2;
            lIntegralValues += lTemp4;
            lIntegralValues.Scale(lJxW[iGauss]);
            
            // one is a matrix and the other is a vector, therefore this manual loop
            for (int iDof = 0; iDof < lDofCount; iDof++)
            {
                lReturnVector(iDof) += lIntegralValues(0, iDof);
            }
        }
    }
       
    return lReturnVector;
}

void FE::FillNonlinearStiffnessMatrix(Teuchos::RCP<Epetra_FECrsMatrix> aMatrix, const Epetra_Vector& aU, const PhysicalParameters& aParams, std::function<void(Teuchos::RCP<Epetra_FECrsMatrix>, int, int, double*, int*)> aFillFunction, bool aUseImport) const
{
    if (!mMeshLoaded) throw "No mesh was loaded!";
    
    Epetra_SerialDenseMatrix lCMatrix = CreateCMatrix(aParams);
    
    // Import neighbour nodes values (values from dofs on other processes which will be needed for this process)
    
    const Epetra_Vector* lU = &aU;
    
    Teuchos::RCP<Epetra_Vector> lUOverlapping;
    
    if (aUseImport)
    {
        lUOverlapping = Teuchos::rcp(new Epetra_Vector(*mStandardDofMapOverlapping));
        
        Epetra_Import lImport(*mStandardDofMapOverlapping, *mStandardDofMap);
        
        lUOverlapping->Import(aU, lImport, Epetra_CombineMode::Add, 0);
        
        lU = lUOverlapping.get();
    }
    
    std::vector<DofInfo> lElementDofInfo;
    
    double lLocalMatrixTime = 0.0;
    
    Time lAssemblyTime;
    lAssemblyTime.Start();
    
    int lElementCount = 0;
    int lElementCountTotal = std::distance(mMesh->local_elements_begin(), mMesh->local_elements_end());
    
	std::vector<int> lIndices;
	std::vector<double> lValues;
	
    for (auto nElem : mMesh->active_local_element_ptr_range())
    {
        lElementDofInfo.clear();
                
        Epetra_SerialDenseMatrix lLocalMatrix = CreateLocalNonlinearStiffnessMatrix(*lU, *nElem, lCMatrix, lElementDofInfo);
		
//         std::vector<int> lNonBCDofsGlobal;
//         std::vector<int> lNonBCDofsLocal;
//         
//         FilterOutBCDofs(lDofIndices, lNonBCDofsGlobal, lNonBCDofsLocal);
        
        // Fill the global matrix
        for (int iDof = 0; iDof < lElementDofInfo.size(); iDof++)
        {
			if (!lElementDofInfo[iDof].IsDirichlet)
			{
				lIndices.clear();
				lValues.clear();
				
				lIndices.reserve(lElementDofInfo.size());
				lValues.reserve(lElementDofInfo.size());
				
				int lDofIdexGlobal = lElementDofInfo[iDof].DofId;
				int lDofIndexLocal = iDof;
				
				for (int iDof2 = 0; iDof2 < lElementDofInfo.size(); iDof2++)
				{
					if (!lElementDofInfo[iDof2].IsDirichlet)
					{
						lIndices.push_back(lElementDofInfo[iDof2].DofId);
						double lValue = lLocalMatrix(lDofIndexLocal, iDof2);
						lValues.push_back(lValue);
					}
				}
				
				aFillFunction(aMatrix, lDofIdexGlobal, lValues.size(), &lValues[0], &lIndices[0]);
			}
			else
			{
				// nothing here
			}
        }
        
        lElementCount++;
        lAssemblyTime.Report("Nonlinear stiffness assembly: " + std::to_string((double)lElementCount / lElementCountTotal * 100) + "%", 2000.0);
    }
}

Epetra_SerialDenseMatrix FE::CreateCMatrix(const PhysicalParameters& aParams) const
{
    Epetra_SerialDenseMatrix lCMatrix(6, 6);
    
    // Shear modulus
    double lG = aParams.E / 2 / (1 + aParams.nu);
    
    // Effective elastic modulus
    double lEEff = aParams.E / (1 - 2 * aParams.nu) / (1 + aParams.nu);
    
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i == j)
            {
                lCMatrix(i, j) = lEEff * (1 - aParams.nu);
            }
            else
            {
                lCMatrix(i, j) = lEEff * aParams.nu;
            }
        }
        lCMatrix(i + 3, i + 3) = lG;
    }
    
    return lCMatrix;
}
void FE::FillDofData(const libMesh::Elem& aElem, std::vector<DofInfo>& aDofInfo, std::vector<int>& aDofNode) const
{
    int iNode = 0;
    
    aDofInfo.clear();
    aDofNode.clear();
    
    // We assume that the nodes in the node_ref_range are ordered in the same way
    // as shape functions in the FE
    
    for (const libMesh::Node& nNode : aElem.node_ref_range())
    {
        std::vector<DofInfo> lDofInfo = DofInfoForNode(nNode);
        
        if (lDofInfo.size() != 3) throw "Wrong number of dofs for a node!";
                
        for (int iDof = 0; iDof < lDofInfo.size(); iDof++)
        {
			aDofInfo.push_back(lDofInfo[iDof]);
            // global dof id, not corrected for boundary conditions
//             aGlobalDofIndices.push_back(lDofInfo[iDof].DofId);
//             aDofDim.push_back(lDofInfo[iDof].Dim);
            // we don't care about the real (global) node id here, only for the index with respect to the current element
            aDofNode.push_back(iNode);
        }
        
        iNode++;
    }
}
bool FE::IsDofLocal(int aGlobalDofIndex) const
{
	if (mDofInfo.size() == 0) return false;
	
	if (mAreDofsContiguous)
	{
		return aGlobalDofIndex >= mDofInfo[0].DofId && aGlobalDofIndex <= mDofInfo[mDofInfo.size() - 1].DofId;
	}
	else
	{
		return std::binary_search(mDofIndices.begin(), mDofIndices.end(), aGlobalDofIndex);
	}
}
std::vector<DofInfo> FE::DofInfoForNode(const libMesh::Node& aNode) const
{
    std::vector<DofInfo> lReturnVector;
	
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
    int lNodeId = aNode.id();
    libMesh::Point lNodeCoords = mMesh->point(lNodeId);
        
    // We assume one dof per node and dimension
    for (int iDim = 0; iDim < 3; iDim++)
    {
        std::vector<libMesh::dof_id_type> lNodeDof;
        lDofMap.dof_indices(&aNode, lNodeDof, iDim);
        
        if (lNodeDof.size() != 1)
            throw "Expected exactly one dof for each node and dimension! Got " + std::to_string(lNodeDof.size()) + " instead.";
        
        DofInfo lInfo;
        
        lInfo.Dim = iDim;
        lInfo.DofId = lNodeDof[0];
        lInfo.NodeId = lNodeId;
        lInfo.X = lNodeCoords(0);
        lInfo.Y = lNodeCoords(1);
        lInfo.Z = lNodeCoords(2);
		
		lInfo.IsDirichlet = std::binary_search(mConstrainedDofsUnique.begin(), mConstrainedDofsUnique.end(), lInfo.DofId);
		
// 		if (std::find(mConstrainedDofsUnique.begin(), mConstrainedDofsUnique.end(), lInfo.DofId) != mConstrainedDofsUnique.end())
// 		{
// 			lInfo.IsDirichlet = true;
// 		}
// 		else
// 		{
// 			lInfo.IsDirichlet = false;
// 		}
		
// 		if (lDofMap.is_constrained_dof(lNodeDof[0])) lInfo.IsDirichlet = true;
// 		else lInfo.IsDirichlet = false;
        
        lReturnVector.push_back(lInfo);
    }
    
    return lReturnVector;    
}

// void FE::FilterOutBCDofs(const std::vector<libMesh::dof_id_type>& aGlobalDofIndices, std::vector<int>& aNonBCDofsGlobal, std::vector<int>& aNonBCDofsLocal) const
// {
//     libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
//     
//     aNonBCDofsGlobal.clear();
//     aNonBCDofsLocal.clear();
//     
//     for (int iDof = 0; iDof < aGlobalDofIndices.size(); iDof++)
//     {
//         libMesh::dof_id_type lDofGlobalNumber = aGlobalDofIndices[iDof];
//         
//         if (!lDofMap.is_constrained_dof(lDofGlobalNumber))
//         {
//             int lNewDofGlobalNumber = lDofGlobalNumber - mDofOffset[lDofGlobalNumber];
//             aNonBCDofsGlobal.push_back(lNewDofGlobalNumber);
//             aNonBCDofsLocal.push_back(iDof);
//         }
//     }
// }

Epetra_SerialDenseMatrix FE::Vectorise(const Epetra_SerialDenseMatrix& aSourceMatrix) const
{
    int lRows = aSourceMatrix.RowDim();
    if (lRows != 3) throw "The source matrix must have 3 rows!";
    int lCols = aSourceMatrix.ColDim();
    if (lCols % 3 != 0) throw "The number of columns of the source matrix must be divisible by 3!";
    
    Epetra_SerialDenseMatrix lReturnMatrix(6, lCols / 3);
    
    for (int iCol = 0; iCol < lReturnMatrix.ColDim(); iCol++)
    {
        lReturnMatrix(0, iCol) = aSourceMatrix(0, iCol * 3);
        lReturnMatrix(1, iCol) = aSourceMatrix(1, iCol * 3 + 1);
        lReturnMatrix(2, iCol) = aSourceMatrix(2, iCol * 3 + 2);
        
        lReturnMatrix(3, iCol) = aSourceMatrix(0, iCol * 3 + 1) + aSourceMatrix(1, iCol * 3);
        lReturnMatrix(4, iCol) = aSourceMatrix(0, iCol * 3 + 2) + aSourceMatrix(2, iCol * 3);
        lReturnMatrix(5, iCol) = aSourceMatrix(1, iCol * 3 + 2) + aSourceMatrix(2, iCol * 3 + 1);
    }
    
    return lReturnMatrix;
}

void FE::Copy3x3Matrix(Epetra_SerialDenseMatrix& aTargetMatrix, const Epetra_SerialDenseMatrix& aSourceMatrix, const int& aTargetRow, const int& aTargetCol, const int aSourceRow, const int aSourceCol) const
{
    for (int j = 0; j < 3; j++)
        for (int i = 0; i < 3; i++)
            aTargetMatrix(aTargetRow + i, aTargetCol + j) = aSourceMatrix(aSourceRow + i, aSourceCol + j);
}
Epetra_SerialDenseMatrix FE::GetTranspose(const Epetra_SerialDenseMatrix& aSourceMatrix) const
{
    int lRows = aSourceMatrix.RowDim();
    int lCols = aSourceMatrix.ColDim();
    
    Epetra_SerialDenseMatrix lReturnMatrix(lCols, lRows);
    
    for (int iRow = 0; iRow < lRows; iRow++)
        for (int iCol = 0; iCol < lCols; iCol++)
            lReturnMatrix(iCol, iRow) = aSourceMatrix(iRow, iCol);
        
    return lReturnMatrix;
}
void FE::AddTranspose3x3(Epetra_SerialDenseMatrix& aSourceMatrix) const
{
	if (aSourceMatrix.ColDim() % 3 != 0) throw "Column number must be a multiple of 3!";
	if (aSourceMatrix.RowDim() % 3 != 0) throw "Row number must be a multiple of 3!";
	
	int lMatCols=  aSourceMatrix.ColDim() / 3;
	int lMatRows = aSourceMatrix.RowDim() / 3;
	
	for (int iRow = 0; iRow < lMatRows; iRow++)
	{
		for (int iCol = 0; iCol < lMatCols; iCol++)
		{
			aSourceMatrix(3 * iRow + 0, 3 * iCol + 0) *= 2;
			aSourceMatrix(3 * iRow + 1, 3 * iCol + 1) *= 2;
			aSourceMatrix(3 * iRow + 2, 3 * iCol + 2) *= 2;
			
			aSourceMatrix(3 * iRow + 0, 3 * iCol + 1) += aSourceMatrix(3 * iRow + 1, 3 * iCol + 0);
			aSourceMatrix(3 * iRow + 0, 3 * iCol + 2) += aSourceMatrix(3 * iRow + 2, 3 * iCol + 0);
			aSourceMatrix(3 * iRow + 1, 3 * iCol + 2) += aSourceMatrix(3 * iRow + 2, 3 * iCol + 1);
			
			aSourceMatrix(3 * iRow + 1, 3 * iCol + 0) = aSourceMatrix(3 * iRow + 0, 3 * iCol + 1);
			aSourceMatrix(3 * iRow + 2, 3 * iCol + 0) = aSourceMatrix(3 * iRow + 0, 3 * iCol + 2);
			aSourceMatrix(3 * iRow + 2, 3 * iCol + 1) = aSourceMatrix(3 * iRow + 1, 3 * iCol + 2);
		}
	}
}
Epetra_SerialDenseMatrix FE::MultiplyTN(const Epetra_SerialDenseMatrix& aMat1, const Epetra_SerialDenseMatrix& aMat2) const
{
    if (aMat1.RowDim() != aMat2.RowDim()) throw "Inconsistent matrices! (" + std::to_string(aMat1.RowDim()) + " != " + std::to_string(aMat2.RowDim()) + ")";
    Epetra_SerialDenseMatrix lReturnMatrix(aMat1.ColDim(), aMat2.ColDim());
    
    for (int iCol = 0; iCol < aMat2.ColDim(); iCol++)
    {
        for (int iRow = 0; iRow < aMat1.ColDim(); iRow++)
        {
            double lValue = 0.0;
            for (int iCond = 0; iCond < aMat1.RowDim(); iCond++)
            {
                lValue += aMat1(iCond, iRow) * aMat2(iCond, iCol);
            }
            lReturnMatrix(iRow, iCol) = lValue;
        }
    }
    
    return lReturnMatrix;
}
int FE::CheckQOrderValidity(int aOrder)
{
	int lMaxOrder = 43; // from libmesh 1.3.0
	if (aOrder >= 0 && aOrder <= lMaxOrder) return aOrder;
	
	throw "Invalid FE quadrature rule order: " + std::to_string(aOrder);
}
void FE::CheckQOrderSuitability(const libMesh::QGauss& aQRule, int aMeshElementOrder)
{
	libMesh::OrderWrapper lOrderWrapper(aQRule.get_order());
	int lQuadratureOrder = lOrderWrapper.get_order();
	int lRecommendedQOrder = 0;
	if (aMeshElementOrder == 1) lRecommendedQOrder = 2;
	else if (aMeshElementOrder == 2) lRecommendedQOrder = 4;
	else std::cout << "No quadrature rule order recommendation exists for element order." << aMeshElementOrder << std::endl;
	
	if (lQuadratureOrder < lRecommendedQOrder) 
		std::cout << "Possibly too low quadrature order used! Used: " << lQuadratureOrder << ", recommended minimum: " << lRecommendedQOrder << std::endl;
}
