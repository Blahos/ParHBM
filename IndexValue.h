
class IndexValue;

#pragma once
#include "ExpandableArray.h"

class IndexValue
{
private:
	// both of these arrays must be always in the same state! (allocated or nullptr, same size)
	ExpandableArray<int> mIndices;
	ExpandableArray<double> mValues;
	
public:
	IndexValue(int aSize);
	IndexValue() : IndexValue(0) { }
    IndexValue(const IndexValue& aOther);
	~IndexValue();
	
	// both array are assumed to always have the same size
	int Size() const { return mIndices.Size(); }
	// do not ever call delete[] on this pointer from outside of this class or you will mess things up
	int* Indices() const { return mIndices.Array(); }
	// do not ever call delete[] on this pointer from outside of this class or you will mess things up
	double* Values() const { return mValues.Array(); }
	// (re)initialises the arrays to the new size
	// after this call, all values in the arrays stop to be valid 
	// and the arrays are guaranteed to be allocated only up to the element (aSize - 1)
	void Init(int aSize);
	void PutScalarValue(double aValue) const;
	void AddScalarValue(double aValue) const;
	void ScaleValues(double aFactor) const;
	// merges 2 IndexValues into this one
	// inputs can not be "this"
	// the current indices and values in "this" will be invalidated
	// indices in both inputs are assumed to be sorted (ascending) and unique
	// indices in any of the inputs can not include max integer value (it is used as a special case)
	void Merge(const IndexValue& aInput1, const IndexValue& aInput2);
	
    IndexValue& operator=(const IndexValue& aOther);
};
