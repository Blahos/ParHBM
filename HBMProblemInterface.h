
// Problem interface for harmonic balance method
// uses the FE class to assemble the system matrices and transforms them into the frequency system (applies HBM)

class HBMProblemInterface;

#pragma once
#include <string>

#include "Epetra_Vector.h"
#include "Teuchos_RCP.hpp"
#include "Epetra_Map.h"
#include "LOCA.H"
#include "LOCA_Epetra_Interface_Required.H"
#include "NOX_Epetra_Interface_Jacobian.H"

#ifdef HAVE_MPI
#  include "mpi.h"
#  include "Epetra_MpiComm.h"
#else
#  include "Epetra_SerialComm.h"
#endif

#include "LinearSystem.h"
#include "HBMSettings.h"
#include "FE.h"
#include "CrsMatrixRow.h"
#include "DofHBMInfo.h"
#include "Aft.h"
#include "ExportInfo.h"

class HBMProblemInterface : 
    public LOCA::Epetra::Interface::Required,
    public NOX::Epetra::Interface::Jacobian
{
public:
    static const std::string cContParamName;
    
private:
	double mTotalDSFillTime = 0.0;
	double mTotalDSFillTimeEpetra = 0.0;
	
    const LinearSystem cLinearSystem;
    FE& cFE;
    const HBMSettings cHBMSettings;
    // filled in constructor, do not touch it later!
    const std::vector<DofHBMInfo> cDofInfo;
    // can not be constant as it will allocate some arrays during runtime depending on the size of the input
    Aft mAft;
    Aft mAftOverlapping;
    const Epetra_Comm& cTrilinosComm;
    // don't touch after construction
    Teuchos::RCP<Epetra_Map> mHBMRowMap;
    Teuchos::RCP<Epetra_Map> mHBMRowMapOverlapping;
	// column map for the HBM matrix
	Teuchos::RCP<Epetra_Map> mHBMColumnMap;
    const int cHBMCoeffCount;
    
    double mCurrentW = 0.0;
    // to force the first assingment to happen even if it happened to be the same value (just in case the first frequency is 0 which is 
	// also the default value)
    bool mIsWInitialised = false;
    
    std::vector<double> mSolutionFrequencies;
    std::vector<Epetra_Vector> mSolutions;
    
    // storage for the F value (computeF) (independent of frequency)
    Teuchos::RCP<Epetra_Vector> mLinearForceHBM;
    
    // will export the current HBM matrix (dynamic stiffness matrix + nonlinear contributions) if the frequency is in one of the given ranges
    std::vector<ExportInfo> mExportHBMInfo;
    int mExportCounter = 0;
    
    // import from non-overlapping to overlapping vector in frequency domain
    Teuchos::RCP<Epetra_Import> mFreqDomImport;
    
    // Use Green strain?
    const bool cUseGreenStrain;
    
    // interruption file name
    std::string mInterruptionFileName;
    
public:
    HBMProblemInterface(const LinearSystem& aLinearSystem, FE& aFE, bool aUseGreenStrain, const HBMSettings& aHBMSettings, const Epetra_Comm& aTrilinosComm);
    // creates and "completes" an empty jacobian matrix (puts zeros where there will be values inserted in the future)
    Teuchos::RCP<Epetra_CrsMatrix> PrepareJacobiMatrix();
    void SaveDofInfo(const std::string& aFileName, double aLengthScale = 1.0) const;
    void SaveSolutions(const std::string& aFileName, double aLengthScale = 1.0, double aTimeScale = 1.0) const;
    // total size of the system (all processes)
    int DofCount() const;
	int DofCountLocal() const;
    const std::vector<DofHBMInfo>& GetDofInfo() const;
    void AddExportInfo(double aMin, double aMax, Multiplicity aMult)
    {
        DoubleRange lRange(aMin, aMax);
        ExportInfo lInfo(lRange, aMult);
        
        mExportHBMInfo.push_back(lInfo);
        
        std::cout << "Export range added to the problem: <" << aMin << ", " << aMax << ">" << std::endl;
    }
    
    // NOX and LOCA interfaces implementation
    virtual void setParameters(const LOCA::ParameterVector& aParams) override;
    virtual bool computeF(const Epetra_Vector& aX, Epetra_Vector& aF, const FillType aFillFlag) override;
	// computes Z*x, where Z is the dynamic stiffness matrix for current frequency
	void ComputeZX(const Epetra_Vector& aX, Epetra_Vector& aF);
	// compute the derivative of the F vector (F = Z(w)x + F_nl(x) - F) by frequency (w)
	void ComputeDFbyDW(const Epetra_Vector& aX, Epetra_Vector& aF);
    virtual bool computeJacobian(const Epetra_Vector& aX, Epetra_Operator& aJac) override;
    virtual void printSolution (const Epetra_Vector& aX, double aContParam) override;
    
    void SetFrequency(double aFrequency);
    
    void SetInterruptionFile(const std::string& aFileName);
    
private:
    void FillDynamicStiffness(Epetra_CrsMatrix& aMatrix, double aFrequency);
    double GetFrequency(double aContParamValue)
    {
        return (1.0 - aContParamValue) * cHBMSettings.StartFrequency + aContParamValue * cHBMSettings.EndFrequency;
    }
    std::vector<double> GetZeros(int aSize)
    {
        std::vector<double> lReturnVector;
        lReturnVector.reserve(aSize);
        for (int i = 0; i < aSize; i++) lReturnVector.push_back(0.0);
        return lReturnVector;
    }
    void AddNonlinearStiffnessF(const Epetra_Vector& aX, Epetra_Vector& aF, double aFrequency);
    void AddNonlinearStiffnessJac(const Epetra_Vector& aX, Epetra_CrsMatrix& aJac, double aFrequency);
    
    void ExportHBMMatrix(const Epetra_Vector& aX, const Epetra_CrsMatrix& aJacMatrix, double aCurrentW);	
    
private:
    static std::vector<DofHBMInfo> CreateDofHBMInfo(const FE& aFE, const HBMSettings& aHBMSettings);
    void CheckForInterruption();
};
