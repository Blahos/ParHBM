

#include <iostream>
#include <string>
#include <fstream>
#include <set>
#include <cmath>
#include <chrono>
#include <functional>
#include <vector>


#ifdef HAVE_MPI
#  include "mpi.h"
#  include "Epetra_MpiComm.h"
#else
#  include "Epetra_SerialComm.h"
#endif

#include "FE.h"
#include "Functions.h"
#include "Time.h"
#include "Config.h"
#include "TaskRunner.h"
#include "Test.h"
#include "Directory.h"
#include "Misc.h"

#include "Mesh/MeshLibmesh.h"

// because FUCKING PETSc redifines MPI_Bcast string by a #define macro
#undef MPI_Bcast

int main(int aArgCount, char** aArgList)
{
    time_t lAppStartTime = Time::CurrentTime();

    // stopwatch
    Time lStopwatch;
    lStopwatch.Start();
	
	
	MPI_Init(&aArgCount, &aArgList);
	
    libMesh::LibMeshInit* lInit = new libMesh::LibMeshInit(aArgCount, aArgList, MPI_COMM_WORLD);

    int lRank;
    int lRankCount;

#ifdef HAVE_MPI
    Epetra_MpiComm lTrilinosComm(MPI_COMM_WORLD);
    MPI_Comm_size(MPI_COMM_WORLD, &lRankCount);
    MPI_Comm_rank(MPI_COMM_WORLD, &lRank);
#else
    Epetra_SerialComm lTrilinosComm;
    lRank = 0;
    lRankCount = 1;
#endif

    std::string lOutputPath = "./";
	
	RunInTryCatch([&]() 
	{
		std::string lConfigPath = "config";
        if (aArgCount >= 2) // first argument is always the name of the exe
        {
            lConfigPath = std::string(aArgList[1]);
            std::cout << "Config path specified: \"" << lConfigPath << "\"" << std::endl;
        }
        else
        {
            std::cout << "No config path specified, using the default: \"" << lConfigPath << "\"" << std::endl;
        }

        if (aArgCount >= 3) // output path
        {
            lOutputPath = std::string(aArgList[2]);
            std::cout << "Output path specified: \"" << lOutputPath << "\"" << std::endl;
        }
        else
        {
            std::cout << "No output path specified, using the default: \"" << lOutputPath << "\"" << std::endl;
        }

		std::string lAppStartTimeString = Time::CurrentUTCTimeStrintMPI(lAppStartTime, MPI_COMM_WORLD);
		
		if (!DirectoryExists(lOutputPath)) throw "The specified output directory \"" + lOutputPath + "\" does not exist! Please create it first.";
        lOutputPath += "/ParHBM_" + lAppStartTimeString + "/";
        std::cout << "Output path after appending the app start time: " << lOutputPath << std::endl;
		
		CreateDirectories(lOutputPath, MPI_COMM_WORLD);
		CreateSymbolicLink(lOutputPath, "Last", MPI_COMM_WORLD);
						
        Config lConfig = Config::LoadConfig(lConfigPath);
        lConfig.Print();

        TaskRunner lTaskRunner(lConfig, lInit->comm(), lTrilinosComm, lOutputPath);

        if (lConfig.DoMatrixOutput) lTaskRunner.RunMatrixOutput();
        if (lConfig.DoEigenfrequencies) lTaskRunner.RunEigenfrequencies();
        if (lConfig.DoFrequencies) lTaskRunner.RunFrequencySet();
        if (lConfig.DoContinuation) lTaskRunner.RunContinuation();

        double lElapsed = lStopwatch.Stop();

        std::cout << "Total program run time: " << lElapsed << " ms" << std::endl;
	}, lRank, true, false);

    lInit->comm().barrier();
    delete lInit;
	MPI_Finalize();
	
    return 0;
}
