
template <typename T>
class ExpandableArray;

#pragma once
#include <string>
#include <cstring>

template <typename T>
class ExpandableArray
{
private:
	T* mArray = nullptr;
	int mRealSize;
	int mSize;
public:
	ExpandableArray(int aSize);
	ExpandableArray() : ExpandableArray(0) { }
    ExpandableArray(const ExpandableArray<T>& aOther);
	~ExpandableArray();
	
	void Init(int aSize);
	ExpandableArray& operator=(const ExpandableArray& aOther);
	T* Array() const;
	int Size() const { return mSize; }
	// this can only make the array smaller (for the ouside user)
	// no reallocation will ever happen
	// the the Size() will become aNewSize if the aNewSize is smaller than the current Size()
	// otherwise the Size() will remain the same
	// this can "erase" some elements (they will still be technically in the allocated array but officially beyond bound
	// no invalid elements will be introduced since the array will never be extended (the virtual one)
	void LimitSize(int aNewSize);
	
	T& operator[](int aIndex) { return mArray[aIndex]; }
};

template<typename T> 
ExpandableArray<T>::ExpandableArray(int aSize)
{
	if (aSize < 0) throw "Invalid array size! (" + std::to_string(aSize) + ")";
	if (aSize > 0)
	{
		mArray = new T[aSize];
	}
	mSize = aSize;
	mRealSize = aSize;
}

template <typename T>
ExpandableArray<T>::ExpandableArray(const ExpandableArray<T>& aOther)
{
	if (aOther.Size() > 0)
	{
		mArray = new T[aOther.mSize];
	}
	
	std::memcpy(mArray, aOther.mArray, aOther.mSize * sizeof(T));
	
	mSize = aOther.mSize;
	mRealSize = mSize;
}

template <typename T>
ExpandableArray<T>::~ExpandableArray()
{
	if (mArray != nullptr)
		delete[] mArray;
}

template <typename T>
void ExpandableArray<T>::Init(int aSize)
{
	mSize = aSize;
	if (aSize > mRealSize)
	{
		if (mArray != nullptr)
			delete[] mArray;
		
		mArray = new T[aSize];
		mRealSize = aSize;
	}
}

template <typename T>
ExpandableArray<T>& ExpandableArray<T>::operator=(const ExpandableArray<T>& aOther)
{
    if (this == &aOther) return *this;
	
	Init(aOther.mSize);
		
	std::memcpy(mArray, aOther.mArray, aOther.mSize * sizeof(T));
	
    return *this;
}
template <typename T>
T* ExpandableArray<T>::Array() const
{
	return mArray;
}
template <typename T>
void ExpandableArray<T>::LimitSize(int aNewSize)
{
	if (aNewSize < 0) aNewSize = 0;
	if (aNewSize < mSize) mSize = aNewSize;
}
