
#pragma once

struct PhysicalParameters
{
public:
    double E = 2e11;
    double nu = 0.3;
    double Rho = 7800;
};
