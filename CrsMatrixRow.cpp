
#include <cstring>

#include "CrsMatrixRow.h"


CrsMatrixRow::CrsMatrixRow(int aSize)
	: mData(aSize)
{
	
}
// to make sure everything gets copied (can be important when returning an instance of this by value from a function, depending on the compiler optimisations)
CrsMatrixRow::CrsMatrixRow(const CrsMatrixRow& aOther)
	: mData(aOther.mData.Size())
{
    this->GlobalRowIndex = aOther.GlobalRowIndex;
	mData = aOther.mData;
}
CrsMatrixRow::~CrsMatrixRow()
{
}
void CrsMatrixRow::Init(int aSize)
{    
	mData.Init(aSize);
}
bool CrsMatrixRow::AreIndicesSorted() const
{
	for (int i = 1; i < mData.Size(); i++)
	{
		if (mData.Indices()[i - 1] > mData.Indices()[i]) return false;
	}
	return true;
}

CrsMatrixRow& CrsMatrixRow::operator=(const CrsMatrixRow& aOther)
{
    if (this == &aOther) return *this;
    
    this->GlobalRowIndex = aOther.GlobalRowIndex;
    this->mData = aOther.mData;
    
    return *this;
}

