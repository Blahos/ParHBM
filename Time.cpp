
#include <iostream>
#include <iomanip>
#include <sstream>
#include "Time.h"


void Time::Start()
{
    mCurrentStart = std::chrono::steady_clock::now();
    mLastReportTime = std::chrono::steady_clock::now();
}

double Time::Stop(const bool& aPrint)
{
    std::chrono::steady_clock::time_point lNow = std::chrono::steady_clock::now();

    std::chrono::duration<double, std::milli> lDurationMS = lNow - mCurrentStart;
    const double lMs = lDurationMS.count();
    
    if (aPrint) std::cout << "Elapsed time: " << lMs << " ms" << std::endl;
    
    return lMs;
}
double Time::Elapsed()
{
    std::chrono::steady_clock::time_point lNow = std::chrono::steady_clock::now();

    std::chrono::duration<double, std::milli> lDurationMS = lNow - mCurrentStart;
    const double lMs = lDurationMS.count();
	
	return lMs;
}

void Time::Report(const std::string& aMessage, double aDurationMS, bool aUpdateLastReportTime)
{
    std::chrono::steady_clock::time_point lNow = std::chrono::steady_clock::now();

    std::chrono::duration<double, std::milli> lDurationMS = lNow - mLastReportTime;
    double lElapsed = lDurationMS.count();
    
    if (lElapsed >= aDurationMS) 
    {
        std::cout << aMessage << std::endl;
        if (aUpdateLastReportTime) mLastReportTime = std::chrono::steady_clock::now();
    }
}
time_t Time::CurrentTime()
{
	std::chrono::system_clock::time_point lNow = std::chrono::system_clock::now();
	return std::chrono::system_clock::to_time_t(lNow);
}
std::string Time::CurrentUTCTimeString(const time_t& aTime)
{
	std::ostringstream ss;
	tm* lGMTime = gmtime(&aTime);
	
	if (lGMTime == nullptr) throw "Invalid GM time!";
	
	ss << std::put_time(lGMTime, "%F_%T");
	return ss.str();	
}
std::string Time::CurrentUTCTimeStrintMPI(time_t aTime, const MPI_Comm& aComm)
{
	if (sizeof(aTime) == sizeof(int)) MPI_Bcast(&aTime, 1, MPI_INT, 0, aComm);
	else if (sizeof(aTime) == sizeof(long)) MPI_Bcast(&aTime, 1, MPI_LONG, 0, aComm);
	else if (sizeof(aTime) == sizeof(long long)) MPI_Bcast(&aTime, 1, MPI_LONG_LONG, 0, aComm);
	else throw "Could not match the size of type_t to any datatype!";
	
	return CurrentUTCTimeString(aTime);
}
