
#include "Directory.h"

#include <fstream>

#include <sys/types.h> // required for stat.h
#include <sys/stat.h> // no clue why required -- man pages say so
#include <sys/types.h> // required for stat.h
#include <unistd.h>

#include "Functions.h"

bool FileExists(const std::string& aPath)
{
    std::ifstream aFile(aPath);
    bool lReturnValue = aFile.good();
    
    if (lReturnValue)
        aFile.close();
    
    return lReturnValue;
}

bool IsAbsolutePath(const std::string& aPath)
{
    if (aPath.length() == 0) throw "Empty path provided, can not determine if it's relative or absolute!";
#ifdef _WIN32
    if (aPath.length() >= 3) && aPath[1] == ':' && aPath[2] == '\\') return true;
#endif
#ifdef __linux__
    if (aPath[0] == '/') return true;
#endif
    
    return false;
}
void CreateDirectory(const std::string& aPath)
{
//	std::cout << "Creating directory: " << aPath << std::endl;
	mode_t lMode = 0733; // UNIX style permissions
	int lError = 0;
	
#ifdef _WIN32
	if (!DirectoryExists(aPath))
	{
		lError = _mkdir(aPath.c_str()); // can be used on Windows
	}
	if (lError != 0) 
	{
		throw "Error \"" + std::to_string(lError) + "\" creating directory: " + aPath;
	}
	return;
#endif
	
#ifdef __linux__
	if (!DirectoryExists(aPath))
	{
// 		std::cout << "Directory " << aPath << " does not exist so it will be created" << std::endl;
		lError = mkdir(aPath.c_str(), lMode); // can be used on non-Windows
	}
// 	else std::cout << "Directory " << aPath << " already exists" << std::endl;
	if (lError != 0)
	{
		throw "Error \"" + std::to_string(lError) + "\" creating directory: " + aPath;
	}
	return;
	#endif
}
void CreateDirectories(const std::string& aPath)
{
// 	std::cout << "CreateDirectories called for: " << aPath << std::endl;
	std::vector<std::string> lPathParts = SplitString(aPath, "/");
	
	std::string lCummulativePath = "";
	
	if (IsAbsolutePath(aPath))
	{
#ifdef _WIN32
		lCummulativePath += aPath.substr(0, 3);
#endif
#ifdef __linux__
		lCummulativePath += aPath.substr(0, 1);
#endif
	}
	
	for (int i = 0; i < lPathParts.size(); i++)
	{
		if (lPathParts[i].length() > 0)
		{
			lCummulativePath += lPathParts[i] + "/";
//  			std::cout << "Creating directory: " << lCummulativePath << std::endl;
			CreateDirectory(lCummulativePath);
		}
	}
}

bool DirectoryExists(const std::string& aPath)
{
	struct stat lStat;
	return (stat(aPath.c_str(), &lStat) == 0);
}
bool LinkExists(const std::string& aPath)
{
	struct stat lStat;
	return (lstat(aPath.c_str(), &lStat) == 0);
}

void CreateDirectories(const std::string& aPath, const MPI_Comm& aComm)
{
//	std::cout << "Create directories (with MPI comm) called for: " << aPath << std::endl;
	MPI_Barrier(aComm);
	
	int lRank;
	MPI_Comm_rank(aComm, &lRank);
	
#ifdef _WIN32
	if (lRank == 0)
	{
		CreateDirectories(aPath);
	}
	MPI_Barrier(aComm);
	return;
#endif
	
#ifdef __linux__
//	std::cout << "Entered linux part" << std::endl;
	if (lRank == 0)
	{
		CreateDirectories(aPath);
	}
	MPI_Barrier(aComm);
	return;
#endif
}

void CreateSymbolicLink(const std::string& aLinkedDirectory, const std::string& aLinkName, const MPI_Comm& aComm)
{
#ifdef __linux__
	int lRank;
	MPI_Comm_rank(aComm, &lRank);
	
	int lSymlinkReturn = 0;
	int lRemoveReturn = 0;
	
	int lSymlinkErrno = 0;
	int lRemoveErrno = 0;
	
	if (lRank == 0)
	{
		if (LinkExists(aLinkName))
		{
			lRemoveReturn = remove(aLinkName.c_str());
			if (lRemoveReturn != 0) lRemoveErrno = errno;
		}
		
		if (lRemoveReturn == 0)
		{
			lSymlinkReturn = symlink(aLinkedDirectory.c_str(), aLinkName.c_str());
			if (lSymlinkReturn != 0) lSymlinkErrno = errno;
		}
	}
	
	MPI_Barrier(aComm);
	MPI_Bcast(&lSymlinkReturn, 1, MPI_INT, 0, aComm);
	MPI_Bcast(&lRemoveReturn, 1, MPI_INT, 0, aComm);
	
	if (lRemoveReturn != 0) std::cout << "Could not remove already existing link, therefore not trying to override it! Error code: " << lRemoveErrno << std::endl; 
	if (lSymlinkReturn != 0) std::cout << "Unable to create the symbolic link for the output directory! Error code: " << lSymlinkErrno << std::endl;
#endif
}

void RemoveDirectoryOrFile(const std::string& aPath, const MPI_Comm& aComm)
{	
	int lRank;
	MPI_Comm_rank(aComm, &lRank);
	
	if (lRank == 0)
	{
		if (IsAbsolutePath(aPath))
		{
			std::cout << "Can not remove file/directory with absolute path!" << std::endl;
			return;
		}
		if (aPath.find("../") != aPath.npos)
		{
			std::cout << "Can not remove path that goes up in the directory tree! (../)" << std::endl;
			return;
		}
		remove(aPath.c_str());
	}
}
