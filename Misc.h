
#pragma once
#include <map>
#include "mpi.h"
#include "Solvers/SolverInfo.h"

#define PI 3.14159265358979323846264338327950288419716939937510582097494459230781
#define BORDER "-------------------------------------------------------------------"
#define OUT_EXTENSION (std::string(".out"))
#define STOP {\
    MPI_Barrier(MPI_COMM_WORLD);\
    std::cout << "Press enter to continue ... " << std::endl;\
    {\
        int lRank;\
        MPI_Comm_rank(MPI_COMM_WORLD, &lRank);\
        if (lRank == 0) getchar();\
    }\
    MPI_Barrier(MPI_COMM_WORLD);\
}

#define KEY_PREFIX_CHAR ('>')

#define COUT_RED 			"\033[1;31m"
#define COUT_GREEN 			"\033[1;32m"
#define COUT_COLOUR_RESET 	"\033[0m"

const std::vector<std::string> C_PredictorTypes = { "Secant", "Tangent", "Constant" };
const std::map<std::string, ParHBM::Solver> C_SolverTypes = 
{
    { "MKLPDSS",        ParHBM::Solver::MKLPDSS },
    { "MUMPS",          ParHBM::Solver::MUMPS }
};

template <class T> 
inline int Sgn(T aVal)
{
    return (T(0) < aVal) - (aVal < T(0));
}

