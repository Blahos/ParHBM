
#pragma once
#include <vector>
#include <map>
#include <functional>
#include "mpi.h"

#include "Epetra_MultiVector.h"
#include "Epetra_CrsMatrix.h"

#ifdef HAVE_MPI
#  include "mpi.h"
#  include "Epetra_MpiComm.h"
#else
#  include "Epetra_SerialComm.h"
#endif

#include "WaveInfo.h"
#include "CrsMatrixRow.h"
#include "DofInfo.h"
#include "DofHBMInfo.h"
#include "PointForceDef.h"
#include "Solvers/SolverInfo.h"

int IndexStandardToHBM(const int aStandardIndex, const int aHarmonicCoeffIndex, const int aHarmonicCoeffCount);
// in place version
void IndexStandardToHBMInPlace(int* const aStandardIndices, int aHarmonicCoeffIndex, const int aSize, const int aHarmonicCoeffCount);
std::vector<int> IndexStandardToHBM(const int* const aStandardIndices, int aHarmonicCoeffIndex, const int aSize, const int aHarmonicCoeffCount);
std::vector<int> IndexStandardToHBM(const std::vector<int>& aStandardIndices, int aHarmonicCoeffIndex, const int aHarmonicCoeffCount);
std::vector<int> IndexStandardToHBM(const int* const aStandardIndices, const int* const aHarmonicCoeffIndices, const int aSize, const int aHarmonicCoeffCount);
std::vector<PointForceDef> ReadExcitation(const std::string aFilePath);

std::vector<WaveInfo> CreateHarmonicInfo(const std::vector<int>& aHarmonicWaves);

void GetMatrixRowValues(const Epetra_CrsMatrix& aMatrix, int aGlobalRowIndex, CrsMatrixRow& aResult);
bool AreColumnIndicesSorted(const Epetra_CrsMatrix& aMatrix);

void AddNodalForce(Epetra_MultiVector& aF, double aX, double aY, double aZ, int aDim, double aMagnitude, std::vector<DofInfo> aDofInfo, const Epetra_Comm& aComm);

void ExportMatrix(const Epetra_CrsMatrix& aMatrix, const std::string& aFileName, int aMaxRows = -1);
void ExportVector(const Epetra_Vector& aVector, const std::string& aFileName);

std::map<std::string, std::vector<std::string>> ParseGeneralConfigFile(const std::string& aFilePath);
// reads all the lines in the file, skipping the comment lines and empty lines
// and returns these lines as a vector of strings
std::vector<std::string> ReadAllValidLines(const std::string& aFilePath);
// skips comment lines and returns the next valid (non comment) line in the file
std::string GetNextValidLine(std::ifstream& aFile, bool& aIsValid);
std::vector<std::string> SplitString(const std::string& aString, const std::string& aDelimiter);

// the scales are to scale the values from the dimensionless units to the real units
void SaveHBMSolutions(const std::vector<double>& aFrequencies, const std::vector<Epetra_Vector>& aSolutions, const std::string aFileName, double aTimeScale, double aLengthScale, const Epetra_Comm& aComm);

std::ostream& operator<<(std::ostream& aStream, const DofInfo& aInfo);
std::ostream& operator<<(std::ostream& aStream, const DofHBMInfo& aInfo);

std::string SkipWhiteSpaces(const std::string& aString);
void CheckString(const std::string& aString, const std::vector<std::string>& aPossibilities, const std::string& aGroupName = "");

std::string WaveTypeString(const WaveType& aType);

std::vector<double> ParseMatlabStyleVector(const std::string& aString);
    
ParHBM::SolverType GetSolverType(ParHBM::Solver aSolver);

void CheckMatrixForZeroRows(const Epetra_CrsMatrix& aMatrix);
void CheckMatrixForEmptyRows(const Epetra_CrsMatrix& aMatrix);

// returns true if no exceptions occured, otherwise false
bool RunInTryCatch(const std::function<void()>& aFunc, int aRank = -1, bool aPrintException = true, bool aThrowOut = true);
// execute the given function in serial, one rank at a time, within the given comm
void MPISerialize(const std::function<void()>& aFunc, const MPI_Comm& aComm);

bool IsInArray(const std::vector<int>& aArray, const int aValue, const bool aIsSorted, const bool aIsContiguous);
// merges two SORTED arrays into one that is also sorted and contains all elements from both arrays (duplicates removed)
void Merge(const std::vector<int>& aArray1, const std::vector<int>& aArray2, std::vector<int>& aArrayResult);
