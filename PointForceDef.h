
#pragma once
#include "WaveType.h"

struct PointForceDef
{
public:
    double X;
    double Y;
    double Z;
    double Dim;
    double WaveIndex;
    WaveType Type;
    double Amp;
};
