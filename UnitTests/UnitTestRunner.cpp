
#include "UnitTestRunner.h"

#include <stdio.h>
#include <sstream>

#include "../Functions.h"
#include "../Misc.h"
#include "../Exception.h"
#include "../Directory.h"

namespace ParHBM
{
	namespace UnitTests
	{
		void UnitTestRunner::AddTestingObject(TestObjectBase* aTestingObject)
		{
			aTestingObject->SetComm(cComm);
			aTestingObject->SetBaseDirectory(cBaseDirectory + "/" + aTestingObject->GetName() + "/");
			mUnitTestObjects.push_back(aTestingObject);
		}
		void UnitTestRunner::RunAllTests()
		{
			int lRank;
			MPI_Comm_rank(cComm, &lRank);
			
			CreateDirectories(cBaseDirectory, cComm);
			
			const int lTestObjectCount = mUnitTestObjects.size();
			
			std::vector<std::string> lFailedTests;
			
			int lTotalTestCount = 0;
			
			////////////
			// RUN TESTS
			////////////
			for (int iObject = 0; iObject < lTestObjectCount; iObject++)
			{				
				const std::string lObjectName = mUnitTestObjects[iObject]->GetName();
				const std::string lObjectBaseDir = mUnitTestObjects[iObject]->GetBaseDirectory();
				
				CreateDirectories(lObjectBaseDir, cComm);
				
				const std::vector<UnitTest> lUnitTests = mUnitTestObjects[iObject]->GetTests();
				
				const int lTestCount = lUnitTests.size();
				
				lTotalTestCount += lTestCount;
				
				for (int iTest = 0; iTest < lTestCount; iTest++)
				{
					const std::string lCompleteTestName = lObjectName + ": " + lUnitTests[iTest].Name;
					try
					{						
						const bool lResult = lUnitTests[iTest].Function();
						
						if (!lResult)
						{
							lFailedTests.push_back(lCompleteTestName);
						}
					}
					catch (ParHBM::Exception& aEx)
					{
						std::cout << COUT_RED << "Rank " << lRank << ": " << lCompleteTestName << " threw a ParHBM exception: " << aEx.Message << COUT_COLOUR_RESET << std::endl;
						lFailedTests.push_back(lCompleteTestName + " critical fail (" + aEx.Message + ")");
					}
					catch (...)
					{
						std::cout << COUT_RED << "Rank " << lRank << ": " << lCompleteTestName << " threw an unknown exception: " << COUT_COLOUR_RESET << std::endl;
						lFailedTests.push_back(lCompleteTestName + " critical fail (unknown exception thrown)");
					}
				}
			}
			
			/////////////////
			// REPORT RESULTS
			/////////////////
						
			MPISerialize([&lRank, &lTotalTestCount, &lFailedTests]()
			{
				std::stringstream lBuffer;
				
				lBuffer << "Unit tests results for rank " << lRank << ": " << std::endl;
				if (lFailedTests.size() == 0)
				{
					lBuffer << COUT_GREEN << "All unit tests (" << lTotalTestCount << ") were successful!" << COUT_COLOUR_RESET << std::endl;
				}
				else
				{					
					lBuffer << COUT_RED << lFailedTests.size() << " out of " << lTotalTestCount << " unit tests failed: " << std::endl;
					
					for (int iTest = 0; iTest < lFailedTests.size(); iTest++)
					{
						lBuffer << "	" << lFailedTests[iTest];
						if (iTest == lFailedTests.size() - 1) 
						{
							lBuffer << COUT_COLOUR_RESET << std::endl;
						}
						else
						{
							lBuffer << std::endl;
						}
					}
				}
				
				std::cout << lBuffer.str();
			}, cComm);
			
			
			// remove all created directories
			// will only work if they are empty, so if some tests doesn't clean up all its files, 
			// its subdirectory and therefore also the base directory of this class will stay in the execution directory
			for (int iObject = 0; iObject < mUnitTestObjects.size(); iObject++)
			{
				RemoveDirectoryOrFile(mUnitTestObjects[iObject]->GetBaseDirectory(), cComm);
			}
			RemoveDirectoryOrFile(cBaseDirectory, cComm);
		}
	}
}
