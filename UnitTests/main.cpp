

#include <iostream>
#include <memory>

#include "mpi.h"

#include "UnitTestRunner.h"
#include "TestObjects/Libmesh_Tests.h"
#include "TestObjects/SerialDenseMatrix_Tests.h"
#include "../Matrix/SerialDenseMatrixEpetra.h"
#include "TestObjects/DofData_Tests.h"

#include "../Exception.h"

int main(int aArgCount, char** aArgList)
{	
	MPI_Init(&aArgCount, &aArgList);
		
	ParHBM::UnitTests::UnitTestRunner lTestRunner(MPI_COMM_WORLD);

	// ADD TESTS
	lTestRunner.AddTestingObject(new ParHBM::UnitTests::Libmesh_Tests());
	lTestRunner.AddTestingObject(new ParHBM::UnitTests::SerialDenseMatrix_Tests<ParHBM::SerialDenseMatrixEpetra>());
	lTestRunner.AddTestingObject(new ParHBM::UnitTests::DofData_Tests());
	
	// RUN TESTS
	lTestRunner.RunAllTests();
	
	lTestRunner.DeleteUnitTestObjects();
	
	MPI_Finalize();
	return 0;
}
