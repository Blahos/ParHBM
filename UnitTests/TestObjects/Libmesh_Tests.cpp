
#include <iostream>
#include <fstream>
#include <stdio.h>

#include "Libmesh_Tests.h"

#include "../../Directory.h"
#include "../../Exception.h"

#include "../../Misc.h"

#include "../../Functions.hpp"

MeshLibmesh* ParHBM::UnitTests::Libmesh_Tests::CreateMesh(const std::string aFilePath, bool aPreCacheData) const
{
	// create a temp mesh file
	if (mRank == 0)
	{
		std::ofstream lFile(aFilePath);
		
		lFile << cSmallMeshFile;
	}
	
	MPI_Barrier(mComm);
	
	// allocated dynamically so its destructor doesn't get called when an exception is thrown
	// that would block the propagation of the exception because some libmesh objects would wait for other ranks
	// to finish their destruction (throwing exception triggers destructors of objects statically allocated within the scope or 
	// something like that)
	
	MeshLibmesh* lReturnMesh = new MeshLibmesh(mComm, 2, aPreCacheData);
	
	lReturnMesh->LoadMesh(aFilePath);
	
	MPI_Barrier(mComm);
	
	// remove the temporary file
	RemoveDirectoryOrFile(aFilePath, mComm);
	
	return lReturnMesh;
}

bool ParHBM::UnitTests::Libmesh_Tests::ElementData_NodeCount(const bool aPreCacheData) const
{
	const std::string lSourceFileName = std::string(__func__) + "_temp_mesh_file.unv";
	const std::string lSourceFileFullPath = mBaseDirectory + "/" + lSourceFileName;
	
	MeshLibmesh* const lTestMesh = CreateMesh(lSourceFileFullPath, aPreCacheData);
	
	for (int iElem = 0; iElem < lTestMesh->GetElementCount(); iElem++)
	{
		const ElementData& lData = lTestMesh->GetElementData(iElem);
		if (lData.NodeCount != 8) 
		{
			delete lTestMesh;
			return false;
		}
	}
	
	delete lTestMesh;
	return true;
}

bool ParHBM::UnitTests::Libmesh_Tests::ElementData_NodeCoordinates(const bool aPreCacheData) const
{
	const std::string lSourceFileName = std::string(__func__) + "_temp_mesh_file.unv";
	const std::string lSourceFileFullPath = mBaseDirectory + "/" + lSourceFileName;
	
	MeshLibmesh* const lTestMesh = CreateMesh(lSourceFileFullPath, aPreCacheData);
	
	for (int iElem = 0; iElem < lTestMesh->GetElementCount(); iElem++)
	{
		const ElementData& lData = lTestMesh->GetElementData(iElem);
		
		bool lAllX0 = true;
		bool lAllY0 = true;
		bool lAllZ0 = true;
		
		for (int iNode = 0; iNode < lData.NodeCount; iNode++)
		{
			const double lX = lData.Nodes[iNode].X;
			const double lY = lData.Nodes[iNode].Y;
			const double lZ = lData.Nodes[iNode].Z;
			
			if (lX != 0.0) lAllX0 = false;
			if (lY != 0.0) lAllY0 = false;
			if (lZ != 0.0) lAllZ0 = false;
			
			if (lX < 0.0 || lX > 1.0 || lY < 0.0 || lY > 1.0 || lZ < 0.0 || lZ > 3.0) 
			{
				delete lTestMesh;
				return false;
			}
		}
		
		if (lAllX0 || lAllY0 || lAllZ0)
		{
			delete lTestMesh;
			return false;
		}
	}
	
	delete lTestMesh;
	return true;
}

bool ParHBM::UnitTests::Libmesh_Tests::ElementData_ShapeFunc_NotEmpty(const bool aPreCacheData) const
{
	const std::string lSourceFileName = std::string(__func__) + "_temp_mesh_file.unv";
	const std::string lSourceFileFullPath = mBaseDirectory + "/" + lSourceFileName;
	
	MeshLibmesh* const lTestMesh = CreateMesh(lSourceFileFullPath, aPreCacheData);
	
	for (int iElem = 0; iElem < lTestMesh->GetElementCount(); iElem++)
	{
		const ElementData& lData = lTestMesh->GetElementData(iElem);
		
		for (int iGauss = 0; iGauss < lData.GaussPointCount; iGauss++)
		{
			if (lData.GaussPoints[iGauss].ShapeFunctionCount == 0)
			{
				delete lTestMesh;
				return false;
			}
		}
	}
	
	delete lTestMesh;
	return true;
}


bool ParHBM::UnitTests::Libmesh_Tests::ElementData_ShapeFunc_Nonzero(const bool aPreCacheData) const
{
	const std::string lSourceFileName = std::string(__func__) + "_temp_mesh_file.unv";
	const std::string lSourceFileFullPath = mBaseDirectory + "/" + lSourceFileName;
	
	MeshLibmesh* const lTestMesh = CreateMesh(lSourceFileFullPath, aPreCacheData);
	
	for (int iElem = 0; iElem < lTestMesh->GetElementCount(); iElem++)
	{
		const ElementData& lData = lTestMesh->GetElementData(iElem);
		
		for (int iGauss = 0; iGauss < lData.GaussPointCount; iGauss++)
		{
			for (int iShape = 0; iShape < lData.GaussPoints[iGauss].ShapeFunctionCount; iShape++)
			{
				const double lVal = lData.GaussPoints[iGauss].ShapeFunctions[iShape].ShapeFuncVal;
				
				if (abs(lVal) < std::numeric_limits<double>::epsilon())
				{
					delete lTestMesh;
					return false;
				}
			}
		}
	}
	
	delete lTestMesh;
	return true;
}
bool ParHBM::UnitTests::Libmesh_Tests::DofData_Size() const
{
	const std::string lSourceFileName = std::string(__func__) + "_temp_mesh_file.unv";
	const std::string lSourceFileFullPath = mBaseDirectory + "/" + lSourceFileName;
	
	MeshLibmesh* const lTestMesh = CreateMesh(lSourceFileFullPath, false);
	
	const DofDataUnique& lDofData = lTestMesh->GetDofDataUnique();
	
	if (lDofData.DofDataLocal.size() == 0 || lDofData.DofDataLocal.size() > 48)
	{
		delete lTestMesh;
		return false;
	}
	if (lDofData.DofDataConstrained.size() != 24)
	{
		delete lTestMesh;
		return false;
	}
	if (lDofData.DofIndicesLocal.size() == 0 || lDofData.DofIndicesLocal.size() > 48)
	{
		delete lTestMesh;
		return false;
	}
	if (lDofData.DofIndicesConstrained.size() != 24)
	{
		delete lTestMesh;
		return false;
	}
	if (lDofData.DofIndicesOverlapping.size() == 0 || lDofData.DofIndicesOverlapping.size() > 48)
	{
		delete lTestMesh;
		return false;
	}
	if (lDofData.DofIndicesSemilocal.size() == 0 || lDofData.DofIndicesSemilocal.size() > 48)
	{
		delete lTestMesh;
		return false;
	}
	
	if (lDofData.DofDataLocal.size() != lDofData.DofIndicesLocal.size())
	{
		delete lTestMesh;
		return false;
	}
	if (lDofData.DofDataConstrained.size() != lDofData.DofIndicesConstrained.size()) 
	{
		delete lTestMesh;
		return false;
	}
	
	delete lTestMesh;
	return true;
}

bool ParHBM::UnitTests::Libmesh_Tests::DofData_Unique() const
{
	const std::string lSourceFileName = std::string(__func__) + "_temp_mesh_file.unv";
	const std::string lSourceFileFullPath = mBaseDirectory + "/" + lSourceFileName;
	
	MeshLibmesh* const lTestMesh = CreateMesh(lSourceFileFullPath, false);
	
	const DofDataUnique& lDofData = lTestMesh->GetDofDataUnique();
	
	if (!IsUnique<DofData, int>(lDofData.DofDataLocal, [](const DofData& aData) { return &aData.Id; }))
	{
		delete lTestMesh;
		return false;
	}
	if (!IsUnique(lDofData.DofIndicesLocal))
	{
		delete lTestMesh;
		return false;
	}
	if (!IsUnique<DofData, int>(lDofData.DofDataConstrained, [](const DofData& aData) { return &aData.Id; }))
	{
		delete lTestMesh;
		return false;
	}
	if (!IsUnique(lDofData.DofIndicesConstrained))
	{
		delete lTestMesh;
		return false;
	}
	if (!IsUnique(lDofData.DofIndicesOverlapping))
	{
		delete lTestMesh;
		return false;
	}
	if (!IsUnique(lDofData.DofIndicesSemilocal))
	{
		delete lTestMesh;
		return false;
	}
	
	delete lTestMesh;
	return true;
}
