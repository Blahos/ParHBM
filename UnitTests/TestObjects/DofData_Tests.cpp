
#include "DofData_Tests.h"

#include "../../Mesh/Data/DofData.h"

bool ParHBM::UnitTests::DofData_Tests::Allgather() const
{
	int lSize;
	int lRank;
	
	MPI_Comm_size(mComm, &lSize);
	MPI_Comm_rank(mComm, &lRank);
	
	std::vector<DofData> lVec;
	
	DofData lD;
	
	lD.Dim = 1;
	lD.Id = lRank;
	lD.IsDirichlet = (lRank % 2) == 0;
	lD.IsLocal = lRank == 0;
	lD.X = 0;
	lD.Y = 1;
	lD.Z = 2;
	
	lVec.push_back(lD);
	
	if (lRank == 0)
	{
		lVec.push_back(lD);
	}
	
	std::vector<DofData> lVecGathered = DofData::MPIAllGather(lVec, mComm);
	
	if (lVecGathered.size() != lSize + 1) return false;
	
	for (int i = 0; i < lVecGathered.size(); i++)
	{
		if (i == 0 || i == 1)
		{
			if (lVecGathered[i].Id != 0) return false;
			if (!lVecGathered[i].IsLocal) return false;
		}
		else
		{
			if (lVecGathered[i].Id != i - 1) return false;
			if (lVecGathered[i].IsLocal) return false;
		}
		
		if (lVecGathered[i].X != 0) return false;
		if (lVecGathered[i].Y != 1) return false;
		if (lVecGathered[i].Z != 2) return false;
	}
	
	return true;
}

