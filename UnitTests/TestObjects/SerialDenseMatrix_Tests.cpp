
#include "SerialDenseMatrix_Tests.h"

#include "../../Matrix/SerialDenseMatrixEpetra.h"

#include <limits>
#include <iomanip>

template class ParHBM::UnitTests::SerialDenseMatrix_Tests<ParHBM::SerialDenseMatrixEpetra>;

template <class T>
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::Constructor_size() const
{
	T lMatrix(3, 4);
	
	if (lMatrix.Rows() != 3) return false;
	if (lMatrix.Cols() != 4) return false;
	
	return true;
}

template <class T>
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::Constructor_copy() const
{
	T lMat1(3, 4);
	
	T lMat2(lMat1);
	
	if (lMat2.Rows() != 3 || lMat2.Cols() != 4) return false;
	
	return true;
}

template <class T>
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::Assignment_operator() const
{
	T lMat1(3, 6);
	T lMat2(1, 7);
	lMat2 = lMat1;
	
	if (lMat2.Rows() != 3 || lMat2.Cols() != 6) return false;
	
	return true;
}

template <class T>
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::ElementAccess_Operator() const
{
	T lMat(4, 3);
	
	const double lVal = 87.093;
	
	lMat(1, 1) = lVal;
	
	const double lCheckVal = lMat(1, 1);
	
	return lVal == lCheckVal;
}

template <class T>
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::PutScalar() const
{
	const double lVal = 13.87;
	const int lRows = 2;
	const int lCols = 5;
	T lMatrix(lRows, lCols);
	
	lMatrix.PutScalar(lVal);
	
	for (int iCol = 0; iCol < lMatrix.Cols(); iCol++)
	{
		for (int iRow = 0; iRow < lMatrix.Rows(); iRow++)
		{
			if (lMatrix(iRow, iCol) != lVal) return false;
		}
	}
	
	return true;
}

template <class T>
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::Scale() const
{
	T lMatrix(3, 4);
	
	const double lScalar = 3.1;
	const double lScale = -2.17;
	
	lMatrix.PutScalar(lScalar);
	lMatrix.Scale(lScale);
	
	for (int iCol = 0; iCol < lMatrix.Cols(); iCol++)
	{
		for (int iRow = 0; iRow < lMatrix.Rows(); iRow++)
		{
			if (lMatrix(iRow, iCol) != lScalar * lScale) return false;
		}
	}
	return true;
}

template <class T>
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::Add() const
{
	const int lRows = 4;
	const int lCols = 3;
	
	T lMat1(lRows, lCols);
	T lMat2(lRows, lCols);
	
	const double lScalar1 = 9.87;
	const double lScalar2 = 2.39;
	
	const double lSum = lScalar1 + lScalar2;
	
	lMat1.PutScalar(lScalar1);
	
	lMat2.PutScalar(lScalar2);
	
	lMat1.Add(lMat2);
	
	for (int iCol = 0; iCol < lCols; iCol++)
	{
		for (int iRow = 0; iRow < lRows; iRow++)
		{
			if (lMat1(iRow, iCol) != lSum) return false;
		}
	}
	
	return true;
}

template<class T> 
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::Multiply_1() const
{
	const int lRows1 = 3;
	const int lCols1 = 6;
	const int lRows2 = lCols1;
	const int lCols2 = 4;
	
	T lMat1(lRows1, lCols1);
	T lMat2(lRows2, lCols2);
	
	T lResult(lRows1, lCols2);
	
	for (int iCol = 0; iCol < lCols1; iCol++)
	{
		for (int iRow = 0; iRow < lRows1; iRow++)
		{
			lMat1(iRow, iCol) = 0.75 * ((iRow + iCol) + 0.41);
		}
	}
	
	for (int iCol = 0; iCol < lCols2; iCol++)
	{
		for (int iRow = 0; iRow < lRows2; iRow++)
		{
			lMat2(iRow, iCol) = -3.41 * ((iRow + 0.25) * (iCol - 1.12));
		}
	}
		
	lResult.Multiply(lMat1, lMat2, false, false, 0.0, 1.0);
	
	if (lResult.Rows() != lRows1) return false;
	if (lResult.Cols() != lCols2) return false;
	
	for (int iCol = 0; iCol < lResult.Cols(); iCol++)
	{
		for (int iRow = 0; iRow < lResult.Rows(); iRow++)
		{
			double lExpectedResult = 0;
			
			for (int iRed = 0; iRed < lCols1; iRed++)
			{
				lExpectedResult += (0.75 * ((iRow + iRed) + 0.41)) * (-3.41 * ((iRed + 0.25) * (iCol - 1.12)));
			}
			
			// smaller multiples of eps (1e2) don't work with Epetra implementation of the matrix
			if (abs(lExpectedResult - lResult(iRow, iCol)) > 1e3 * std::numeric_limits<double>::epsilon()) 
			{
				return false;
			}
		}
	}
	
	return true;
}

template<class T> 
bool ParHBM::UnitTests::SerialDenseMatrix_Tests<T>::Multiply_2() const
{
	const int lRows1 = 3;
	const int lCols1 = 2;
	const int lRows2 = 4;
	const int lCols2 = lRows1;
	
	T lMat1(lRows1, lCols1);
	T lMat2(lRows2, lCols2);
	
	T lResult(lCols1, lRows2);
	
	const double lResultOffset = 9.81;
	
	lResult.PutScalar(lResultOffset);
	
	for (int iCol = 0; iCol < lCols1; iCol++)
	{
		for (int iRow = 0; iRow < lRows1; iRow++)
		{
			lMat1(iRow, iCol) = 3.14 * ((iRow + iCol) - 3);
		}
	}
	
	for (int iCol = 0; iCol < lCols2; iCol++)
	{
		for (int iRow = 0; iRow < lRows2; iRow++)
		{
			lMat2(iRow, iCol) = (iRow - 1.1) * (iCol - 0.34);
		}
	}
	
	const double lThisCoeff = 2.1;
	const double lABCoeff = -3.12;
	
	lResult.Multiply(lMat1, lMat2, true, true, lThisCoeff, lABCoeff);
	
	if (lResult.Rows() != lCols1) return false;
	if (lResult.Cols() != lRows2) return false;
	
	for (int iCol = 0; iCol < lResult.Cols(); iCol++)
	{
		for (int iRow = 0; iRow < lResult.Rows(); iRow++)
		{
			double lExpectedResult = lResultOffset * lThisCoeff;
			
			for (int iRed = 0; iRed < lRows1; iRed++)
			{
				lExpectedResult += lABCoeff * ((3.14 * ((iRed + iRow) - 3)) * ((iRed - 0.34) * (iCol - 1.1)));
			}
			
			if (abs(lExpectedResult - lResult(iRow, iCol)) > 1e2 * std::numeric_limits<double>::epsilon()) 
			{
				return false;
			}
		}
	}
	
	return true;
}

