
namespace ParHBM
{
	namespace UnitTests
	{
		template <class T>
		class SerialDenseMatrix_Tests;
	}
}

#pragma once 
#include "TestObjectBase.h"

#include "../../Matrix/SerialDenseMatrix.h"

namespace ParHBM
{
	namespace UnitTests
	{
		template <class T>
		class SerialDenseMatrix_Tests : public TestObjectBase
		{
		private:
		public:
			virtual std::string GetName() const override { return std::string("SerialDenseMatrix_Tests<") + typeid(T).name() + ">"; }
			virtual std::vector<UnitTest> GetTests() const override
			{
				std::vector<UnitTest> lReturnVector;
				
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Constructor_size()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Constructor_copy()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Assignment_operator()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementAccess_Operator()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(PutScalar()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Scale()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Add()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Multiply_1()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Multiply_2()));
				
				return lReturnVector;
			}
			
			bool Constructor_size() const;
			
			bool Constructor_copy() const;
			
			bool Assignment_operator() const;
			
			bool ElementAccess_Operator() const;
			
			bool PutScalar() const;
			
			bool Scale() const;
			
			bool Add() const;
			
			bool Multiply_1() const;
			bool Multiply_2() const;
		};
	}
}
