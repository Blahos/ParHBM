
namespace ParHBM
{
	namespace UnitTests
	{
		class Libmesh_Tests;
	}
}

#pragma once 
#include "TestObjectBase.h"

#include "../../Mesh/MeshLibmesh.h"


namespace ParHBM
{
	namespace UnitTests
	{
		class Libmesh_Tests : public TestObjectBase
		{
		private:
			
			static const std::string cSmallMeshFile;
			
			// mesh will be created multiple times (in multiple tests), so we make a function for it
			MeshLibmesh* CreateMesh(const std::string aFilePath, bool aPreCacheData) const;
			
		public:
			virtual std::vector<UnitTest> GetTests() const override
			{
				std::vector<ParHBM::UnitTests::UnitTest> lReturnVector;
				
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_NodeCount(false)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_NodeCount(true)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_NodeCoordinates(false)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_NodeCoordinates(true)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_ShapeFunc_NotEmpty(false)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_ShapeFunc_NotEmpty(true)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_ShapeFunc_Nonzero(false)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(ElementData_ShapeFunc_Nonzero(true)));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(DofData_Size()));
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(DofData_Unique()));
				
				return lReturnVector;
			}
			// not necessary, but will give a nicer name than the system's typeid
			virtual std::string GetName() const override { return "Libmesh_Tests"; }
			
			bool ElementData_NodeCount(const bool aPreCacheData) const;
			bool ElementData_NodeCoordinates(const bool aPreCacheData) const;
			bool ElementData_ShapeFunc_NotEmpty(const bool aPreCacheData) const;
			bool ElementData_ShapeFunc_Nonzero(const bool aPreCacheData) const;
			bool DofData_Size() const;
			bool DofData_Unique() const;
		};
	}
}
