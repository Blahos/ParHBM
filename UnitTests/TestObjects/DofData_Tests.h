
namespace ParHBM
{
	namespace UnitTests
	{
		class DofData_Tests;
	}
}

#pragma once
#include "TestObjectBase.h"

namespace ParHBM
{
	namespace UnitTests
	{
		class DofData_Tests : public TestObjectBase
		{
		public:
			std::vector<UnitTest> GetTests() const override
			{
				std::vector<UnitTest> lReturnVector;
				
				lReturnVector.push_back(METHOD_TO_UNIT_TEST(Allgather()));
				
				return lReturnVector;
			}
			
			bool Allgather() const;
		};
	}
}
