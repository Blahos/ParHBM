
namespace ParHBM
{
	namespace UnitTests
	{
		class TestObjectBase;
	}
}

#pragma once 
#include <vector>
#include <string>
#include "mpi.h"

#include "../UnitTest.h"

#define METHOD_TO_UNIT_TEST(method) ParHBM::UnitTests::UnitTest([this]() { return method; }, #method)

namespace ParHBM
{
	namespace UnitTests
	{
		class TestObjectBase
		{
		protected:
			MPI_Comm mComm;
			int mRank = -1;
			std::string mBaseDirectory;
			
		public:
			virtual std::string GetName() const { return std::string(typeid(*this).name()); }
			virtual std::vector<UnitTest> GetTests() const = 0;
			
			void SetComm(const MPI_Comm& aComm) 
			{ 
				mComm = aComm; 
				MPI_Comm_rank(mComm, &mRank);
			}
			void SetBaseDirectory(const std::string& aPath)
			{
				mBaseDirectory = aPath;
			}
			std::string GetBaseDirectory() const { return mBaseDirectory; }
		};
	}
}
