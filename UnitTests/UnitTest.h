
namespace ParHBM
{
	namespace UnitTests
	{
		struct UnitTest;
	}
}

#pragma once
#include <functional>
#include <string>

namespace ParHBM
{
	namespace UnitTests
	{
		struct UnitTest
		{
		public:
			const std::function<bool()> Function;
			const std::string Name;
			
			UnitTest(const std::function<bool()> aFunction, const std::string& aName)
				: Function(aFunction), Name(aName) { }
			
			UnitTest(const UnitTest& aOther)
				: Function(aOther.Function), Name(aOther.Name) { }
		};
	}
}
