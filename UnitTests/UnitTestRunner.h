
namespace ParHBM
{
	namespace UnitTests
	{
		class UnitTestsRunner;
	}
}

#pragma once 
#include <vector>
#include <functional>
#include <string>

#include "mpi.h"

#include "TestObjects/TestObjectBase.h"

namespace ParHBM
{
	namespace UnitTests
	{
		class UnitTestRunner
		{
		private:
			const std::string cBaseDirectory = "./.unit_tests_temp/";
			std::vector<const TestObjectBase*> mUnitTestObjects;
			const MPI_Comm cComm;
			
		public:
			UnitTestRunner(const MPI_Comm& aComm) : cComm(aComm) { }
			
			void RunAllTests();
			void AddTestingObject(TestObjectBase* aTestingObject);
			
			void DeleteUnitTestObjects()
			{
				for (int i = 0; i < mUnitTestObjects.size(); i++)
				{
					delete mUnitTestObjects[i];
				}
			}
		};
	}
}
