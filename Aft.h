
class Aft;

#pragma once
#include <vector>

#include "Epetra_Vector.h"

// #include <fftw3.h>

#include "Misc.h"
#include "WaveInfo.h"
#include "CrsMatrixRow.h"
#include "DofInfo.h"
#include "DofHBMInfo.h"

class Aft
{
private:
    const std::vector<int> cPhysicalDofs;
    const std::vector<DofHBMInfo> cHBMDofs;
    
    std::vector<double> mCosValues;
    std::vector<double> mSinValues;
    const int cTimePointCount;
    // on this process
    const int cPhysicalDofCount;
    
//     const int cFftwTotalSize;
    // decided on runtime
    int mFftwBigTotalSize = -1;
    
//     fftw_complex* const cFreqDomainFftwData;
//     fftw_complex* const cTimeDomainFftwData;
    
    // for an entire row of jacobian (which will have size of the whole problem, not just for one process)
    // can't be exact because it will be allocated once we know how many elements there are in on row of the matrix (it's a sparse matrix)
//     fftw_complex* mFreqDomainFftwDataBig = nullptr;
//     fftw_complex* mTimeDomainFftwDataBig = nullptr;
    
//     const fftw_plan cFreqToTimePlan;
//     const fftw_plan cTimeToFreqPlan;
//     // for jacobian
//     fftw_plan mTimeToFreqPlanBig;
//     bool mIsBigPlanCreated = false;
    
public:
    Aft(int aTimePointCount, const std::vector<DofInfo>& aPhysicalDofs, const std::vector<DofHBMInfo>& aHBMDofs);
    Aft(int aTimePointCount, const std::vector<int>& aPhysicalDofs, const std::vector<DofHBMInfo>& aHBMDofs);
    ~Aft()
    {
//         delete[] cFreqDomainFftwData;
//         delete[] cTimeDomainFftwData;
//         if (mFreqDomainFftwDataBig != nullptr) delete[] mFreqDomainFftwDataBig; 
//         if (mTimeDomainFftwDataBig != nullptr) delete[] mTimeDomainFftwDataBig;
//         fftw_destroy_plan(cFreqToTimePlan);
//         fftw_destroy_plan(cTimeToFreqPlan);
//         if (mIsBigPlanCreated) fftw_destroy_plan(mTimeToFreqPlanBig);
    }
    
    int GetTimePointCount() const { return cTimePointCount; }
    // assuming that the variables in the aX (vector in frequency domain) are grouped by physical dofs, i.e. variables
    // for different harmonics for a same dof are together
    // We take MultiVector as the argument, but we expect it to always only have one vector not more
    std::vector<double> FreqToTime(const Epetra_MultiVector& aX, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex) const;
//     std::vector<double> FreqToTimeFftw(const Epetra_MultiVector& aX, const std::vector<WaveInfo>& aHarmonics) const;
    std::vector<double> TimeToFreq(const Epetra_MultiVector& aF, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex) const;
//     std::vector<double> TimeToFreqFftw(const std::vector<double>& aF, const std::vector<WaveInfo>& aHarmonics) const;
    // aMaxSize - maximum size of the aJacRowAllTimes vector that will ever be passed here
//     std::vector<double> TimeToFreqFftwBig(const std::vector<double>& aJacRowAllTimes, const std::vector<WaveInfo>& aHarmonics, int aScaleHarmonic, int aMaxSize);
    std::vector<CrsMatrixRow> TimeToFreq(const Epetra_CrsMatrix& aJac, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex) const;
    // variant returning transformation of just one row (still multiple rows in frequency domain, but much fewer) (to save memory, you can add the results row by row)
    std::vector<CrsMatrixRow> TimeToFreq(const Epetra_CrsMatrix& aJac, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex, int aLocalMatrixRowIndex) const;
	// attempt for faster version
	void TimeToFreq2(const Epetra_CrsMatrix& aJac, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex, int aLocalMatrixRowIndex, std::vector<CrsMatrixRow>& aResult) const;
	// Should be faster, accesses the source matrix elements directly and inserts the transformed values directly into the target matrix
	void TimeToFreqDirect(const Epetra_CrsMatrix& aJacTime, Epetra_CrsMatrix& aJacFreq, const std::vector<WaveInfo>& aHarmonics, int aTimePointIndex, int aLocalMatrixRowIndex);
    
private:
    double GetHarmonicValue(int aTimePointIndex, const WaveInfo& aWaveInfo) const;
    // dynamic allocation for the "big" transform (jacobian rows)
//     void AllocateBigPlan(int aMaxSize);
    std::vector<int> DofInfoToDofId(const std::vector<DofInfo>& aDofInfo) const;
};

