

namespace ParHBM
{
	class Exception;
}

#pragma once 
#include <string>
#include <numeric>
#include <iostream>

#define PARHBM_EXCEPTION_MEMBER(message) ParHBM::Exception(#message, -1, __func__, typeid(*this).name());
#define PARHBM_EXCEPTION_GLOBAL(message) ParHBM::Exception(#message, -1, __func__, "--- not member of a class ---");

namespace ParHBM
{
	class Exception
	{
	public:
		const std::string Message;
		const int Code;
		const std::string FunctionName;
		const std::string ClassName;
		
		Exception(const std::string& aMessage, const int aCode = 0, const std::string& aFunctionName = "", const std::string& aClassName = "");
		
		friend std::ostream& operator<<(std::ostream& aInStream, const Exception& aEx);
	};
}
