
class Time;

#pragma once
#include <chrono>

#include "mpi.h"

class Time
{
private:
    std::chrono::steady_clock::time_point mCurrentStart = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point mLastReportTime = std::chrono::steady_clock::now();
    
public:
    void Start();
    double Stop(const bool& aPrint = false);
    double Elapsed();
    // if the given duration has elapsed then display the message and restart itself (call Start()). Otherwise do nothing.
    void Report(const std::string& aMessage, double aDurationMS, bool aUpdateLastReportTime = true);
	
	static time_t CurrentTime();
	static std::string CurrentUTCTimeString(const time_t& aTime);
	// the aTime value will be taken from rank 0 and broadcasted to all other ranks, then the same value will be converted to a string on all ranks
	static std::string CurrentUTCTimeStrintMPI(time_t aTime, const MPI_Comm& aComm);
};
