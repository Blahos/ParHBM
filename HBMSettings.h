
class HBMSettings;

#pragma once
#include <vector>

#include "WaveInfo.h"

class HBMSettings
{
public:
    std::vector<WaveInfo> HarmonicInfo;
    std::vector<int> HarmonicWaves;
    int IntegrationPointCount;
    double StartFrequency;
    double EndFrequency;    
};
