
class FE;

#pragma once

#include <vector>
#include <fstream>
#include <algorithm>
#include <set>
#include <functional>

#include "Epetra_SerialDenseMatrix.h"
#include "Epetra_FECrsMatrix.h"
#include "Epetra_FEVector.h"
#include "Epetra_SerialDenseVector.h"
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"
#include "Epetra_Import.h"

#include "libmesh/libmesh.h"
#include "libmesh/fe_base.h"
#include "libmesh/mesh.h"
#include "libmesh/distributed_mesh.h"
#include "libmesh/elem.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/equation_systems.h"
#include "libmesh/dof_map.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/zero_function.h"

#include "PhysicalParameters.h"
#include "DofInfo.h"

class FE
{
private:
    const double cLengthScale;
    // number of dofs of the whole problem
    int mDofCount = -1;
    // number of dofs for this process
    int mDofCountLocal = -1;
    bool mMeshLoaded = false;
    // Standalone entities, smart pointers, these members are assigned with created values (new ...)
    libMesh::UniquePtr<libMesh::FEBase> mFE = libMesh::UniquePtr<libMesh::FEBase>(nullptr);
    libMesh::UniquePtr<libMesh::DistributedMesh> mMesh = libMesh::UniquePtr<libMesh::DistributedMesh>(nullptr);
	// dof info (and indices) for this mpi rank (indices are also in the info, but we store them once more separately as a shortcut 
	// for optimisation purposes
    std::vector<DofInfo> mDofInfo;
	std::vector<int> mDofIndices;
	// flag if the dofs in dof info are in order n n+1 n+2 n+3 ... (for whatever n)
	// set in constructor
	bool mAreDofsContiguous = false;
    // We only have the dof ids here, no need for anything else. 
    // It is not easy to get all the other meta data for the overlapping nodes
    std::vector<int> mDofOverlappingIndices;
	// dofs on all elements that are on this rank or dofs on elements
	// that coincide with nodes that are on this rank
	// will be used for constructing column maps for matrices
    std::vector<int> mDofSemilocalIndices;
	
    // preallocated nonlinear stiffness, ready to be filled. This can be just filled and returned so we don't have to call fillcomplete every time
    Teuchos::RCP<Epetra_FECrsMatrix> mNonlinStiffnessInner;
    bool mIsNonlinStiffInnerAllocated = false;
    
    // Non pointer members
    libMesh::QGauss mQRule;
    
    const std::string cSystemName = "TheSystem";
    const std::string cVarXName = "VarX";
    const std::string cVarYName = "VarY";
    const std::string cVarZName = "VarZ";
    
	// list of all constrained dof ids (on all ranks)
	// filled in constructor, don't modify later!
	std::vector<int> mConstrainedDofsUnique;
    
    // Raw pointers that do not need to be destroyed because they are only shortcuts to some members of other memebrs (mesh, fe, etc.)
    const std::vector<std::vector<libMesh::Real>>*          mPhi  = nullptr;
    const std::vector<libMesh::Real>*                       mJxW  = nullptr;
    const std::vector<libMesh::Point>*                      mXYZ  = nullptr;
    const std::vector<std::vector<libMesh::RealGradient>>*  mDPhi = nullptr;
    
    // Raw pointers that need to be destroyed in the destructor
    const Epetra_Map* mStandardDofMap = nullptr;
    const Epetra_Map* mStandardDofMapOverlapping = nullptr;
    // this one was a managed pointer (UniquePtr) before, but that was for SOME FUCKING REASON causing seg fault with PETSc
    libMesh::EquationSystems* mEquationSystems = nullptr;
    
    // arbitrary test value, for various debugging and testing purposes
    mutable int mTestValue = 0;
    
public:
    FE(double aLengthScale = 1.0, int aQuadratureOrder = 4);
    ~FE();
    
    void LoadMesh(const std::string& aFileName, const libMesh::Parallel::Communicator& aLibmeshComm, const Epetra_Comm& aEpetraComm);
    
    Teuchos::RCP<Epetra_FECrsMatrix> CreateStiffnessMatrix(const PhysicalParameters& aParams, std::vector<Epetra_SerialDenseMatrix>* aLocalMatrices = nullptr) const;
    // Returns pointer to the class member matrix. Do not make any changes to that matrix outside of this class, don't ever delete the pointer
    Teuchos::RCP<Epetra_FECrsMatrix> CreateNonlinearStiffnessMatrix(const Epetra_Vector& aU, const PhysicalParameters& aParams, bool aUseImport = true);
    Teuchos::RCP<Epetra_FECrsMatrix> CreateMassMatrix(const PhysicalParameters& aParams, std::vector<Epetra_SerialDenseMatrix>* aLocalMatrices = nullptr) const;
    Teuchos::RCP<Epetra_MultiVector> CreateRHS(const PhysicalParameters& aParams, const std::function<Epetra_SerialDenseVector(double, double, double)>& aBodyForce) const;
//     Teuchos::RCP<Epetra_MultiVector> CreateNonlinearStiffnessRHS(const Epetra_Vector& aU, const PhysicalParameters& aParams) const;
    Teuchos::RCP<Epetra_MultiVector> CreateNonlinearStiffnessRHS(const Epetra_Vector& aU, const PhysicalParameters& aParams, bool aUseImport = true) const;
    
    const Epetra_Map& GetRowMap() const;
    const Epetra_Map& GetRowMapOverlapping() const;
    int DofCount() const;
    int DofCountLocal() const;
    const std::vector<DofInfo>& GetDofInfo() const;
    const std::vector<int>& GetDofOverlappingIndices() const;
	// dofs on all elements that are on this rank or dofs on elements
	// that coincide with nodes that are on this rank
	// will be used for constructing column maps for matrices
	const std::vector<int>& GetDofSemilocalIndices() const;
    
    void SetTestValue(int aTestValue) const { mTestValue = aTestValue; }
    
	// zeroes out the elements in the vector that correspond to dirichlet dofs (in physical dof system)
	// assumes a vector with distribution (map) equivalent to the mDofInfo array 
	void ZeroOutDirichletDofs(Epetra_MultiVector& aVec) const;
    
private:
    Epetra_SerialDenseMatrix CreateLocalStiffnessMatrix(const libMesh::Elem& aElem, const Epetra_SerialDenseMatrix& aCMatrix, std::vector<DofInfo>& aDofInfo) const;
	
    Epetra_SerialDenseMatrix CreateLocalNonlinearStiffnessMatrix(const Epetra_Vector& aUOverlapping, const libMesh::Elem& aElem, const Epetra_SerialDenseMatrix& aCMatrix, std::vector<DofInfo>& aDofInfo) const;
	
    Epetra_SerialDenseMatrix CreateLocalMassMatrix(const libMesh::Elem& aElem, const PhysicalParameters& aParams, std::vector<DofInfo>& aDofInfo) const;
    Epetra_SerialDenseVector CreateLocalRHS(const libMesh::Elem& aElem, const PhysicalParameters& aParams, const std::function<Epetra_SerialDenseVector(double, double, double)>& aBodyForce, std::vector<DofInfo>& aDofInfo) const;
    Epetra_SerialDenseVector CreateLocalNonlinearStiffnessRHS(const Epetra_Vector& aUOverlapping, const libMesh::Elem& aElem, const Epetra_SerialDenseMatrix& aCMatrix, std::vector<DofInfo>& aDofInfo) const;    
    
    void FillNonlinearStiffnessMatrix(Teuchos::RCP<Epetra_FECrsMatrix> aMatrix, const Epetra_Vector& aU, const PhysicalParameters& aParams, std::function<void(Teuchos::RCP<Epetra_FECrsMatrix>, int, int, double*, int*)> aFillFunction, bool aUseImport) const;
    
    Epetra_SerialDenseMatrix CreateCMatrix(const PhysicalParameters& aParams) const;
    void FillDofData(const libMesh::Elem& aElem, std::vector<DofInfo>& aDofInfo, std::vector<int>& aDofNode) const;
	// checks whether a global dof index is local to this rank
	bool IsDofLocal(int aGlobalDofIndex) const;
    std::vector<DofInfo> DofInfoForNode(const libMesh::Node& aNode) const;
//     void FilterOutBCDofs(const std::vector<libMesh::dof_id_type>& aGlobalDofIndices, std::vector<int>& aNonBCDofsGlobal, std::vector<int>& aNonBCDofsLocal) const;
    Epetra_SerialDenseMatrix Vectorise(const Epetra_SerialDenseMatrix& aSourceMatrix) const;
    void Copy3x3Matrix(Epetra_SerialDenseMatrix& aTargetMatrix, const Epetra_SerialDenseMatrix& aSourceMatrix, const int& aTargetRow, const int& aTargetCol, const int aSourceRow = 0, const int aSourceCol = 0) const;
    Epetra_SerialDenseMatrix GetTranspose(const Epetra_SerialDenseMatrix& aSourceMatrix) const;
	// adds a transpose of each 3x3 matrix in the block to itself
	void AddTranspose3x3(Epetra_SerialDenseMatrix& aSourceMatrix) const;
    // first matrix is transposed, second is not
    Epetra_SerialDenseMatrix MultiplyTN(const Epetra_SerialDenseMatrix& aMat1, const Epetra_SerialDenseMatrix& aMat2) const;
	
	static int CheckQOrderValidity(int aOrder);
	static void CheckQOrderSuitability(const libMesh::QGauss& aQRule, int aMeshElementOrder);
};
