
#pragma once
#include "string"

enum class WaveType
{
    DC, Cos, Sin
};
