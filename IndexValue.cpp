
#include "IndexValue.h"

#include <string>
#include <iostream>
#include <limits>

IndexValue::IndexValue(int aSize)
	: mIndices(aSize), mValues(aSize)
{
}
IndexValue::IndexValue(const IndexValue& aOther)
	: mIndices(aOther.mIndices), mValues(aOther.mValues)
{
}
IndexValue::~IndexValue()
{
}

void IndexValue::Init(int aSize)
{
	mIndices.Init(aSize);
	mValues.Init(aSize);
}
void IndexValue::PutScalarValue(double aValue) const
{
	if (mValues.Size() <= 0) return;
	
	double* mValuesArray = mValues.Array();
	if (mValuesArray == nullptr) throw "Arrays are not initialised!";
	
	for (int i = 0; i < mValues.Size(); i++)
	{
		mValuesArray[i] = aValue;
	}
}
void IndexValue::AddScalarValue(double aValue) const
{
	if (mValues.Size() <= 0) return;
	
	double* mValuesArray = mValues.Array();
	if (mValuesArray == nullptr) throw "Arrays are not initialised!";
	
	for (int i = 0; i < mValues.Size(); i++)
	{
		mValuesArray[i] += aValue;
	}
}
void IndexValue::ScaleValues(double aFactor) const
{
	if (mValues.Size() <= 0) return;
	
	double* mValuesArray = mValues.Array();
	if (mValuesArray == nullptr) throw "Arrays are not initialised!";
	
	for (int i = 0; i < mValues.Size(); i++)
	{
		mValuesArray[i] *= aFactor;
	}
}

void IndexValue::Merge(const IndexValue& aInput1, const IndexValue& aInput2)
{
	if (this == &aInput1) throw "First  input points to the same address as this!";
	if (this == &aInput2) throw "Second input points to the same address as this!";
	
	int lInput1Size = aInput1.Size();
	int lInput2Size = aInput2.Size();
		
	const int* const lInput1Indices = aInput1.Indices();
	const int* const lInput2Indices = aInput2.Indices();
	
	const double* const lInput1Values = aInput1.Values();
	const double* const lInput2Values = aInput2.Values();
	
	Init(lInput1Size + lInput2Size);
	
	int* const lMyIndices = Indices();
	double* const lMyValues = Values();
	
	int lPos1 = 0;
	int lPos2 = 0;
	
	int lPosThis = 0;
	
	while (lPos1 < lInput1Size || lPos2 < lInput2Size)
	{
		int lCurrentIndex = std::numeric_limits<int>::max();
		if (lPos1 < lInput1Size && lInput1Indices[lPos1] < lCurrentIndex) lCurrentIndex = lInput1Indices[lPos1];
		if (lPos2 < lInput2Size && lInput2Indices[lPos2] < lCurrentIndex) lCurrentIndex = lInput2Indices[lPos2];
		
		lMyIndices[lPosThis] = lCurrentIndex;
		lMyValues[lPosThis] = 0.0;
		
		if (lPos1 < lInput1Size && lInput1Indices[lPos1] == lCurrentIndex)
		{
			lMyValues[lPosThis] += lInput1Values[lPos1];
			lPos1++;
		}
		if (lPos2 < lInput2Size && lInput2Indices[lPos2] == lCurrentIndex)
		{
			lMyValues[lPosThis] += lInput2Values[lPos2];
			lPos2++;
		}
		lPosThis++;
	}
	
	// truncate this to the effectively used size
	mIndices.LimitSize(lPosThis);
	mValues.LimitSize(lPosThis);
}

IndexValue& IndexValue::operator=(const IndexValue& aOther)
{
    if (this == &aOther) return *this;
	
	mIndices = aOther.mIndices;
	mValues = aOther.mValues;
	
    return *this;
}
