
#include <iostream>

#include "MKLPDSSData.h"
#include "Functions.h"

#include "mpi.h"
#include "mkl_cluster_sparse_solver.h"

void Solve(const MKLPDSSData& aData, double* const aSolution)
{
	long long lPt[64];
	int lIParm[64];
	
	for (int i = 0; i < 64; i++) lPt[i] = 0;
	for (int i = 0; i < 64; i++) lIParm[i] = 0;
	
	lIParm[0] = 1; // don't use default values
	lIParm[1] = 10; // distributed dissection of the matrix
// 	lIParm[9] = 13; 
	lIParm[17] = 0; // report number of nonzeros
// 	lIParm[26] = 1; // matrix indices checking
	lIParm[34] = 0; // one based indexing
// 	lIParm[36] = 0; // matrix storage format
	lIParm[39] = 2; // distributed matrix, rhs and solution (all with same row distribution)
	lIParm[40] = aData.FirstRow; // first row for this process
	lIParm[41] = aData.LastRow; // last row for this process
		
	int lMatrixType = 1;
	int lPhase = 13;
	
	int lEqNum = aData.NumEq;
	int lMessageLevel = 0;
	int lNRHS = 1;
	int lFortranMPIComm = MPI_Comm_c2f(MPI_COMM_WORLD);
	
	int lError = 0;
	int lDummy1 = 1;
	int lDummy0 = 0;
	
	double* lRHSCopy = new double[aData.RHSSize];
	for (int i = 0; i < aData.RHSSize; i++) lRHSCopy[i] = aData.RHS[i];
	
	std::cout << "Calling the cluster_sparse_solver() ..." << std::endl;
	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, aData.Values, aData.IndexOffset, aData.ColumnIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, aSolution, &lFortranMPIComm, &lError);
		
	std::cout << "Calling the cluster_sparse_solver() ... finished" << std::endl;
	
	if (lError != 0) throw "MKLPDSS (solve phase) returned error " + std::to_string(lError) + "!";
	
	// release
	lPhase = -1;
	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, aData.Values, aData.IndexOffset, aData.ColumnIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, aSolution, &lFortranMPIComm, &lError);
	
	if (lError != 0) throw "MKLPDSS (free phase) returned error " + std::to_string(lError) + "!";
	
	delete[] lRHSCopy;
}
