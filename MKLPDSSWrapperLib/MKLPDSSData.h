
struct MKLPDSSData;

#pragma once
#include <fstream>
#include <string>
#include <iomanip>
#include <limits>

#include "Functions.h"
#include "mpi.h"

struct MKLPDSSData
{
public:
	int NumEq;
	int FirstRow;
	int LastRow;
	int* IndexOffset;
	int IndexOffsetSize;
	int* ColumnIndices;
	int ColumnIndicesSize;
	double* Values;
	int ValuesSize;
	double* RHS;
	int RHSSize;
	int SolutionSize;
	
	void ExportToFile(const std::string& aFilePath, const MPI_Comm& aComm)
	{
		int lRank;
		MPI_Comm_rank(aComm, &lRank);
		
		std::ofstream lFile(aFilePath + "_rank" + std::to_string(lRank));
		lFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
		
		lFile << NumEq << " ";
		lFile << FirstRow << " ";
		lFile << LastRow << " ";
		
		ExportArray(lFile, IndexOffset, IndexOffsetSize);
		ExportArray(lFile, ColumnIndices, ColumnIndicesSize);
		ExportArray(lFile, Values, ValuesSize);
		ExportArray(lFile, RHS, RHSSize);
		
		lFile << SolutionSize << " ";
	}
	
	static MKLPDSSData ImportFromFile(const std::string& aFilePath, const MPI_Comm& aComm)
	{
		MKLPDSSData lReturnStruct;
		
		int lRank;
		MPI_Comm_rank(aComm, &lRank);
		
		std::ifstream lFile(aFilePath + "_rank" + std::to_string(lRank));
		
		lFile >> lReturnStruct.NumEq;
		lFile >> lReturnStruct.FirstRow;
		lFile >> lReturnStruct.LastRow;
		
		ImportArray(lFile, lReturnStruct.IndexOffset, lReturnStruct.IndexOffsetSize);
		ImportArray(lFile, lReturnStruct.ColumnIndices, lReturnStruct.ColumnIndicesSize);
		ImportArray(lFile, lReturnStruct.Values, lReturnStruct.ValuesSize);
		ImportArray(lFile, lReturnStruct.RHS, lReturnStruct.RHSSize);
		
		lFile >> lReturnStruct.SolutionSize;
		
		return lReturnStruct;
	}

};
