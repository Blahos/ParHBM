
#pragma once
#include "MKLPDSSData.h"

void Solve(const MKLPDSSData& aData, double* const aSolution);
