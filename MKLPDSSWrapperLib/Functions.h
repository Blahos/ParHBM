
#pragma once
#include <limits>
#include <iomanip>

template <class T>
void ExportArray(std::ostream& aStream, T* const aData, const int aSize)
{
	aStream << aSize << " ";
	aStream << std::setprecision(std::numeric_limits<double>::digits10 + 1);
	
	for (int i = 0; i < aSize; i++)
	{
		aStream << aData[i] << " ";
	}
}

template <class T>
void ImportArray(std::istream& aStream, T*& aData, int& aSize)
{	
	aStream >> aSize;
	
	aData = new T[aSize];
	
	for (int i = 0; i < aSize; i++)
	{
		aStream >> aData[i];
	}
}

