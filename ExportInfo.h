
struct ExportInfo;

#pragma once
#include "DoubleRange.h"
#include "Multiplicity.h"

struct ExportInfo
{
public:
    DoubleRange Range;
    Multiplicity Mult;
    
    ExportInfo(const DoubleRange& aRange, const Multiplicity& aMult) : Range(aRange), Mult(aMult) { }
};
