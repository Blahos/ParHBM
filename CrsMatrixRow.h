
class CrsMatrixRow;

#pragma once
#include <iostream>

#include "IndexValue.h"

class CrsMatrixRow
{
private:
	IndexValue mData;
public:
    int GlobalRowIndex = -1;
    
public:
    CrsMatrixRow() { }
    CrsMatrixRow(int aSize);
    CrsMatrixRow(const CrsMatrixRow& aOther);
    ~CrsMatrixRow();
	
    void Init(int aSize);
	int Size() const { return mData.Size(); }
	IndexValue* Data() { return &mData; }
	const IndexValue& Data() const { return mData; }
	bool AreIndicesSorted() const;
    
    CrsMatrixRow& operator=(const CrsMatrixRow& aOther);
    
};
