
#include "Config.h"
#include "Misc.h"
#include "Functions.h"
#include "Functions.hpp"
// #include "Nonlinearities/NonlinearitiesFactory.h"
// #include "Aft/AftFactory.h"
// #include "DSBuilder/DSBuilderFactory.h"

// tasks to perform
const std::string Config::KEY_DOEIG         = "doeig";
const std::string Config::KEY_DOCONT        = "docont";
const std::string Config::KEY_DOFREQ        = "dofreq";
const std::string Config::KEY_DOMATRIXOUT	= "domatrixout";

// other options
const std::string Config::KEY_EXC           = "exc";
const std::string Config::KEY_DOF           = "dof";
const std::string Config::KEY_HARM          = "harm";
const std::string Config::KEY_HARMSELECT    = "harmselect";
const std::string Config::KEY_HARMLIST      = "harmlist";
const std::string Config::KEY_NONLIN        = "nonlin";
const std::string Config::KEY_START         = "start";
const std::string Config::KEY_END           = "end";
const std::string Config::KEY_INT           = "int";
const std::string Config::KEY_WHOLE         = "whole";
const std::string Config::KEY_STEPSC        = "stepscont";
const std::string Config::KEY_STEPSN        = "stepsnewt";
const std::string Config::KEY_NORM          = "normnewt";
const std::string Config::KEY_STEPINI       = "stepinit";
const std::string Config::KEY_STEPMIN       = "stepmin";
const std::string Config::KEY_STEPMAX       = "stepmax";
const std::string Config::KEY_PRED          = "pred";
const std::string Config::KEY_SCALEARC      = "scalearc";
const std::string Config::KEY_AFT           = "aft";
const std::string Config::KEY_DSBUILD       = "dsbuild";
const std::string Config::KEY_SOLVER        = "solver";
const std::string Config::KEY_PRECOND       = "precond";
const std::string Config::KEY_MESH          = "mesh";
const std::string Config::KEY_YOUNG         = "young";
const std::string Config::KEY_POISSON       = "poisson";
const std::string Config::KEY_DENSITY       = "density";
const std::string Config::KEY_SCALEL        = "scalel";
const std::string Config::KEY_SCALEW        = "scalew";
const std::string Config::KEY_SCALET        = "scalet";
const std::string Config::KEY_RAYLEIGHK     = "rayleighk";
const std::string Config::KEY_RAYLEIGHM     = "rayleighm";
const std::string Config::KEY_USEGREEN      = "usegreen";
const std::string Config::KEY_EIGNUM        = "eignum";
const std::string Config::KEY_FREQLIST		= "freqlist";
const std::string Config::KEY_MATOUTGLOBAL	= "matoutglobal";
const std::string Config::KEY_MATOUTLOCAL	= "matoutlocal";
const std::string Config::KEY_USEPREV		= "useprev";
const std::string Config::KEY_QUAD			= "qorder";

const std::string Config::KEY_TEST          = "test";

const std::map<std::string, std::function<std::vector<std::string>(const std::string&)>> Config::C_ConfigDefinition = 
{
	// tasks to perform
    { Config::KEY_DOEIG,        RET_ONE_STR("0")        },
    { Config::KEY_DOCONT,       RET_ONE_STR("0")        },
    { Config::KEY_DOFREQ,       RET_ONE_STR("0")        },
    { Config::KEY_DOMATRIXOUT,	RET_ONE_STR("0")		},
    
    // other options
    { Config::KEY_EXC,          RET_ONE_STR("") 		},
    { Config::KEY_HARM,         RET_ONE_STR("1")        },
    { Config::KEY_HARMSELECT,   RET_ONE_STR("1")        },
    { Config::KEY_HARMLIST,     RET_ONE_STR("1") 		},
    { Config::KEY_START,        RET_ONE_STR("0.1")		},
    { Config::KEY_END,          RET_ONE_STR("10")		},
    { Config::KEY_INT,          RET_ONE_STR("4")        },
    { Config::KEY_STEPSC,       RET_ONE_STR("1000")     },
    { Config::KEY_STEPSN,       RET_ONE_STR("10")       },
    { Config::KEY_NORM,         RET_ONE_STR("1e-6")     },
    { Config::KEY_STEPINI,      RET_ONE_STR("1e-4")     },
    { Config::KEY_STEPMIN,      RET_ONE_STR("1e-6")     },
    { Config::KEY_STEPMAX,      RET_ONE_STR("1e-1")     },
    { Config::KEY_PRED,         RET_ONE_STR("Tangent")  },
    { Config::KEY_SCALEARC,     RET_ONE_STR("1")        },
    { Config::KEY_SOLVER,       RET_ONE_STR("MUMPS")	},
    { Config::KEY_PRECOND,      RET_ONE_STR("") 		},
    { Config::KEY_MESH,         THROW_NO_DEFAULT        },
    { Config::KEY_YOUNG,        THROW_NO_DEFAULT        },
    { Config::KEY_POISSON,      THROW_NO_DEFAULT        },
    { Config::KEY_DENSITY,      THROW_NO_DEFAULT        },
    { Config::KEY_SCALEL,       RET_ONE_STR("1.0")      },
    { Config::KEY_SCALEW,       RET_ONE_STR("1.0")      },
    { Config::KEY_SCALET,       RET_ONE_STR("1.0")      },
    { Config::KEY_RAYLEIGHK,    RET_ONE_STR("0.0")		},
    { Config::KEY_RAYLEIGHM,    RET_ONE_STR("0.0")		},
    { Config::KEY_USEGREEN,     RET_ONE_STR("0")        },
    { Config::KEY_EIGNUM,       RET_ONE_STR("0")        },
    { Config::KEY_FREQLIST,		RET_EMPTY_STR_VEC		},
	{ Config::KEY_MATOUTGLOBAL,	RET_ONE_STR("1")		},
	{ Config::KEY_MATOUTLOCAL,	RET_ONE_STR("1")		},
	{ Config::KEY_USEPREV,		RET_ONE_STR("0")		},
	{ Config::KEY_QUAD,			RET_ONE_STR("4")		},
	
    { Config::KEY_TEST,         RET_ONE_STR("0")		},
};

Config Config::LoadConfig(const std::string& aConfigFilePath)
{
    std::map<std::string, std::vector<std::string>> lMap = ParseGeneralConfigFile(aConfigFilePath);
    
    std::map<std::string, std::vector<std::string>> lConfigMap;
    
    for (auto nKeyValue : C_ConfigDefinition)
    {
        std::string lKey = nKeyValue.first;
        
        bool lIsInLoadedMap = lMap.find(lKey) != lMap.end();
        
        if (lIsInLoadedMap) lConfigMap[lKey] = lMap[lKey];
        else 
        {
            std::cout << "Key \"" + lKey + "\" not specified in \"" + aConfigFilePath + "\", attempting to use default" << std::endl;
            // get the default value (might raise an exception if the default value does not exist)
            lConfigMap[lKey] = nKeyValue.second(lKey);
            std::cout << "Default value used for the key \"" + lKey + "\": " << std::endl;
            for (int i = 0; i < lConfigMap[lKey].size(); i++)
            {
                std::cout << "\"" << lConfigMap[lKey][i] << "\"" << std::endl;
            }
            std::cout << std::endl;
        }
    }
    
    // list loaded keys that will be unused
    // this is just to inform the user that they configuration file contains some unused definitions
    std::cout << "Unused keys in file \"" + aConfigFilePath + "\": " << std::endl;
    bool lAnyUnused = false;
    for (auto nKeyValue : lMap)
    {
        std::string lKey = nKeyValue.first;
        
        bool lIsInUsedMap = lConfigMap.find(lKey) != lConfigMap.end();
        
        if (!lIsInUsedMap) 
        {
            std::cout << lKey << std::endl;
            lAnyUnused = true;
        }
    }
    if (!lAnyUnused)
        std::cout << "--- all provided keys will be used ---" << std::endl;
    
    Config lReturnConfig;
    
    // get the config path and file name
    int lSeparatorPos = aConfigFilePath.size() - 1;
    while (lSeparatorPos > 0 && aConfigFilePath[lSeparatorPos] != '/') lSeparatorPos--;
    
    if (lSeparatorPos == 0)
    {
        lReturnConfig.ConfigFilePath = ".";
        lReturnConfig.ConfigFileName = aConfigFilePath;
    }
    else
    {
        lReturnConfig.ConfigFilePath = aConfigFilePath.substr(0, lSeparatorPos + 1);
        lReturnConfig.ConfigFileName = aConfigFilePath.substr(lSeparatorPos + 1, aConfigFilePath.size() - lSeparatorPos - 1);
    }
    
    // parse loaded key-value pairs (value is a vector of strings here
    
    std::vector<std::string> lTempLines;
//     int lTempLineInd = 0;
    
    // matrices
//     lTempLines = lConfigMap[KEY_MASS];
//     lReturnConfig.MassMatrices = ParseMatrixDefinition(lTempLines, "Mass");
//     lTempLines = lConfigMap[KEY_DAMP];
//     lReturnConfig.DampingMatrices = ParseMatrixDefinition(lTempLines, "Damping");
//     lTempLines = lConfigMap[KEY_STIFF];
//     lReturnConfig.StiffnessMatrices = ParseMatrixDefinition(lTempLines, "Stiffness");
    
	////////
	// Tasks
	////////
	
    // do eigenfrequencies?
    lReturnConfig.DoEigenfrequencies = ParseOneLineBool(lConfigMap[KEY_DOEIG], "Do eigenfrequencies");
    
    // do continuation?
    lReturnConfig.DoContinuation = ParseOneLineBool(lConfigMap[KEY_DOCONT], "Do continuation");
	
	// do frequency list?
	lReturnConfig.DoFrequencies = ParseOneLineBool(lConfigMap[KEY_DOFREQ], "Do frequencies");
	
	// do matrix output?
	lReturnConfig.DoMatrixOutput = ParseOneLineBool(lConfigMap[KEY_DOMATRIXOUT], "Do matrix output");
	
    // excitation force
    lTempLines = lConfigMap[KEY_EXC];
    if (lTempLines.size() != 1) throw "Excitation force definition must have exactly one line!";
    lReturnConfig.ExcitationForceFile = lTempLines[0];
    
    // nonlinearities files
//     lTempLines = lConfigMap[KEY_NONLIN];
//     if (lTempLines.size() % 2 != 0) throw "Nonlinearities defininition must have an even number of lines!";
//     lTempLineInd = 0;
//     while (lTempLineInd < lTempLines.size())
//     {
//         NonlinearityDefinition lNewDef;
//         lNewDef.File = lTempLines[lTempLineInd++];
//         lNewDef.Type = lTempLines[lTempLineInd++];
//         CheckString(lNewDef.Type, C_NonlinearitiesFactory, "nonlinearity type");
//         lReturnConfig.Nonlinearities.push_back(lNewDef);
//     }
    
    // number of physical dofs
//     lReturnConfig.DofCount = ParseOneLineInt(lConfigMap[KEY_DOF], "Dof count");
    
    // number of harmonics
    lReturnConfig.HarmonicWaveCount = ParseOneLineInt(lConfigMap[KEY_HARM], "Harmonic count");
    
    if (lReturnConfig.HarmonicWaveCount <= 0) throw "Number of harmonic waves (key \"" + KEY_HARM + ")\" must be positive!";
    
    lReturnConfig.HarmonicWaveSelected = ParseOneLineBool(lConfigMap[KEY_HARMSELECT], "Harmonics selected");
    
    // Load list of selected harmonics
    if (lReturnConfig.HarmonicWaveSelected)
    {
        lReturnConfig.HarmonicWaveList = ParseHarmonicWaveList(lConfigMap[KEY_HARMLIST], lReturnConfig.HarmonicWaveCount);
    }
    else // set selected harmonics the "simple way" (from zero to count - 1)
    {
        for (int i = 0; i < lReturnConfig.HarmonicWaveCount; i++)
            lReturnConfig.HarmonicWaveList.push_back(i);
    }
    
    // start frequency
    lReturnConfig.FrequencyStart = ParseOneLineDouble(lConfigMap[KEY_START], "Start frequency");
    
    // end frequency
    lReturnConfig.FrequencyEnd = ParseOneLineDouble(lConfigMap[KEY_END], "End frequency");
    
    // integration point count
    lReturnConfig.IntPointCount = ParseOneLineInt(lConfigMap[KEY_INT], "Integration point count");
    
    // save whole solutions
//     lReturnConfig.SaveWholeSolutions = ParseOneLineBool(lConfigMap[KEY_WHOLE], "Save whole solutions");
    
    // continuation settings
    
    // max newton steps
    lReturnConfig.MaxStepsNewton = ParseOneLineInt(lConfigMap[KEY_STEPSN], "Newton step count");
    
    // max continuation steps
    lReturnConfig.MaxStepsContinuation = ParseOneLineInt(lConfigMap[KEY_STEPSC], "Continuation step count");
    
    // newton norm
    lReturnConfig.NewtonNormF = ParseOneLineDouble(lConfigMap[KEY_NORM], "Newton tolerance norm");
    
    // init step
    lReturnConfig.StepSizeInitial = ParseOneLineDouble(lConfigMap[KEY_STEPINI], "Initial conntinuation step");
    
    // min step
    lReturnConfig.StepSizeMin = ParseOneLineDouble(lConfigMap[KEY_STEPMIN], "Min conntinuation step");
    
    // max step
    lReturnConfig.StepSizeMax = ParseOneLineDouble(lConfigMap[KEY_STEPMAX], "Max conntinuation step");
    
    // predictor
    lTempLines = lConfigMap[KEY_PRED];
    if (lTempLines.size() != 1) throw "Predictor definition must have exactly one line!";
    lReturnConfig.PredictorType = lTempLines[0];
    CheckString(lReturnConfig.PredictorType, C_PredictorTypes, KEY_PRED);
    
    // arc length scaling
    lReturnConfig.EnableArcLengthScaling = ParseOneLineBool(lConfigMap[KEY_SCALEARC], "Arc length scaling");
    
    // aft type
//     lTempLines = lConfigMap[KEY_AFT];
//     if (lTempLines.size() != 1) throw "Aft type definition must have exactly one line!";
//     lReturnConfig.AftType = lTempLines[0];
//     CheckString(lReturnConfig.AftType, C_AftFactory, KEY_AFT);
    
    // dynamics stiffness matrix builder type
//     lTempLines = lConfigMap[KEY_DSBUILD];
//     if (lTempLines.size() != 1) throw "DS builder type definition must have exactly one line!";
//     lReturnConfig.DSBuilderType = lTempLines[0];
//     CheckString(lReturnConfig.DSBuilderType, C_DSBuilderFactory, KEY_DSBUILD);
    
    // solver
    lTempLines = lConfigMap[KEY_SOLVER];
    if (lTempLines.size() != 1) throw "Solver definition must have exactly one line!";
    CheckString(lTempLines[0], C_SolverTypes, KEY_SOLVER);
    lReturnConfig.SolverString = lTempLines[0];
    lReturnConfig.Solver = C_SolverTypes.at(lTempLines[0]);
    
    // preconditioner
    lTempLines = lConfigMap[KEY_PRECOND];
    if (lTempLines.size() != 1) throw "Preconditioner definition must have exactly one line!";
    lReturnConfig.Preconditioner = lTempLines[0];
    
    // mesh file path
    lTempLines = lConfigMap[KEY_MESH];
    if (lTempLines.size() != 1) throw "Mesh file path definition must have exactly one line!";
    lReturnConfig.MeshFile = lTempLines[0];
    
    // material properties
    // young's modulus
    lReturnConfig.E = ParseOneLineDouble(lConfigMap[KEY_YOUNG], "Young's modulus");
    // poisson's ratio
    lReturnConfig.Nu = ParseOneLineDouble(lConfigMap[KEY_POISSON], "Poisson's ratio");
    // density
    lReturnConfig.Rho = ParseOneLineDouble(lConfigMap[KEY_DENSITY], "Density");
    
    // unit scaling coefficients
    // length
    lReturnConfig.ScaleL = ParseOneLineDouble(lConfigMap[KEY_SCALEL], "Scaling coefficient for length");
    // weight
    lReturnConfig.ScaleW = ParseOneLineDouble(lConfigMap[KEY_SCALEW], "Scaling coefficient for weight");
    // time
    lReturnConfig.ScaleT = ParseOneLineDouble(lConfigMap[KEY_SCALET], "Scaling coefficient for time");
    
    // Rayleigh (proportional) damping
    // K coeff
    lReturnConfig.RayleighK = ParseOneLineDouble(lConfigMap[KEY_RAYLEIGHK], "Rayleigh damping coefficient for stiffness");
    // M coeff
    lReturnConfig.RayleighM = ParseOneLineDouble(lConfigMap[KEY_RAYLEIGHM], "Rayleigh damping coefficient for mass");
    
    // use Green strain?
    lReturnConfig.UseGreen = ParseOneLineBool(lConfigMap[KEY_USEGREEN], "Use Green strain flag");
	
    // number of eigenfrequencies to compute
    if (lReturnConfig.DoEigenfrequencies) lReturnConfig.EigenfreqNum = ParseOneLineInt(lConfigMap[KEY_EIGNUM], "Number of eigenfrequencies");
    else lReturnConfig.EigenfreqNum = 0;
	
	// list of frequencies for individual frequency HBM
	if (lReturnConfig.DoFrequencies)
	{
		lReturnConfig.FrequencyList = ParseValueList(lConfigMap[KEY_FREQLIST]);
		
		if (lReturnConfig.FrequencyList.size() == 0) std::cout << "\"Do frequencies\" task is on but no frequencies provided in the list." << std::endl;
	}
	
	// settings for matrix output
	lReturnConfig.OutputLocalMatrices = ParseOneLineBool(lConfigMap[KEY_MATOUTLOCAL], "Output local matrices");
	lReturnConfig.OutputGlobalMatrices = ParseOneLineBool(lConfigMap[KEY_MATOUTGLOBAL], "Output global matrices");
	
	// use previous solution for next frequency as starting point? (in frequnecy list task)
	lReturnConfig.UsePrevious = ParseOneLineBool(lConfigMap[KEY_USEPREV], "Use previous solution");
	
	lReturnConfig.QuadratureOrder = ParseOneLineInt(lConfigMap[KEY_QUAD], "FE quadrature order");
    
    // test value
    lReturnConfig.TestValue = ParseOneLineInt(lConfigMap[KEY_TEST], "Test value");
    
    return lReturnConfig;
}

void Config::Print() const
{
    std::cout << BORDER << std::endl;
    std::cout << "Config: " << std::endl;
    std::cout << "Config path: " << ConfigFilePath << std::endl;
    std::cout << "Config file name: " << ConfigFileName << std::endl;
    std::cout << "Mesh file path: " << MeshFile << std::endl;
    std::cout << "Tasks: " << std::endl;
    std::cout << "  Output matrices: " << (DoMatrixOutput ? "true" : "false") << std::endl;
    std::cout << "  Eigenfreq.: " << (DoEigenfrequencies ? "true" : "false") << std::endl;
    std::cout << "  Frequency list: " << (DoFrequencies ? "true" : "false") << std::endl;
    std::cout << "  Continuation: " << (DoContinuation ? "true" : "false") << std::endl;
//     std::cout << "Physical dof count: " << DofCount << std::endl;
    std::cout << "Harmonic waves count: " << HarmonicWaveCount << std::endl;
    std::cout << "Used harmonic wave indices: ";
    for (int i = 0; i < HarmonicWaveList.size(); i++) 
    {
        std::cout << HarmonicWaveList[i];
        if (i < HarmonicWaveList.size() - 1) std::cout << ", ";
        else std::cout << std::endl;
    }
    
    if (DoMatrixOutput)
	{
		std::cout << "Matrix output: " << std::endl;
		std::cout << "  Global matrices: " << (OutputGlobalMatrices ? "true" : "false") << std::endl;
		std::cout << "  Local  matrices: " << (OutputLocalMatrices ? "true" : "false") << std::endl;
	}
	if (DoEigenfrequencies)
	{
		std::cout << "Number of computed eigenfrequencies: " << EigenfreqNum << std::endl;
	}
    if (DoFrequencies)
	{
		std::cout << "Use previous solution as initial guess: " << (UsePrevious ? "true" : "false") << std::endl;
		std::cout << "List of individual frequencies: ";
		for (int i = 0; i < FrequencyList.size(); i++)
		{
			std::cout << FrequencyList[i];
			if (i < FrequencyList.size() - 1) std::cout << "; ";
			else std::cout << std::endl;
		}
	}
	
    std::cout << "Number of time integration points: " << IntPointCount << std::endl;
	std::cout << "Newton norm F: " << NewtonNormF << std::endl;
	std::cout << "Max newton steps: " << MaxStepsNewton << std::endl;
	
	if (DoContinuation)
	{
		std::cout << "Continuation settings: " << std::endl;
		std::cout << "Start frequency: " << FrequencyStart << std::endl;
		std::cout << "End frequency: " << FrequencyEnd << std::endl;
		std::cout << "  Max continuation steps: " << MaxStepsContinuation << std::endl;
		std::cout << "  Init step: " << StepSizeInitial << std::endl;
		std::cout << "  Min step: " << StepSizeMin << std::endl;
		std::cout << "  Max step: " << StepSizeMax << std::endl;
		std::cout << "  Predictor type: " << PredictorType << std::endl;
		std::cout << "  Arc length scaling: " << (EnableArcLengthScaling ? "true" : "false") << std::endl;
	}
    std::cout << "Solver: " << SolverString << std::endl;
    std::cout << "Preconditioner string: " << Preconditioner << std::endl;
    std::cout << "Material properties: " << std::endl;
    std::cout << "  Young's modulus (E): " << E << std::endl;
    std::cout << "  Poisson's ratio (nu): " << Nu << std::endl;
    std::cout << "  Density (Rho): " << Rho << std::endl;
    std::cout << "Unit scaling: " << std::endl;
    std::cout << "  Length (meters): " << ScaleL << std::endl;
    std::cout << "  Weight (kilograms): " << ScaleW << std::endl;
    std::cout << "  Time (seconds): " << ScaleT << std::endl;
    std::cout << "Rayleight (proportional) damping coefficients: " << std::endl;
    std::cout << "  Stiffness: " << RayleighK << std::endl;
    std::cout << "  Mass: " << RayleighM << std::endl;
    std::cout << "Using Green strain: " << (UseGreen ? "true" : "false") << std::endl;
	std::cout << "FE quadrature order: " << QuadratureOrder << std::endl;
	
    std::cout << BORDER << std::endl;
}

int Config::ParseOneLineInt(const std::vector<std::string>& aLines, const std::string& aPropetyName)
{
    if (aLines.size() != 1) throw "\"" + aPropetyName + "\" definition must have exactly one line!";
    return std::stoi(aLines[0]);
}
double Config::ParseOneLineDouble(const std::vector<std::string>& aLines, const std::string& aPropetyName)
{
    if (aLines.size() != 1) throw "\"" + aPropetyName + "\" definition must have exactly one line!";
    return std::stod(aLines[0]);
}
bool Config::ParseOneLineBool(const std::vector<std::string>& aLines, const std::string& aPropetyName)
{
    if (aLines.size() != 1) throw "\"" + aPropetyName + "\" definition must have exactly one line!";
    int lBoolInt = std::stoi(aLines[0]);
    if (lBoolInt == 0) return false;
    else if (lBoolInt == 1) return true;
    else throw "Invalid bool flag for \"" + aPropetyName + "\", only 0 or 1 are accepted!";
}
// std::vector<MatrixDefinition> Config::ParseMatrixDefinition(const std::vector<std::string>& aLines, const std::string& aMatrixName)
// {
//     std::vector<MatrixDefinition> lReturnVector;
//     
//     int lLineInd = 0;
//     
//     if (aLines.size() % 2 != 0) throw aMatrixName + " matrix definition must have an even number of lines!";
//     if (aLines.size() <= 0) throw aMatrixName + " matrix definition can't be empty!";
//     
//     while (lLineInd < aLines.size())
//     {
//         MatrixDefinition lNewDef;
//         
//         lNewDef.File = aLines[lLineInd++];
//         lNewDef.Type = aLines[lLineInd++];
//         
//         CheckString(lNewDef.Type, C_MatrixTypes, "matrix type");
//         
//         lReturnVector.push_back(lNewDef);
//     }
//     
//     return lReturnVector;
// }
std::vector<int> Config::ParseHarmonicWaveList(const std::vector<std::string>& aLines, const int& aNumberOfWavesCheck)
{
    if (aLines.size() != aNumberOfWavesCheck)
        throw "Incorrect number of harmonic waves (" + std::to_string(aLines.size()) + ") listed! Expected " + std::to_string(aNumberOfWavesCheck) + ". Value with key \"" + KEY_HARMSELECT + "\" was set to true (1) so the list of harmonic coefficients is expected. The size of the list must be equal to the number of harmonic waves required (key \"" + KEY_HARM + "\"). Harmonic indices must be listed one per row in the config file. If you do not wish to select harmonic indices manually, disable the \"" + KEY_HARMSELECT + "\" option.";
            
    std::vector<int> lReturnVector;
    
    for (int i = 0; i < aLines.size(); i++)
    {
        int lWaveIndex = -111;
        try
        {
            lWaveIndex = std::stoi(aLines[i]);
        }
        catch (std::exception& aEx)
        {
            throw "Error parsing string \"" + aLines[i] + "\" as integer! Inner exception message: " + std::string(aEx.what());
        }
        catch (const char* aEx)
        {
            throw "Error parsing string \"" + aLines[i] + "\" as integer! Inner exception message: " + std::string(aEx);
        }
        
        if (lWaveIndex < 0) throw "Can not use negative harmonic index!";
        
        // Check if the index is not already present
        if(std::find(lReturnVector.begin(), lReturnVector.end(), lWaveIndex) != lReturnVector.end())
        {
            throw "Harmonic index \"" + std::to_string(lWaveIndex) + "\" is used multiple times! This is not allowed.";
        }
        
        if (lReturnVector.size() > 0 && lWaveIndex < lReturnVector[lReturnVector.size() - 1])
        {
            throw "Please be so kind and order the harmonic wave indices in ascending order. Thank you!";
        }
        
        lReturnVector.push_back(lWaveIndex);
    }
    
    return lReturnVector;
}
std::vector<double> Config::ParseValueList(const std::vector<std::string>& aLines, const std::string& aDelimiter)
{
	std::vector<double> lReturnVector;
    
    for (int iLine = 0; iLine < aLines.size(); iLine++)
    {
		std::vector<std::string> lList = SplitString(aLines[iLine], aDelimiter);
		
		for (int iToken = 0; iToken < lList.size(); iToken++)
		{
			const std::string& lOneString = lList[iToken];
			// check for a matlab style vector
			if (lOneString.find(":") != std::string::npos)
			{
				// parse as matlab vector
				std::vector<double> lValues = ParseMatlabStyleVector(lOneString);
				
				for (int i = 0; i < lValues.size(); i++)
					lReturnVector.push_back(lValues[i]);
			}
			else	
			{
				// parse as a single double value
				double lValue;
			
				try
				{
					lValue = std::stod(lList[iToken]);
				}
				catch (std::exception& aEx)
				{
					throw "Error parsing string \"" + lList[iToken] + "\" as double! Inner exception message: " + std::string(aEx.what());
				}
				catch (const char* aEx)
				{
					throw "Error parsing string \"" + lList[iToken] + "\" as double! Inner exception message: " + std::string(aEx);
				}
				
				lReturnVector.push_back(lValue);
			}
		}
    }
    
    return lReturnVector;
}
// void Config::PrintMatrixDefinitions(const std::vector<MatrixDefinition>& aDef) const
// {
//     for (int i = 0; i < aDef.size(); i++)
//     {
//         std::cout << "\"" << aDef[i].File << "\" (" << aDef[i].Type << ")" << std::endl;
//     }
// }
// void Config::PrintNonlinearityDefinitions(const std::vector<NonlinearityDefinition>& aDef) const
// {
//     for (int i = 0; i < aDef.size(); i++)
//     {
//         std::cout << "\"" << aDef[i].File << "\" (" << aDef[i].Type << ")" << std::endl;
//     }
// }
