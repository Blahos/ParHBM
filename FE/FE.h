
namespace ParHBM
{
	template <class Mat>
	class FE;
}

#pragma once
#include "../PhysicalParameters.h"

namespace ParHBM
{
	template <class Mat>
	class FE
	{
	private:
		Mat CreateCMatrix(const PhysicalParameters& aParams) const;
		
	public:
		
	};
}
