
#include "FE.h"

#include "../Matrix/SerialDenseMatrixEpetra.h"

template class ParHBM::FE<ParHBM::SerialDenseMatrixEpetra>;

template <class Mat>
Mat ParHBM::FE<Mat>::CreateCMatrix(const PhysicalParameters& aParams) const
{
    Mat lCMatrix(6, 6);
    
    // Shear modulus
    const double lG = aParams.E / 2.0 / (1 + aParams.nu);
    
    // Effective elastic modulus
    const double lEEff = aParams.E / (1 - 2 * aParams.nu) / (1 + aParams.nu);
    
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (i == j)
            {
                lCMatrix(i, j) = lEEff * (1 - aParams.nu);
            }
            else
            {
                lCMatrix(i, j) = lEEff * aParams.nu;
            }
        }
        lCMatrix(i + 3, i + 3) = lG;
    }
    
    return lCMatrix;
}
