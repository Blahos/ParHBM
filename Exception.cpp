
#include "Exception.h"

ParHBM::Exception::Exception(const std::string& aMessage, const int aCode, const std::string& aFunctionName, const std::string& aClassName)
	: Message(aMessage), Code(aCode), FunctionName(aFunctionName), ClassName(aClassName)
{
	
}

std::ostream& operator<<(std::ostream& aInStream, const ParHBM::Exception& aEx)
{
	aInStream << "Message: " << aEx.Message << std::endl;
	aInStream << "Code: " << aEx.Code << std::endl;
	aInStream << "Function: " << aEx.FunctionName << std::endl;
	aInStream << "Class: " << aEx.ClassName << std::endl;
	
	return aInStream;
}
