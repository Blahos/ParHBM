
namespace ParHBM
{
	class SerialDenseMatrixEpetra;
}

#pragma once
#include "SerialDenseMatrix.h"
#include "Epetra_SerialDenseMatrix.h"

namespace ParHBM
{
	class SerialDenseMatrixEpetra : public SerialDenseMatrix<SerialDenseMatrixEpetra>
	{
	private:
		Epetra_SerialDenseMatrix mMatrix;
		
	public:
		
		SerialDenseMatrixEpetra(const int aRows, const int aCols) : mMatrix(aRows, aCols) { }
		SerialDenseMatrixEpetra(const SerialDenseMatrixEpetra& aOther) : mMatrix(aOther.mMatrix) { }
		
		void Init(const int aRows, const int aCols);
		void PutScalar(const double aValue);
		void Scale(const double aValue);
		int Rows() const;
		int Cols() const;
		void Add(const SerialDenseMatrixEpetra& aOther);
		
		void Multiply(const SerialDenseMatrixEpetra& aA, const SerialDenseMatrixEpetra& aB, const bool aTransposeA, const bool aTransposeB, const double aCoeffThis, const double aCoeffAB);
		
		double& operator()(const int aRowIndex, const int aColIndex);
		const double& operator()(const int aRowIndex, const int aColIndex) const;
		SerialDenseMatrixEpetra& operator=(const SerialDenseMatrixEpetra& aOther);
	};
}
