
namespace ParHBM
{
	template <class T>
	class SerialDenseMatrix;
}

#pragma once

namespace ParHBM
{
	template <class T>
	class SerialDenseMatrix
	{
	private:
	public:
		
		void PutScalar(const double aValue) { static_cast<T&>(*this).PutScalar(aValue); }
		void Scale(const double aValue) { static_cast<T&>(*this).Scale(aValue); }
		int Rows() const { return static_cast<T&>(*this).Rows(); }
		int Cols() const { return static_cast<T&>(*this).Cols(); }
		void Add(const T& aOther) { static_cast<T&>(*this).Add(aOther); }
		
		// performs this = coeffThis * this + coeffAB * A * B
		void Multiply(const T& aA, const T& aB, const bool aTransposeA, const bool aTransposeB, const double aCoeffThis, const double aCoeffAB)
		{ static_cast<T&>(*this).Multiply(aA, aB, aTransposeA, aTransposeB, aCoeffThis, aCoeffAB); }
		void SetProduct(const T& aA, const T& aB, const bool aTransposeA = false, const bool aTransposeB = false)
		{ Multiply(aA, aB, aTransposeA, aTransposeB, 0.0, 1.0); }
		void AddProduct(const T& aA, const T& aB, const bool aTransposeA = false, const bool aTransposeB = false)
		{ Multiply(aA, aB, aTransposeA, aTransposeB, 1.0, 1.0); }
		
		
		double& operator()(const int aRowIndex, const int aColIndex) { return static_cast<T&>(*this)(aRowIndex, aColIndex); }
		const double& operator()(const int aRowIndex, const int aColIndex) const { return static_cast<const T&>(*this)(aRowIndex, aColIndex); }
		T& operator=(const T& aOther) { return static_cast<T&>(*this).operator=(aOther); }
	};
}
