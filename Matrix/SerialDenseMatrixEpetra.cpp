
#include "SerialDenseMatrixEpetra.h"

void ParHBM::SerialDenseMatrixEpetra::PutScalar(const double aValue)
{
	for (int iCol = 0; iCol < mMatrix.ColDim(); iCol++)
	{
		for (int iRow = 0; iRow < mMatrix.RowDim(); iRow++)
		{
			mMatrix(iRow, iCol) = aValue;
		}
	}
}
void ParHBM::SerialDenseMatrixEpetra::Scale(const double aValue)
{
	mMatrix.Scale(aValue);
}

int ParHBM::SerialDenseMatrixEpetra::Rows() const
{
	return mMatrix.RowDim();
}

int ParHBM::SerialDenseMatrixEpetra::Cols() const
{
	return mMatrix.ColDim();
}
void ParHBM::SerialDenseMatrixEpetra::Add(const ParHBM::SerialDenseMatrixEpetra& aOther)
{
	mMatrix += aOther.mMatrix;
}
void ParHBM::SerialDenseMatrixEpetra::Multiply(const SerialDenseMatrixEpetra& aA, const SerialDenseMatrixEpetra& aB, const bool aTransposeA, const bool aTransposeB, const double aCoeffThis, const double aCoeffAB)
{
	mMatrix.Multiply(aTransposeA ? 'T' : 'N', aTransposeB ? 'T' : 'N', aCoeffAB, aA.mMatrix, aB.mMatrix, aCoeffThis);
}

double& ParHBM::SerialDenseMatrixEpetra::operator()(const int aRowIndex, const int aColIndex)
{
	return mMatrix(aRowIndex, aColIndex);
}

const double & ParHBM::SerialDenseMatrixEpetra::operator()(const int aRowIndex, const int aColIndex) const
{
	return mMatrix(aRowIndex, aColIndex);
}

ParHBM::SerialDenseMatrixEpetra& ParHBM::SerialDenseMatrixEpetra::operator=(const SerialDenseMatrixEpetra& aOther)
{
	if (this == &aOther) return *this;
	
	mMatrix = aOther.mMatrix;
	
	return *this;
}
