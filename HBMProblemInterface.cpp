
#include <fstream>
#include <iomanip>
#include <map>

#include "HBMProblemInterface.h"
#include "Functions.h"
#include "Time.h"
#include "Interruption.h"
#include "Directory.h"

const std::string HBMProblemInterface::cContParamName = "ContParam";

HBMProblemInterface::HBMProblemInterface(const LinearSystem& aLinearSystem, FE& aFE, bool aUseGreenStrain, const HBMSettings& aHBMSettings, const Epetra_Comm& aTrilinosComm)
    : cLinearSystem(aLinearSystem), cFE(aFE), cHBMSettings(aHBMSettings), cDofInfo(CreateDofHBMInfo(cFE, cHBMSettings)), mAft(aHBMSettings.IntegrationPointCount, aFE.GetDofInfo(), cDofInfo),
    mAftOverlapping(aHBMSettings.IntegrationPointCount, aFE.GetDofOverlappingIndices(), cDofInfo),    
    cTrilinosComm(aTrilinosComm), cHBMCoeffCount(aHBMSettings.HarmonicInfo.size()),
    cUseGreenStrain(aUseGreenStrain)
{
    std::vector<int> lLocalDofIds = DofHBMInfo::GetDofIDs(cDofInfo);
    
    // HBM row map
    mHBMRowMap = Teuchos::rcp(
        new Epetra_Map(aFE.DofCount() * cHBMCoeffCount, aFE.DofCountLocal() * cHBMCoeffCount, &lLocalDofIds[0], 0, cTrilinosComm));
    
    std::cout << "Total HBM problem size: " << aFE.DofCount() * cHBMCoeffCount << std::endl;
    
    // create the overlapping map in frequency domain
    const std::vector<int>& lPhysicalDofsOverlapping = aFE.GetDofOverlappingIndices();
    std::vector<int> lHBMDofsOverlapping;
    lHBMDofsOverlapping.reserve(lPhysicalDofsOverlapping.size() * cHBMCoeffCount);
    for (int iDof = 0; iDof < lPhysicalDofsOverlapping.size(); iDof++)
    {
        const int lPhysicalDof = lPhysicalDofsOverlapping[iDof];
        for (int iHarm = 0; iHarm < cHBMCoeffCount; iHarm++)
        {
            const int lHBMDof = IndexStandardToHBM(lPhysicalDof, iHarm, cHBMCoeffCount);
            lHBMDofsOverlapping.push_back(lHBMDof);
        }
    }
    
    mHBMRowMapOverlapping = Teuchos::rcp(new Epetra_Map(-1, lHBMDofsOverlapping.size(), &lHBMDofsOverlapping[0], 0, cTrilinosComm));
	
	// create column map for the HBM matrix
    const std::vector<int>& lPhysicalDofsSemilocal = aFE.GetDofSemilocalIndices();
    std::vector<int> lHBMDofsSemilocal;
	lHBMDofsSemilocal.reserve(lPhysicalDofsSemilocal.size() * cHBMCoeffCount);
	for (int iDof = 0; iDof < lPhysicalDofsSemilocal.size(); iDof++)
	{
		const int lPhysicalDof = lPhysicalDofsSemilocal[iDof];
		for (int iHarm = 0; iHarm < cHBMCoeffCount; iHarm++)
		{
			const int lHBMDof = IndexStandardToHBM(lPhysicalDof, iHarm, cHBMCoeffCount);
			lHBMDofsSemilocal.push_back(lHBMDof);
		}
	}
	
	mHBMColumnMap = Teuchos::rcp(new Epetra_Map(-1, lHBMDofsSemilocal.size(), &lHBMDofsSemilocal[0], 0, cTrilinosComm));
	 
    mFreqDomImport = Teuchos::rcp(new Epetra_Import(*(mHBMRowMapOverlapping.get()), *(mHBMRowMap.get())));
	
    mLinearForceHBM = Teuchos::rcp(new Epetra_Vector(*(mHBMRowMap.get())));
    
    for (int iHarm = 0; iHarm < std::min({ cHBMCoeffCount, (int)(cLinearSystem.F.size()) }); iHarm++)
    {
        const WaveType lWaveType = cHBMSettings.HarmonicInfo[iHarm].Type;
        const Epetra_MultiVector& lF = *(cLinearSystem.F[iHarm].get());
        
        const int lNumElements = lF.Map().NumMyElements();
        const int* lIndices = lF.Map().MyGlobalElements();
        
        const std::vector<int> lIndicesHBM = IndexStandardToHBM(lIndices, iHarm, lNumElements, cHBMCoeffCount);
        
        double* lValues = new double[lNumElements];
        
        const int lResult = lF.ExtractCopy(lValues, lNumElements);
        if (lResult != 0) throw "Error extracting values from the external force vector!";
        
        if (lWaveType == WaveType::Cos || lWaveType == WaveType::Sin)
        {
            for (int i = 0; i < lNumElements; i++) lValues[i] *= 0.5;
        }
        
        mLinearForceHBM->SumIntoGlobalValues(lNumElements, lValues, &lIndicesHBM[0]);
        
        delete[] lValues;
    }
}

Teuchos::RCP<Epetra_CrsMatrix> HBMProblemInterface::PrepareJacobiMatrix()
{    
//     int lStandardRowCountTotal = cLinearSystem.K->RowMap().NumGlobalElements();
    const int lStandardRowCount = cLinearSystem.Matrices.K->RowMap().NumMyElements();
//     const int* lStandardRows = cLinearSystem.Matrices.K->RowMap().MyGlobalElements();
    
    if (lStandardRowCount != cFE.GetDofInfo().size()) throw "Dof count not matching!";
    
//     const double lDummyZero = 0.0;
    
    const Epetra_CrsMatrix& lKLocal = *(cLinearSystem.Matrices.K.get());
    const Epetra_CrsMatrix& lDLocal = *(cLinearSystem.Matrices.D.get());
    const Epetra_CrsMatrix& lMLocal = *(cLinearSystem.Matrices.M.get());
    
	Teuchos::RCP<Epetra_CrsMatrix> lKNonlin = Teuchos::null;
	if (cUseGreenStrain)
	{
		// create a dummy nonlinear stiffness matrix
		Teuchos::RCP<Epetra_Vector> lUDummy = Teuchos::rcp(new Epetra_Vector(lKLocal.RowMap()));
		lUDummy->PutScalar(0.0);

		lKNonlin = cFE.CreateNonlinearStiffnessMatrix(*lUDummy, cLinearSystem.PhysicalParams());
	}
    
    std::cout << "Start of the jacobian preparation" << std::endl;
    cTrilinosComm.Barrier();
    
    Time lTime;
    lTime.Start();
        
	CrsMatrixRow lKValues;
	CrsMatrixRow lDValues;
	CrsMatrixRow lMValues;
	
	CrsMatrixRow lKNonlinValues;
	
	CrsMatrixRow lKValuesHarm;
	CrsMatrixRow lDValuesHarm;
	CrsMatrixRow lMValuesHarm;
	
	CrsMatrixRow lKNonlinValuesHarm;
	
	CrsMatrixRow lWholeRowValues;
	CrsMatrixRow lWholeRowValuesTemp;
	
	std::vector<CrsMatrixRow> lAllRows;
	lAllRows.reserve(lStandardRowCount * cHBMCoeffCount);
	
	// just for indices storing, values not relevant
	// we use this class because it has the merge method (maybe consider implementing merge method for ExpandableArray)
// 	CrsMatrixRow lAllColumnIndices;
// 	CrsMatrixRow lAllColumnIndicesTemp;
	
	// to decide whether we need to refill tha K, M and D matrix row values
	int lPreviousPhysicalDofId = -1;
	
	Time lSortTime;
	double lSortElapsed = 0.0;
	Time lMergeTime;
	double lMergeElapsed = 0.0;
	Time lMerge2Time;
	double lMerge2Elapsed = 0.0;
	
	const int lReportPeriodMS = 2000;
	
    for (int iDof = 0; iDof < cDofInfo.size(); iDof++)
    {
        const int lRowGlobalIndex = cDofInfo[iDof].PhysicalDofInfo.DofId;
		const int lHarmIndex = cDofInfo[iDof].HarmonicCoeffIndex;
		const int lHBMRowGlobalIndex = IndexStandardToHBM(lRowGlobalIndex, lHarmIndex, cHBMCoeffCount);
		lWholeRowValues.GlobalRowIndex = lHBMRowGlobalIndex;
		
		lWholeRowValues.Init(0);
		lWholeRowValuesTemp.Init(0);
		
		if (cDofInfo[iDof].PhysicalDofInfo.IsDirichlet)
		{
// 			std::cout << iDof << " is dirichlet dof" << std::endl;
			// shortcut to deal with dirichlet dofs
			lWholeRowValues.Init(1);
			lWholeRowValues.Data()->Indices()[0] = lHBMRowGlobalIndex;
			// we use zero because this is just for determining the pattern, otherwise this would be one
			lWholeRowValues.Data()->Values()[0] = 0.0; 
			
			lAllRows.push_back(lWholeRowValues);
			
// 			lMerge2Time.Start();
// 			// add column index
// 			lAllColumnIndicesTemp = lAllColumnIndices;
// 			lAllColumnIndices.Data()->Merge(*lAllColumnIndicesTemp.Data(), *lWholeRowValues.Data());
// 			lMerge2Elapsed += lMerge2Time.Stop();
			
			continue;
		}
// 		std::cout << iDof << " is NOT dirichlet dof" << std::endl;
        
		// do we need to refill the values?
		if (lRowGlobalIndex != lPreviousPhysicalDofId)
		{
			GetMatrixRowValues(lKLocal, lRowGlobalIndex, lKValues);
			GetMatrixRowValues(lDLocal, lRowGlobalIndex, lDValues);
			GetMatrixRowValues(lMLocal, lRowGlobalIndex, lMValues);
			
// 			if (lMValues.Size() == 0) throw "Empty K row: " + std::to_string(lRowGlobalIndex);
			
			lKValues.Data()->PutScalarValue(0.0);
			lDValues.Data()->PutScalarValue(0.0);
			lMValues.Data()->PutScalarValue(0.0);
			
			if (cUseGreenStrain)
			{
				// we don't use the local (dereferenced) version of the nonlinear matrix because it can be null 
				// (depending on the use of Green strain)
				GetMatrixRowValues(*lKNonlin, lRowGlobalIndex, lKNonlinValues);
				lKNonlinValues.Data()->PutScalarValue(0.0);
			}
			
			// we are only allocating indices here so the sort won't mess up anything (not adding any actual values)
			// the sort is necessary for merging (global sort, across K, M, D and Knonlin)
			
			lSortTime.Start();
			std::sort(lKValues.Data()->Indices(), lKValues.Data()->Indices() + lKValues.Size());
			std::sort(lMValues.Data()->Indices(), lMValues.Data()->Indices() + lMValues.Size());
			std::sort(lDValues.Data()->Indices(), lDValues.Data()->Indices() + lDValues.Size());
			
			if (cUseGreenStrain)
			{
				std::sort(lKNonlinValues.Data()->Indices(), lKNonlinValues.Data()->Indices() + lKNonlinValues.Size());
			}
			lSortElapsed += lSortTime.Stop();
			
			lPreviousPhysicalDofId = lRowGlobalIndex;
		}
		
		lKValuesHarm = lKValues;
		lMValuesHarm = lMValues;
		lDValuesHarm = lDValues;
		
		const WaveInfo& lWave = cHBMSettings.HarmonicInfo[lHarmIndex];
		const int lWavePartnerOffset = lWave.GetPartnerOffset();
		
		// linear stiffness
		IndexStandardToHBMInPlace(lKValuesHarm.Data()->Indices(), lHarmIndex, lKValuesHarm.Size(), cHBMCoeffCount);
		
		lMergeTime.Start();
		lWholeRowValuesTemp = lWholeRowValues;
		lWholeRowValues.Data()->Merge(*lWholeRowValuesTemp.Data(), *lKValuesHarm.Data());
		lMergeElapsed += lMergeTime.Stop();
		
		// nonlinear stiffness
		if (cUseGreenStrain)
		{
			for (int jHarm = 0; jHarm < cHBMCoeffCount; jHarm++)
			{
				lKNonlinValuesHarm = lKNonlinValues;
				IndexStandardToHBMInPlace(lKNonlinValuesHarm.Data()->Indices(), jHarm, lKNonlinValuesHarm.Size(), cHBMCoeffCount);
				
				lMergeTime.Start();
				lWholeRowValuesTemp = lWholeRowValues;
				lWholeRowValues.Data()->Merge(*lWholeRowValuesTemp.Data(), *lKNonlinValuesHarm.Data());
				lMergeElapsed += lMergeTime.Stop();
			}
		}
		
		// mass
		if (lWave.Type != WaveType::DC)
		{
			IndexStandardToHBMInPlace(lMValuesHarm.Data()->Indices(), lHarmIndex, lMValuesHarm.Size(), cHBMCoeffCount);
			
			lMergeTime.Start();
			lWholeRowValuesTemp = lWholeRowValues;
			lWholeRowValues.Data()->Merge(*lWholeRowValuesTemp.Data(), *lMValuesHarm.Data());
			lMergeElapsed += lMergeTime.Stop();
		}
		
		//damping
		if (lWave.Type != WaveType::DC)
		{
			IndexStandardToHBMInPlace(lDValuesHarm.Data()->Indices(), lHarmIndex + lWavePartnerOffset, lDValuesHarm.Size(), cHBMCoeffCount);
			
			lMergeTime.Start();
			lWholeRowValuesTemp = lWholeRowValues;
			lWholeRowValues.Data()->Merge(*lWholeRowValuesTemp.Data(), *lDValuesHarm.Data());
			lMergeElapsed += lMergeTime.Stop();
		}
		
		if (!lWholeRowValues.AreIndicesSorted())
		{
			throw "Indices inserted into the jacobi matrix for row " + std::to_string(lHBMRowGlobalIndex) + " are not sorted!";
		}
		
		lAllRows.push_back(lWholeRowValues);
		
		// add new column indices into the list of all of them (while keeping the list sorted and unique)
		// for the matrix column map
// 		lMerge2Time.Start();
// 		lAllColumnIndicesTemp = lAllColumnIndices;
// 		lAllColumnIndices.Data()->Merge(*lAllColumnIndicesTemp.Data(), *lWholeRowValues.Data());
// 		lMerge2Elapsed += lMerge2Time.Stop();
        
        // report progress
        std::string lReportString = "Jacobi matrix values preparation progress: " +  std::to_string((double)(iDof + 1) / cDofInfo.size() * 100) + "% (total time = " + std::to_string(lTime.Elapsed()) + ")";
        lTime.Report(lReportString, lReportPeriodMS, true);
		
        if (iDof == cDofInfo.size() - 1) lTime.Report(lReportString, 0);
    }
    
    std::cout << "Creating the jacobi matrix object ..." << std::endl;
    // create the proper column map (listing global indices of all columns the matrix has on this rank)
	// maybe this map will in the end be the same as the overlapping hbm map (mHBMRowMapOverlapping)
//     Epetra_Map lColMap(-1, lAllColumnIndices.Size(), lAllColumnIndices.Data()->Indices(), 0, cTrilinosComm);
	
    Teuchos::RCP<Epetra_CrsMatrix> lReturnMatrix = Teuchos::rcp(new Epetra_CrsMatrix(Epetra_DataAccess::Copy, *mHBMRowMap, *mHBMColumnMap, 0));
	
    std::cout << "Creating the jacobi matrix object ... done" << std::endl;
	
    std::cout << "Filling the jacobi matrix with prepared values ..." << std::endl;
	
	std::cout << "Printing matrix global rows: " << std::endl;
	
	for (int i = 0; i < lAllRows.size(); i++)
	{
		const int lInsertResult = lReturnMatrix->InsertGlobalValues(lAllRows[i].GlobalRowIndex, lAllRows[i].Size(), lAllRows[i].Data()->Values(), lAllRows[i].Data()->Indices());
		
		if (lInsertResult < 0) throw "InsertGlobalValues failed in preparation of jacobi matrix for row " + std::to_string(lAllRows[i].GlobalRowIndex) + "!";
	}
	
    std::cout << "Filling the jacobi matrix with prepared values ... done" << std::endl;
    
    std::cout << "Completing jacobi matrix ... " << std::endl;
    lReturnMatrix->FillComplete();
    
    std::cout << "Completing jacobi matrix ... done" << std::endl;
	
// 	std::cout << "Checking jacobi matrix for empty rows ... " << std::endl;
// 	STOP
// 	CheckMatrixForEmptyRows(*lReturnMatrix);
// 	std::cout << "Checking jacobi matrix for empty rows ... done" << std::endl;
// 	STOP
	
    return lReturnMatrix;
}

void HBMProblemInterface::SaveDofInfo(const std::string& aFileName, double aLengthScale) const
{
    const int lRank = cTrilinosComm.MyPID();
    
    std::ofstream lOutputFile(aFileName + "_" + "proc" + std::to_string(lRank) + ".txt");
    if (!lOutputFile.is_open()) throw "Could not create an output file \"" + aFileName + "\" on process num. " + std::to_string(lRank);
	
    const std::string lDelim = ",";
	
    // Header
    lOutputFile << "Node ID" << lDelim;
    lOutputFile << "Phys. dof ID" << lDelim;
    lOutputFile << "Dim" << lDelim;
    lOutputFile << "X" << lDelim;
    lOutputFile << "Y" << lDelim;
    lOutputFile << "Z" << lDelim;
    lOutputFile << "HBM dof ID" << lDelim;
    lOutputFile << "Wave index" << lDelim;
    lOutputFile << "Harm. coeff index" << lDelim;
    lOutputFile << "Harm. type" << lDelim;
    lOutputFile << std::endl;
    
    lOutputFile << std::setprecision(std::numeric_limits<double>::digits10 + 1);
    
    for (int i = 0; i < cDofInfo.size(); i++)
    {
        const DofHBMInfo& lInfo = cDofInfo[i];
        
        lOutputFile << lInfo.PhysicalDofInfo.NodeId << lDelim;
        lOutputFile << lInfo.PhysicalDofInfo.DofId << lDelim;
        lOutputFile << lInfo.PhysicalDofInfo.Dim << lDelim;
        lOutputFile << lInfo.PhysicalDofInfo.X << lDelim;
        lOutputFile << lInfo.PhysicalDofInfo.Y << lDelim;
        lOutputFile << lInfo.PhysicalDofInfo.Z << lDelim;
        lOutputFile << lInfo.DofIdHBM << lDelim;
        lOutputFile << lInfo.HarmonicWaveIndex << lDelim;
        lOutputFile << lInfo.HarmonicCoeffIndex << lDelim;
        lOutputFile << lInfo.HarmonicTypeInt() << std::endl;
    }
    
    lOutputFile.close();
}

void HBMProblemInterface::SaveSolutions(const std::string& aFileName, double aLengthScale, double aTimeScale) const
{
	SaveHBMSolutions(mSolutionFrequencies, mSolutions, aFileName, aTimeScale, aLengthScale, cTrilinosComm);
}
int HBMProblemInterface::DofCount() const
{
    return cFE.DofCount() * cHBMSettings.HarmonicInfo.size();
}
int HBMProblemInterface::DofCountLocal() const
{
	return cFE.DofCountLocal() * cHBMSettings.HarmonicInfo.size();
}
const std::vector<DofHBMInfo>& HBMProblemInterface::GetDofInfo() const
{
    return cDofInfo;
}
void HBMProblemInterface::setParameters(const LOCA::ParameterVector& aParams)
{
    CheckForInterruption();
    double lParamValue = aParams.getValue(HBMProblemInterface::cContParamName);
    
    double lNewW = GetFrequency(lParamValue);
    
    SetFrequency(lNewW);
}
void HBMProblemInterface::SetFrequency(double aFrequency)
{
    if (mCurrentW != aFrequency || !mIsWInitialised)
    {
        mCurrentW = aFrequency;
        mIsWInitialised = true;
    }
}

bool HBMProblemInterface::computeF(const Epetra_Vector& aX, Epetra_Vector& aF, const FillType aFillFlag)
{
    CheckForInterruption();
	
	ComputeZX(aX, aF);
	
    const Epetra_Vector& lLinearForceLocal = *(mLinearForceHBM.get());
    
    for (int i = 0; i < aF.Map().NumMyElements(); i++)
    {
        aF[i] -= lLinearForceLocal[i];
    }
    
    if (cUseGreenStrain)
    {
        AddNonlinearStiffnessF(aX, aF, mCurrentW);
    }
    return true;
}
void HBMProblemInterface::ComputeZX(const Epetra_Vector& aX, Epetra_Vector& aF)
{
	aF.Scale(0.0);
	
	const Epetra_CrsMatrix& lK = *(cLinearSystem.Matrices.K.get());
	const Epetra_CrsMatrix& lM = *(cLinearSystem.Matrices.M.get());
	const Epetra_CrsMatrix& lD = *(cLinearSystem.Matrices.D.get());
	
	Epetra_Vector lXForOneHarmCoeff(lM.RowMap(), false);
	Epetra_Vector lKx(lM.RowMap(), false);
	Epetra_Vector lMx(lM.RowMap(), false);
	Epetra_Vector lDx(lM.RowMap(), false);
	
	// local indices (just a list from 0 to n - 1, where n is the number of rows (of M, K or D matrix) owned by this rank)
	std::vector<int> lLocalIndices;
	lLocalIndices.reserve(lM.RowMap().NumMyElements());
	for (int i = 0; i < lM.RowMap().NumMyElements(); i++)
		lLocalIndices.push_back(i);
	
	for (int iHarm = 0; iHarm < cHBMCoeffCount; iHarm++)
	{
		const WaveInfo& lWave = cHBMSettings.HarmonicInfo[iHarm];
		
		// HBM indices for this hbm coeff index (for the X vector)
		std::vector<int> lHBMIndicesX = IndexStandardToHBM(lLocalIndices, iHarm, cHBMCoeffCount);
		
		// fill the vector with coeffs for the current harmonic
		for (int i = 0; i < lXForOneHarmCoeff.MyLength(); i++)
			lXForOneHarmCoeff[i] = aX[lHBMIndicesX[i]];
		
		// compute Kx
		lK.Multiply(false, lXForOneHarmCoeff, lKx);
		if (lWave.Type != WaveType::DC) 
			lKx.Scale(0.5);
		
		// fill in the Kx values
		for (int i = 0; i < lHBMIndicesX.size(); i++)
		{
			aF[lHBMIndicesX[i]] += lKx[i];
		}
		
		// only multiplication by K for the DC harmonic so we finish here for this one
		if (lWave.Type == WaveType::DC) continue;
		
		const int lWavePartnerOffset = lWave.GetPartnerOffset();
				
		// indices for the partner (in terms of cos/sin) wave
		// could be optimised by instead using the prev. hbm indices + cHBMCoeffCount * partner_offset because that is what this is
 		// currently going to result to (unless the indexing startegy has changed)
		std::vector<int> lHBMIndicesXOther = IndexStandardToHBM(lLocalIndices, iHarm + lWavePartnerOffset, cHBMCoeffCount);
		
		// compute Mx and Dx
		lM.Multiply(false, lXForOneHarmCoeff, lMx);
		lMx.Scale(-0.5 * lWave.WaveIndex * lWave.WaveIndex * mCurrentW * mCurrentW);
		
		lD.Multiply(false, lXForOneHarmCoeff, lDx);
		// the + and - signs for damping might seem to be switched but that's because we use one "block column" of the DS matrix
		// not a row. We are multiplying one part of the X vectors and putting it into 2 different locations (2 waves, cos and sin) 
		// in the F vector
		// so when we are on the "cosine row", we take that row and the row below, which is sine and has damping with the - sign
		// and opposite for the "sine row"
		if 		(lWave.Type == WaveType::Cos) lDx.Scale(-0.5 * lWave.WaveIndex * mCurrentW);
		else if (lWave.Type == WaveType::Sin) lDx.Scale( 0.5 * lWave.WaveIndex * mCurrentW);
		else throw "Unknown wave type!";

		for (int i = 0; i < lHBMIndicesX.size(); i++)
		{
			aF[lHBMIndicesX[i]] += lMx[i];
			aF[lHBMIndicesXOther[i]] += lDx[i];
		}
	}
}

void HBMProblemInterface::ComputeDFbyDW(const Epetra_Vector& aX, Epetra_Vector& aF)
{
	// TODO check if this is correct with the new dof system (not removing dirichlet dofs)
	aF.Scale(0.0);
	
	const Epetra_CrsMatrix& lM = *(cLinearSystem.Matrices.M.get());
	const Epetra_CrsMatrix& lD = *(cLinearSystem.Matrices.D.get());
	
	// all the matrices (M and D) should have the same row map
	Epetra_Vector lXForOneHarmCoeff(lM.RowMap(), false);
	Epetra_Vector lMx(lM.RowMap(), false);
	Epetra_Vector lDx(lM.RowMap(), false);
	
	// local indices (just a list from 0 to n - 1, where n is the number of rows (of M, K or D matrix) owned by this rank)
	std::vector<int> lLocalIndices;
	lLocalIndices.reserve(lM.RowMap().NumMyElements());
	for (int i = 0; i < lM.RowMap().NumMyElements(); i++)
		lLocalIndices.push_back(i);
	
	for (int iHarm = 0; iHarm < cHBMCoeffCount; iHarm++)
	{
		const WaveInfo& lWave = cHBMSettings.HarmonicInfo[iHarm];
		
		// nothing to do for the DC harmonic
		if (lWave.Type == WaveType::DC) continue;
		
		const int lWavePartnerOffset = lWave.GetPartnerOffset();
		
		// HBM indices for this hbm coeff index (for the X vector)
		std::vector<int> lHBMIndicesX = IndexStandardToHBM(lLocalIndices, iHarm, cHBMCoeffCount);
		// indices for the partner (in terms of cos/sin) wave
		// could be optimised by instead using the prev. hbm indices + cHBMCoeffCount * partner_offset because that is what this is
// 		// currently going to result to (unless the indexing startegy has changed)
		std::vector<int> lHBMIndicesXOther = IndexStandardToHBM(lLocalIndices, iHarm + lWavePartnerOffset, cHBMCoeffCount);
				
		// fill the vector with coeffs for the current harmonic
		for (int i = 0; i < lXForOneHarmCoeff.MyLength(); i++)
			lXForOneHarmCoeff[i] = aX[lHBMIndicesX[i]];
		
		// compute Mx and Dx
		lM.Multiply(false, lXForOneHarmCoeff, lMx);
		lMx.Scale(-2 * 0.5 * lWave.WaveIndex * lWave.WaveIndex * mCurrentW);
		
		lD.Multiply(false, lXForOneHarmCoeff, lDx);
		// the + and - signs for damping might seem to be switched but that's because we use one "block column" of the DS matrix
		// not a row. We are multiplying one part of the X vectors and putting it into 2 different locations (2 waves, cos and sin) 
		// in the F vector
		// so when we are on the "cosine row", we take that row and the row below, which is sine and has damping with the - sign
		// and opposite for the "sine row"
		if 		(lWave.Type == WaveType::Cos) lDx.Scale(-0.5 * lWave.WaveIndex); 
		else if (lWave.Type == WaveType::Sin) lDx.Scale( 0.5 * lWave.WaveIndex);
		else throw "Unknown wave type!";
		
		for (int i = 0; i < lHBMIndicesX.size(); i++)
		{
			aF[lHBMIndicesX[i]] += lMx[i];
			aF[lHBMIndicesXOther[i]] += lDx[i];
		}
	}
}

bool HBMProblemInterface::computeJacobian(const Epetra_Vector& aX, Epetra_Operator& aJac)
{
    CheckForInterruption();
//     std::cout << "Entering jacobian: " << std::endl;
    
    Epetra_CrsMatrix* lJac = dynamic_cast<Epetra_CrsMatrix*>(&aJac);
    Epetra_CrsMatrix& lJacLocal = *lJac;
    
    lJacLocal.PutScalar(0.0);
    
    FillDynamicStiffness(lJacLocal, mCurrentW);

    if (cUseGreenStrain)
    {
        AddNonlinearStiffnessJac(aX, lJacLocal, mCurrentW);
    }
    return true;
}

void HBMProblemInterface::printSolution(const Epetra_Vector& aX, double aContParam)
{
    CheckForInterruption();
    double lFreq = GetFrequency(aContParam);
    
    mSolutionFrequencies.push_back(lFreq);
    mSolutions.push_back(aX);
}

void HBMProblemInterface::SetInterruptionFile(const std::string& aFileName)
{
    mInterruptionFileName = aFileName;
}

void HBMProblemInterface::FillDynamicStiffness(Epetra_CrsMatrix& aMatrix, double aFrequency)
{
	Time lTime;
	lTime.Start();
	
    const Epetra_CrsMatrix& lK = *cLinearSystem.Matrices.K;
    const Epetra_CrsMatrix& lD = *cLinearSystem.Matrices.D;
    const Epetra_CrsMatrix& lM = *cLinearSystem.Matrices.M;
    
    const Epetra_Map& lStandardMap = lK.RowMap();
    
    // zero everything out
    aMatrix.PutScalar(0.0);
	
	CrsMatrixRow lKRowValues;
	CrsMatrixRow lDRowValues;
	CrsMatrixRow lMRowValues;
	
	CrsMatrixRow lMRowScaledValues;
	CrsMatrixRow lDRowScaledValues;
	
	CrsMatrixRow lKRowHalfValues;
	
	// for merging K and M parts of the matrix row
	CrsMatrixRow lKMMerge;
	// for merging all three (K, D and M) parts of a row together (to make one insert into the matrix instead of three)
	CrsMatrixRow lKDMMerge;
    
	int lPreviousPhysicalDofId = -1;
	
    for (int iDof = 0; iDof < cDofInfo.size(); iDof++)
    {
        const int lRowGlobalIndex = cDofInfo[iDof].PhysicalDofInfo.DofId;
		const int lHarmIndex = cDofInfo[iDof].HarmonicCoeffIndex;
		const int lHBMRowGlobalIndex = IndexStandardToHBM(lRowGlobalIndex, lHarmIndex, cHBMCoeffCount);
		
        if (lRowGlobalIndex != lPreviousPhysicalDofId)
		{
			// extract matrix row values
			// the D and M values won't be needed for the DC harmonic but it's still better to do the extraction here once because mostly (in case of more harmonics) the values will be used
			GetMatrixRowValues(lK, lRowGlobalIndex, lKRowValues);
			GetMatrixRowValues(lD, lRowGlobalIndex, lDRowValues);
			GetMatrixRowValues(lM, lRowGlobalIndex, lMRowValues);
			
			// cache the half value as it might be used multiple times
			lKRowHalfValues = lKRowValues;
			lKRowHalfValues.Data()->ScaleValues(0.5);
			
			lPreviousPhysicalDofId = lRowGlobalIndex;		
		}
		
		// pointer shortcuts
		const int* const lKRowValuesIndices = lKRowValues.Data()->Indices();
		const double* const lKRowValuesValues = lKRowValues.Data()->Values();	
        
		const WaveInfo& lWave = cHBMSettings.HarmonicInfo[lHarmIndex];
		const int lWaveIndex = lWave.WaveIndex;
		const int lWavePartnerOffset = lWave.GetPartnerOffset();
		
		if (lWave.Type == WaveType::DC)
		{
			std::vector<int> lColIndicesHBM = IndexStandardToHBM(lKRowValuesIndices, lHarmIndex, lKRowValues.Size(), cHBMCoeffCount);
			
			aMatrix.SumIntoGlobalValues(lHBMRowGlobalIndex, lKRowValues.Size(), &lKRowValuesValues[0], &lColIndicesHBM[0]);
		}
		else 
		{
			double lDScaleSign;
			if (lWave.Type == WaveType::Cos) lDScaleSign = 1.0;
			else if (lWave.Type == WaveType::Sin) lDScaleSign = -1.0;
			else throw "Unknown wave type!";

			// scale D values 
			lDRowScaledValues = lDRowValues;
			lDRowScaledValues.Data()->ScaleValues(lDScaleSign * 0.5 * aFrequency * lWaveIndex);
			
			// scale M values
			lMRowScaledValues = lMRowValues;
			lMRowScaledValues.Data()->ScaleValues(-0.5 * aFrequency * aFrequency * lWaveIndex * lWaveIndex);
			
			// merge K and M values together (they belong to the same harmonic index)
			lKMMerge.Data()->Merge(*lKRowHalfValues.Data(), *lMRowScaledValues.Data());
			
			// determine harmonic column indices for KM (merged) and D
			IndexStandardToHBMInPlace(lKMMerge.Data()->Indices(), lHarmIndex, lKMMerge.Size(), cHBMCoeffCount);
			IndexStandardToHBMInPlace(lDRowScaledValues.Data()->Indices(), lHarmIndex + lWavePartnerOffset, lDRowScaledValues.Size(), cHBMCoeffCount);
			
			// merge KM with D (they belong to different harmonics so they have to be merged on the harmonic index level)
			lKDMMerge.Data()->Merge(*lKMMerge.Data(), *lDRowScaledValues.Data());
			
			// fill the matrix row
			Time lTimeE;
			lTimeE.Start();
			aMatrix.SumIntoGlobalValues(lHBMRowGlobalIndex, lKDMMerge.Size(), lKDMMerge.Data()->Values(), lKDMMerge.Data()->Indices());
			mTotalDSFillTimeEpetra += lTimeE.Stop();
		}
    }
    
    double lElapsed = lTime.Stop();
	mTotalDSFillTime += lElapsed;
	std::cout << "Total DS fill time: " << mTotalDSFillTime << std::endl;
	std::cout << "Total DS fill time (Epetra): " << mTotalDSFillTimeEpetra << std::endl;
}

void HBMProblemInterface::AddNonlinearStiffnessF(const Epetra_Vector& aX, Epetra_Vector& aF, double aFrequency)
{
    Time lTime;
    lTime.Start();
    
    const int lTimePointCount = mAft.GetTimePointCount();
    
    Epetra_Vector lXOverlapping(*mHBMRowMapOverlapping);
    
    lXOverlapping.Import(aX, *mFreqDomImport, Epetra_CombineMode::Add, 0);
    
    Teuchos::RCP<Epetra_Vector> lXOverlappingTime = Teuchos::rcp(new Epetra_Vector(cFE.GetRowMapOverlapping()));
    
    double lNonlinFEvalTime = 0.0;
    
    for (int iTime = 0; iTime < lTimePointCount; iTime++)
    {
        std::vector<double> lTimeValues = mAftOverlapping.FreqToTime(lXOverlapping, cHBMSettings.HarmonicInfo, iTime);
        
        lXOverlappingTime->ReplaceGlobalValues(lTimeValues.size(), &lTimeValues[0], lXOverlappingTime->Map().MyGlobalElements());
        
        Time lNonlinEvalTime;
        lNonlinEvalTime.Start();
        Teuchos::RCP<Epetra_MultiVector> lFNonlin = cFE.CreateNonlinearStiffnessRHS(*(lXOverlappingTime.get()), cLinearSystem.PhysicalParams(), false);
        lNonlinFEvalTime += lNonlinEvalTime.Stop();
		
		cFE.ZeroOutDirichletDofs(*lFNonlin);
        
        std::vector<double> lFNonlinFreq = mAft.TimeToFreq(*lFNonlin, cHBMSettings.HarmonicInfo, iTime);
        
        aF.SumIntoGlobalValues(aF.Map().NumMyElements(), &lFNonlinFreq[0], aF.Map().MyGlobalElements());
    }
    
    double lElapsed = lTime.Stop();
    std::cout << "Nonlinear F AFT time: " << lElapsed << " ms (F eval: " << lNonlinFEvalTime << " ms)" << std::endl;
}

void HBMProblemInterface::AddNonlinearStiffnessJac(const Epetra_Vector& aX, Epetra_CrsMatrix& aJac, double aFrequency)
{
    Time lTime;
    lTime.Start();
	
    double lNonlinJEvalTime = 0.0;
	double lEpetraFillTime = 0.0;
    
    const int lTimePointCount = mAft.GetTimePointCount();
    
    Epetra_Vector lXOverlapping(*mHBMRowMapOverlapping);
    
    lXOverlapping.Import(aX, *mFreqDomImport, Epetra_CombineMode::Add, 0);
    
    Teuchos::RCP<Epetra_Vector> lXOverlappingTime = Teuchos::rcp(new Epetra_Vector(cFE.GetRowMapOverlapping()));
	
	Time lEpetraFill;
	
	std::vector<CrsMatrixRow> lJacNonlinFreq(cHBMCoeffCount, CrsMatrixRow());
    
    for (int iTime = 0; iTime < lTimePointCount; iTime++)
    {
        const std::vector<double> lTimeValues = mAftOverlapping.FreqToTime(lXOverlapping, cHBMSettings.HarmonicInfo, iTime);
		
        lXOverlappingTime->ReplaceGlobalValues(lTimeValues.size(), &lTimeValues[0], lXOverlappingTime->Map().MyGlobalElements());
        
        Time lNonlinEvalTime;
        lNonlinEvalTime.Start();
        Teuchos::RCP<Epetra_CrsMatrix> lJacNonlin = cFE.CreateNonlinearStiffnessMatrix(*lXOverlappingTime, cLinearSystem.PhysicalParams(), false);
        lNonlinJEvalTime += lNonlinEvalTime.Stop();
        
        for (int iRowTimeDomain = 0; iRowTimeDomain < lJacNonlin->NumMyRows(); iRowTimeDomain++)
        {
			mAft.TimeToFreq2(*lJacNonlin, cHBMSettings.HarmonicInfo, iTime, iRowTimeDomain, lJacNonlinFreq);
// 			mAft.TimeToFreqDirect(*lJacNonlin, aJac, cHBMSettings.HarmonicInfo, iTime, iRowTimeDomain);
			
            for (int iRowFreqDomain = 0; iRowFreqDomain < lJacNonlinFreq.size(); iRowFreqDomain++)
            {
				const int lLocalHBMDofIndex = IndexStandardToHBM(iRowTimeDomain, iRowFreqDomain, cHBMCoeffCount);
				const DofHBMInfo& lDofInfo = cDofInfo[lLocalHBMDofIndex];
				
				// dirichlet rows don't add anything to the nonlinear stiffness matrix
				if (lDofInfo.PhysicalDofInfo.IsDirichlet) continue;
				
                const CrsMatrixRow& lRow = lJacNonlinFreq[iRowFreqDomain];
				
				lEpetraFill.Start();
                aJac.SumIntoGlobalValues(lRow.GlobalRowIndex, lRow.Size(), lRow.Data().Values(), lRow.Data().Indices());
				lEpetraFillTime += lEpetraFill.Stop();
            }
        }
    }
    
    double lTotalElapsed = lTime.Stop();
    std::cout << "Nonlinear Jac AFT time: " << lTotalElapsed << std::endl;
	std::cout << "	Epetra fill: " << lEpetraFillTime << std::endl; 
	std::cout << "	Nonlin. matrix assembly: " << lNonlinJEvalTime << std::endl;
}

void HBMProblemInterface::ExportHBMMatrix(const Epetra_Vector& aX, const Epetra_CrsMatrix& aJacMatrix, double aCurrentW)
{
    std::vector<int> lPositionsToErase;
    for (int i = 0; i < mExportHBMInfo.size(); i++)
    {
        if (mExportHBMInfo[i].Range.IsIn(aCurrentW))
        {
            std::cout << "Exporting HBM matrix ... " << std::endl;
            std::string lFileName = "HBM_matrix_" + std::to_string(mExportCounter) + "_proc" + std::to_string(cTrilinosComm.MyPID()) + ".txt";
            ExportMatrix(aJacMatrix, lFileName);
            
            if (cTrilinosComm.MyPID() == 0)
            {
                std::ofstream lFreqFile("HBM_matrix_" + std::to_string(mExportCounter) + "_freq.txt");
                lFreqFile << mCurrentW << std::endl;
                lFreqFile.close();
            }
            
            std::ofstream lXVectorFile("HBM_matrix_" + std::to_string(mExportCounter) + "_Xvec_proc" + std::to_string(cTrilinosComm.MyPID()) + ".txt");
            
            mExportCounter++;
            
            lXVectorFile << std::setprecision(std::numeric_limits<long double>::digits10 + 1);
            for (int j = 0; j < aX.Map().NumMyElements(); j++)
            {
                lXVectorFile << aX[j];
                if (j < aX.Map().NumMyElements() - 1) lXVectorFile << ", ";
            }
            lXVectorFile << std::endl;
            lXVectorFile.close();
            
            std::cout << "HBM matrix exported for range <" << mExportHBMInfo[i].Range.Min << ", " << mExportHBMInfo[i].Range.Max << ">" << std::endl;
            
            if (mExportHBMInfo[i].Mult == Multiplicity::Once)
            {
                lPositionsToErase.push_back(i);
            }
        }
    }
    
    // Erase used ranges
    for (int i = 0; i < lPositionsToErase.size(); i++)
    {
        const std::vector<ExportInfo>::iterator lIt = mExportHBMInfo.begin() + lPositionsToErase[i] - i;
        std::cout << "Export info <" << lIt->Range.Min << ", " << lIt->Range.Max << "> erased" << std::endl;
        mExportHBMInfo.erase(lIt);
    }
}

std::vector<DofHBMInfo> HBMProblemInterface::CreateDofHBMInfo(const FE& aFE, const HBMSettings& aHBMSettings)
{
    if (aFE.DofCountLocal() <= 0) throw "Invalid number of dofs for this process!";
    
    // fill the dof info
    const std::vector<DofInfo>& lPhysicalDofInfo = aFE.GetDofInfo();
    const int lHarmonicCoeffCount = aHBMSettings.HarmonicInfo.size();
    // these dof infos should be ordered by dof id
    // check the ordering
    for (int i = 0; i < lPhysicalDofInfo.size() - 1; i++)
    {
        if (lPhysicalDofInfo[i].DofId >= lPhysicalDofInfo[i + 1].DofId) throw "Physical dof ids are not ordered!";
    }
    
    std::vector<DofHBMInfo> lReturnVector(lPhysicalDofInfo.size() * lHarmonicCoeffCount, DofHBMInfo());
		
    for (int iDof = 0; iDof < lPhysicalDofInfo.size(); iDof++)
    {
        const DofInfo& lDofPhys = lPhysicalDofInfo[iDof];
        for (int iHarm = 0; iHarm < lHarmonicCoeffCount; iHarm++)
        {
			const int lLocalHBMDofInd = IndexStandardToHBM(iDof, iHarm, lHarmonicCoeffCount);
            const WaveInfo& lWaveInfo = aHBMSettings.HarmonicInfo[iHarm];
            
            DofHBMInfo& lNewInfo = lReturnVector[lLocalHBMDofInd];
            lNewInfo.PhysicalDofInfo = lDofPhys;
            lNewInfo.DofIdHBM = IndexStandardToHBM(lDofPhys.DofId, iHarm, lHarmonicCoeffCount);
            lNewInfo.HarmonicCoeffIndex = iHarm;
            lNewInfo.HarmonicWaveIndex = lWaveInfo.WaveIndex;
            lNewInfo.HarmonicType = lWaveInfo.Type;
        }
    }
    
    // we should be always using global indices when accessing matrices and vectors
    // but if we use local indexing for vectors for instance, the F[i] will correspond to dof mDofInfo[i]
    std::sort(lReturnVector.begin(), lReturnVector.end(), [](const DofHBMInfo& aInfo1, const DofHBMInfo& aInfo2) { return aInfo1.DofIdHBM < aInfo2.DofIdHBM; });
    
    return lReturnVector;
}

void HBMProblemInterface::CheckForInterruption()
{
    // mpi does not have bool apparently
    int lInterrupt = 0;
    if (mInterruptionFileName.length() > 0 && cTrilinosComm.MyPID() == 0 && !FileExists(mInterruptionFileName))
    {
        lInterrupt = true;
    }
    
    cTrilinosComm.Broadcast(&lInterrupt, 1, 0);
    
    if (lInterrupt) 
    {
        throw Interruption();
    }
}
