
struct WaveInfo;

#pragma once
#include "WaveType.h"

struct WaveInfo
{
public:
    int WaveIndex;
    WaveType Type;
	int GetPartnerOffset() const
	{
		switch (Type)
		{
			case WaveType::DC: 
				return 0;
				break;
			case WaveType::Cos:
				return 1;
				break;
			case WaveType::Sin:
				return -1;
				break;
			default:
				throw "Unknown wave type!";
				break;
		}
	}
};
