
class MeshLibmesh;

#pragma once
#include "mpi.h"

#include "libmesh/mesh_communication.h"
#include "libmesh/libmesh.h"
#include "libmesh/fe_base.h"
#include "libmesh/distributed_mesh.h"
#include "libmesh/elem.h"
#include "libmesh/quadrature_gauss.h"
#include "libmesh/linear_implicit_system.h"
#include "libmesh/equation_systems.h"
#include "libmesh/dof_map.h"
#include "libmesh/dirichlet_boundaries.h"
#include "libmesh/zero_function.h"
#include "libmesh/mesh_tools.h"

#include "Data/MeshData.h"
#include "MeshInterface.h"
#include "../DofInfo.h"

// class for providing mesh interface (to libmesh or whatnot)
// no dependencies on any matrix classes used elsewhere in this project (Epetra or whatnot)
class MeshLibmesh : public MeshInterface
{
private:
	// libmesh needs at least one element in the command line (otherwise you'll get seg fault, because the library always reads argv[0])
	// the second flag is to prevent libmesh from ruining std::cout (it stays ruined even after libmesh finishes, i.e. libmesh init is destroyed, bastards don't bother to restore it to its original state)
	const char* cDummyCharArray[2] = { "libmesh_dummy", "--separate-libmeshout" };
	const bool cPreCacheMeshData;
	
	// can be mutable since it's not important to the outside (users) of this class
	// yes it's returned but only as a const reference
	mutable MeshData mMeshData;
	
// 	const libMesh::Parallel::Communicator& cLibmeshComm;
	// id of the variable system that is associated to all the dofs (libmesh thing)
	int mSystemNumber = -1;
    // number of dofs of the whole mesh (across all ranks)
    int mDofCount = -1;
    // number of dofs for this process
    int mDofCountLocal = -1;
    bool mMeshLoaded = false;
    // Standalone entities, smart pointers, these members are assigned with created values (new ...)
	// ORDER of these members is IMPORTANT for proper libmesh finalisation!!!
	const libMesh::UniquePtr<libMesh::LibMeshInit> cLibmeshInit;
    const libMesh::UniquePtr<libMesh::QGauss> cQRule;
    libMesh::UniquePtr<libMesh::DistributedMesh> mMesh = libMesh::UniquePtr<libMesh::DistributedMesh>(nullptr);
    libMesh::UniquePtr<libMesh::FEBase> mFE = libMesh::UniquePtr<libMesh::FEBase>(nullptr);
    libMesh::UniquePtr<libMesh::EquationSystems> mEquationSystems = libMesh::UniquePtr<libMesh::EquationSystems>(nullptr);
	
	DofDataUnique mDofDataUnique;
		
    const std::string cSystemName = "TheSystem";
    const std::string cVarXName = "VarX";
    const std::string cVarYName = "VarY";
    const std::string cVarZName = "VarZ";
    
	// list of all constrained dof ids (on all ranks)
	// filled when mesh loaded, don't modify later!
	std::vector<int> mConstrainedDofsUnique;
	
    // Raw pointers that do not need to be destroyed because they are only shortcuts to some members of other memebrs (mesh, fe, etc.)
    const std::vector<std::vector<libMesh::Real>>*          mPhi  = nullptr;
    const std::vector<libMesh::Real>*                       mJxW  = nullptr;
    const std::vector<libMesh::Point>*                      mXYZ  = nullptr;
    const std::vector<std::vector<libMesh::RealGradient>>*  mDPhi = nullptr;
	
	// since libmesh offers only iterators over elements and not random access,
	// we will cache pointers to all the local elements so we can randomly access it even when the 
	// mesh data is not pre-cached
	std::vector<libMesh::Elem*> mLibmeshElements;
	
public:
	MeshLibmesh(const MPI_Comm& aComm = MPI_COMM_WORLD, int aQuadraticRuleOrder = 4, bool aPreCacheMeshData = false);
	virtual ~MeshLibmesh();
	
    virtual void LoadMesh(const std::string& aFileName) override;
	virtual int GetElementCount() const override;
	
	// return values of these calls will only be valid until a next call to these methods
	virtual const ElementData& GetElementData(int aElementIndex) const override;
	
	virtual const DofDataUnique& GetDofDataUnique() const override;
	
private:
	static int CheckQOrderValidity(int aOrder);
	static void CheckQOrderSuitability(const libMesh::QGauss& aQRule, int aMeshElementOrder);
	
	void PreCacheMeshData();
	void AddDataForElement(const libMesh::Elem* const aElement, MeshData& aMeshData) const;
	// fill provided node and dof data for the given node
	void FillNodeAndDofData(const libMesh::Node& aNode, NodeData& aNodeData, std::vector<DofData>& aDofData) const;
	
	// clear and allocate (std::vector::reserve) mesh data to accommodate all the elements requested
	// must be done to prevent reallocations of the std::vector during push backs, which invalidates any pointers acquired 
	// before it
	void PrepareDataForElement(libMesh::Elem* const aElement, MeshData& aMeshData) const;
	void PrepareDataForElements(const std::vector<libMesh::Elem*>& aElements, MeshData& aMeshData) const;
	
	void PrepareDofDataUnique(DofDataUnique& aData) const;
	
};
