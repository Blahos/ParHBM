
class MeshInterface;

#pragma once
#include <vector>
#include <string>

#include "Data/ElementData.h"
#include "Data/DofData.h"
#include "Data/DofDataUnique.h"

class MeshInterface
{
	
public:
	
	virtual ~MeshInterface() { };
	
	virtual void LoadMesh(const std::string& aFileName) = 0;
	virtual int GetElementCount() const = 0;
	
	// return values of these calls will only be valid until a next call to these methods
	virtual const ElementData& GetElementData(int aElementIndex) const = 0;
	virtual const DofDataUnique& GetDofDataUnique() const = 0;
};
