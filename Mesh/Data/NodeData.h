
struct NodeData;

#pragma once
#include "DofData.h"

struct NodeData
{
public:
	int Id;
	
	// since some local (on this mpi rank) elements can contain nodes that are not local for this rank,
	// we need to differentiate that
	bool IsLocal;
	
	double X;
	double Y;
	double Z;
	
	const DofData* Dofs;
	int DofCount;
	
	friend std::ostream& operator<<(std::ostream& aInStream, const NodeData& aData);
};
