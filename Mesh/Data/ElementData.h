
struct ElementData;

#pragma once
#include <iostream>

#include "NodeData.h"
#include "DofData.h"
#include "GaussPointData.h"

struct ElementData
{
public:
	int Id;
	
	const NodeData* Nodes;
	int NodeCount;
	
	const DofData* Dofs;
	int DofCount;
	
	const GaussPointData* GaussPoints;
	int GaussPointCount;
	
	friend std::ostream& operator<<(std::ostream& aInStream, const ElementData& aData);
};
