
struct ShapeFunctionData;

#pragma once
#include <iostream>

struct ShapeFunctionData
{
public:
	double ShapeFuncVal;
	double ShapeFuncDiffXVal;
	double ShapeFuncDiffYVal;
	double ShapeFuncDiffZVal;
	
	friend std::ostream& operator<<(std::ostream& aInStream, const ShapeFunctionData& aData);
};

