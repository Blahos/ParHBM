
struct GaussPointData;

#pragma once
#include <iostream>

#include "ShapeFunctionData.h"

struct GaussPointData
{
public:
	const ShapeFunctionData* ShapeFunctions;
	int ShapeFunctionCount;
	
	double JacobianXWeight;
	
	double X;
	double Y;
	double Z;
	
	friend std::ostream& operator<<(std::ostream& aInStream, const GaussPointData& aData);
};
