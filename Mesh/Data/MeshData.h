
class MeshData;

#pragma once
#include <vector>

#include "ElementData.h"
#include "NodeData.h"
#include "DofData.h"
#include "GaussPointData.h"
#include "ShapeFunctionData.h"

class MeshData
{
public:
	std::vector<ElementData> Elements;
	std::vector<NodeData> Nodes;
	std::vector<DofData> Dofs;
	std::vector<GaussPointData> GaussPoints;
	// one per quadrature point and node on the element (we assume same shape functions for all dofs on one node)
	std::vector<ShapeFunctionData> ShapeFunctions;
	
};
