

#include "ElementData.h"
#include "NodeData.h"
#include "DofData.h"
#include "GaussPointData.h"
#include "ShapeFunctionData.h"

std::ostream& operator<<(std::ostream& aInStream, const ElementData& aData)
{
	aInStream << "ID: " << aData.Id << std::endl;
	
	aInStream << "Nodes (" << aData.NodeCount << ")" << std::endl;
	aInStream << "{" << std::endl;
	
	for (int i = 0; i < aData.NodeCount; i++)
	{
		aInStream << "Node" << std::endl;
		aInStream << "{" << std::endl;
		aInStream << aData.Nodes[i] << std::endl;
		aInStream << "}" << std::endl;
	}
	
	aInStream << "}" << std::endl;
	
	aInStream << "Gauss points (" << aData.GaussPointCount << ")" << std::endl;
	aInStream << "{" << std::endl;
	
	for (int i = 0; i < aData.GaussPointCount; i++)
	{
		aInStream << "Gauss point" << std::endl;
		aInStream << "{" << std::endl;
		aInStream << aData.GaussPoints[i] << std::endl;
		aInStream << "}" << std::endl;
	}
	
	aInStream << "}";
	
	return aInStream;
}
std::ostream& operator<<(std::ostream& aInStream, const NodeData& aData)
{
	aInStream << "ID: " << aData.Id << std::endl;
	aInStream << "Is local: " << (aData.IsLocal ? "true" : "false") << std::endl;
	aInStream << "Position (X, Y, Z): " << aData.X << ", " << aData.Y << ", " << aData.Z << std::endl;
	
	aInStream << "Dofs (" << aData.DofCount << ")" << std::endl;
	aInStream << "{" << std::endl;
	
	for (int i = 0; i < aData.DofCount; i++)
	{
		aInStream << "Dof" << std::endl;
		aInStream << "{" << std::endl;
		aInStream << aData.Dofs[i] << std::endl;
		aInStream << "}" << std::endl;
	}
	
	aInStream << "}";
	
	return aInStream;
}

std::ostream& operator<<(std::ostream& aInStream, const DofData& aData)
{
	aInStream << "ID: " << aData.Id << std::endl;
	aInStream << "Dim: " << aData.Dim << std::endl;
	aInStream << "Position (X, Y, Z): " << aData.X << ", " << aData.Y << ", " << aData.Z << std::endl;
	aInStream << "Is local: " << (aData.IsLocal ? "true" : "false") << std::endl;
	aInStream << "Is dirichlet: " << (aData.IsDirichlet ? "true" : "false");
	
	return aInStream;
}

std::ostream& operator<<(std::ostream& aInStream, const GaussPointData& aData)
{
	aInStream << "JxW value: " << aData.JacobianXWeight << std::endl;
	aInStream << "Position (X, Y, Z): " << aData.X << ", " << aData.Y << ", " << aData.Z << std::endl;
	aInStream << "Shape functions (" << aData.ShapeFunctionCount << ")" << std::endl;
	aInStream << "{" << std::endl;
	
	for (int i = 0; i < aData.ShapeFunctionCount; i++)
	{
		aInStream << "Shape function" << std::endl;
		aInStream << "{" << std::endl;
		aInStream << aData.ShapeFunctions[i] << std::endl;
		aInStream << "}" << std::endl;
	}
	
	aInStream << "}";
	
	return aInStream;
}

std::ostream& operator<<(std::ostream& aInStream, const ShapeFunctionData& aData)
{
	aInStream << "Function value: " << aData.ShapeFuncVal << std::endl;
	aInStream << "Function derivative values: " 
	<< aData.ShapeFuncDiffXVal << ", "
	<< aData.ShapeFuncDiffYVal << ", "
	<< aData.ShapeFuncDiffZVal << ", ";
	return aInStream;
}
