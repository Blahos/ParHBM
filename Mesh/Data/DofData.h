
struct DofData;

#pragma once
#include <iostream>
#include <vector>

#include "mpi.h"

// because PETSc is a bitch and redefines this value as a macro of its own
#undef MPI_Allgatherv

struct DofData
{
private:
	static bool mMPIDataTypeInitialised;
	static MPI_Datatype mMPIDataType;
	static const int cMemberCount = 7;
	
public:
	int Id;
	int Dim;
	
	// since some local (on this mpi rank) elements can contain nodes (and therefore also dofs) that are not local for this rank,
	// we need to differentiate that
	bool IsLocal;
	bool IsDirichlet;
	
	double X;
	double Y;
	double Z;
	
	friend std::ostream& operator<<(std::ostream& aInStream, const DofData& aData);
	
	static std::vector<DofData> MPIAllGather(const std::vector<DofData>& aInput, const MPI_Comm& aComm)
	{
		if (!mMPIDataTypeInitialised)
		{
			const int lBlockLengths[cMemberCount] = { 1, 1, 1, 1, 1, 1, 1 };
			const MPI_Aint lOffsets[cMemberCount] = 
			{
				offsetof(DofData, Id),
				offsetof(DofData, Dim),
				offsetof(DofData, IsLocal),
				offsetof(DofData, IsDirichlet),
				offsetof(DofData, X),
				offsetof(DofData, Y),
				offsetof(DofData, Z),
			};
			
			const MPI_Datatype lTypes[cMemberCount] = 
			{ 
				MPI_INT,
				MPI_INT, 
				MPI_CXX_BOOL, 
				MPI_CXX_BOOL, 
				MPI_DOUBLE,
				MPI_DOUBLE,
				MPI_DOUBLE
			};
			
			MPI_Type_create_struct(cMemberCount, lBlockLengths, lOffsets, lTypes, &mMPIDataType);
			
			MPI_Type_commit(&mMPIDataType);
			
			mMPIDataTypeInitialised = true;
		}
		
		const int lMySize = aInput.size();
		
		int lCommSize;
		MPI_Comm_size(aComm, &lCommSize);
		
		int* lSizes = new int[lCommSize];
		int* lOnes = new int[lCommSize];
		int* lIdentity = new int[lCommSize];
		
		for (int i = 0; i < lCommSize; i++)
		{
			lOnes[i] = 1;
			lIdentity[i] = i;
		}
		
		MPI_Allgatherv(&lMySize, 1, MPI_INT, lSizes, lOnes, lIdentity, MPI_INT, aComm);
		
		int lTotalSize = 0;
		
		int* lDisplacements = new int[lCommSize];
		
		for (int i = 0; i < lCommSize; i++)
		{
			lDisplacements[i] = lTotalSize;
			lTotalSize += lSizes[i];
		}
		
		std::vector<DofData> lReturnVector;
		lReturnVector.resize(lTotalSize);

		MPI_Allgatherv(&aInput[0], aInput.size(), mMPIDataType, &lReturnVector[0], lSizes, lDisplacements, mMPIDataType, aComm);
		
		delete[] lSizes;
		delete[] lOnes;
		delete[] lIdentity;
		delete[] lDisplacements;
		
		return lReturnVector;
	}
};
