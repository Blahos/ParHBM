
struct DofDataUnique;

#pragma once
#include <vector>
#include "DofData.h"

struct DofDataUnique
{
public:
	std::vector<DofData> DofDataLocal;
	std::vector<int> DofIndicesLocal;
	std::vector<int> DofIndicesOverlapping;
	std::vector<int> DofIndicesSemilocal;
	
	std::vector<DofData> DofDataConstrained;
	std::vector<int> DofIndicesConstrained;
	
	bool DofIndicesContiguous;
};
