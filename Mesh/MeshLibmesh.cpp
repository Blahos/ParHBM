
#include "MeshLibmesh.h"

#include "../Exception.h"

#include "../Misc.h"
#include "../Functions.h"

MeshLibmesh::MeshLibmesh(const MPI_Comm& aComm, int aQuadraticRuleOrder, bool aPreCacheMeshData)
	: 
	cPreCacheMeshData(aPreCacheMeshData),
	cLibmeshInit(new libMesh::LibMeshInit(sizeof(cDummyCharArray) / sizeof(const char*), cDummyCharArray, aComm)),
	cQRule(new libMesh::QGauss(3, libMesh::OrderWrapper(CheckQOrderValidity(aQuadraticRuleOrder))))
{
	
}

MeshLibmesh::~MeshLibmesh()
{
	
}

void MeshLibmesh::LoadMesh(const std::string& aFileName)
{
	///////////////////
    // Prepare the mesh
    ///////////////////
    if (mMeshLoaded) throw "Mesh already loaded!";
    
//     std::cout << "Loading mesh from file: " << aFileName << std::endl;
    
// 	const int lLibmeshRank = cLibmeshInit->comm().rank();
	
    mMesh = libMesh::UniquePtr<libMesh::DistributedMesh>(new libMesh::DistributedMesh(cLibmeshInit->comm()));
    
    mMesh->read(aFileName);
    
    mMesh->prepare_for_use();
    
//     mMesh->print_info();
	
	if (mMesh->local_elements_begin() == mMesh->local_elements_end())
		throw ParHBM::Exception("No mesh elements present on this rank! Try using fewer MPI processes.", 0, __func__, typeid(*this).name());
	
	libMesh::Order lMeshOrder = libMesh::Order::INVALID_ORDER;
	
	for (auto nElem = mMesh->local_elements_begin(); nElem != mMesh->local_elements_end(); nElem++)
	{
		libMesh::Order lElemOrder = (*nElem)->default_order();
		
		if (lMeshOrder == libMesh::Order::INVALID_ORDER) lMeshOrder = lElemOrder;
		if (lMeshOrder != lElemOrder) throw "Element orders are not consistent! Found element with order " + std::to_string(lElemOrder) + " while previous elements were all of order " + std::to_string(lMeshOrder) + ".";
	}
	
	std::vector<int> lOrderGatherArray;
	
	cLibmeshInit->comm().allgather((int)lMeshOrder, lOrderGatherArray);
	
	for (int i = 1; i < lOrderGatherArray.size(); i++)
	{
		if (lOrderGatherArray[i] != lOrderGatherArray[0])
			throw "Inconsistent element orders! Rank 0 has elements of order " + std::to_string(lOrderGatherArray[0]) + " while rank " + std::to_string(i) + " has elements of order " + std::to_string(lOrderGatherArray[i]) + ".";
	}
	
	CheckQOrderSuitability(*cQRule, lOrderGatherArray[0]);
	
// 	std::cout << "Mesh order checked, all elements have order: " << lMeshOrder << std::endl;
    
    mMeshLoaded = true;
    
    //////////////////////////////
    // Prepare the equation system
    //////////////////////////////
    
    mEquationSystems = libMesh::UniquePtr<libMesh::EquationSystems>(new libMesh::EquationSystems(*mMesh));
    
    mEquationSystems->add_system<libMesh::LinearImplicitSystem>(cSystemName);
	
// 	// we have one system so its number better be 0
// 	mSystemNumber = 0;
	mSystemNumber = mEquationSystems->get_system(cSystemName).number();
	
// 	printf("Rank %d system number: %d\n", cLibmeshInit->comm().rank(), mSystemNumber);
    
    std::vector<std::string> lVariableNames;
    lVariableNames.push_back(cVarXName);
    lVariableNames.push_back(cVarYName);
    lVariableNames.push_back(cVarZName);
    
    mEquationSystems->get_system(cSystemName).add_variables(lVariableNames, lMeshOrder);
    
    std::vector<unsigned int> lVarIDs;
    lVarIDs.push_back(0);
    lVarIDs.push_back(1);
    lVarIDs.push_back(2);
    
    libMesh::ZeroFunction<libMesh::Number> lZero;
    
    std::set<libMesh::boundary_id_type> lBoundaries = mMesh->get_boundary_info().get_boundary_ids();
	
	// put all the boundary ids from the map to a vector
	std::vector<libMesh::boundary_id_type> lTempBcIdVec;
	for (auto nBid = lBoundaries.begin(); nBid != lBoundaries.end(); nBid++) lTempBcIdVec.push_back(*nBid);
	
	// gather values for bids from all processes
	cLibmeshInit->comm().allgather(lTempBcIdVec);
	
	// add boundary ids into the map that are not there yet (to satisfy some libmesh assertion that the boundary ids must be the same on all ranks when it checks the dirichlet conditions
	
	for (auto nBid = lTempBcIdVec.begin(); nBid != lTempBcIdVec.end(); nBid++)
	{
		if (lBoundaries.find(*nBid) == lBoundaries.end()) 
			lBoundaries.insert(*nBid);
	}
	
    libMesh::DirichletBoundary lDirichlet(lBoundaries, lVarIDs, lZero);
    
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
	
    lDofMap.add_dirichlet_boundary(lDirichlet);
	
    mEquationSystems->init();
    
//     std::cout << "Equation system before constraint gather: " << std::endl;
//     mEquationSystems->print_info();
    
    ///////////////////////
    // Prepare the FE class
    ///////////////////////
    
    const libMesh::FEType lType = mEquationSystems->get_system(cSystemName).variable_type(0);
    mFE = libMesh::FEBase::build(3, lType);
    mFE->attach_quadrature_rule(cQRule.get());
	
    // you have to call everything that you will need at the beginning so libmesh knows it has to recalculate those values for each element reinit
    // we could theoretically create a separate fe object for each matrix and rhs, because each of these need a different 
    // set of fields, so we wouldn't recalculate all the fields with each reinit
    mPhi  = &mFE->get_phi();
    mDPhi = &mFE->get_dphi();
    mJxW  = &mFE->get_JxW();
    mXYZ  = &mFE->get_xyz();
    
//     mFE->print_info(std::cout);
    
	// cache elements
	
	mLibmeshElements.reserve(GetElementCount());
	
    for (auto nElem : mMesh->active_local_element_ptr_range())
	{
		mLibmeshElements.push_back(nElem);
	}
	
	if (cPreCacheMeshData)
	{
		PreCacheMeshData();
	}
	
	// Prepare unique dof data (lists of indices and information about dofs, no duplicity over elements)
	PrepareDofDataUnique(mDofDataUnique);
}

int MeshLibmesh::GetElementCount() const
{
	return mMesh->n_active_local_elem();
}

const ElementData& MeshLibmesh::GetElementData(int aElementIndex) const
{
	if (aElementIndex < 0 || aElementIndex >= GetElementCount()) throw "Invalid element index!";
	
	if (cPreCacheMeshData)
	{
		return mMeshData.Elements[aElementIndex];
	}
	else
	{
		PrepareDataForElement(mLibmeshElements[aElementIndex], mMeshData);
		AddDataForElement(mLibmeshElements[aElementIndex], mMeshData);
		
		return mMeshData.Elements[0];
	}
}

const DofDataUnique& MeshLibmesh::GetDofDataUnique() const
{
	return mDofDataUnique;
}

int MeshLibmesh::CheckQOrderValidity(int aOrder)
{
	const int lMaxOrder = 43; // from libmesh 1.3.0
	if (aOrder >= 0 && aOrder <= lMaxOrder) return aOrder;
	
	throw "Invalid FE quadrature rule order: " + std::to_string(aOrder);
}
void MeshLibmesh::CheckQOrderSuitability(const libMesh::QGauss& aQRule, int aMeshElementOrder)
{
	libMesh::OrderWrapper lOrderWrapper(aQRule.get_order());
	int lQuadratureOrder = lOrderWrapper.get_order();
	int lRecommendedQOrder = 0;
	if (aMeshElementOrder == 1) lRecommendedQOrder = 2;
	else if (aMeshElementOrder == 2) lRecommendedQOrder = 4;
	else std::cout << "No quadrature rule order recommendation exists for element order." << aMeshElementOrder << std::endl;
	
	if (lQuadratureOrder < lRecommendedQOrder) 
		std::cout << "Possibly too low quadrature order used! Used: " << lQuadratureOrder << ", recommended minimum: " << lRecommendedQOrder << std::endl;
}

void MeshLibmesh::PreCacheMeshData()
{
	// prepare the mesh data for all elements
	PrepareDataForElements(mLibmeshElements, mMeshData);
	
    for (int iElem = 0; iElem < mLibmeshElements.size(); iElem++)
	{
		AddDataForElement(mLibmeshElements[iElem], mMeshData);
	}	
}
void MeshLibmesh::AddDataForElement(const libMesh::Elem* const aElement, MeshData& aMeshData) const
{
// 	std::cout << "Adding data for next element" << std::endl;
	mFE->reinit(aElement);
	
	const int lLibmeshRank = cLibmeshInit->comm().rank();
	
	// shortcut pointers
    const std::vector<libMesh::Point>& lXYZ = *mXYZ;
    const std::vector<std::vector<libMesh::Real>>& lPhi = *mPhi;
    const std::vector<std::vector<libMesh::RealGradient>>& lDPhi = *mDPhi;
    const std::vector<libMesh::Real>& lJxW = *mJxW;
			
	// dof map to get dofs for nodes
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
	// storge for dof id of a node (one dof)
	std::vector<libMesh::dof_id_type> lDofForNode;
	
	ElementData lNewElementData;
	lNewElementData.Id = aElement->id();
	lNewElementData.NodeCount = aElement->n_nodes();
	lNewElementData.DofCount = 0;		
	lNewElementData.GaussPointCount = mFE->n_quadrature_points();
	
	for (auto& nNode : aElement->node_ref_range())
	{
		lNewElementData.DofCount += nNode.n_dofs(mSystemNumber);
// 		libMesh::Point lNodeCoords = mMesh->point(nNode.id());
		
		NodeData lNewNodeData;
		std::vector<DofData> lNewDofData;
		
		FillNodeAndDofData(nNode, lNewNodeData, lNewDofData);
		
		for (int iDof = 0; iDof < lNewDofData.size(); iDof++)
			aMeshData.Dofs.push_back(lNewDofData[iDof]);
			
		
// 		lNewNodeData.DofCount = nNode.n_dofs(mSystemNumber);
// 		lNewNodeData.Id = nNode.id();
// 		lNewNodeData.IsLocal = nNode.processor_id() == lLibmeshRank;
// 		lNewNodeData.X = lNodeCoords(0);
// 		lNewNodeData.Y = lNodeCoords(1);
// 		lNewNodeData.Z = lNodeCoords(2);
// 		
// 		for (int iDim = 0; iDim < 3; iDim++)
// 		{
// 			// get dof index
// 			lDofMap.dof_indices(&nNode, lDofForNode, iDim);
// 			
// 			DofData lNewDofData;
// 			lNewDofData.Dim = iDim;
// 			lNewDofData.Id = (int)lDofForNode[0];
// 			lNewDofData.IsLocal = lNewNodeData.IsLocal;
// 			lNewDofData.X = lNewNodeData.X;
// 			lNewDofData.Y = lNewNodeData.Y;
// 			lNewDofData.Z = lNewNodeData.Z;
// 			lNewDofData.IsDirichlet = lDofMap.is_constrained_dof(lDofForNode[0]);
// 			
// 			aMeshData.Dofs.push_back(lNewDofData);
// 		}
		
		lNewNodeData.Dofs = &(aMeshData.Dofs.back()) - lNewNodeData.DofCount + 1;
		
		aMeshData.Nodes.push_back(lNewNodeData);
	}
	lNewElementData.Nodes = &(aMeshData.Nodes.back()) - lNewElementData.NodeCount + 1;
	lNewElementData.Dofs = &(aMeshData.Dofs.back()) - lNewElementData.DofCount + 1;
	
	for (int iGauss = 0; iGauss < mFE->n_quadrature_points(); iGauss++)
	{
		GaussPointData lNewGaussPointData;
		lNewGaussPointData.ShapeFunctionCount = aElement->n_nodes();
		
		lNewGaussPointData.X = lXYZ[iGauss](0);
		lNewGaussPointData.Y = lXYZ[iGauss](1);
		lNewGaussPointData.Z = lXYZ[iGauss](2);
		lNewGaussPointData.JacobianXWeight = lJxW[iGauss];
		
		for (int iNode = 0; iNode < aElement->n_nodes(); iNode++)
		{
			ShapeFunctionData lNewShapeFunctionData;
			
			lNewShapeFunctionData.ShapeFuncVal = lPhi[iNode][iGauss];
			
			lNewShapeFunctionData.ShapeFuncDiffXVal = lDPhi[iNode][iGauss](0);
			lNewShapeFunctionData.ShapeFuncDiffYVal = lDPhi[iNode][iGauss](1);
			lNewShapeFunctionData.ShapeFuncDiffZVal = lDPhi[iNode][iGauss](2);
			
			aMeshData.ShapeFunctions.push_back(lNewShapeFunctionData);
		}
		lNewGaussPointData.ShapeFunctions = &(aMeshData.ShapeFunctions.back()) - lNewGaussPointData.ShapeFunctionCount + 1;
		
		
		aMeshData.GaussPoints.push_back(lNewGaussPointData);
	}	
	lNewElementData.GaussPoints = &(aMeshData.GaussPoints.back()) - lNewElementData.GaussPointCount + 1;
	
	aMeshData.Elements.push_back(lNewElementData);
}

void MeshLibmesh::FillNodeAndDofData(const libMesh::Node& aNode, NodeData& aNodeData, std::vector<DofData>& aDofData) const
{
	const int lLibmeshRank = cLibmeshInit->comm().rank();
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
	// storge for dof id of a node (one dof)
	std::vector<libMesh::dof_id_type> lDofForNode;
	
	libMesh::Point lNodeCoords = mMesh->point(aNode.id());
	
	aNodeData.DofCount = aNode.n_dofs(mSystemNumber);
	aNodeData.Id = aNode.id();
	aNodeData.IsLocal = aNode.processor_id() == lLibmeshRank;
	aNodeData.X = lNodeCoords(0);
	aNodeData.Y = lNodeCoords(1);
	aNodeData.Z = lNodeCoords(2);
	
	for (int iDim = 0; iDim < 3; iDim++)
	{
		// get dof index
		lDofMap.dof_indices(&aNode, lDofForNode, iDim);
		
		DofData lNewDofData;
		lNewDofData.Dim = iDim;
		lNewDofData.Id = (int)lDofForNode[0];
		lNewDofData.IsLocal = aNodeData.IsLocal;
		lNewDofData.X = aNodeData.X;
		lNewDofData.Y = aNodeData.Y;
		lNewDofData.Z = aNodeData.Z;
		lNewDofData.IsDirichlet = lDofMap.is_constrained_dof(lDofForNode[0]);
		
		aDofData.push_back(lNewDofData);
	}
}

void MeshLibmesh::PrepareDataForElement(libMesh::Elem* const aElement, MeshData& aMeshData) const
{
	std::vector<libMesh::Elem*> lTemp = { aElement };
	PrepareDataForElements(lTemp, aMeshData);
}

void MeshLibmesh::PrepareDataForElements(const std::vector<libMesh::Elem*>& aElements, MeshData& aMeshData) const
{
	// first count everything for pre-allocation
	int lElementCount = 0;
	int lNodeCount = 0;
	int lDofCount = 0;
	int lGaussPointCount = 0;
	int lShapeFunctionCount = 0;
	
    for (auto nElem : mMesh->active_local_element_ptr_range())
	{
		mFE->reinit(nElem);
		
		lElementCount++;
		lNodeCount += nElem->n_nodes();
		
		for (auto& nNode : nElem->node_ref_range())
			lDofCount += nNode.n_dofs(mSystemNumber);
		
		lGaussPointCount += mFE->n_quadrature_points();
		lShapeFunctionCount += mFE->n_quadrature_points() * nElem->n_nodes();
	}
	
	// we assume 3 dofs per node everywhere
	if (lNodeCount * 3 != lDofCount) throw "Number of dofs must be 3x number of nodes!";
	
	// clear arrays, just to make sure 
	aMeshData.Elements.clear();
	aMeshData.Nodes.clear();
	aMeshData.Dofs.clear();
	aMeshData.GaussPoints.clear();
	aMeshData.ShapeFunctions.clear();
	
	// pre-allocate arrays
	aMeshData.Elements.reserve(lElementCount);
	aMeshData.Nodes.reserve(lNodeCount);
	aMeshData.Dofs.reserve(lDofCount);
	aMeshData.GaussPoints.reserve(lGaussPointCount);
	aMeshData.ShapeFunctions.reserve(lShapeFunctionCount);
}

void MeshLibmesh::PrepareDofDataUnique(DofDataUnique& aData) const
{
    libMesh::DofMap& lDofMap = mEquationSystems->get_system(cSystemName).get_dof_map();
	
	const std::function<bool(const DofData& aData1, const DofData& aData2)> lDofDataCompare = 
		[](const DofData& aData1, const DofData& aData2) { return aData1.Id < aData2.Id; };
	
	/////////////////
	// LOCAL DOF DATA
	/////////////////
	aData.DofDataLocal.reserve(lDofMap.n_local_dofs());
	
	// we don't really need this, but the function needs it
	NodeData lDummyNodeData;
	
	std::vector<DofData> lDofsForNode;
	
	int lDofsConstrainedCount = 0;
    // Indices strictly for this process
    for (auto nNode : mMesh->local_node_ptr_range())
    {
		lDofsForNode.clear();
		
		FillNodeAndDofData(*nNode, lDummyNodeData, lDofsForNode);
		
        for (int iDof = 0; iDof < lDofsForNode.size(); iDof++)
        {
			aData.DofDataLocal.push_back(lDofsForNode[iDof]);
			
			if (lDofsForNode[iDof].IsDirichlet)
				lDofsConstrainedCount++;
        }
    }
    
    std::sort(aData.DofDataLocal.begin(), aData.DofDataLocal.end(), lDofDataCompare);
    
	/////////////////////////////
	// LOCAL DOF INDICES
	// CONTIGUOUSNESS CHECK
	// LOCAL CONSTRAINED DOF DATA
	/////////////////////////////
	aData.DofIndicesLocal.reserve(aData.DofDataLocal.size());
	std::vector<DofData> lDofDataConstrainedLocal;
	lDofDataConstrainedLocal.reserve(lDofsConstrainedCount);
	
	aData.DofIndicesContiguous = true;
	
	for (int iDof = 0; iDof < aData.DofDataLocal.size(); iDof++)
	{
		aData.DofIndicesLocal.push_back(aData.DofDataLocal[iDof].Id);
		
		if (aData.DofDataLocal[iDof].IsDirichlet)
			lDofDataConstrainedLocal.push_back(aData.DofDataLocal[iDof]);
		
		if (iDof > 0)
		{
			if (aData.DofDataLocal[iDof].Id != aData.DofDataLocal[iDof - 1].Id + 1)
				aData.DofIndicesContiguous = false;
		}
	}
	
	std::sort(lDofDataConstrainedLocal.begin(), lDofDataConstrainedLocal.end(), lDofDataCompare);
	
	/////////////////////////////////
	// CONSTRAINED DOF DATA ALLGATHER
	/////////////////////////////////
	std::vector<DofData> lDofDataConstrainedAll = DofData::MPIAllGather(lDofDataConstrainedLocal, cLibmeshInit->comm().get());
	
	///////////////////////////////////////
	// CONSTRAINED DOF DATA UNIQUE ASSEMBLY
	///////////////////////////////////////
	aData.DofDataConstrained.reserve(lDofDataConstrainedAll.size());
	for (int i = 0; i < lDofDataConstrainedLocal.size(); i++)
	{
		aData.DofDataConstrained.push_back(lDofDataConstrainedLocal[i]);
	}
	
	for (int i = 0; i < lDofDataConstrainedAll.size(); i++)
	{
		const bool lIsInLocal = std::binary_search(lDofDataConstrainedLocal.begin(), lDofDataConstrainedLocal.end(), lDofDataConstrainedAll[i], lDofDataCompare);
		
		if (!lIsInLocal)
		{
			const int lSearchedId = lDofDataConstrainedAll[i].Id;
			const bool lIsInAll = std::find_if(aData.DofDataConstrained.begin() + lDofDataConstrainedLocal.size(), aData.DofDataConstrained.end(), [&](const DofData& aData) { return aData.Id == lSearchedId; }) != aData.DofDataConstrained.end();
			
			if (!lIsInAll)
			{
				aData.DofDataConstrained.push_back(lDofDataConstrainedAll[i]);
			}
		}
	}
	
	std::sort(aData.DofDataConstrained.begin(), aData.DofDataConstrained.end(), lDofDataCompare);
	
	//////////////////////////
	// CONSTRAINED DOF INDICES
	//////////////////////////
	aData.DofIndicesConstrained.reserve(aData.DofDataConstrained.size());
	
	for (int i = 0; i < aData.DofDataConstrained.size(); i++)
	{
		aData.DofIndicesConstrained.push_back(aData.DofDataConstrained[i].Id);
	}
	
	//////////////////////////
	// DOF INDICES OVERLAPPING
	//////////////////////////
	// only the dofs in the "overlapping category" that are not in the local indices
	std::vector<int> lDofIndicesOverlappingTemp;
	
	std::vector<libMesh::dof_id_type> lElemDofs;
	for (auto nElem : mMesh->active_local_element_ptr_range())
    {
		lElemDofs.clear();
        lDofMap.dof_indices(nElem, lElemDofs);
        
        for (int iDof = 0; iDof < lElemDofs.size(); iDof++)
        {
			const int lDofId = lElemDofs[iDof];
            const bool lIsInLocal = IsInArray(aData.DofIndicesLocal, lDofId, true, aData.DofIndicesContiguous);
			
            if (!lIsInLocal)
			{
				const bool lIsInOverlapping = IsInArray(lDofIndicesOverlappingTemp, lDofId, false, false);
				
				if (!lIsInOverlapping)
					lDofIndicesOverlappingTemp.push_back(lDofId);
			}
        }
    }
    
    std::sort(lDofIndicesOverlappingTemp.begin(), lDofIndicesOverlappingTemp.end());
	
	Merge(aData.DofIndicesLocal, lDofIndicesOverlappingTemp, aData.DofIndicesOverlapping);
	
	////////////////////////
	// DOF INDICES SEMILOCAL
	////////////////////////
	// only the dofs in the "semilocal category" that are not in the overlapping indices
	std::vector<int> lDofIndicesSemilocalTemp;
	
	for (auto nElem = mMesh->active_semilocal_elements_begin(); nElem != mMesh->active_semilocal_elements_end(); nElem++)
	{
		lElemDofs.clear();
        lDofMap.dof_indices(*nElem, lElemDofs);
		
        for (int iDof = 0; iDof < lElemDofs.size(); iDof++)
        {
			const int lDofId = lElemDofs[iDof];
            const bool lIsInLocal = IsInArray(aData.DofIndicesLocal, lDofId, true, aData.DofIndicesContiguous);
			
            if (!lIsInLocal)
			{
				const bool lIsInOverlapping = IsInArray(lDofIndicesOverlappingTemp, lDofId, true, false);
				
				if (!lIsInOverlapping)
				{
					const bool lIsInSemilocal = IsInArray(lDofIndicesSemilocalTemp, lDofId, false, false);
					if (!lIsInSemilocal)
					{
						lDofIndicesSemilocalTemp.push_back(lDofId);
					}
				}
			}
        }
	}
	
    std::sort(lDofIndicesSemilocalTemp.begin(), lDofIndicesSemilocalTemp.end());
	
	Merge(aData.DofIndicesOverlapping, lDofIndicesSemilocalTemp, aData.DofIndicesSemilocal);
}
