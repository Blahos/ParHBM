
struct DofInfo;

#pragma once
#include <iostream>
#include <vector>

struct DofInfo
{
public:
    int DofId;
    int NodeId;
    int Dim;
    double X;
    double Y;
    double Z;
	bool IsDirichlet;
    
public:
    static std::vector<int> GetDofIDs(const std::vector<DofInfo>& aDofs)
    {
        std::vector<int> lReturnVector;
        lReturnVector.reserve(aDofs.size());
        
        for (int i = 0; i < aDofs.size(); i++)
            lReturnVector.push_back(aDofs[i].DofId);
        
        return lReturnVector;
    }
    
};
