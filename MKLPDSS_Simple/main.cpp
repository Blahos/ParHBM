#include <iostream>

#include "mpi.h"
#include "mkl_cluster_sparse_solver.h"

#include "../MKLPDSSWrapperLib/Functions.h"
#include "../MKLPDSSWrapperLib/MKLPDSSData.h"

#include <fstream>

int main(int aArgCount, char** aArgList)
{
	MPI_Init(&aArgCount, &aArgList);
	
	int lRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &lRank);

	MKLPDSSData lData = MKLPDSSData::ImportFromFile("MKLPDSS_DATA_TEST_FILE", MPI_COMM_WORLD);
	
	long long lPt[64];
	int lIParm[64];
	
	for (int i = 0; i < 64; i++) lPt[i] = 0;
	for (int i = 0; i < 64; i++) lIParm[i] = 0;
	
	lIParm[0] = 1; // don't use default values
	lIParm[1] = 10; // distributed dissection of the matrix
// 	lIParm[9] = 13; 
	lIParm[17] = 0; // report number of nonzeros
// 	lIParm[26] = 1; // matrix indices checking
	lIParm[34] = 0; // one based indexing
// 	lIParm[36] = 0; // matrix storage format
	lIParm[39] = 2; // distributed matrix, rhs and solution (all with same row distribution)
	lIParm[40] = lData.FirstRow; // first row for this process
	lIParm[41] = lData.LastRow; // last row for this process
		
	int lMatrixType = 1;
	int lPhase = 13;
	
	int lEqNum = lData.NumEq;
	int lMessageLevel = 0;
	int lNRHS = 1;
	int lFortranMPIComm = MPI_Comm_c2f(MPI_COMM_WORLD);
	
	int lError = 0;
	
	double* lRHSCopy = new double[lData.RHSSize];
	for (int i = 0; i < lData.RHSSize; i++) lRHSCopy[i] = lData.RHS[i];
	
	double* lSolutionBuffer = new double[lData.SolutionSize];
	
	int lDummy1 = 1;
	int lDummy0 = 0;
	
	//TODO figure out if this barrier is necessary
	MPI_Barrier(MPI_COMM_WORLD);
	
	std::cout << "Calling the cluster_sparse_solver() ..." << std::endl;
	
	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lData.Values, lData.IndexOffset, lData.ColumnIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
		
	std::cout << "Calling the cluster_sparse_solver() ... finished" << std::endl;
	
	if (lError != 0) throw "MKLPDSS (solve phase) returned error " + std::to_string(lError) + "!";
	
	std::ofstream lSolutionFile("MKLPDSS_TEST_SOLUTION_rank" + std::to_string(lRank));
	ExportArray(lSolutionFile, lSolutionBuffer, lData.SolutionSize);
	
// 	std::cout << "Solution copied" << std::endl;
	
	// release
	lPhase = -1;
	cluster_sparse_solver(lPt, &lDummy1, &lDummy1, &lMatrixType, &lPhase, &lEqNum, lData.Values, lData.IndexOffset, lData.ColumnIndices, &lDummy0, &lNRHS, lIParm, &lMessageLevel, lRHSCopy, lSolutionBuffer, &lFortranMPIComm, &lError);
	
	if (lError != 0) throw "MKLPDSS (free phase) returned error " + std::to_string(lError) + "!";
	
	//TODO figure out if this barrier is necessary
	MPI_Barrier(MPI_COMM_WORLD);
	
	delete[] lRHSCopy;
	delete[] lSolutionBuffer;
	
	MPI_Finalize();
	
	std::cout << "MKLPDSS test program finished" << std::endl;
	return 0;	
}
