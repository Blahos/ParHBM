
class NonlinearStaticProblem;

#pragma once
#include "NOX_Epetra_Interface_Required.H"
#include "NOX_Epetra_Interface_Jacobian.H"

#include "Teuchos_RCP.hpp"

#include "Epetra_MultiVector.h"
#include "Epetra_CrsMatrix.h"
#include "Epetra_Map.h"
#include "Epetra_DataAccess.h"

#ifdef HAVE_MPI
#  include "mpi.h"
#  include "Epetra_MpiComm.h"
#else
#  include "Epetra_SerialComm.h"
#endif

#include "FE.h"
#include "PhysicalParameters.h"

class NonlinearStaticProblem : public NOX::Epetra::Interface::Required, public NOX::Epetra::Interface::Jacobian
{
private:
    // do not delete this
    FE* const cFE;
    const Epetra_Comm& cTrilinosComm;
    const PhysicalParameters cParams;
    const std::function<Epetra_SerialDenseVector(double, double, double)> cBodyForce;
    const double cBodyForceScale;
    
    Teuchos::RCP<Epetra_FECrsMatrix> mK;
    Teuchos::RCP<Epetra_MultiVector> mF;
public:
    //The fe won't be deleted in this class, so you have to delete it outside (if it was dynamically allocated)
    NonlinearStaticProblem(FE* const aFE, const PhysicalParameters& aParams, const std::function<Epetra_SerialDenseVector(double, double, double)>& aBodyForce, const Epetra_Comm& aComm, double aBodyForceScale);
    
    const Epetra_Map& GetRowMap() const;
    
    // Interface implemetations
    bool computeF(const Epetra_Vector& aX, Epetra_Vector& aF, NOX::Epetra::Interface::Required::FillType aFillType);
    bool computeJacobian(const Epetra_Vector& aX, Epetra_Operator& aJac);

private:
};
