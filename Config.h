
class Config;

#pragma once
#include <string>
#include <vector>
#include <map>
#include <functional>

#include "Solvers/SolverInfo.h"

// #include "MatrixDefinition.h"
// #include "Nonlinearities/NonlinearityDefinition.h"
// #include "ProblemParams.h"

// the return after the throw is there just so the compiler knows what the function returns
// it should never get to that point because of the throw
#define THROW_NO_DEFAULT { [](const std::string& aStr) { throw std::string("\"") + aStr + "\" has no default value!"; return std::vector<std::string>(); } }
// this macro only works if you pass one string literal into it
#define RET_ONE_STR(STR_VAL) [](const std::string& aStr) { return std::vector<std::string>( { STR_VAL } ); }
#define RET_EMPTY_STR_VEC [](const std::string& aStr) { return std::vector<std::string>(); }

class Config
{
// static constants
public:
    // config keywords definition
	
	// tasks to perform
    static const std::string KEY_DOEIG;         // flag for running eigenfrequency calculation
    static const std::string KEY_DOCONT;        // flag for running continuation
    static const std::string KEY_DOFREQ;		// flag for running HBM for set of frequencies
    static const std::string KEY_DOMATRIXOUT;	// flag to output linear matrices (mass and stiffness) to files
    
    // other config options
    static const std::string KEY_EXC;           // excitation force def
    static const std::string KEY_DOF;           // number of physical degrees of freedom
    static const std::string KEY_HARM;          // number of harmonic waves
    static const std::string KEY_HARMSELECT;    // select only certain harmonice waves?
    static const std::string KEY_HARMLIST;      // list of selected harmonic waves
    static const std::string KEY_NONLIN;        // nonlinearities def
    static const std::string KEY_START;         // start continuation frequency
    static const std::string KEY_END;           // end continuation frequency
    static const std::string KEY_INT;           // number of integration points
    static const std::string KEY_WHOLE;         // save whole solutions (bool)
    static const std::string KEY_STEPSC;        // number of continuation steps
    static const std::string KEY_STEPSN;        // number of newton steps
    static const std::string KEY_NORM;          // norm criteria for newton convergence
    static const std::string KEY_STEPINI;       // initial continuation step
    static const std::string KEY_STEPMIN;       // minimum continuation step
    static const std::string KEY_STEPMAX;       // maximum continuation step
    static const std::string KEY_PRED;          // continuation predictor
    static const std::string KEY_SCALEARC;      // scaling for arc length continuation
    static const std::string KEY_AFT;           // aft type
    static const std::string KEY_DSBUILD;       // dynamic stiffness matrix builder type
    static const std::string KEY_SOLVER;        // what solver to use
    static const std::string KEY_PRECOND;       // what preconditioner to use
    static const std::string KEY_MESH;          // path to the mesh file
    static const std::string KEY_YOUNG;         // Young's modulus    
    static const std::string KEY_POISSON;       // Poisson's ratio
    static const std::string KEY_DENSITY;       // material density
    static const std::string KEY_SCALEL;        // scaling for length
    static const std::string KEY_SCALEW;        // scaling for weight
    static const std::string KEY_SCALET;        // scaling for time
    static const std::string KEY_RAYLEIGHK;     // Rayleigh damping - coefficient for the stiffness matrix
    static const std::string KEY_RAYLEIGHM;     // Rayleigh damping - coefficient for the mass matrix
    static const std::string KEY_USEGREEN;      // flag for using the green strain (material nonlinearity)
    static const std::string KEY_EIGNUM;        // number of eigenfrequencies to calculate
    static const std::string KEY_FREQLIST;		// list of frequencies for individual HBM analysis
    static const std::string KEY_MATOUTGLOBAL;	// output global matrices (only for the matrix output task)
    static const std::string KEY_MATOUTLOCAL;	// output local matrices (only for the matrix output task)
    static const std::string KEY_USEPREV;		// in frequency list iteration, flag whether to use the result from previous frequency as a starting guess for newton for the next frequency
    static const std::string KEY_QUAD;			// order of quadrature used in FE
    
    static const std::string KEY_TEST;          // arbitrary value used for debugging or testing purposes
    
    // list of parameter keys and functions that return default values for those  parameters
    // the argument passed into the function should be the parameter name 
    // so it gets passed into the exception if there is no default so the user knows
    // which parameter they need to provide
    static const std::map<std::string, std::function<std::vector<std::string>(const std::string&)>> C_ConfigDefinition;

public:
    // Load function
    static Config LoadConfig(const std::string& aConfigFilePath);
    // print
    void Print() const;
    
public:
    
    // flags for running particular tasks
    bool DoEigenfrequencies;
    bool DoContinuation;
	bool DoFrequencies;
	bool DoMatrixOutput;
	
    std::string ConfigFilePath;
    std::string ConfigFileName;
    // relative paths with respect to the config path
//     std::vector<MatrixDefinition> MassMatrices;
//     std::vector<MatrixDefinition> DampingMatrices;
//     std::vector<MatrixDefinition> StiffnessMatrices;
    std::string ExcitationForceFile;
    std::string ContinuationSettingsFile;
//     std::vector<NonlinearityDefinition> Nonlinearities;
    
    // number of physical degrees of freedom (i.e. in time domain)
//     int DofCount;
    // number of harmonic waves (first harmonic is the DC component)
    // that means that the number of harmonic dofs will be DOFCount * (2 * HarmonicCount - 1)
    
    int HarmonicWaveCount;
    // Determines whether only certain harmonic waves will be selected or all should be used (in interval 0:HarmonicWaveCount - 1)
    bool HarmonicWaveSelected;
    // used to select only certain harmonic waves for the solution approximation (if not set, waves [0, 1, 2, ..., HarmonicWaveCount - 1] will be used)
    // Size of this vector must be the same as HarmonicWaveCount
    // This list must be SORTED in ascending order
    // That means that if the 0th harmonic is to be present, it must be at the beginning of the list
    // For each harmonic (except the DC of course) the coeff ordering is always cos and sin (next to each other)
    std::vector<int> HarmonicWaveList;
    
    // start continuation frequency
    double FrequencyStart;
    // end continuation frequency
    double FrequencyEnd;
    // integration point count
    int IntPointCount;
    // should the interface store the whole solutions? (all harmonic coefficients)
//     bool SaveWholeSolutions;
    // type of aft used
//     std::string AftType;
    // type of the dynamic stiffness matrix builder used
//     std::string DSBuilderType;
    
    // continuation settings
    int MaxStepsContinuation;
    int MaxStepsNewton;
    double NewtonNormF;
    double StepSizeInitial;
    double StepSizeMin;
    double StepSizeMax;
    std::string PredictorType; // secant, constant, tangent
    bool EnableArcLengthScaling;
    ParHBM::Solver Solver;
    std::string SolverString; // only for printing
    std::string Preconditioner; // for ifpack or whatever preconditioning package we use
    std::string MeshFile; // path to the file with mesh
    
    // physical parameters of the body
    // young modulus
    double E;
    // poisson ration
    double Nu;
    // density
    double Rho;
    
    // basic units scaling coefficients
    // length
    double ScaleL;
    // weight
    double ScaleW;
    // time
    double ScaleT;
    
    // Rayleigh (proportional) damping
    // coefficient for the stiffness matrix
    double RayleighK;
    // coefficient for the mass matrix
    double RayleighM;
    
    // use Green strain flag
    bool UseGreen;
        
    // number of eigenfrequencies to calculate
    int EigenfreqNum;
	
	// list of frequencies for individual HBM analysis (no continuation)
	std::vector<double> FrequencyList;
	
	// flags for matrix output (for matrix output task only)
	bool OutputLocalMatrices;
	bool OutputGlobalMatrices;
	
	// flag whether to use solution from previous frequency as a starting guess in newton for the next freuquency (in frequency list task)
	bool UsePrevious;
	
	// FE quadrature order
	int QuadratureOrder;
    
    // arbitrary value for test purposes
    int TestValue;
    
private:
    static int ParseOneLineInt(const std::vector<std::string>& aLines, const std::string& aPropetyName = "");
    static double ParseOneLineDouble(const std::vector<std::string>& aLines, const std::string& aPropetyName = "");
    static bool ParseOneLineBool(const std::vector<std::string>& aLines, const std::string& aPropetyName = "");
//     static std::vector<MatrixDefinition> ParseMatrixDefinition(const std::vector<std::string>& aLines, const std::string& aMatrixName);
    static std::vector<int> ParseHarmonicWaveList(const std::vector<std::string>& aLines, const int& aNumberOfWavesCheck);
	static std::vector<double> ParseValueList(const std::vector<std::string>& aLines, const std::string& aDelimiter = ";");
    
//     void PrintMatrixDefinitions(const std::vector<MatrixDefinition>& aDef) const;
//     void PrintNonlinearityDefinitions(const std::vector<NonlinearityDefinition>& aDef) const;
    
};

